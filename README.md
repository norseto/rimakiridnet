# Rimakiri .Net
いろいろ書き溜めた業務系の.NET用フレームワークなどをまとめたものです。
以下のパッケージがあります。

### Net.Rimakiri.Command
トランザクション管理下でロジックを実行するためのフレームワーク（net40、core 2.1/2.2、standard 2.0）

### Net.Rimakiri.Command.Database
Net.Rimakiri.Commandでデータベース処理を行うためのオプションパッケージ（net40、core 2.1/2.2、standard 2.0）

### Net.Rimakiri.Command.Transactions
Net.Rimakiri.CommandのトランザクションでTransactionScopeを使用するためのオプションパッケージ（net40、net451、core 2.1/2.2、standard 2.0）

### Net.Rimakiri.Data
生のSQLを作成、実行するためのライブラリ（net40、core 2.1/2.2、standard 2.0）

### Net.Rimakiri.Container
DIコンテナのUnity Application Blockの複数の設定を使用可能としたライブラリ（net40、net45）

### Net.Rimakiri.Container.Web
Net.Rimakiri.ContainerをASP.NETで使用可能とするためのオプションパッケージ（net40、net45）

### RDBMS用のオプションパッケージ
#### Net.Rimakiri.Data.DB2
IBM DB2用のオプションパッケージ（net40、core 2.1/2.2、standard 2.0）

#### Net.Rimakiri.Data.Oracle
ORACLE用のオプションパッケージ（net40、core 2.1/2.2、standard 2.0）

#### Net.Rimakiri.Data.SqlServer
MS SQL Server用のオプションパッケージ（net40、core 2.1/2.2、standard 2.0）
