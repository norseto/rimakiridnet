### Docker containers for test.
You should build and run these database containers before running full tests.

Followings are names of database servers used in tests.
- DbServ: MS SQL Server
- OraDbServ: ORACLE
- DB2DbServ: DB2

Adding these names in the hosts file is the most simple way to avoid changing test configurations.

**Some tests will fail because MS SQL Server for Linux doesn't have MSDTC.**
