// ==============================================================================
//     Net.Rimakiri.Data.DB2
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using Net.Rimakiri.Data.Interfaces;
using Net.Rimakiri.Data.TypeWrappers;
#if (NET40)
using IBM.Data.DB2;
#else
using IBM.Data.DB2.Core;
#endif

namespace Net.Rimakiri.Data.DB2 {
	/// <summary>
	/// IBM DB2用の<see cref="IDatabaseTypeMap{T}"/>クラスです。
	/// </summary>
	/// <remarks>
	/// 詳細は、<see href="https://www.ibm.com/support/knowledgecenter/ja/SSEPGG_9.5.0/com.ibm.swg.im.dbclient.adonet.ref.doc/doc/DB2TypeEnumeration.html" />
	/// </remarks>
	public class DB2DbTypeMap : IDatabaseTypeMap<DB2Type> {
		private readonly AssignableTypeMap<DB2Type> wrapperTypes = new AssignableTypeMap<DB2Type>();

		/// <summary>
		/// インスタンスを初期化します。
		/// </summary>
		public DB2DbTypeMap() {
			wrapperTypes[typeof(CharType)] = DB2Type.Char;
			wrapperTypes[typeof(NCharType)] = DB2Type.Char;
			wrapperTypes[typeof(VarCharType)] = DB2Type.VarChar;
			wrapperTypes[typeof(NVarCharType)] = DB2Type.VarChar;
			wrapperTypes[typeof(DateType)] = DB2Type.Date;
			wrapperTypes[typeof(TimestampType)] = DB2Type.Timestamp;
			wrapperTypes[typeof(INumericWrapper)] = DB2Type.Decimal;
		}

		#region Implementation of IDatabaseTypeMap<out DB2Type>
		/// <inheritdoc />
		public DB2Type GetDatabaseType(string parameterName, IBindValue value) {
			var type = value.BindMetaData.DataType;

			if (typeof(IDataWrapper).IsAssignableFrom(type)) {
				if (wrapperTypes.TryGet(type, out var ret)) {
					return ret;
				}
			}

			if (type == typeof(string)) {
				return DB2Type.VarChar;
			}

			switch (Type.GetTypeCode(type)) {
			case TypeCode.Byte:
			case TypeCode.SByte:
			case TypeCode.Int16:
				return DB2Type.SmallInt;
			case TypeCode.UInt16:
			case TypeCode.Int32:
				return DB2Type.Integer;
			case TypeCode.UInt32:
			case TypeCode.Int64:
				return DB2Type.BigInt;
			case TypeCode.UInt64:
			case TypeCode.Decimal:
				return DB2Type.Decimal;
			case TypeCode.Double:
				return DB2Type.Double;
			case TypeCode.Single:
				return DB2Type.Real;
			}

			if (type == typeof(char[])) {
				return DB2Type.Char;
			}
			if (type == typeof(DateTime)) {
				return DB2Type.Timestamp;
			}

			throw new InvalidOperationException($"Unsupported type: {type}");
		}
		#endregion
	}
}
