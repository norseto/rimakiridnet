// ==============================================================================
//     Net.Rimakiri.Data.DB2
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

#if (NET40 || NET45 || NET451 || NET452 || NET46 || NET461 || NET462 || NET47 || NET471 || NET472)
#define NETFRAMEWORK
#endif

using System;
using System.Data;
using Net.Rimakiri.Data.Interfaces;
using Net.Rimakiri.Data.Util;

#if (NETFRAMEWORK)
using IBM.Data.DB2;
#else
using IBM.Data.DB2.Core;
#endif
namespace Net.Rimakiri.Data.DB2 {
	/// <summary>
	/// IBM DB2用の<see cref="IConnectionFactory"/>クラスです。
	/// </summary>
	/// <inheritdoc />
	public class DB2ConnectionFactory : ConnectionFactoryBase<DB2Connection> {
		private readonly DB2Dialect defaultDialect = new DB2Dialect();
		#region Overrides of ConnectionFactoryBase<DB2Connection>
		/// <inheritdoc />
		protected override IDatabaseDialect DefaultDialect => defaultDialect;

		#region Overrides of ConnectionFactoryBase<DB2Connection>
		/// <inheritdoc />
		public override Func<IsolationLevel, IsolationLevel> IsolationLevelMapping {
			get { return (org) => org == IsolationLevel.Snapshot ? IsolationLevel.Serializable : org; }
		}
#if (NETFRAMEWORK)
#else
		/// <inheritdoc />
		/// <summary>
		/// 接続の追加設定を行います。
		/// </summary>
		/// <param name="connection">生成された接続</param>
		/// <param name="global">グローバルトランザクション内の場合True</param>
		/// <param name="isolation">要求されているトランザクション分離レベル</param>
		protected override void SetupConnection(DB2Connection connection, bool global, IsolationLevel isolation) {
			if (global) {
				throw new InvalidOperationException("This factory does not support global transaction. Use database transaction");
			}
		}
#endif
		#endregion

		/// <inheritdoc />
		protected override DB2Connection MakeConnectionInstance() {
			return new DB2Connection(ConnectionString);
		}
		#endregion
	}
}
