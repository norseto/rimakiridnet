// ==============================================================================
//     Net.Rimakiri.Data.DB2
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using Net.Rimakiri.Command;
#if (NET40)
using IBM.Data.DB2;
#else
using IBM.Data.DB2.Core;
#endif

namespace Net.Rimakiri.Data.DB2 {
	/// <inheritdoc cref="IRetryPolicy" />
	/// <summary>
	/// IBM DB2用のリトライポリシーです。
	/// </summary>
	/// <remarks>
	/// SQLSTATEが57033または40001（デッドロック）の場合にリトライ可能と判定します。
	/// </remarks>
	public class DB2RetryPolicy : RetryPolicyBase, IRetryPolicy {
		#region Implementation of IRetryPolicy
		/// <inheritdoc />
		public virtual bool IsRetryable(Exception error, int tryCount) {
			var sqlEx = error.FindCause<DB2Exception>();
			if (sqlEx == null) {
				return false;
			}
			var sqlState = Errors(sqlEx.Errors).FirstOrDefault(e => e.SQLState != null);
			if (sqlState == null) {
				return false;
			}

			switch (sqlState.SQLState) {
			case "40001":
			case "57014":
			case "57033":
				Delay(tryCount);
				return true;
			}
			return false;
		}
		#endregion

		private static IEnumerable<DB2Error> Errors(DB2ErrorCollection collection) {
			foreach (var err in collection) {
				if (err is DB2Error error) {
					yield return error;
				}
			}
		}
	}
}
