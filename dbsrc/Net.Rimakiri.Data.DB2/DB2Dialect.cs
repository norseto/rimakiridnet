// ==============================================================================
//     Net.Rimakiri.Data.DB2
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Data;
using Net.Rimakiri.Data.Interfaces;
using Net.Rimakiri.Data.Util;
#if (NET40)
using IBM.Data.DB2;
#else
using IBM.Data.DB2.Core;
#endif

namespace Net.Rimakiri.Data.DB2 {
	/// <summary>
	/// IBM DB2用の<see cref="IDatabaseDialect"/>クラスです。
	/// </summary>
	/// <inheritdoc />
	public class DB2Dialect : TypeMapDatabaseDialect<DB2Type> {
		/// <summary>
		/// <see cref="IDatabaseTypeMap{DB2Type}"/>を指定してインスタンスを
		/// 初期化します。
		/// </summary>
		/// <param name="typeMap">使用する<see cref="IDatabaseTypeMap{DB2Type}"/></param>
		/// <inheritdoc />
		protected DB2Dialect(IDatabaseTypeMap<DB2Type> typeMap) : base(typeMap) {
		}

		/// <summary>
		/// デフォルトの<see cref="IDatabaseTypeMap{DB2Type}"/>を指定してインスタンスを
		/// 初期化します。
		/// </summary>
		/// <inheritdoc />
		public DB2Dialect() : this(new DB2DbTypeMap()) {
			// Empty
		}

		#region Overrides of TypeMapDatabaseDialect<DB2Type>
		/// <inheritdoc />
		public override string CurrentTimestampLiteral => "CURRENT TIMESTAMP";

		/// <inheritdoc />
		public override string CurrentUtcTimestampLiteral
			=> "(CURRENT TIMESTAMP - CURRENT TIMEZONE)";

		/// <inheritdoc />
		public override string DummyTable => "SYSIBM.SYSDUMMY1";

		/// <inheritdoc />
		public override string DummyTableWithFrom => "FROM SYSIBM.SYSDUMMY1";

		/// <inheritdoc />
		public override string ParameterPrefix => "@";

		/// <inheritdoc />
		public override void SetupParameter(IDbDataParameter target, IBindValue value) {
			var parameterName = NameFor(value.Identity);
			var dbType = TypeMap.GetDatabaseType(parameterName, value);
			var db2Cmd = target as DB2Parameter;
			if (db2Cmd == null) {
				return;
			}
			db2Cmd.DB2Type = dbType;
			
			// DB2.Core ignores setting precision etc. before setting its type.
			// So set them again. May should be implemented in framework.
			var meta = value.BindMetaData;
			var size = meta.Size;
			if (size > 0) {
				db2Cmd.Size = size;
			}
			if (meta.Precision > 0) {
				db2Cmd.Precision = meta.Precision;
			}
			if (meta.Scale > 0) {
				db2Cmd.Scale = meta.Scale;
			}
		}
		#endregion
	}
}
