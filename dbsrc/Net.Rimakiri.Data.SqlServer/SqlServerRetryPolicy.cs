// ==============================================================================
//     Net.Rimakiri.Data.SqlServer
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Data.SqlClient;
using Net.Rimakiri.Command;

namespace Net.Rimakiri.Data.SqlServer {
	/// <summary>
	/// Microsoft SQL Server用のリトライポリシーです。
	/// </summary>
	/// <remarks>
	/// SQL Serverのデッドロック（#1205）、ロック解除エラー（#1203）
	/// およびスナップショット分離トランザクションの更新競合(#3960)
	/// の場合にリトライ可能と判定します。
	/// </remarks>
	/// <inheritdoc cref="IRetryPolicy" />
	public class SqlServerRetryPolicy : RetryPolicyBase, IRetryPolicy {
		#region Implementation of IRetryPolicy
		/// <inheritdoc />
		public virtual bool IsRetryable(Exception error, int tryCount) {
			var sqlEx = error.FindCause<SqlException>();
			if (sqlEx == null) {
				return false;
			}
			var number = sqlEx.Number;
			if (number == 1205 || number == 1203 || number == 3960) {
				Delay(tryCount);
				return true;
			}

			return false;
		}
		#endregion
	}
}
