// ==============================================================================
//     Net.Rimakiri.Data.SqlServer
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Data;
using Net.Rimakiri.Data.Interfaces;
using Net.Rimakiri.Data.TypeWrappers;

namespace Net.Rimakiri.Data.SqlServer {
	/// <summary>
	/// MS SQL Server用<see cref="IDatabaseTypeMap{SqlDbType}"/>の実装クラスです。
	/// <see cref="SqlServerDialect"/>により使用されます。
	/// </summary>
	/// <inheritdoc />
	public class SqlServerDbTypeMap : IDatabaseTypeMap<SqlDbType> {
		private readonly AssignableTypeMap<SqlDbType> wrapperTypes = new AssignableTypeMap<SqlDbType>();

		/// <summary>
		/// インスタンスを初期化します。
		/// </summary>
		public SqlServerDbTypeMap() {
			wrapperTypes[typeof(CharType)] = SqlDbType.Char;
			wrapperTypes[typeof(NCharType)] = SqlDbType.NChar;
			wrapperTypes[typeof(VarCharType)] = SqlDbType.VarChar;
			wrapperTypes[typeof(NVarCharType)] = SqlDbType.NVarChar;
			wrapperTypes[typeof(DateType)] = SqlDbType.Date;
			wrapperTypes[typeof(TimestampType)] = SqlDbType.DateTime;
			wrapperTypes[typeof(INumericWrapper)] = SqlDbType.Decimal;
		}

		#region Implementation of ISqlDbTypeMap
		/// <inheritdoc />
		public virtual SqlDbType GetDatabaseType(string parameterName, IBindValue value) {
			var type = value.BindMetaData.DataType;

			if (typeof(IDataWrapper).IsAssignableFrom(type)) {
				if (wrapperTypes.TryGet(type, out var ret)) {
					return ret;
				}
			}

			if (type == typeof(string)) {
				return (value.BindMetaData.Size <= 8000) ? SqlDbType.NVarChar : SqlDbType.NText;
			}

			switch (Type.GetTypeCode(type)) {
			case TypeCode.Byte:
				return SqlDbType.TinyInt;
			case TypeCode.SByte:
			case TypeCode.Int16:
				return SqlDbType.SmallInt;
			case TypeCode.UInt16:
			case TypeCode.Int32:
				return SqlDbType.Int;
			case TypeCode.UInt32:
			case TypeCode.Int64:
				return SqlDbType.BigInt;
			case TypeCode.UInt64:
			case TypeCode.Decimal:
				return SqlDbType.Decimal;
			case TypeCode.Double:
				return SqlDbType.Real;
			case TypeCode.Single:
				return SqlDbType.Float;
			}

			if (type == typeof(char[])) {
				return SqlDbType.NChar;
			}
			if (type == typeof(DateTime)) {
				return SqlDbType.DateTime;
			}

			throw new InvalidOperationException($"Unsupported type: {type}");
		}
		#endregion
	}
}
