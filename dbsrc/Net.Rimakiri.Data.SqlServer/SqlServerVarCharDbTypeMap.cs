// ==============================================================================
//     Net.Rimakiri.Data.SqlServer
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Data;
using Net.Rimakiri.Data.Interfaces;
using Net.Rimakiri.Data.TypeWrappers;

namespace Net.Rimakiri.Data.SqlServer {
	/// <summary>
	/// MS SQL Server用<see cref="IDatabaseTypeMap{SqlDbType}"/>の実装クラスです。
	/// 文字列に対して、NVarCharではなく、VarCharを使用します。
	/// <see cref="SqlServerDialect"/>により使用されます。
	/// </summary>
	/// <inheritdoc />
	public class SqlServerVarCharDbTypeMap : SqlServerDbTypeMap {
		#region Implementation of ISqlDbTypeMap
		/// <inheritdoc />
		public override SqlDbType GetDatabaseType(string parameterName, IBindValue value) {
			var type = value.BindMetaData.DataType;

			#region WrapperTypeMapping
			if (type == typeof(CharType)) {
				return SqlDbType.Char;
			}
			if (type == typeof(string)) {
				return (value.BindMetaData.Size <= 8000) ? SqlDbType.VarChar : SqlDbType.Text;
			}
			#endregion

			if (type == typeof(char[])) {
				return SqlDbType.Char;
			}

			return base.GetDatabaseType(parameterName, value);
		}
		#endregion
	}
}
