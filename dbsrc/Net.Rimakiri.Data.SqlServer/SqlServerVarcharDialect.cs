// ==============================================================================
//     Net.Rimakiri.Data.SqlServer
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data.SqlServer {
	/// <inheritdoc />
	/// <summary>
	/// Microsoft SQL Server 用の<see cref="IDatabaseDialect" />実装です。
	/// </summary>
	/// <remarks>
	/// 文字列は全てVarCharとして扱います。
	/// </remarks>
	public class SqlServerVarCharDialect : SqlServerDialect {
		/// <summary>
		/// インスタンスを初期化します。
		/// </summary>
		/// <inheritdoc />
		public SqlServerVarCharDialect() : base(new SqlServerVarCharDbTypeMap()) {
			// Empty
		}
	}
}
