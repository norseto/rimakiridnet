// ==============================================================================
//     Net.Rimakiri.Data.SqlServer
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Data;
using System.Data.SqlClient;
using Net.Rimakiri.Data.Interfaces;
using Net.Rimakiri.Data.Util;

namespace Net.Rimakiri.Data.SqlServer {
	/// <inheritdoc />
	/// <summary>
	/// Microsoft SQL Server 用の<see cref="IConnectionFactory" />実装です。
	/// </summary>
	public class SqlServerConnectionFactory : ConnectionFactoryBase<SqlConnection> {
		/// <summary>
		/// グローバルトランザクションを使用時に、X_ACT_ABORT ON の設定を行うか否か
		/// を設定します。デフォルトはTrueです。これを行わないと分散トランザクション
		/// 使用時にエラーとなります。
		/// </summary>
		public bool XactAbort { set; get; } = true;

		#region Overrides of ConnectionFactoryBase
		/// <inheritdoc />
		protected override IDatabaseDialect DefaultDialect { get; } = new SqlServerDialect();

		/// <inheritdoc />
		/// <summary>
		/// 接続インスタンスを生成します。
		/// </summary>
		/// <returns>生成したインスタンス</returns>
		protected override SqlConnection MakeConnectionInstance() {
			return new SqlConnection(ConnectionString);
		}

		/// <inheritdoc />
		protected override void SetupConnection(SqlConnection connection, bool global, IsolationLevel isolation) {
			if (global) {
				SetupXactAbortOn(connection);
			}
		}
		#endregion

		private void SetupXactAbortOn(SqlConnection con) {
			if (!XactAbort) {
				return;
			}
			try {
				con.Open();
				using (var cmd = con.CreateCommand()) {
					cmd.Connection = con;
					cmd.CommandText = "SET XACT_ABORT ON";
					cmd.ExecuteNonQuery();
				}
			}
			catch (Exception) {
				ForceClose(con);
				throw;
			}
		}
	}
}
