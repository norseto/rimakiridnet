// ==============================================================================
//     Net.Rimakiri.Data.SqlServer
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Data;
using System.Data.SqlClient;
using Net.Rimakiri.Data.Interfaces;
using Net.Rimakiri.Data.Util;

namespace Net.Rimakiri.Data.SqlServer {
	/// <inheritdoc />
	/// <summary>
	/// Microsoft SQL Server 用の<see cref="IDatabaseDialect" />実装です。
	/// </summary>
	/// <remarks>
	/// 文字列は全てNVarCharとして扱います。
	/// </remarks>
	public class SqlServerDialect : TypeMapDatabaseDialect<SqlDbType> {
		/// <summary>
		/// <see cref="IDatabaseTypeMap{SqlDbType}"/>を指定してインスタンスを
		/// 初期化します。
		/// </summary>
		/// <param name="typeMap">使用する<see cref="IDatabaseTypeMap{SqlDbType}"/></param>
		/// <inheritdoc />
		protected SqlServerDialect(IDatabaseTypeMap<SqlDbType> typeMap)
			: base(typeMap) {
		}

		/// <summary>
		/// デフォルトの<see cref="IDatabaseTypeMap{SqlDbType}"/>を使用する
		/// インスタンスを初期化します。
		/// </summary>
		/// <inheritdoc />
		public SqlServerDialect() : this(new SqlServerDbTypeMap()) {
		}

		#region Overrides of TypeMapDatabaseDialect<ISqlDbTypeMap>
		/// <inheritdoc />
		public override string CurrentTimestampLiteral => "GETDATE()";

		/// <inheritdoc />
		public override string CurrentUtcTimestampLiteral => "GETUTCDATE()";

		/// <inheritdoc />
		public override string DummyTable => string.Empty;

		/// <inheritdoc />
		public override string DummyTableWithFrom => string.Empty;

		/// <inheritdoc />
		public override string ParameterPrefix => "@";

		/// <inheritdoc />
		public override void SetupParameter(IDbDataParameter target, IBindValue value) {
			var parameterName = NameFor(value.Identity);
			var dbType = TypeMap.GetDatabaseType(parameterName, value);
			((SqlParameter)target).SqlDbType = dbType;
		}
		#endregion
	}
}
