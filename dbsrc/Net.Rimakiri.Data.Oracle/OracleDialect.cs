// ==============================================================================
//     Net.Rimakiri.Data.Oracle
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Data;
using Net.Rimakiri.Data.Interfaces;
using Net.Rimakiri.Data.Util;
using Oracle.ManagedDataAccess.Client;

namespace Net.Rimakiri.Data.Oracle {
	/// <summary>
	/// ORACLE RDBMS用の<see cref="IDatabaseDialect"/>クラスです。
	/// </summary>
	/// <seealso cref="OracleDbTypeMap"/>
	/// <inheritdoc />
	public class OracleDialect : TypeMapDatabaseDialect<OracleDbType> {
		/// <summary>
		/// <see cref="IDatabaseTypeMap{OracleDbType}"/>を指定してインスタンスを
		/// 初期化します。
		/// </summary>
		/// <param name="typeMap">使用する<see cref="IDatabaseTypeMap{OracleDbType}"/></param>
		/// <inheritdoc />
		protected OracleDialect(IDatabaseTypeMap<OracleDbType> typeMap)
			: base(typeMap) {
		}

		/// <summary>
		/// デフォルトの<see cref="OracleDbTypeMap"/>を使用するインスタンスを
		/// 初期化します。
		/// </summary>
		/// <inheritdoc />
		public OracleDialect() : this(new OracleDbTypeMap()) {
		}

		#region Overrides of TypeMapDatabaseDialect<IOracleDbTypeMap>
		/// <inheritdoc />
		public override string CurrentTimestampLiteral => "SYSTIMESTAMP";

		/// <inheritdoc />
		public override string CurrentUtcTimestampLiteral => "SYS_EXTRACT_UTC(SYSTIMESTAMP)";

		/// <inheritdoc />
		public override string DummyTable => "DUAL";

		/// <inheritdoc />
		public override string DummyTableWithFrom => "FROM DUAL";

		/// <inheritdoc />
		public override string ParameterPrefix => ":";

		/// <inheritdoc />
		public override void SetupParameter(IDbDataParameter target, IBindValue value) {
			var parameterName = NameFor(value.Identity);
			var dbType = TypeMap.GetDatabaseType(parameterName, value);
			var oraCmd = target as OracleParameter;
			if (oraCmd == null) {
				return;
			}
			oraCmd.OracleDbType = dbType;
			var meta = value.BindMetaData;
			if (meta.Precision > 0) {
				oraCmd.Precision = meta.Precision;
			}
			if (meta.Scale > 0) {
				oraCmd.Scale = meta.Scale;
			}
			if (meta.Size > 0) {
				oraCmd.Size = meta.Size;
			}
		}
		#endregion
	}
}
