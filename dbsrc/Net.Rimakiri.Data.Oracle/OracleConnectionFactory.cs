// ==============================================================================
//     Net.Rimakiri.Data.Oracle
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Data;
using Net.Rimakiri.Data.Interfaces;
using Net.Rimakiri.Data.Util;
using Oracle.ManagedDataAccess.Client;

namespace Net.Rimakiri.Data.Oracle {
	/// <summary>
	/// ORACLE RDBMS用の<see cref="IConnectionFactory"/>クラスです。
	/// </summary>
	/// <inheritdoc />
	public class OracleConnectionFactory : ConnectionFactoryBase<OracleConnection> {
		/// <summary>
		/// トランザクション分離レベルにSERIALIZABLE/SNAPSHOT/REPEATABLE READが指定
		/// されている状態で、グローバルトランザクションを使用時に、ISOLATION LEVEL の
		/// 設定を行うか否かを設定します。デフォルトはTrueです。
		/// </summary>
		/// <remarks>
		/// ORACLEでは、分散トランザクション下でSERIALIZABLE分離レベルはサポートされ
		/// ていませんが、データベース接続時に強制的にSERIALIZABLE分離レベルを設定します。
		/// これは、単一のデータベース接続しか使用しない状況下で、グローバルトランザクション
		/// を使用するような場合に有効です。しかし、単一のデータベース接続しか使用しない
		/// のであれば、ローカル（データベース）トランザクションで足りると考えられます。
		/// </remarks>
		public bool SetSerialize { set; get; } = true;

		#region Overrides of ConnectionFactoryBase<OracleConnection>
		/// <inheritdoc />
		protected override IDatabaseDialect DefaultDialect { get; } = new OracleDialect();

		/// <inheritdoc />
		public override Func<IsolationLevel, IsolationLevel> IsolationLevelMapping {
			get {
				return (org) => org == IsolationLevel.RepeatableRead ? IsolationLevel.Serializable
							: org == IsolationLevel.Snapshot ? IsolationLevel.Serializable
							: IsolationLevel.ReadCommitted;
			}
		}

		/// <inheritdoc />
		protected override OracleConnection MakeConnectionInstance() {
			return new OracleConnection(ConnectionString);
		}

		/// <inheritdoc />
		/// <summary>
		/// <see cref="SetSerialize"/>が有効で、かつグローバルトランザクション使用時に
		/// かつ、<paramref name="isolation" />が<see cref="IsolationLevel.RepeatableRead"/>、
		/// <see cref="IsolationLevel.Serializable"/>または<see cref="IsolationLevel.Snapshot"/>のとき、
		/// SET TRANSACTION ISOLATION LEVEL SERIALIZABLE を実行します。
		/// </summary>
		protected override void SetupConnection(OracleConnection connection, bool global, IsolationLevel isolation) {
			if (!global || !SetSerialize) {
				return;
			}
			if (isolation == IsolationLevel.RepeatableRead || isolation == IsolationLevel.Snapshot
					|| isolation == IsolationLevel.Serializable) {
				SetupSerializable(connection);
			}
		}

		private void SetupSerializable(OracleConnection connection) {
			try {
				connection.Open();
				using (var cmd = connection.CreateCommand()) {
					cmd.Connection = connection;
					cmd.CommandText = "SET TRANSACTION ISOLATION LEVEL SERIALIZABLE";
					cmd.ExecuteNonQuery();
				}
			}
			catch (Exception) {
				ForceClose(connection);
				throw;
			}
		}
		#endregion
	}
}
