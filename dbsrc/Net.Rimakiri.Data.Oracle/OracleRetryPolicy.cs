// ==============================================================================
//     Net.Rimakiri.Data.Oracle
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using Net.Rimakiri.Command;
using Oracle.ManagedDataAccess.Client;

namespace Net.Rimakiri.Data.Oracle {
	/// <summary>
	/// ORACLE RDBMS用のリトライポリシーです。
	/// </summary>
	/// <remarks>
	/// ORACLEのデッドロック検出(ORA-00060)、シリアライズエラー（ORA-08177）
	/// の場合にリトライ可能と判定します。
	/// </remarks>
	/// <inheritdoc cref="IRetryPolicy" />
	public class OracleRetryPolicy : RetryPolicyBase, IRetryPolicy {
		#region Implementation of IRetryPolicy
		/// <inheritdoc />
		public virtual bool IsRetryable(Exception error, int tryCount) {
			var sqlEx = error.FindCause<OracleException>();
			if (sqlEx == null) {
				return false;
			}
			var number = sqlEx.Number;
			if (number == 60 || number == 8177) {
				Delay(tryCount);
				return true;
			}

			return false;
		}
		#endregion
	}
}
