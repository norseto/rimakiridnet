// ==============================================================================
//     Net.Rimakiri.Data.Oracle
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using Net.Rimakiri.Data.Interfaces;
using Net.Rimakiri.Data.TypeWrappers;
using Oracle.ManagedDataAccess.Client;

namespace Net.Rimakiri.Data.Oracle {
	/// <summary>
	/// デフォルトの<see cref="IDatabaseTypeMap{OracleDbType}"/>の実装クラスです。
	/// <see cref="OracleDialect"/>により使用されます。
	/// </summary>
	/// <remarks>
	/// <see cref="string"/>は<see cref="OracleDbType.NVarchar2"/>に、
	/// <see cref="DateTime"/>は<see cref="OracleDbType.TimeStamp"/>に
	/// 割当てます。詳細は、<see href="http://otndnld.oracle.co.jp/document/products/oracle10g/101/doc_v12/win.101/B15519-01/featOraCommand.htm"/>
	/// </remarks>
	/// <inheritdoc />
	public class OracleDbTypeMap : IDatabaseTypeMap<OracleDbType> {
		private readonly AssignableTypeMap<OracleDbType> wrapperTypes = new AssignableTypeMap<OracleDbType>();

		/// <summary>
		/// インスタンスを初期化します。
		/// </summary>
		public OracleDbTypeMap() {
			wrapperTypes[typeof(CharType)] = OracleDbType.Char;
			wrapperTypes[typeof(NCharType)] = OracleDbType.NChar;
			wrapperTypes[typeof(VarCharType)] = OracleDbType.Varchar2;
			wrapperTypes[typeof(NVarCharType)] = OracleDbType.NVarchar2;
			wrapperTypes[typeof(DateType)] = OracleDbType.Date;
			wrapperTypes[typeof(TimestampType)] = OracleDbType.TimeStamp;
			wrapperTypes[typeof(INumericWrapper)] = OracleDbType.Decimal;
		}

		#region Implementation of IOracleDbTypeMap
		/// <inheritdoc />
		public OracleDbType GetDatabaseType(string parameterName, IBindValue value) {
			var type = value.BindMetaData.DataType;

			if (typeof(IDataWrapper).IsAssignableFrom(type)) {
				if (wrapperTypes.TryGet(type, out var ret)) {
					return ret;
				}
			}

			if (type == typeof(string)) {
				return OracleDbType.NVarchar2;
			}

			switch (Type.GetTypeCode(type)) {
			case TypeCode.Byte:
				return OracleDbType.Byte;
			case TypeCode.Int16:
				return OracleDbType.Int16;
			case TypeCode.Int32:
				return OracleDbType.Int32;
			case TypeCode.Int64:
				return OracleDbType.Int64;
			case TypeCode.Decimal:
				return OracleDbType.Decimal;
			case TypeCode.Double:
				return OracleDbType.Double;
			case TypeCode.Single:
				return OracleDbType.Single;
			}

			if (type == typeof(char[])) {
				return OracleDbType.NChar;
			}
			if (type == typeof(DateTime)) {
				return OracleDbType.TimeStamp;
			}

			throw new InvalidOperationException($"Unsupported type: {type}");
		}
		#endregion
	}
}
