using Net.Rimakiri.Command;

namespace DocExample.CS {

	#region CommandContext
	public class MyContext : CommandContext {
		public string UserName { get; set; }
	}
	#endregion

	#region ICommand
	public interface IMyCommand : ICommand<MyContext> {
		// Empty
	}
	#endregion

	#region CommandImpl
	public class MyCommandImpl : IMyCommand {
		/// <inheritdoc />
		public TxScope TxScope { get; set; }

		/// <inheritdoc />
		public Idempotent Idempotent { get; set; }

		/// <inheritdoc />
		public void PreExecute(MyContext context) {
			// Nothing to do.
		}

		/// <inheritdoc />
		public void PostExecute(MyContext context) {
			// Nothing to do.
		}

		/// <inheritdoc />
		public void Execute(MyContext context) {
			// TODO: Implement this!
		}
	}
	#endregion

	#region CommandServiceBuilder
	public class MyBuilder : CommandServiceBuilder<MyContext> {
		// Empty
	}

	public interface IMyBuilder : ICommandServiceBuilder<MyContext> {
		// Empty
	}
	#endregion

	#region LifecycleListener
	public class MyListener : IContextLifecycleListener<MyContext> {
		/// <inheritdoc />
		public void OnPostContextCreate(MyContext context) {
			context.UserName = "Somebody";
		}

		/// <inheritdoc />
		public void OnPreContextDestroy(MyContext context) {
			// Nothing to do
		}
	}
	#endregion

	public class Main {
		#region BuildExample
		public static void Run() {
			var builder = new MyBuilder();
			var service = builder.Build();
			var cmd = new MyCommandImpl();

			service.ExecuteCommand(cmd);
		}
		#endregion
	}

	public class MainLsnr {
		#region BuildExampleListener
		public static void Run() {
			var builder = new MyBuilder {ContextLifecycleListener = new MyListener()};
			var service = builder.Build();
			var cmd = new MyCommandImpl();

			service.ExecuteCommand(cmd);
		}
		#endregion
	}
}
