﻿Imports Net.Rimakiri.Command

Namespace DocExample.VB

#Region "CommandContext"
	Public Class MyContext
		Inherits CommandContext
		Public Property UserName As String
	End Class
#End Region

#Region "ICommand"
	Public Interface IMyCommand
		Inherits ICommand(Of MyContext)
		' Empty
	End Interface
#End Region

#Region "CommandImpl"
	Public Class MyCommandImpl
		Implements IMyCommand

		Dim _txScope As TxScope
		Dim _idempotent As Idempotent

		Public ReadOnly Property TxScope_ICommand As TxScope Implements ICommand(Of MyContext).TxScope
			Get
				Return _txScope
			End Get
		End Property

		Public Property TxScope As TxScope
			Get
				Return _txScope
			End Get
			Set(ByVal value As TxScope)
				_txScope = value
			End Set
		End Property

		Public ReadOnly Property Idempotent_ICommand As Idempotent Implements ICommand(Of MyContext).Idempotent
			Get
				Return _idempotent
			End Get
		End Property

		Public Property Idempotent As Idempotent
			Get
				Return _idempotent
			End Get
			Set(ByVal value As Idempotent)
				_idempotent = value
			End Set
		End Property

		Public Sub PreExecute(ByVal context As MyContext) Implements ICommand(Of MyContext).PreExecute
			' Empty
		End Sub

		Public Sub PostExecute(ByVal context As MyContext) Implements ICommand(Of MyContext).PostExecute
			' Empty
		End Sub

		Public Sub Execute(ByVal context As MyContext) Implements ICommand(Of MyContext).Execute
			' TODO: Implement this!
		End Sub
	End Class
#End Region

#Region "CommandServiceBuilder"
	Public Class MyBuilder
		Inherits CommandServiceBuilder(Of MyContext)
		' Empty
	End Class

	Public Interface IMyBuilder
		Inherits ICommandServiceBuilder(Of MyContext)
		' Empty
	End Interface
#End Region

#Region "LifecycleListener"
	Public Class MyListener
		Implements IContextLifecycleListener(Of MyContext)

		Public Sub OnPostContextCreate(ByVal context As MyContext) _
			Implements IContextLifecycleListener(Of MyContext).OnPostContextCreate
			context.UserName = "Somebody"
		End Sub

		Public Sub OnPreContextDestroy(ByVal context As MyContext) _
			Implements IContextLifecycleListener(Of MyContext).OnPreContextDestroy
			' Empty
		End Sub
	End Class
#End Region

	Public Class Main

#Region "BuildExample"
		Public Sub Run()
			Dim builder As New MyBuilder
			Dim cmd As New MyCommandImpl
			Dim service As ICommandService = builder.Build()

			service.ExecuteCommand(cmd)
		End Sub
#End Region

	End Class

	Public Class MainLs

#Region "BuildExampleListener"
		Public Sub Run()
			Dim builder As New MyBuilder
			Dim cmd As New MyCommandImpl
			Dim service As ICommandService = builder.Build()

			service.ExecuteCommand(cmd)
		End Sub
#End Region

	End Class
End Namespace

