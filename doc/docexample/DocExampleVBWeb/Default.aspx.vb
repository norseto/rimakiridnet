﻿Imports Rimakiri.Container
Imports DocExample.VB.DocExample.VB

Public Class _Default
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

	End Sub

#Region "DefaultAspx"
	<DIDepend()>
	Public Property Command() As IMyCommand

	<DIDepend("TxService")>
	Public Property Builder() As MyBuilder

	<DIDepend()>
	Public Property Container() As IDIContainer

	Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
		Dim service = Builder.Build()
		service.ExecuteCommand(Command)
	End Sub
#End Region
End Class
