﻿Imports Net.Rimakiri.Container
Imports Net.Rimakiri.Container.Web

Public Class Global_asax
	Inherits System.Web.HttpApplication

#Region "GlovalAsax"
	Private Const RegistryKey As String = "#DIContainerRegistry"

    Public ReadOnly Property Registry() As DIContainerRegistry
        Get
            Return Application.Item(RegistryKey)
        End Get
    End Property

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
		Dim reg = DIContainerRegistry.Load(Nothing, New WebConfigurationLoader)
		Application.Item(RegistryKey) = reg
	End Sub

	Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
		If Not Registry Is Nothing Then
			Registry.Dispose()
		End If
	End Sub

	Sub Application_PreRequestHandlerExecute(ByVal sender As Object, ByVal e As EventArgs)
		Dim handler = TryCast(HttpContext.Current.Handler, Page)
		If handler Is Nothing Then
			Return
		End If
		Registry.Buildup(handler, handler.GetType().Name)
	End Sub
#End Region

	Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
	End Sub

	Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
		' 各要求の開始時に呼び出されます。
	End Sub

	Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
		' 使用の認証時に呼び出されます。
	End Sub

	Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
		' エラーの発生時に呼び出されます。
	End Sub

	Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
		' セッションの終了時に呼び出されます。
	End Sub

End Class
