using System;
using System.Web;
using System.Web.UI;
using Net.Rimakiri.Container;
using Net.Rimakiri.Container.Web;

namespace DocExampleCSWeb {
	public class Global : System.Web.HttpApplication {
		#region GlovalAsax
		private const string RegistryKey = "#DIContainerRegistry";

		public DIContainerRegistry Registry {
			get { return Application[RegistryKey] as DIContainerRegistry; }
		}

		void Application_Start(object sender, EventArgs e) {
			var registry = DIContainerRegistry.Load(null, new WebConfigurationLoader());
			Application[RegistryKey] = registry;
		}

		void Application_End(object sender, EventArgs e) {
			if (Registry != null) {
				Registry.Dispose();
			}
		}

		protected void Application_PreRequestHandlerExecute(object sender, EventArgs e) {
			var handler = HttpContext.Current.Handler as Page;
			if (handler == null) {
				return;
			}
			Registry.Buildup(handler, handler.GetType().Name);
		}
		#endregion

		void Application_Error(object sender, EventArgs e) {
			// ハンドルされていないエラーが発生したときに実行するコードです
		}

		void Session_Start(object sender, EventArgs e) {
			// 新規セッションを開始したときに実行するコードです
		}

		void Session_End(object sender, EventArgs e) {
			// セッションが終了したときに実行するコードです 
			// メモ: Web.config ファイル内で sessionstate モードが InProc に設定されているときのみ、
			// Session_End イベントが発生します。 session モードが StateServer か、または 
			// SQLServer に設定されている場合、イベントは発生しません。
		}
	}
}
