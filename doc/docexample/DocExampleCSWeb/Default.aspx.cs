using System;
using DocExample.CS;
using Net.Rimakiri.Container;

namespace DocExampleCSWeb {
	public partial class _Default : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {
		}

		#region DefaultAspx
		[DIDepend]
		public IMyCommand Command { get; set; }

		[DIDepend("TxService")]
		public MyBuilder Builder { get; set; }

		[DIDepend]
		public IDIContainer Container { get; set; }

		protected void Button1_Click(object sender, EventArgs e) {
			var service = Builder.Build();

			service.ExecuteCommand(Command);
		}
		#endregion
	}
}
