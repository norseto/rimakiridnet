// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Linq.Expressions;
using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data.Util {

	/// <summary>
	/// モデルのタイプの直接のプロパティのみを対象とした
	/// <see cref="IModelBinding{TModel}"/>
	/// クラスです。プロパティのプロパティなどはサポートしません。
	/// </summary>
	/// <typeparam name="TModel">モデルのタイプ</typeparam>
	internal class SimpleModelBindingImpl<TModel> : IModelBinding<TModel> {
		private readonly TModel model;
		private readonly BindingMetaDataCollection collection;

		/// <summary>
		/// インスタンスを初期化します。
		/// </summary>
		/// <param name="dict">プロパティメタ情報</param>
		/// <param name="model">対象モデルインスタンス</param>
		public SimpleModelBindingImpl(BindingMetaDataCollection dict, TModel model) {
			collection = dict;
			this.model = model;
		}

		/// <inheritdoc />
		public IBindValue Create<TValue>(string identity, Expression<Func<TModel, TValue>> propertyLambda) {
			var value = GetBindingMetaDataAndValue(propertyLambda, out var meta);
			identity = identity ?? meta.Identity;
			return BindValue.Create(identity, value, meta.Size, meta.Precision, meta.Scale);
		}

		/// <inheritdoc />
		public IBindValue WithFormat(string identity, string format, Expression<Func<TModel, string>> propertyLambda,
					char? escape = null) {
			var value = GetBindingMetaDataAndValue(propertyLambda, out var meta);
			identity = identity ?? meta.Identity;
			return (escape == null) ? BindValue.WithFormat(identity, format, value, meta.Size)
					: BindValue.WithFormat(identity, format, value, escape.Value, meta.Size);
		}

		/// <inheritdoc />
		public IBindValue WithFormat<TValue>(string identity, string format, Expression<Func<TModel, TValue>> propertyLambda,
					char? escape = null) where TValue : IStringDataWrapper {
			var value = GetBindingMetaDataAndValue(propertyLambda, out var meta);
			identity = identity ?? meta.Identity;
			return (escape == null) ? BindValue.WithFormat(identity, format, value, meta.Size)
					: BindValue.WithFormat(identity, format, value, escape.Value, meta.Size);
		}

		/// <inheritdoc />
		public IBindValue Create<TValue>(Expression<Func<TModel, TValue>> propertyLambda)
				=> Create(null, propertyLambda);

		/// <inheritdoc />
		public IBindValue WithFormat(string format,
					Expression<Func<TModel, string>> propertyLambda, char? escape = null)
				=> WithFormat(null, format, propertyLambda, escape);

		/// <inheritdoc />
		public IBindValue WithFormat<TValue>(string format, Expression<Func<TModel, TValue>> propertyLambda,
					char? escape = null) where TValue : IStringDataWrapper
				=> WithFormat(null, format, propertyLambda, escape);

		private TValue  GetBindingMetaDataAndValue<TValue>(Expression<Func<TModel, TValue>> lambda,
					out BindingMetaData attribute) {
			BindingMetaData metaData = null;
			var value = DigValue(lambda, model, lambda.Body, ref metaData, 0);
			attribute = metaData;
			return (TValue)value;
		}

		private object DigValue<TValue>(Expression<Func<TModel, TValue>> lambda,
							object target, Expression expression,
								ref BindingMetaData metaData, int depth) {
			var memberExp = expression as MemberExpression;
			if (memberExp == null) {
				throw new ArgumentException("Unsupported lambda expression: " + lambda);
			}

			var expType = memberExp.Expression.Type;
			var name = memberExp.Member.Name;
			var meta = collection.GetMetaData(expType, name, out var hasType);

			switch (meta) {
			case null when hasType:
				throw new ArgumentException(lambda + " is not valid property.");
			case null:
				throw new ArgumentException("Unsupported member expression: " + lambda);
			}

			if (depth == 0) {
				metaData = meta;
			}

			if (memberExp.Expression is ParameterExpression childExp
					&& childExp.Type == typeof(TModel)) {
				return meta.GetValue(target);
			}

			var value = DigValue(lambda, target, memberExp.Expression, ref metaData, depth + 1);
			return value == null ? null : meta.GetValue(value);
		}
	}
}
