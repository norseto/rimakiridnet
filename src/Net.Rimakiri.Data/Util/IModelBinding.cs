// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Linq.Expressions;
using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data.Util {
	/// <summary>
	/// モデルのメタ情報（属性）をもとに<see cref="IBindValue"/>を生成する
	/// クラスが実装するインターフェイスです。
	/// </summary>
	/// <typeparam name="TModel">モデルのタイプ</typeparam>
	/// <seealso cref="ModelBindingFactory{TModel}"/>
	/// <seealso cref="BindValue"/>
	/// <seealso cref="EntityBindingAttribute"/>
	public interface IModelBinding<TModel> {
		/// <summary>
		/// 属性の情報をもとに<see cref="IBindValue"/>インスタンスを生成します。
		/// </summary>
		/// <typeparam name="TValue">属性のタイプ</typeparam>
		/// <param name="identity">パラメタの識別子</param>
		/// <param name="propertyLambda">プロパティ取得ラムダ</param>
		/// <returns>生成した<see cref="IBindValue"/>インスタンス</returns>
		/// <seealso cref="BindValue.Create{T}"/>
		/// <remarks>識別子は属性で指定されているものよりもパラメタで指定されたものが優先されます</remarks>
		IBindValue Create<TValue>(string identity,
			Expression<Func<TModel, TValue>> propertyLambda);

		/// <summary>
		/// 属性の情報をもとに<see cref="IBindValue"/>インスタンスを生成します。
		/// </summary>
		/// <param name="identity">パラメタの識別子</param>
		/// <param name="propertyLambda">プロパティ取得ラムダ</param>
		/// <param name="format">フォーマット</param>
		/// <param name="escape">エスケープ文字</param>
		/// <returns>生成した<see cref="IBindValue"/>インスタンス</returns>
		/// <seealso cref="BindValue.WithFormat(string,string,string,char,int)"/>
		/// <remarks>識別子は属性で指定されているものよりもパラメタで指定されたものが優先されます</remarks>
		IBindValue WithFormat(string identity, string format,
			Expression<Func<TModel, string>> propertyLambda, char? escape = null);

		/// <summary>
		/// 属性の情報をもとに<see cref="IBindValue"/>インスタンスを生成します。
		/// </summary>
		/// <typeparam name="TValue">属性のタイプ</typeparam>
		/// <param name="identity">パラメタの識別子</param>
		/// <param name="propertyLambda">プロパティ取得ラムダ</param>
		/// <param name="format">フォーマット</param>
		/// <param name="escape">エスケープ文字</param>
		/// <returns>生成した<see cref="IBindValue"/>インスタンス</returns>
		/// <seealso cref="BindValue.WithFormat{T}(string,string,T,char,int)"/>
		/// <remarks>識別子は属性で指定されているものよりもパラメタで指定されたものが優先されます</remarks>
		IBindValue WithFormat<TValue>(string identity, string format,
			Expression<Func<TModel, TValue>> propertyLambda, char? escape = null)
				where TValue : IStringDataWrapper;

		/// <summary>
		/// 属性の情報をもとに<see cref="IBindValue"/>インスタンスを生成します。
		/// </summary>
		/// <typeparam name="TValue">属性のタイプ</typeparam>
		/// <param name="propertyLambda">プロパティ取得ラムダ</param>
		/// <returns>生成した<see cref="IBindValue"/>インスタンス</returns>
		/// <seealso cref="BindValue.Create{T}"/>
		/// <remarks>識別子は属性から取得します。</remarks>
		IBindValue Create<TValue>(Expression<Func<TModel, TValue>> propertyLambda);

		/// <summary>
		/// 属性の情報をもとに<see cref="IBindValue"/>インスタンスを生成します。
		/// </summary>
		/// <param name="propertyLambda">プロパティ取得ラムダ</param>
		/// <param name="format">フォーマット</param>
		/// <param name="escape">エスケープ文字</param>
		/// <returns>生成した<see cref="IBindValue"/>インスタンス</returns>
		/// <seealso cref="BindValue.WithFormat(string,string,string,char,int)"/>
		/// <remarks>識別子は属性から取得します。</remarks>
		IBindValue WithFormat(string format,
			Expression<Func<TModel, string>> propertyLambda, char? escape = null);

		/// <summary>
		/// 属性の情報をもとに<see cref="IBindValue"/>インスタンスを生成します。
		/// </summary>
		/// <typeparam name="TValue">属性のタイプ</typeparam>
		/// <param name="propertyLambda">プロパティ取得ラムダ</param>
		/// <param name="format">フォーマット</param>
		/// <param name="escape">エスケープ文字</param>
		/// <returns>生成した<see cref="IBindValue"/>インスタンス</returns>
		/// <seealso cref="BindValue.WithFormat{T}(string,string,T,char,int)"/>
		/// <remarks>識別子は属性から取得します。</remarks>
		IBindValue WithFormat<TValue>(string format,
			Expression<Func<TModel, TValue>> propertyLambda, char? escape = null)
				where TValue : IStringDataWrapper;
	}
}
