// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data.Util {
	/// <inheritdoc />
	/// <summary>
	/// <see cref="IConnectionFactory"/>のベースとなるクラスです。
	/// </summary>
	/// <typeparam name="T">実際に生成される<see cref="IDbConnection"/>のタイプ</typeparam>
	public abstract class ConnectionFactoryBase<T> : IConnectionFactory
				where T:DbConnection {
		private IDatabaseDialect dialect;
		private string connectionStringName;
		private string name;

		/// <summary>
		/// デフォルトの<see cref="IDatabaseDialect"/>を取得します。
		/// </summary>
		protected abstract IDatabaseDialect DefaultDialect { get; }

		/// <summary>
		/// 識別名を取得または設定します。設定されていない場合には、
		/// 接続文字列名が識別名として使用されます。
		/// </summary>
		/// <inheritdoc />
		public string Name {
			get => name ?? connectionStringName;
			set => name = value;
		}

		/// <summary>
		/// 接続文字列を設定します。
		/// </summary>
		public string ConnectionString { set; protected get; }

		/// <summary>
		/// 接続文字列名を設定します。
		/// </summary>
		public string ConnectionStringName {
			set {
				connectionStringName = value;
				ConnectionString = ConfigurationManager.
						ConnectionStrings[value].ConnectionString;
			}
		}
		
		#region Implementation of IConnectionFactory
		/// <inheritdoc />
		/// <param name="global">グローバルトランザクション内の場合True</param>
		/// <param name="isolation">要求されているトランザクション分離レベル</param>
		/// <returns>生成したデータベース接続</returns>
		public DbConnection NewConnection(bool global, IsolationLevel isolation) {
			T connection = MakeConnectionInstance();
			SetupConnection(connection, global, isolation);
			return connection;
		}

		/// <inheritdoc />
		public IDatabaseDialect DatabaseDialect {
			get => dialect ?? DefaultDialect;
			set => dialect = value;
		}

		/// <inheritdoc />
		public virtual Func<IsolationLevel, IsolationLevel> IsolationLevelMapping => null;
		#endregion

		/// <summary>
		/// 接続インスタンスを生成します。
		/// </summary>
		/// <returns>生成したインスタンス</returns>
		protected abstract T MakeConnectionInstance();

		/// <summary>
		/// 接続の追加設定を行います。
		/// </summary>
		/// <param name="connection">生成された接続</param>
		/// <param name="global">グローバルトランザクション内の場合True</param>
		/// <param name="isolation">要求されているトランザクション分離レベル</param>
		protected virtual void SetupConnection(T connection, bool global, IsolationLevel isolation) {
		}

		/// <summary>
		/// 接続を強制的にクローズします。
		/// </summary>
		/// <param name="con">対象接続</param>
		protected static void ForceClose(IDbConnection con) {
			try {
				con?.Dispose();
			}
			catch (Exception) {
				// Ignore.
			}
		}
	}
}
