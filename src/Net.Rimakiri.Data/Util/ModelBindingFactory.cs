// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;

namespace Net.Rimakiri.Data.Util {
	/// <inheritdoc />
	/// <summary>
	/// バインドする際のサイズ情報を指定する属性クラスです。
	/// </summary>
	/// <seealso cref="ModelBindingFactory{TModel}"/>
	/// <seealso cref="IModelBinding{TModel}"/>
	/// <seealso cref="DynamicModelBinding"/>
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field
			| AttributeTargets.ReturnValue)]
	public class EntityBindingAttribute : Attribute {
		/// <summary>
		/// 識別子を設定または取得します。
		/// </summary>
		public string Identity { get; set; }
		/// <summary>
		/// サイズを設定または取得します。
		/// </summary>
		public int Size { get; set; }
		/// <summary>
		/// 値の有効桁数を設定または取得します。
		/// </summary>
		public byte Precision { get; set; }
		/// <summary>
		/// 値の小数点以下桁数を設定または取得します。
		/// </summary>
		public byte Scale { get; set; }
	}

	/// <summary>
	/// <see cref="IModelBinding{TModel}"/>を生成するファクトリ
	/// クラスです。
	/// </summary>
	/// <remarks>
	/// 例のように<see cref="EntityBindingAttribute"/>属性が付与されたモデル
	/// クラスを指定してファクトリインスタンスを生成し、個別のモデルを対象とした
	/// <see cref="IModelBinding{TModel}"/>インスタンスを生成して、バインドを
	/// 実行します。生成されたファクトリインスタンスについては、マルチスレッド
	/// での使用が可能です。
	/// <example>
	/// <code language="C#">
	/// public class Customer {
	///		[EntityBinding(Identity = "name", Size = 20)]
	///		public string Name { get; set; }
	///		public Address Contact { get; set; }
	/// }
	/// public class Address {
	///		[EntityBinding(Identity = "zip", Size = 8)]
	///		public string Zip { get; set; }
	///		[EntityBinding(Identity = "address", Size = 50)]
	///		public string Address { get; set; }
	/// }
	/// ...
	///	public void DoBind(IDatabaseDialect dialect, DbCommand command,
	///							Customer customer) {
	///		// Contactプロパティのタイプも同時に指定します。
	///		var factory = new ModelBindingFactory&lt;Customer&gt;(typeof(Address));
	///		var bndr = factory.Create(data);
	///		var select = new SimpleFragment("SELECT * FROM SAMPLE");
	///		var cond1 = new BasicCondition("NAME = {0}", bndr.Create(m => m.Name));
	///		var cond2 = new NotNullCondition("ADDRESS LIKE {0} ESCAPE '#'",
	///							bndr.WithFormat(BindValue.ForwardMatch, m => m.Contact.Address, '#'));
	///		var statement = select.Concat(new WhereFragment(cond1.And(cond2)));
	///		var builder = new ValueBinderBuilder(statement);
	///		var binder = builder.Build(dialect);
	///
	///		binder.Bind(command);
	///	}
	/// </code>
	/// </example>
	/// </remarks>
	/// <typeparam name="TModel"></typeparam>
	public class ModelBindingFactory<TModel> {
		private BindingMetaDataCollection metaCollection;
		private readonly IEnumerable<Type> additional;

		/// <summary>
		/// 生成するインスタンスがダイナミックラムダをサポートするかを設定または
		/// 取得します（デフォルトはFalse）。
		/// </summary>
		/// <remarks>
		/// ダイナミックラムダサポートは非常に強力です（プロパティのプロパティ
		/// など、設定可能な値はモデルクラスのメンバーに限られない）が、
		/// ラムダのコンパイルを行うため、パフォーマンス上のドローバックが存在
		/// します。また、ダイナミックラムダ指定の場合にのみ、メソッドに付与された
		/// <see cref="EntityBindingAttribute"/>が使用されます。
		/// </remarks>
		public bool Dynamic { get; set; }

		/// <summary>
		/// プロパティ/フィールドの属性検索の際、メンバーの祖先も検索するかを指定します。
		/// デフォルトはFalseです。
		/// </summary>
		public bool Inherit { get; set; }

		/// <summary>
		/// 追加タイプを指定してインスタンスを初期化します。
		/// </summary>
		/// <param name="additionals">追加タイプ</param>
		/// <remarks>
		/// 指定した追加タイプのプロパティについても解析され、プロパティの
		/// プロパティなどをラムダで指定しても読み込まれるようになります。
		/// <see cref="Dynamic"/>が指定されている場合は追加タイプの指定の必要
		/// はありません。
		/// </remarks>
		public ModelBindingFactory(params Type[] additionals) {
			additional = additionals.ToArray();
		}

		/// <summary>
		/// 明示的にモデルのプロパティ情報の収集を行います。
		/// </summary>
		/// <returns>このインスタンス</returns>
		public ModelBindingFactory<TModel> Initialize() {
			Analyze();

			return this;
		}

		private BindingMetaDataCollection Analyze() {
			var dict = metaCollection;
			if (dict != null) {
				return dict;
			}

			dict = new BindingMetaDataCollection { Inherit = Inherit };
			dict.StudyType(typeof(TModel));
			foreach (var type in additional) {
				dict.StudyType(type);
			}

			Interlocked.CompareExchange(ref metaCollection, dict, null);

			return dict;
		}

		/// <summary>
		/// <see cref="IModelBinding{TModel}"/>インスタンスを生成します。
		/// </summary>
		/// <param name="model">モデルインスタンス</param>
		/// <returns>生成したインスタンス</returns>
		public IModelBinding<TModel> Create(TModel model) {
			if (Dynamic) {
				return new DynamicModelBindingImpl<TModel>(model, Inherit);
			}
			return	new SimpleModelBindingImpl<TModel>(Analyze(), model);
		}
	}

	/// <summary>
	/// <see cref="BindingMetaData"/>のコレクションクラスです。
	/// </summary>
	internal class BindingMetaDataCollection {
		private readonly Dictionary<Type, IDictionary<string, BindingMetaData>> typeDict
				= new Dictionary<Type, IDictionary<string, BindingMetaData>>();

		public bool Inherit { get; set; }

		/// <summary>
		/// 指定されたクラスの情報を蓄積します。
		/// </summary>
		/// <param name="type">対象クラス</param>
		public void StudyType(Type type) {
			if (typeDict.ContainsKey(type)) {
				return;
			}
			Dictionary<string, BindingMetaData> dict
					= new Dictionary<string, BindingMetaData>();
			var props = type.GetMembers()
					.Where(m => m is PropertyInfo || m is FieldInfo);
			var inherit = Inherit;
			foreach (var prop in props) {
				var name = prop.Name;
				dict.Add(name, BindingMetaData.Create(prop, inherit));
			}

			typeDict.Add(type, dict);
		}

		/// <summary>
		/// メタデータを取得します。
		/// </summary>
		/// <param name="type">取得するメタデータが定義されているタイプ</param>
		/// <param name="name">取得するメタデータのメンバー名</param>
		/// <param name="hasType">タイプが含まれていた場合True</param>
		/// <returns>取得したメタデータ</returns>
		public BindingMetaData GetMetaData(Type type, string name, out Boolean hasType) {
			if (!typeDict.ContainsKey(type)) {
				hasType = false;
				return null;
			}

			hasType = true;
			var dic = typeDict[type];
			return !dic.ContainsKey(name) ? null : dic[name];
		}
	}

	/// <summary>
	/// モデルのバインド情報を保持します。
	/// </summary>
	internal class BindingMetaData {
		/// <summary>
		/// メタ情報を設定または取得します。
		/// </summary>
		private EntityBindingAttribute Attribute { get; }

		/// <summary>
		/// 値取得デリゲートを設定または取得します。
		/// </summary>
		private Delegate Getter { get; }

		/// <summary>
		/// メタ情報から識別子を取得します。
		/// </summary>
		public string Identity => Attribute?.Identity;

		/// <summary>
		/// メタ情報からサイズ情報を取得します。
		/// </summary>
		public int Size => Attribute?.Size ?? -1;

		/// <summary>
		/// メタ情報から有効桁数情報を取得します。
		/// </summary>
		public byte Precision => Attribute?.Precision ?? (byte)0;

		/// <summary>
		/// メタ情報から小数点以下桁数情報を取得します。
		/// </summary>
		public byte Scale => Attribute?.Scale ?? (byte)0;

		/// <summary>
		/// プロパティの値を取得します。
		/// </summary>
		/// <param name="model">モデル</param>
		/// <returns>プロパティの値</returns>
		public object GetValue(object model) {
			return Getter.DynamicInvoke(model);
		}

		/// <summary>
		/// インスタンスを生成します。
		/// </summary>
		/// <param name="prop">メンバー情報</param>
		/// <param name="inherit">祖先も属性検索する場合True</param>
		/// <returns>生成したインスタンス</returns>
		public static BindingMetaData Create(MemberInfo prop, bool inherit) {
			var attr = System.Attribute.GetCustomAttribute(prop,
				typeof(EntityBindingAttribute), inherit) as EntityBindingAttribute;
			var getter = GetGetDelegate(prop);
			return new BindingMetaData(attr, getter);
		}

		/// <summary>
		/// 値を取得するデリゲートを作成します。
		/// </summary>
		/// <param name="info">メンバー情報</param>
		/// <returns>値取得デリゲート</returns>
		private static Delegate GetGetDelegate(MemberInfo info) {
			var pInfo = info as PropertyInfo;
			if (pInfo != null) {
				var funcType = typeof(Func<,>).MakeGenericType(pInfo.DeclaringType, pInfo.PropertyType);
				return Delegate.CreateDelegate(funcType, pInfo.GetGetMethod());
			}

			var mInfo = info as FieldInfo;
			if (mInfo != null) {
				return new Func<object, object>(m => mInfo.GetValue(m));
			}

			throw new ArgumentException("Cannot get public property info: " + info);
		}

		/// <summary>
		/// インスタンスを初期化します。
		/// </summary>
		/// <param name="attr">メタ情報</param>
		/// <param name="getter">値取得デリゲート</param>
		private BindingMetaData(EntityBindingAttribute attr, Delegate getter) {
			Attribute = attr;
			Getter = getter;
		}
	}
}
