// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Collections.Generic;
using System.Data.Common;

namespace Net.Rimakiri.Data.Util {
	/// <summary>
	/// <see cref="DbDataReader"/>のユーティリティクラスです。
	/// </summary>
	public static class DbDataReaderExtension {
		/// <summary>
		/// 指定したカラムの<see cref="string"/>値を取得します。
		/// </summary>
		/// <param name="reader">対象<see cref="DbDataReader"/></param>
		/// <param name="colName">カラム名</param>
		/// <returns>カラムの値</returns>
		public static string GetString(this DbDataReader reader, string colName) {
			var ord = reader.GetOrdinal(colName);
			return reader.IsDBNull(ord) ? null : reader.GetString(ord);
		}

		/// <summary>
		/// 指定したカラムの<see cref="short"/>値を取得します。
		/// </summary>
		/// <param name="reader">対象<see cref="DbDataReader"/></param>
		/// <param name="colName">カラム名</param>
		/// <returns>カラムの値</returns>
		public static short? GetShort(this DbDataReader reader, string colName) {
			var ord = reader.GetOrdinal(colName);
			return reader.IsDBNull(ord)? (short?)null: reader.GetInt16(ord);
		}

		/// <summary>
		/// 指定したカラムの<see cref="long"/>値を取得します。
		/// </summary>
		/// <param name="reader">対象<see cref="DbDataReader"/></param>
		/// <param name="colName">カラム名</param>
		/// <returns>カラムの値</returns>
		public static long? GetLong(this DbDataReader reader, string colName) {
			var ord = reader.GetOrdinal(colName);
			return reader.IsDBNull(ord)? (long?)null: reader.GetInt64(ord);
		}

		/// <summary>
		/// 指定したカラムの<see cref="int"/>値を取得します。
		/// </summary>
		/// <param name="reader">対象<see cref="DbDataReader"/></param>
		/// <param name="colName">カラム名</param>
		/// <returns>カラムの値</returns>
		public static int? GetInteger(this DbDataReader reader, string colName) {
			var ord = reader.GetOrdinal(colName);
			return reader.IsDBNull(ord)? (int?)null: reader.GetInt32(ord);
		}

		/// <summary>
		/// 指定したカラムの<see cref="DateTime"/>値を取得します。
		/// </summary>
		/// <param name="reader">対象<see cref="DbDataReader"/></param>
		/// <param name="colName">カラム名</param>
		/// <returns>カラムの値</returns>
		public static DateTime? GetDateTime(this DbDataReader reader, string colName) {
			var ord = reader.GetOrdinal(colName);
			return reader.IsDBNull(ord) ? (DateTime?)null : reader.GetDateTime(ord);
		}

		/// <summary>
		/// 指定したカラムの<see cref="decimal"/>値を取得します。
		/// </summary>
		/// <param name="reader">対象<see cref="DbDataReader"/></param>
		/// <param name="colName">カラム名</param>
		/// <returns>カラムの値</returns>
		public static decimal? GetDecimal(this DbDataReader reader, string colName) {
			var ord = reader.GetOrdinal(colName);
			return reader.IsDBNull(ord) ? (decimal?)null : reader.GetDecimal(ord);
		}

		/// <summary>
		/// 指定したカラムの<see cref="double"/>値を取得します。
		/// </summary>
		/// <param name="reader">対象<see cref="DbDataReader"/></param>
		/// <param name="colName">カラム名</param>
		/// <returns>カラムの値</returns>
		public static double? GetDouble(this DbDataReader reader, string colName) {
			var ord = reader.GetOrdinal(colName);
			return reader.IsDBNull(ord) ? (double?)null : reader.GetDouble(ord);
		}

		/// <summary>
		/// 指定したカラムの<see cref="float"/>値を取得します。
		/// </summary>
		/// <param name="reader">対象<see cref="DbDataReader"/></param>
		/// <param name="colName">カラム名</param>
		/// <returns>カラムの値</returns>
		public static float? GetFloat(this DbDataReader reader, string colName) {
			var ord = reader.GetOrdinal(colName);
			return reader.IsDBNull(ord) ? (float?)null : reader.GetFloat(ord);
		}

		/// <summary>
		/// <see cref="DbDataReader"/>の結果をすべて指定のモデルに
		/// マッピングします。
		/// </summary>
		/// <typeparam name="T">モデルのタイプ</typeparam>
		/// <param name="reader">データ読取り元<see cref="DbDataReader"/></param>
		/// <param name="mapper">マッピングを行う<see cref="DbDataReaderMapper{T}"/></param>
		/// <returns>モデルインスタンス</returns>
		public static IEnumerable<T> MapBy<T>(this DbDataReader reader, DbDataReaderMapper<T> mapper)
			where T : new() {
			while (reader.Read()) {
				yield return mapper.Map(reader);
			}
		}
	}
}
