// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Linq;
using System.Linq.Expressions;
using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data.Util {
	/// <summary>
	/// モデルの属性に従って<see cref="IBindValue"/>を生成するクラスです。
	/// </summary>
	/// <remarks>
	/// 非常に強力ですが、ラムダのコンパイルを行うため、パフォーマンス上の
	/// ドローバックが存在します。
	/// </remarks>
	/// <seealso cref="EntityBindingAttribute"/>
	internal class DynamicModelBindingImpl<TModel> : IModelBinding<TModel> {
		private readonly TModel model;
		private readonly bool inherit;

		/// <summary>
		/// モデルを指定してインスタンスを初期化します。
		/// </summary>
		/// <param name="model">対象モデル</param>
		/// <param name="inherit">祖先も属性検索する場合True</param>
		public DynamicModelBindingImpl(TModel model, bool inherit) {
			this.model = model;
			this.inherit = inherit;
		}

		/// <inheritdoc />
		public IBindValue Create<TValue>(string identity,
				Expression<Func<TModel, TValue>> propertyLambda) {
			var id = identity;
			var value = GetValueAndMetaData(model, propertyLambda, ref id, out var size,
							out var precision, out var scale);
			return BindValue.Create(id, value, size, precision, scale);
		}

		/// <inheritdoc />
		public IBindValue WithFormat(string identity, string format,
					Expression<Func<TModel, string>> propertyLambda, char? escape = null) {
			var id = identity;
			var value = GetValueAndMetaData(model, propertyLambda, ref id, out var size,
							out _, out _);
			return (escape == null) ? BindValue.WithFormat(id, format, value, size)
					: BindValue.WithFormat(id, format, value, escape.Value, size);
		}

		/// <inheritdoc />
		public IBindValue WithFormat<TValue>(string identity, string format,
					Expression<Func<TModel, TValue>> propertyLambda, char? escape = null)
						where TValue : IStringDataWrapper {
			var id = identity;
			var value = GetValueAndMetaData(model, propertyLambda, ref id, out var size,
							out _, out _);
			return (escape == null) ? BindValue.WithFormat(id, format, value, size)
					: BindValue.WithFormat(id, format, value, escape.Value, size);
		}

		/// <inheritdoc />
		public IBindValue Create<TValue>(Expression<Func<TModel, TValue>> propertyLambda)
				=> Create(null, propertyLambda);

		/// <inheritdoc />
		public IBindValue WithFormat(string format,
					Expression<Func<TModel, string>> propertyLambda, char? escape = null)
				=> WithFormat(null, format, propertyLambda, escape);

		/// <inheritdoc />
		public IBindValue WithFormat<TValue>(string format,
					Expression<Func<TModel, TValue>> propertyLambda, char? escape = null)
						where TValue : IStringDataWrapper
			=> WithFormat(null, format, propertyLambda, escape);
		
		private TValue GetValueAndMetaData<TObject, TValue>(TObject target,
							Expression<Func<TObject, TValue>> propertyLambda,
								ref string identity, out int size, out byte precision, out byte scale) {
			size = -1;
			precision = 0;
			scale = 0;

			var func = propertyLambda.Body as Func<TObject, TValue>;
			var value = func != null ? func(target)
						: propertyLambda.Compile().Invoke(target);
			var attr = GetBindingAttribute(propertyLambda);
			if (attr != null) {
				size = attr.Size;
				precision = attr.Precision;
				scale = attr.Scale;
				if (identity == null) {
					identity = attr.Identity;
				}
			}

			return value;
		}

		private EntityBindingAttribute GetBindingAttribute<TObject, TValue>(
						Expression<Func<TObject, TValue>> property) {
			switch (property.Body) {
			case MemberExpression memberExp:
				return Attribute.GetCustomAttributes(memberExp.Member, typeof(EntityBindingAttribute), inherit)
						.FirstOrDefault() as EntityBindingAttribute;
			case MethodCallExpression methodCallExpression:
				return methodCallExpression.Method.ReturnTypeCustomAttributes.GetCustomAttributes(
						typeof(EntityBindingAttribute), true).FirstOrDefault() as EntityBindingAttribute;
			}

			return null;
		}
	}

	/// <summary>
	/// モデルの属性に従って<see cref="IBindValue"/>を生成するクラスです。
	/// </summary>
	/// <remarks>
	/// 非常に強力ですが、ラムダのコンパイルを行うため、パフォーマンス上の
	/// ドローバックが存在します。
	/// Webサーバ処理など、定型処理を行う場合は可能な限り
	/// <see cref="ModelBindingFactory{TModel}"/>を使用してください。
	/// </remarks>
	/// <seealso cref="EntityBindingAttribute"/>
	/// <seealso cref="BindValue"/>
	/// <seealso cref="ModelBindingFactory{TModel}"/>
	public static class DynamicModelBinding {
		/// <summary>
		/// 属性の情報をもとに<see cref="IBindValue"/>インスタンスを生成します。
		/// </summary>
		/// <typeparam name="TValue">属性のタイプ</typeparam>
		/// <param name="identity">パラメタの識別子</param>
		/// <param name="funcLambda">値取得ラムダ</param>
		/// <returns>生成した<see cref="IBindValue"/>インスタンス</returns>
		/// <seealso cref="BindValue.Create{T}"/>
		/// <remarks>識別子は属性で指定されているものよりもパラメタで指定されたものが優先されます</remarks>
		public static IBindValue Create<TValue>(string identity,
						Expression<Func<TValue>> funcLambda) {
			var id = identity;
			var value = GetValueAndMetaData(funcLambda, ref id, out var size,
							out var precision, out var scale);
			return BindValue.Create(id, value, size, precision, scale);
		}

		/// <summary>
		/// 属性の情報をもとに<see cref="IBindValue"/>インスタンスを生成します。
		/// </summary>
		/// <param name="identity">パラメタの識別子</param>
		/// <param name="funcLambda">値取得ラムダ</param>
		/// <param name="format">フォーマット</param>
		/// <param name="escape">エスケープ文字</param>
		/// <returns>生成した<see cref="IBindValue"/>インスタンス</returns>
		/// <seealso cref="BindValue.WithFormat(string,string,string,char,int)"/>
		/// <remarks>識別子は属性で指定されているものよりもパラメタで指定されたものが優先されます</remarks>
		public static IBindValue WithFormat(string identity, string format,
					Expression<Func<string>> funcLambda, char? escape = null) {
			var id = identity;
			var value = GetValueAndMetaData(funcLambda, ref id, out var size,
							out _, out _);
			return (escape == null) ? BindValue.WithFormat(id, format, value, size)
					: BindValue.WithFormat(id, format, value, escape.Value, size);
		}

		/// <summary>
		/// 属性の情報をもとに<see cref="IBindValue"/>インスタンスを生成します。
		/// </summary>
		/// <typeparam name="TValue">文字列ラッパーのタイプ</typeparam>
		/// <param name="identity">パラメタの識別子</param>
		/// <param name="funcLambda">値取得ラムダ</param>
		/// <param name="format">フォーマット</param>
		/// <param name="escape">エスケープ文字</param>
		/// <returns>生成した<see cref="IBindValue"/>インスタンス</returns>
		/// <seealso cref="BindValue.WithFormat{T}(string,string,T,char,int)"/>
		/// <remarks>識別子は属性で指定されているものよりもパラメタで指定されたものが優先されます</remarks>
		public static IBindValue WithFormat<TValue>(string identity, string format,
						Expression<Func<TValue>> funcLambda, char? escape = null)
						where TValue : IStringDataWrapper {
			var id = identity;
			var value = GetValueAndMetaData(funcLambda, ref id, out var size,
							out _, out _);
			return (escape == null) ? BindValue.WithFormat(id, format, value, size)
					: BindValue.WithFormat(id, format, value, escape.Value, size);
		}

		/// <summary>
		/// 属性の情報をもとに<see cref="IBindValue"/>インスタンスを生成します。
		/// </summary>
		/// <typeparam name="TValue">属性のタイプ</typeparam>
		/// <param name="funcLambda">値取得ラムダ</param>
		/// <returns>生成した<see cref="IBindValue"/>インスタンス</returns>
		/// <remarks>識別子は属性から取得します。</remarks>
		/// <seealso cref="BindValue.Create{T}"/>
		public static IBindValue Create<TValue>(Expression<Func<TValue>> funcLambda)
				=> Create(null, funcLambda);

		/// <summary>
		/// 属性の情報をもとに<see cref="IBindValue"/>インスタンスを生成します。
		/// </summary>
		/// <param name="funcLambda">値取得ラムダ</param>
		/// <param name="format">フォーマット</param>
		/// <param name="escape">エスケープ文字</param>
		/// <returns>生成した<see cref="IBindValue"/>インスタンス</returns>
		/// <remarks>識別子は属性から取得します。</remarks>
		/// <seealso cref="BindValue.WithFormat(string,string,string,char,int)"/>
		public static IBindValue WithFormat(string format,
					Expression<Func<string>> funcLambda, char? escape = null)
				=> WithFormat(null, format, funcLambda, escape);

		/// <summary>
		/// 属性の情報をもとに<see cref="IBindValue"/>インスタンスを生成します。
		/// </summary>
		/// <typeparam name="TValue">属性のタイプ</typeparam>
		/// <param name="funcLambda">値取得ラムダ</param>
		/// <param name="format">フォーマット</param>
		/// <param name="escape">エスケープ文字</param>
		/// <returns>生成した<see cref="IBindValue"/>インスタンス</returns>
		/// <remarks>識別子は属性から取得します。</remarks>
		/// <seealso cref="BindValue.WithFormat{T}(string,string,T,char,int)"/>
		public static IBindValue WithFormat<TValue>(string format,
						Expression<Func<TValue>> funcLambda, char? escape = null)
						where TValue : IStringDataWrapper
				=> WithFormat(null, format, funcLambda, escape);
		
		private static TValue GetValueAndMetaData<TValue>(Expression<Func<TValue>> fieldFunc,
								ref string identity, out int size, out byte precision, out byte scale) {
			size = -1;
			precision = 0;
			scale = 0;

			var func = fieldFunc.Body as Func<TValue>;
			var value = func != null ? func()
							: fieldFunc.Compile().Invoke();
			var attr = GetBindingAttribute(fieldFunc);
			if (attr != null) {
				size = attr.Size;
				precision = attr.Precision;
				scale = attr.Scale;
				if (identity == null) {
					identity = attr.Identity;
				}
			}

			return value;
		}


		private static EntityBindingAttribute GetBindingAttribute<TValue>(
								Expression<Func<TValue>> lambda) {
			switch (lambda.Body) {
			case MemberExpression memberExp:
				return Attribute.GetCustomAttributes(memberExp.Member, typeof(EntityBindingAttribute))
						.FirstOrDefault() as EntityBindingAttribute;
			case MethodCallExpression methodCallExpression:
				return methodCallExpression.Method.ReturnTypeCustomAttributes.GetCustomAttributes(
						typeof(EntityBindingAttribute), true).FirstOrDefault() as EntityBindingAttribute;
			}

			return null;
		}
	}
}
