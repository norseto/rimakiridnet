// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Net.Rimakiri.Data.Util {
	/// <inheritdoc />
	/// <summary>
	/// マッピングする際のカラム情報を指定する属性クラスです。
	/// </summary>
	/// <seealso cref="DbDataReaderMapper{TModel}"/>
	[AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
	public class EntityMappingAttribute : Attribute {
		/// <summary>
		/// カラム名を設定または取得します。
		/// </summary>
		public string CName { get; set; }

		/// <summary>
		/// カラム名グループのキーを設定または取得します。
		/// </summary>
		public int GroupKey { get; set; }
	}

	/// <summary>
	/// <see cref="DbDataReader"/>のレコードの内容をモデルのクラスの
	/// プロパティにマップするユーティリティクラスです。
	/// </summary>
	/// <typeparam name="TModel">モデルのタイプ</typeparam>
	/// <remarks>
	/// 以下のように
	/// <see>
	/// <cref>MapTo{TValue}(string, Expression{Func{TModel,TValue}})</cref>
	/// </see>
	/// メソッドで、カラム名と設定先プロパティ（ラムダ式）を指定して
	/// マッピングを追加します。 その後、<see cref="Map(DbDataReader)"/>
	/// でプロパティが設定された モデルを生成するか、
	/// <see cref="Map(DbDataReader,TModel)"/>で
	/// 既存のモデルインスタンスにプロパティとして結果を設定します。
	/// <example>
	/// <code language="C#">
	///	public class TypeTestModel {
	///		public int Id { get; set; }
	///		public int? intType { get; set; }
	///		public short? shortType { get; set; }
	///		public long? longType {get; set; }
	///		public string stringType { get;set; }
	///		public float? floatType { get; set; }
	///		public double? doubleType { get; set; }
	///		public DateTime? dateTmeType { get; set; }
	///		public decimal? decimalType { get;set; }
	///	}
	/// 
	///	protected override void DoInReader(DbDataReader reader) {
	///		var mapper = new DbDataReaderMapper&lt;TypeTestModel&gt;();
	///		mapper.MapTo("ID", m => m.Id);
	///		mapper.MapTo("INT_TYPE", m => m.intType);
	///		mapper.MapTo("SHORT_TYPE", m => m.shortType);
	///		mapper.MapTo("LONG_TYPE", m => m.longType);
	///		mapper.MapTo("STRING_TYPE", m => m.stringType);
	///		mapper.MapTo("FLOAT_TYPE", m => m.floatType);
	///		mapper.MapTo("DOUBLE_TYPE", m => m.doubleType);
	///		mapper.MapTo("DATETIME_TYPE", m => m.dateTmeType);
	///		mapper.MapTo("DECIMAL_TYPE", m => m.decimalType);
	///
	///		while (reader.Read()) {
	///			var model = mapper.Map(reader);
	///			results.Add(model);
	///		}
	///	}
	/// </code>
	/// </example>
	/// <see cref="EntityBindingAttribute"/>属性を指定することで名称
	/// を省略可能です。先の例を属性を使用した方法にした場合、以下のよう
	/// になります。
	/// <example>
	/// <code language="C#">
	///	public class TypeTestModel {
	///     [EntityMapping(CName = "ID")]
	///		public int Id { get; set; }
	///     [EntityMapping(CName = "INT_TYPE")]
	///		public int? intType { get; set; }
	///     [EntityMapping(CName = "SHORT_TYPE")]
	///		public short? shortType { get; set; }
	///     [EntityMapping(CName = "LONG_TYPE")]
	///		public long? longType {get; set; }
	///     [EntityMapping(CName = "STRING_TYPE")]
	///		public string stringType { get;set; }
	///     [EntityMapping(CName = "FLOAT_TYPE")]
	///		public float? floatType { get; set; }
	///     [EntityMapping(CName = "DOUBLE_TYPE")]
	///		public double? doubleType { get; set; }
	///     [EntityMapping(CName = "DATETIME_TYPE")]
	///		public DateTime? dateTmeType { get; set; }
	///     [EntityMapping(CName = "DECIMAL_TYPE")]
	///		public decimal? decimalType { get;set; }
	///	}
	/// 
	///	protected override void DoInReader(DbDataReader reader) {
	///		var mapper = new DbDataReaderMapper&lt;TypeTestModel&gt;();
	///		mapper.MapTo(m => m.Id);
	///		mapper.MapTo(m => m.intType);
	///		mapper.MapTo(m => m.shortType);
	///		mapper.MapTo(m => m.longType);
	///		mapper.MapTo(m => m.stringType);
	///		mapper.MapTo(m => m.floatType);
	///		mapper.MapTo(m => m.doubleType);
	///		mapper.MapTo(m => m.dateTmeType);
	///		mapper.MapTo(m => m.decimalType);
	///
	///		while (reader.Read()) {
	///			var model = mapper.Map(reader);
	///			results.Add(model);
	///		}
	///	}
	/// </code>
	/// </example>
	/// </remarks>
	public class DbDataReaderMapper<TModel> where TModel: new() {
		private readonly List<Action<DbDataReader, TModel>> actions
					= new List<Action<DbDataReader, TModel>>();
		
		private bool unmodifiable;
		private int groupKey;

		private void CheckUnmodifiable() {
			if (unmodifiable) {
				throw new InvalidOperationException("Not modifiable.");
			}
		}

		/// <summary>
		/// Mark unmodifiable.
		/// </summary>
		/// <returns>This instance.</returns>
		public DbDataReaderMapper<TModel> SetUnmodifiable() {
			unmodifiable = true;
			return this;
		}

		/// <summary>
		/// <see cref="EntityMappingAttribute"/>からカラム名を取得する際の
		/// <see cref="EntityMappingAttribute.GroupKey"/>を設定または取得します。
		/// </summary>
		public int GroupKey {
			get => groupKey;
			set {
				CheckUnmodifiable();
				groupKey = value;
			}
		}

		/// <summary>
		/// モデルを生成して結果のマッピングを行い、生成したモデルを返却します。
		/// </summary>
		/// <param name="r">マッピング元の<see cref="DbDataReader"/></param>
		/// <returns>生成したモデル</returns>
		public TModel Map(DbDataReader r) {
			return Map(r, new TModel());
		}

		/// <summary>
		/// 指定のモデルに対して結果のマッピングを行います。
		/// </summary>
		/// <param name="r">マッピング元の<see cref="DbDataReader"/></param>
		/// <param name="model">マッピング先のモデル</param>
		/// <returns>マッピング先のモデル</returns>
		public TModel Map(DbDataReader r, TModel model) {
			foreach (var action in actions) {
				action(r, model);
			}
			return model;
		}

		/// <summary>
		/// マッピング定義を追加します。<paramref name="name"/>で指定されたカラムの
		/// 内容を、<paramref name="propertyLambda"/>で指定されたプロパティへ設定
		/// するようマッピング定義を追加します。
		/// </summary>
		/// <typeparam name="TValue">プロパティのタイプ</typeparam>
		/// <param name="name"><see cref="DbDataReader"/>のカラム名</param>
		/// <param name="propertyLambda">プロパティのラムダ式</param>
		/// <returns>このインスタンス</returns>
		public DbDataReaderMapper<TModel> MapTo<TValue>(string name,
						Expression<Func<TModel, TValue>> propertyLambda) {
			CheckUnmodifiable();
			var colName = name;
			if (colName == null) {
				var attr = GetBindingAttribute(propertyLambda);
				if (attr == null || (colName = attr.CName) == null) {
					throw new ArgumentNullException(nameof(name));
				}
			}

			var setter = GetSetter(propertyLambda);
			var reader = GetReader<TValue>(colName);
			actions.Add((r, m) => setter(m, reader(r)));
			return this;
		}

		/// <summary>
		/// <typeparamref name="TModel"/>で指定されている<see cref="EntityMappingAttribute"/>
		/// 属性で設定されているマッピング定義を追加します。
		/// </summary>
		/// <returns>このインスタンス</returns>
		public DbDataReaderMapper<TModel> ApplyMapTo() {
			CheckUnmodifiable();
			var key = GroupKey;
			Func<PropertyInfo, EntityMappingAttribute> getCustom =
					p => Attribute.GetCustomAttribute(p, typeof(EntityMappingAttribute))
							as EntityMappingAttribute;
			var targets = typeof(TModel).GetProperties()
					.Select(p => new { prop = p, attr = getCustom(p) })
					.Where(t => t.attr != null && t.attr.GroupKey == key);

			var rmethod = GetType().GetMethod(nameof(GetReader),
								BindingFlags.NonPublic | BindingFlags.Instance);
			Debug.Assert(rmethod != null, "rmethod != null");

			targets.ToList().ForEach(
				(t) => {
					var valueType = t.prop.PropertyType;
					var mInfo = rmethod.MakeGenericMethod(valueType);
					var reader = (Delegate)mInfo.Invoke(this, new object[]{ t.attr.CName });
					var actionType = typeof(Action<,>).MakeGenericType(typeof(TModel), valueType);
					var setter = Delegate.CreateDelegate(actionType, t.prop.GetSetMethod());
					actions.Add((r, m) => setter.DynamicInvoke(m, reader.DynamicInvoke(r)));
				});

			return this;
		}

		/// <summary>
		/// マッピング定義を追加します。<paramref name="propertyLambda"/>で指定
		/// されたプロパティへ設定するようマッピング定義を追加します。
		/// カラム名はそのプロパティの<see cref="EntityMappingAttribute"/>で指定
		/// された内容が使用されます。
		/// </summary>
		/// <typeparam name="TValue">プロパティのタイプ</typeparam>
		/// <param name="propertyLambda">プロパティのラムダ式</param>
		/// <returns>このインスタンス</returns>
		public DbDataReaderMapper<TModel> MapTo<TValue>(
						Expression<Func<TModel, TValue>> propertyLambda) {
			return MapTo(null, propertyLambda);
		}

		/// <summary>
		/// Convert object to specifiee type. Override this if you want to
		/// implement special data convertion.
		/// </summary>
		/// <typeparam name="TValue">Real object type.</typeparam>
		/// <param name="source">source object</param>
		/// <param name="name">column name of source object</param>
		/// <returns>converted object</returns>
		public virtual TValue ConvertTo<TValue>(object source, string name) {
			if (source is IConvertible) {
				return (TValue)Convert.ChangeType(source, typeof(TValue));
			}
			var tc = TypeDescriptor.GetConverter(typeof(TValue));
			return (TValue)tc.ConvertFrom(source);
		}
		
		private Action<TObject,TValue> GetSetter<TObject,TValue>(
						Expression<Func<TObject,TValue>> property) {
			var memberExp = (MemberExpression)property.Body;
			var propInfo = (PropertyInfo)memberExp.Member;
			var setter = propInfo.GetSetMethod();
			var del = Delegate.CreateDelegate(typeof(Action<TObject,TValue>), setter);
			return (Action<TObject,TValue>)del;
		}

		private Func<DbDataReader, TValue> GetReader<TValue>(string name) {
			return (r) => {
				var idx = r.GetOrdinal(name);
				if (r.IsDBNull(idx)) {
					return default(TValue);
				}
				var o = r.GetValue(idx);
				if (o is TValue value) {
					return value;
				}

				return ConvertTo<TValue>(o, name);
			};
		}

		private EntityMappingAttribute GetBindingAttribute<TObject, TValue>(
						Expression<Func<TObject, TValue>> property) {
			if (property.Body is MemberExpression memberExp) {
				return Attribute.GetCustomAttributes(memberExp.Member, typeof(EntityMappingAttribute))
						.OfType<EntityMappingAttribute>().FirstOrDefault(a => a.GroupKey == GroupKey);
			}

			return null;
		}
	}
}

