// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Data;
using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data.Util {
	/// <summary>
	/// 何らかのタイプマップをもとにデータベース型を決定する<see cref="IDatabaseDialect"/>
	/// の抽象基底クラスです。
	/// </summary>
	/// <typeparam name="T">データベースの型のタイプ</typeparam>
	public abstract class TypeMapDatabaseDialect<T> : IDatabaseDialect {
		private readonly IDatabaseTypeMap<T> defaultTypeMap;
		private IDatabaseTypeMap<T> typeMap;
		private IBoundValueVerifier boundValueVerifier;

		/// <summary>
		/// タイプマップを取得します。
		/// </summary>
		public IDatabaseTypeMap<T> TypeMap {
			get => typeMap ?? defaultTypeMap;
			set => typeMap = value;
		}

		/// <summary>
		/// デフォルトのタイプマップを指定してインスタンスを生成します。
		/// </summary>
		/// <param name="typeMap">デフォルトのタイプマップ</param>
		protected TypeMapDatabaseDialect(IDatabaseTypeMap<T> typeMap) {
			defaultTypeMap = typeMap;
		}

		#region Implementation of IDatabaseDialect
		/// <inheritdoc />
		public ShortSizePolicy ShortSizePolicy { get; set; }

		/// <inheritdoc />
		public IBoundValueVerifier BoundValueVerifier {
			get => boundValueVerifier ?? BoundValueVerifierFactory.GetBoundValueVerifier(ShortSizePolicy);
			set => boundValueVerifier = value;
		}

		/// <inheritdoc />
		public string NameFor(string identifier) {
			return ParameterPrefix + identifier;
		}

		/// <inheritdoc />
		public abstract string CurrentTimestampLiteral { get; }

		/// <inheritdoc />
		public abstract string CurrentUtcTimestampLiteral { get; }

		/// <inheritdoc />
		public abstract string ParameterPrefix { get; }

		/// <inheritdoc />
		public abstract string DummyTable { get; }

		/// <inheritdoc />
		public abstract string DummyTableWithFrom { get; }

		/// <inheritdoc />
		public abstract void SetupParameter(IDbDataParameter target, IBindValue value);
		#endregion
	}
}
