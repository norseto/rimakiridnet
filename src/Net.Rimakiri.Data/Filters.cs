// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data {
	/// <summary>
	/// <see cref="IFragment"/>をフィルタするためのデリゲートを定義したクラスです。
	/// </summary>
	/// <seealso cref="IBindingSupport{T}.ExcludeFilter"/>
	public static class Filters {
		/// <summary>
		/// 値がNULLのものを抽出するための<see cref="FragmentFilter"/>です。
		/// </summary>
		/// <remarks>
		/// 以下の内容のデリゲートです。
		/// <example>
		/// <code language="C#">
		/// v => v.Value == null || v.Value == DBNull.Value
		/// </code>
		/// <code language="VB">
		/// Function(v) v.Value Is Nothing OrElse v.Value = DBNull.Value
		/// </code>
		/// </example>
		/// </remarks>
		public static readonly FragmentFilter NotNull = v => v.Value == null || v.Value == DBNull.Value;

		/// <summary>
		/// 値がNULLまたは空文字列のものを抽出するための<see cref="FragmentFilter"/>です。
		/// </summary>
		/// <remarks>
		/// 以下の内容のデリゲートです。
		/// <example>
		/// <code language="C#">
		/// v => v.Value == null || v.Value == DBNull.Value || string.Empty.Equals(v.Value)
		/// </code>
		/// <code language="VB">
		/// Function(v) v.Value Is Nothing OrElse v.Value = DBNull.Value || String.Empty.Equals(v.Value)
		/// </code>
		/// </example>
		/// </remarks>
		public static readonly FragmentFilter NotNullOrEmpty = v => NotNull(v) || string.Empty.Equals(v.Value);
	}
}
