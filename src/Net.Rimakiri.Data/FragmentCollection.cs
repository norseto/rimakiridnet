// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Collections.Generic;
using System.Linq;
using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data {
	/// <summary>
	/// 複数の<see cref="IFragment"/>を子要素としてもつクラスです。
	/// </summary>
	/// <inheritdoc />
	public class FragmentCollection : IFragment {
		private readonly List<IFragment> children = new List<IFragment>();

		/// <summary>
		/// 子要素の数を取得します。
		/// </summary>
		public int Count => children.Count;

		/// <summary>
		/// 有効な子要素を取得します。
		/// </summary>
		protected IEnumerable<IFragment> Children {
			get { return children.Where(c => c.Enabled); }
		}

		/// <summary>
		/// セパレータを設定または取得します。
		/// </summary>
		protected string Separator { get; set; }

		/// <summary>
		/// スペースをセパレータとしてインスタンスを初期化します。
		/// </summary>
		public FragmentCollection() {
			Separator = " ";
		}

		/// <summary>
		/// <see cref="IFragment"/>要素を追加します。
		/// </summary>
		/// <param name="fragments">追加する要素</param>
		/// <returns>このインスタンス</returns>
		public FragmentCollection Append(params IFragment[] fragments) {
			return fragments == null ? this : Append(fragments.AsEnumerable());
		}


		/// <summary>
		/// <see cref="IFragment"/>要素を追加します。
		/// </summary>
		/// <param name="fragments">追加する要素</param>
		/// <returns>このインスタンス</returns>
		public FragmentCollection Append(IEnumerable<IFragment> fragments) {
			if (fragments == null) {
				return this;
			}
			children.AddRange(fragments.Where(c => c != null));
			return this;
		}

		#region Implementation of IFragment
		/// <inheritdoc />
		public bool Enabled => Children.FirstOrDefault() != null;

		/// <inheritdoc />
		public string ToLiteralString(IFragmentContext ctx) {
			if (!Enabled) {
				return null;
			}
			var result = string.Join(Separator, GetAllTexts(ctx));
			return string.IsNullOrEmpty(result) ? null : result;
		}

		/// <inheritdoc />
		public IEnumerable<IFragment> ToEnumerable() {
			return children;
		}
		#endregion

		private IEnumerable<string> GetAllTexts(IFragmentContext ctx) {
			foreach (var child in Children) {
				var text = child.ToLiteralString(ctx);
				if (!string.IsNullOrEmpty(text)) {
					yield return text;
				}
			}
		}
	}
}
