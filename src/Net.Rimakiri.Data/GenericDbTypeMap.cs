// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Net.Rimakiri.Data.Interfaces;
using Net.Rimakiri.Data.TypeWrappers;

namespace Net.Rimakiri.Data {
	/// <summary>
	/// タイプの継承関係をもとに値を取得するクラスです。
	/// </summary>
	/// <typeparam name="T">格納する値のタイプ</typeparam>
	public class AssignableTypeMap<T> {
		private readonly List<KeyValuePair<Type,T>> list = new List<KeyValuePair<Type,T>>();

		/// <summary>
		/// タイプと値を追加します。
		/// </summary>
		/// <param name="type">タイプ</param>
		/// <param name="value">値</param>
		private void Add(Type type, T value) {
			list.Add(new KeyValuePair<Type, T>(type, value));
		}

		/// <summary>
		/// 指定のタイプに対応する値を設定します。
		/// </summary>
		/// <param name="type">値を設定するタイプ</param>
		/// <returns>タイプに対応した値</returns>
		public T this[Type type] {
			set => Add(type, value);
		}

		/// <summary>
		/// 値を取得します。
		/// </summary>
		/// <param name="type">検索するタイプ</param>
		/// <param name="value">値</param>
		/// <returns>存在した場合True</returns>
		/// <remarks>
		/// <see cref="this"/>で設定したタイプのうち、<paramref name="type"/>
		/// で指定したタイプに設定できるものを検索し、存在した場合に値を
		/// <paramref name="value"/>に設定します。
		/// </remarks>
		public bool TryGet(Type type, out T value) {
			var item = list.FirstOrDefault(p => p.Key.IsAssignableFrom(type));
			value = item.Value;
			return item.Key != null;
		}
	}

	/// <inheritdoc />
	/// <summary>
	/// デフォルトの<see cref="IDatabaseTypeMap{DbType}"/>の実装クラスです。
	/// バイナリなどは未対応です。また、文字列はStringとして扱います。
	/// </summary>
	public class GenericDbTypeMap : IDatabaseTypeMap<DbType> {
		private readonly AssignableTypeMap<DbType> wrapperTypes = new AssignableTypeMap<DbType>();

		/// <summary>
		/// インスタンスを初期化します。
		/// </summary>
		public GenericDbTypeMap() {
			wrapperTypes[typeof(CharType)] = DbType.String;
			wrapperTypes[typeof(NCharType)] = DbType.String;
			wrapperTypes[typeof(VarCharType)] = DbType.String;
			wrapperTypes[typeof(NVarCharType)] = DbType.String;
			wrapperTypes[typeof(DateType)] = DbType.Date;
			wrapperTypes[typeof(TimestampType)] = DbType.DateTime;
			wrapperTypes[typeof(INumericWrapper)] = DbType.Decimal;
		}

		/// <inheritdoc />
		public virtual DbType GetDatabaseType(string parameterName, IBindValue value) {
			var type = value.BindMetaData.DataType;

			if (typeof(IDataWrapper).IsAssignableFrom(type)) {
				if (wrapperTypes.TryGet(type, out var ret)) {
					return ret;
				}
			}

			if (type == typeof(string)) {
				return DbType.String;
			}

			switch (Type.GetTypeCode(type)) {
			case TypeCode.Byte:
				return DbType.Byte;
			case TypeCode.SByte:
				return DbType.SByte;
			case TypeCode.Int16:
				return DbType.Int16;
			case TypeCode.UInt16:
				return DbType.UInt16;
			case TypeCode.Int32:
				return DbType.Int32;
			case TypeCode.UInt32:
				return DbType.UInt32;
			case TypeCode.Int64:
				return DbType.Int64;
			case TypeCode.UInt64:
				return DbType.UInt64;
			case TypeCode.Decimal:
				return DbType.Decimal;
			case TypeCode.Double:
				return DbType.Double;
			case TypeCode.Single:
				return DbType.Single;
			}

			if (type == typeof(char[])) {
				return DbType.String;
			}
			if (type == typeof(DateTime)) {
				return DbType.DateTime;
			}

			throw new InvalidOperationException($"Unsupported type: {type}");
		}
	}
}
