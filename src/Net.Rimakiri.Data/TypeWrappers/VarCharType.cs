// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data.TypeWrappers {
	/// <summary>
	/// データベースのVARCHAR（固定長文字列）型を表すクラスです。
	/// </summary>
	/// <remarks>
	/// <see cref="IDatabaseDialect"/>により通常は、<see cref="string"/>はデータベース
	/// のNVARCHARまたはVARCHARにマッピングされますが、特に指定したい場合に使用します。
	/// なお、マッピング挙動については、カスタムの<see cref="IDatabaseDialect"/>により、
	/// <see cref="VarCharType"/>であっても、NVARCHARにマッピングされる場合も
	/// ありえます。
	/// </remarks>
	/// <inheritdoc />
	/// <remarks>
	/// 継承して長さを固定化したクラスを作成すると、毎回長さを指定する必要がなくなります。
	/// </remarks>
	/// <seealso cref="NVarCharType"/>
	public class VarCharType : StringDataWrapper {
		/// <summary>
		/// 値を指定してインスタンスを生成します。
		/// </summary>
		/// <param name="value">値</param>
		/// <param name="size">長さ</param>
		/// <inheritdoc />
		public VarCharType(string value, int size = 0) : base(value, size) {
		}

		#region Overrides of StringDataWrapper
		/// <inheritdoc />
		public override object FromString(string value) {
			return new VarCharType(value);
		}
		#endregion
	}
}
