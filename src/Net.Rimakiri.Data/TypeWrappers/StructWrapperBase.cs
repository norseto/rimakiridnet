﻿// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data.TypeWrappers {
	/// <summary>
	/// <see cref="IDataWrapper"/>のユーティリティ基底クラスです。
	/// </summary>
	/// <typeparam name="T">ラップする値のタイプ</typeparam>
	public class StructWrapperBase<T> : IDataWrapper where T : struct {
		private readonly T? value;

		#region Implementation of IDataWrapper
		/// <inheritdoc />
		public virtual object Value => value;
		#endregion

		/// <summary>
		/// 値を指定してインスタンスを初期化します。
		/// </summary>
		/// <param name="value">値</param>
		protected StructWrapperBase(T? value) {
			this.value = value;
		}
	}
}
