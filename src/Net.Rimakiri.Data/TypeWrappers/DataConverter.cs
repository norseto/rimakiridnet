// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Text.RegularExpressions;
using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data.TypeWrappers {
	/// <summary>
	/// 値を変換するクラスです。
	/// </summary>
	internal static class DataConverter {
		/// <summary>
		/// 値を変換します。
		/// </summary>
		/// <param name="source">元の値</param>
		/// <param name="format">フォーマット</param>
		/// <param name="escape">エスケープ文字</param>
		/// <returns>フォーマット変換後、元のクラスに戻した値</returns>
		public static object Convert(object source, string format, char? escape) {
			var wrapper = source as IStringDataWrapper;
			var value = wrapper != null ? wrapper.Value : source;
			if (value != null && escape != null) {
				value = EscapeWildcard(value, escape.Value);
			}
			var converted = source == null ? null : string.Format(format, value);
			return wrapper != null ? wrapper.FromString(converted) : converted;
		}

		private static string EscapeWildcard(object value, char escape) {
			var pattern = @"[_%\[" + escape + @"]";
			var replacement = escape == '$' ? "$$$0" : "" + escape + "$0";
			return Regex.Replace(value.ToString(), pattern, replacement);
		}
	}
}
