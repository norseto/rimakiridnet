// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data.TypeWrappers {
	/// <summary>
	/// <see cref="decimal"/>のラッパーの抽象基底クラスです。
	/// </summary>
	/// <remarks>
	/// 継承して有効桁数と小数点以下桁数を固定化したクラスを作成すると、
	/// 毎回有効桁数、小数点以下桁数を指定する必要がなくなります。
	/// </remarks>
	/// <inheritdoc cref="INumericWrapper"/>
	/// <seealso cref="BindValue.Numeric{T}"/>
	public class DecimalType : StructWrapperBase<decimal>, INumericWrapper {
		#region Implementation of INumericWrapper
		/// <inheritdoc />
		public virtual byte Precision { get; private set; }
		/// <inheritdoc />
		public virtual byte Scale { get; private set; }
		#endregion

		/// <inheritdoc />
		/// <summary>
		/// 値を指定してインスタンスを初期化します。
		/// </summary>
		/// <param name="value">値</param>
		/// <param name="precision">有効桁数</param>
		/// <param name="scale">小数点以下桁数</param>
		public DecimalType(decimal? value, byte precision = 0, byte scale = 0)
			: base(value) {
			Precision = precision;
			Scale = scale;
		}
	}
}
