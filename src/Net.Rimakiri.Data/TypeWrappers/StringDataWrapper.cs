// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data.TypeWrappers {
	/// <summary>
	/// 文字列を格納するラッパーの抽象基底クラスです。
	/// </summary>
	/// <inheritdoc cref="IStringDataWrapper" />
	public abstract class StringDataWrapper : WrapperBase<string>, IStringDataWrapper {
		/// <summary>
		/// 値を指定してインスタンスを初期化します。
		/// </summary>
		/// <param name="value">値</param>
		/// <param name="size">長さ</param>
		/// <inheritdoc />
		protected StringDataWrapper(string value, int size = 0) : base(value) {
			Size = size;
		}

		#region Implementation of IStringDataWrapper
		/// <inheritdoc />
		public abstract object FromString(string value);

		/// <inheritdoc />
		public int Size { get; private set; }
		#endregion
	}
}
