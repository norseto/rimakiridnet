// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data {
	/// <summary>
	/// SQLのINの内容を展開するクラスです。
	/// </summary>
	/// <typeparam name="T">値のタイプ</typeparam>
	/// <remarks>
	/// このクラスは、以下の例のような使い方を想定しています。
	/// <example>
	/// <code language="C#">
	/// var expander = new InExpander&lt;string&gt;("id", "NAME IN ({0})", 5, 20);
	/// expander.AddValue("Hoge");
	/// expander.AddValue("Fuga");
	///
	/// var cond = new BasicCondition(expander.Text, expander.BindValues.ToArray());
	/// </code>
	/// </example>
	/// <c>expander.Text</c>は、以下のような内容へと展開されます。
	/// <code>NAME IN ({0}, {1}, {2}, {3}, {4})</code>
	/// <c>fixedSize</c>パラメータが5に指定されているため、常に5のパラメータが使用されます。
	/// これは、クエリキャッシュのヒット率を高めるために寄与します。
	/// また、パラメータの識別子は、<c>id1</c>～<c>id5</c>が使用されます。
	/// 実際に値が指定されていない<c>id3</c>～<c>id5</c>には指定されたパラメタのうち、
	/// NULLでないものが（二重に）割り当てられます。これにより、NOT IN (...)の場合でも
	/// 安全に使用できます（NOT IN にNULLが割り当てられている場合、常に条件に合致します）。
	/// 全てNULLの場合にはNULLが割り当てられます。
	/// 最終的には、以下のような処理を経てSQL文が生成されます。
	/// <example>
	/// <code language="C#">
	/// var statement = new SimpleFragment("SELECT * FROM T_SAMPLE")
	///							.Concat(new WhereFragment(cond));
	/// var builder = new ValueBinderBuilder(statement);
	/// DbCommand command = ...
	/// IDatabaseDialect dialect = ...
	/// var binder = builder.Build(dialect);
	/// binder.Bind(command);
	/// </code>
	/// </example>
	/// 上記の例では、<c>command.Text</c>には以下の内容が設定されます（SQL Serverの場合）。
	/// <code>
	/// SELECT * FROM T_SAMPLE WHERE NAME IN (@id1, @id2, @id3, @id4, @id5)
	/// </code>
	/// <c>@id1</c>には、<c>Hoge</c>が、<c>@id2</c>には<c>Fuga</c>がバインドされます。
	/// その他のパラメータには<c>Hoge</c>がバインドされます。
	/// </remarks>
	/// <remarks>
	/// NOT IN条件で、このクラスによりバインドされたパラメタの値を<see cref="IValueBinder.Replace"/>や、
	/// <see cref="IBindingSupport{T}.ReplaceValue"/>などでNULLに変更すると予期せぬ結果を
	/// 招きます。これらのメソッドで値をNULLに変更しないでください。
	/// </remarks>
	/// <seealso cref="BasicCondition"/>
	/// <seealso cref="SimpleFragment"/>
	/// <seealso cref="FragmentExtension.Concat"/>
	/// <seealso cref="WhereFragment"/>
	/// <seealso cref="ValueBinderBuilder"/>
	/// <seealso cref="IValueBinder"/>
	public class InExpander<T> {
		private const string DefaultFormat = "{0}";
		private readonly string prefix;
		private readonly int fixedSize;
		private readonly string format;
		private readonly int defaultSize;
		private readonly byte defaultPrecision;
		private readonly byte defaultScale;
		private readonly List<IBindValue> paramValues = new List<IBindValue>();
		private readonly List<IBindValue> bindValues = new List<IBindValue>();
		private int index;
		private string text;

		#region Guessed Sizes
		private bool guessed;
		private int guessedSize;
		private byte guessedPrecision;
		private byte guessedScale;
		#endregion

		/// <summary>
		/// 展開されたテキストを取得します。
		/// </summary>
		/// <remarks>
		/// 必要に応じて<see cref="Expand"/>が実行されます。
		/// </remarks>
		public string Text {
			get {
				Expand();
				return text;
			}
		}

		private int Size {
			get {
				GuessSizes();
				return guessedSize;
			}
		}

		private byte Precision {
			get {
				GuessSizes();
				return guessedPrecision;
			}
		}

		private byte Scale {
			get {
				GuessSizes();
				return guessedScale;
			}
		}

		/// <summary>
		/// 展開された値を取得します。
		/// </summary>
		/// <remarks>
		/// 値が追加されていない場合は、空のリストを返却します。
		/// また、必要に応じて<see cref="Expand"/>が実行されます。
		/// </remarks>
		public IEnumerable<IBindValue> BindValues {
			get {
				Expand();
				return bindValues;
			}
		}

		private string NextIdentity => prefix + ++index;

		private void GuessSizes() {
			if (guessed) {
				return;
			}
			guessed = true;
			guessedPrecision = BindValue.PickPrecision(null, defaultPrecision);
			guessedScale = BindValue.PickScale(null, defaultScale);
			guessedSize = BindValue.PickSize(null, defaultSize);
		}

		private void GuessSizes(T value) {
			if (guessed) {
				return;
			}
			guessed = true;
			guessedPrecision = BindValue.PickPrecision(value, defaultPrecision);
			guessedScale = BindValue.PickScale(value, defaultScale);
			guessedSize = BindValue.PickSize(value, defaultSize);
		}

		/// <summary>
		/// フォーマット、固定サイズ、パラメータ情報を指定してインスタンスを初期化します。
		/// </summary>
		/// <param name="prefix">パラメータ識別子のプレフィックス</param>
		/// <param name="format">フォーマット。{0}の部分がパラメータ一覧に展開されます</param>
		/// <param name="fixedSize">
		/// 固定サイズを指定します。0より大きいサイズを指定すると<see cref="AddValue"/>
		/// や、<see cref="AddValues"/>で追加した値の数に拘わらず指定の数のパラメータと
		/// して展開されます。
		/// </param>
		/// <param name="size">値の長さ</param>
		/// <param name="precision">値の有効桁数</param>
		/// <param name="scale">値の小数点以下桁数</param>
		/// <remarks>
		/// <paramref name="fixedSize"/>を指定すると、常に同じSQL文のフォーマットとなるため、
		/// クエリキャッシュを有効に使用できるようになります。
		/// </remarks>
		/// <remarks>
		/// <paramref name="size"/>、<paramref name="precision"/>および<paramref name="scale"/>
		/// が指定されておらず、<typeparamref name="T"/>が<see cref="IStringDataWrapper"/>や
		/// <see cref="INumericWrapper"/>の場合、最初に追加された値の長さ、有効桁数、小数点以下桁数が
		/// 使用されます。指定されている場合、これらの値が使用されます。
		/// </remarks>
		public InExpander(string prefix, string format, int fixedSize = -1, int size = -1,
					byte precision = 0, byte scale = 0) {
			this.prefix = prefix;
			this.format = string.IsNullOrEmpty(format) ? DefaultFormat : format;
			this.fixedSize = fixedSize;
			defaultSize = size;
			defaultPrecision = precision;
			defaultScale = scale;
			if (!typeof(IDataWrapper).IsAssignableFrom(typeof(T))) {
				GuessSizes();
			}
		}

		/// <inheritdoc />
		/// <summary>
		/// 固定サイズ、パラメータ情報を指定してインスタンスを初期化します。
		/// </summary>
		/// <param name="prefix">パラメータ識別子のプレフィックス</param>
		/// <param name="fixedSize">
		/// 固定サイズを指定します。0より大きいサイズを指定すると<see cref="AddValue"/>
		/// や、<see cref="AddValues"/>で追加した値の数に拘わらず指定の数のパラメータと
		/// して展開されます。
		/// </param>
		/// <param name="size">値の長さ</param>
		/// <param name="precision">値の有効桁数</param>
		/// <param name="scale">値の小数点以下桁数</param>
		/// <remarks>
		/// デフォルトのフォーマット{0}が使用されます。
		/// </remarks>
		/// <remarks>
		/// <paramref name="fixedSize"/>を指定すると、常に同じSQL文のフォーマットとなるため、
		/// クエリキャッシュを有効に使用できるようになります。
		/// </remarks>
		public InExpander(string prefix, int fixedSize = -1, int size = -1,
					byte precision = 0, byte scale = 0)
					: this(prefix, DefaultFormat, fixedSize, size, precision, scale) {
			// Empty
		}

		/// <summary>
		/// 値を追加します。
		/// </summary>
		/// <param name="value">追加する値</param>
		/// <returns>このインスタンス</returns>
		public InExpander<T> AddValue(T value) {
			if (value == null) {
				return this;
			}

			if (value is IDataWrapper wrapper && wrapper.Value == null) {
				return this;
			}
			GuessSizes(value);
			paramValues.Add(BindValue.Create(NextIdentity, value, Size, Precision, Scale));
			MarkModified();
			return this;
		}

		/// <summary>
		/// 値を追加します。
		/// </summary>
		/// <param name="values">追加する値</param>
		/// <returns>このインスタンス</returns>
		public InExpander<T> AddValues(IEnumerable<T> values) {
			if (values == null) {
				return this;
			}
			foreach (var value in values) {
				AddValue(value);
			}
			return this;
		}

		/// <summary>
		/// 展開します。
		/// </summary>
		/// <returns>このインスタンス</returns>
		public InExpander<T> Expand() {
			if (text != null) {
				return this;
			}
			var expanded = MakeExpandedText();
			text = string.Format(format, expanded);
			return this;
		}

		private void MarkModified() {
			text = null;
		}

		private string MakeExpandedText() {
			Fill();
			var values = bindValues;
			var sb = new StringBuilder(values.Count * 7);

			for (int i = 0, l = values.Count; i < l; i++) {
				if (i != 0) {
					sb.Append(", ");
				}
				sb.Append("{").Append(i).Append("}");
			}
			return sb.ToString();
		}

		private void Fill() {
			bindValues.Clear();
			var max = (fixedSize > 0) ? fixedSize : paramValues.Count;
			bindValues.AddRange(paramValues.Take(max));
			var fillParam = paramValues.FirstOrDefault(p => p.Value != null);
			var fillValue = fillParam?.RawValue;
			var back = index;
			try {
				var size = Size;
				var precision = Precision;
				var scale = Scale;
				for (int i = bindValues.Count, l = Math.Max(fixedSize, bindValues.Count); i < l; i++) {
					bindValues.Add(BindValue.InnerCreate<T>(NextIdentity, fillValue, size, precision, scale));
				}
			}
			finally {
				index = back;
			}
		}
	}
}

