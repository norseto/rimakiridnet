// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data {
	/// <summary>
	/// <see cref="IValueBinder"/>インスタンスを生成するビルダクラスです。
	/// </summary>
	/// <seealso cref="IFragment"/>
	/// <seealso cref="ICondition"/>
	/// <seealso cref="IBindValue"/>
	/// <seealso cref="BindValue"/>
	public class ValueBinderBuilder {
		private readonly List<IFragment> conditions = new List<IFragment>();

		/// <summary>
		/// フォーマットおよびバインドする値を指定してインスタンスを初期化します。
		/// </summary>
		/// <param name="format">フォーマット</param>
		/// <param name="values">バインドする値</param>
		/// <remarks>
		/// 指定されたフォーマットおよびバインドする値から、<see cref="BasicFragment"/>が生成
		/// し、これを最初の<see cref="ICondition"/>としてインスタンスを初期化します。
		/// </remarks>
		/// <inheritdoc />
		public ValueBinderBuilder(string format, params IBindValue[] values)
					: this(new BasicFragment(format, values)) {
		}

		/// <summary>
		/// 最初の<see cref="IFragment"/>を指定してインスタンスを初期化します。
		/// </summary>
		/// <param name="firstConditions">初期<see cref="ICondition"/></param>
		public ValueBinderBuilder(params IFragment[] firstConditions) {
			AddFragments(firstConditions);
		}

		/// <summary>
		/// <see cref="IValueBinder"/>をビルドします。
		/// </summary>
		/// <param name="dialect">使用する<see cref="IDatabaseDialect"/>インスタンス</param>
		/// <returns>ビルドした<see cref="IValueBinder"/>インスタンス</returns>
		/// <remarks>
		/// SQL文はこのビルド時に確定されます。<see cref="BasicCondition"/>の
		/// <see cref="BasicCondition.ExcludeFilter"/>を設定することでSQL文から
		/// 除外された条件は、ここで返却した<see cref="IValueBinder"/>の
		/// <see cref="IValueBinder.Replace"/>で除外されない値に変更しても
		/// SQL文は変わりません。
		/// </remarks>
		/// <seealso cref="BasicCondition"/>
		public IValueBinder Build(IDatabaseDialect dialect) {
			var condition = new FragmentCollection().Append(conditions);
			var context = new FragmentContextImpl {
								DatabaseDialect = dialect,
							};
			var text = condition.ToLiteralString(context);
			var values = new List<IBindValue>(context.BindValues);
			return new ValueBinderImpl(dialect, text, values);
		}

		/// <summary>
		/// <see cref="IFragment"/>を追加します。追加された<see cref="IFragment"/>は
		/// 単純にスペース区切りで連結されます。
		/// </summary>
		/// <param name="condition">追加する<see cref="IFragment"/></param>
		/// <returns>このインスタンス</returns>
		public ValueBinderBuilder Concat(params IFragment[] condition) {
			AddFragments(condition);
			return this;
		}

		private void AddFragments(params IFragment[] condition) {
			if (condition == null) {
				return;
			}
			conditions.AddRange(condition.Where(cond => cond != null));
		}
	}

	internal class ValueBinderImpl : IValueBinder {
		private readonly IDatabaseDialect dialect;
		private readonly string text;
		private readonly List<BindValue> values = new List<BindValue>();
		private readonly IDictionary<string, BindValue> dict = new Dictionary<string, BindValue>();

		public string Statement => text;

		public int Count => values.Count;

		public IBindValue this[int idx] => values[idx];

		public IBindValue this[string id] => dict[id];

		public ValueBinderImpl(IDatabaseDialect dialect, string text,
					IEnumerable<IBindValue> values) {
			this.dialect = dialect;
			this.text = text;

			foreach (var bindValue in values) {
				var value = new BindValue(bindValue);
				if (dict.TryGetValue(value.Identity, out var current) && current == value) {
					continue;
				}
				this.values.Add(value);
				dict.Add(value.Identity, value);
			}
		}

		#region Implementation of IValueBinder
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:")]
		public IDbCommand Bind(IDbCommand command) {
			command.CommandText = text;
			command.CommandType = CommandType.Text;
			var commandParameters = command.Parameters;
			commandParameters.Clear();
			foreach (var bindValue in values) {
				var parameter = command.CreateParameter();
				parameter.ParameterName = dialect.NameFor(bindValue.Identity);
				try {
					SetupParameter(parameter, bindValue);
					dialect.SetupParameter(parameter, bindValue);
					dialect.BoundValueVerifier.VerifyParameter(parameter, bindValue);
					commandParameters.Add(parameter);
				}
				catch (Exception ex) {
					throw new InvalidOperationException("Failed to setup value: " + parameter.ParameterName, ex);
				}
			}
			return command;
		}

		private static void SetupParameter(IDbDataParameter parameter, IBindValue bindValue) {
			parameter.Value = bindValue.Value ?? DBNull.Value;

			var meta = bindValue.BindMetaData;
			var size = meta.Size;
			if (size > 0) {
				parameter.Size = size;
			}
			if (meta.Precision > 0) {
				parameter.Precision = meta.Precision;
			}
			if (meta.Scale > 0) {
				parameter.Scale = meta.Scale;
			}
		}

		public IValueBinder Replace(string identity, object value) {
			if (dict.TryGetValue(identity, out var current)) {
				current.RawValue = value;
			}
			return this;
		}
		#endregion
	}
}
