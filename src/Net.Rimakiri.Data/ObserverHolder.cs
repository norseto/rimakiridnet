// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data {
	/// <summary>
	/// <see cref="IStructureObserver"/>を保持するためのクラスです。
	/// </summary>
	internal class ObserverHolder {
		private readonly WeakReference observer;

		/// <summary>
		/// 保持する<see cref="IStructureObserver"/>を指定して
		/// インスタンスを初期化します。
		/// </summary>
		/// <param name="observer"></param>
		public ObserverHolder(IStructureObserver observer) {
			this.observer = new WeakReference(observer);
		}

		/// <summary>
		/// イベントを発火します。
		/// </summary>
		/// <param name="fragment">発火ソース</param>
		public void FireEvent(IFragment fragment) {
			var target = observer.Target as IStructureObserver;
			target?.NotifyStructureMayChanged(fragment);
		}
	}

	internal static class OvserverHolderEx {
		/// <summary>
		/// イベントを発火します。
		/// </summary>
		/// <param name="target">発火対象</param>
		/// <param name="fragment">発火ソース</param>
		public static void Fire(this ObserverHolder target, IFragment fragment)
			=> target?.FireEvent(fragment);
	}
}
