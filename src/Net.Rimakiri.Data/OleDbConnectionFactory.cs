// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

#if (NET40 || NET45 || NET451 || NET452 || NET46 || NET461 || NET462 || NET47 || NET471 || NET472)
#define NETFRAMEWORK
#endif
#if (NETFRAMEWORK)
using System.Data.OleDb;
using Net.Rimakiri.Data.Interfaces;
using Net.Rimakiri.Data.Util;

namespace Net.Rimakiri.Data {
	/// <summary>
	/// OLEデータベースに対する<see cref="IConnectionFactory"/>クラスです。
	/// </summary>
	/// <inheritdoc />
	/// <seealso cref="GenericDatabaseDialect"/>
	public class OleDbConnectionFactory : ConnectionFactoryBase<OleDbConnection> {
		#region Overrides of ConnectionFactoryBase<OleDbConnection>
		/// <inheritdoc />
		protected override IDatabaseDialect DefaultDialect { get; }
			= new GenericDatabaseDialect();

		/// <inheritdoc />
		protected override OleDbConnection MakeConnectionInstance() {
			var con = new OleDbConnection(ConnectionString);
			return con;
		}
		#endregion
	}
}
#endif
