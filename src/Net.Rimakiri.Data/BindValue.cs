// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using Net.Rimakiri.Data.Interfaces;
using Net.Rimakiri.Data.TypeWrappers;
using Net.Rimakiri.Data.Util;

namespace Net.Rimakiri.Data {
	/// <inheritdoc />
	/// <summary>
	/// SQLにバインドする値を表す<see cref="IBindValue"/>インターフェイス
	/// の実装クラスです。
	/// </summary>
	/// <seealso cref="BasicCondition"/>
	/// <seealso cref="BasicFragment"/>
	/// <seealso cref="ModelBindingFactory{TModel}"/>
	/// <seealso cref="IModelBinding{TModel}"/>
	/// <seealso cref="DynamicModelBinding"/>
	public sealed class BindValue : IBindValue {
		/// <summary>
		/// 前方一致検索時のフォーマット
		/// </summary>
		/// <seealso cref="WithFormat{T}(string,string,T,int)"/>
		/// <seealso cref="WithFormat(string,string,string,int)"/>
		/// <seealso cref="WithFormat{T}(string,string,T,char,int)"/>
		/// <seealso cref="WithFormat(string,string,string,char,int)"/>
		public const string ForwardMatch = "{0}%";

		/// <summary>
		/// 後方一致検索時のフォーマット
		/// </summary>
		/// <seealso cref="WithFormat{T}(string,string,T,int)"/>
		/// <seealso cref="WithFormat(string,string,string,int)"/>
		/// <seealso cref="WithFormat{T}(string,string,T,char,int)"/>
		/// <seealso cref="WithFormat(string,string,string,char,int)"/>
		public const string BackwardMatch = "%{0}";

		/// <summary>
		/// 中間一致検索時のフォーマット
		/// </summary>
		/// <seealso cref="WithFormat{T}(string,string,T,int)"/>
		/// <seealso cref="WithFormat(string,string,string,int)"/>
		/// <seealso cref="WithFormat{T}(string,string,T,char,int)"/>
		/// <seealso cref="WithFormat(string,string,string,char,int)"/>
		public const string MiddleMatch = "%{0}%";

		private object value;

		private sealed class BindMetaDataImpl : IBindMetaData {
			#region Implementation of IBindMetaData
			public Type DataType { get; internal set; }
			public bool Nullable { get; internal set; }
			public byte Precision { get; internal set; }
			public byte Scale { get; internal set; }
			public int Size { get; internal set; }
			#endregion
		}

		#region Implementation of IBindValue
		private readonly string identity;
		/// <inheritdoc />
		public string Identity => identity;

		private object cachedValue;

		/// <inheritdoc />
		/// <summary>
		/// 値を設定または取得します。設定した値が、<see cref="IDataWrapper"/>
		/// インスタンスの場合、取得される値はもとの値になります。
		/// </summary>
		public object Value {
			get {
				if (cachedValue == null) {
					var origin = RawValue;
					var format = Format;
					if (format != null) {
						origin = DataConverter.Convert(origin, format, Escape);
					}
					var wrapper = origin as IDataWrapper;
					var candidate = (wrapper == null) ? origin : wrapper.Value;
					cachedValue = candidate ?? DBNull.Value;
				}
				return cachedValue == DBNull.Value ? null : cachedValue;
			}
		}

		/// <inheritdoc />
		public object RawValue {
			get => value;
			internal set {
				this.value = value;
				cachedValue = null;
			}
		}

		/// <inheritdoc />
		public string Format { get; private set; }

		/// <inheritdoc />
		public char? Escape { get; private set; }

		/// <inheritdoc />
		public IBindMetaData BindMetaData { get; internal set; }
		#endregion

		/// <summary>
		/// 識別子を指定してインスタンスを初期化します。
		/// </summary>
		/// <param name="identity">識別子</param>
		private BindValue(string identity) {
			this.identity = identity;
		}


		/// <summary>
		/// インスタンスを初期化します（コピーコンストラクタ）。
		/// </summary>
		/// <param name="origin">元のインスタンス</param>
		internal BindValue(IBindValue origin) {
			identity = origin.Identity;
			Format = origin.Format;
			Escape = origin.Escape;
			BindMetaData = origin.BindMetaData;
			RawValue = origin.RawValue;
		}

		#region Factory methods
		/// <summary>
		/// インスタンスを生成します。
		/// </summary>
		/// <typeparam name="T">値の型</typeparam>
		/// <param name="identity">パラメタの識別子</param>
		/// <param name="rawValue">値</param>
		/// <param name="size">値のサイズ</param>
		/// <param name="precision">値の有効桁数</param>
		/// <param name="scale">値の小数点以下桁数</param>
		/// <returns>生成したインスタンス</returns>
		internal static BindValue InnerCreate<T>(string identity, object rawValue, int size = -1,
			byte precision = 0, byte scale = 0) {
			if (identity == null) {
				throw new ArgumentNullException(nameof(identity));
			}

			var type = typeof(T);
			if (type.IsGenericType &&
				type.GetGenericTypeDefinition() == typeof(Nullable<>)) {
				type = Nullable.GetUnderlyingType(type);
			}

			precision = PickPrecision(rawValue, precision);
			scale = PickScale(rawValue, scale);
			size = PickSize(rawValue, size);

			var meta = new BindMetaDataImpl {
				DataType = type,
				Size = PickSize(rawValue, size),
				Precision = precision,
				Scale = scale,
				Nullable = false,
			};
			var ret = new BindValue(identity) {
				RawValue = rawValue,
				BindMetaData = meta,
			};
			return ret;
		}
		/// <summary>
		/// インスタンスを生成します。
		/// </summary>
		/// <typeparam name="T">値の型</typeparam>
		/// <param name="identity">パラメタの識別子</param>
		/// <param name="value">値</param>
		/// <param name="size">値のサイズ</param>
		/// <param name="precision">値の有効桁数</param>
		/// <param name="scale">値の小数点以下桁数</param>
		/// <returns>生成したインスタンス</returns>
		/// <remarks>
		/// <paramref name="size"/>、<paramref name="precision"/>および<paramref name="scale"/>
		/// が指定されておらず、<typeparamref name="T"/>が<see cref="IStringDataWrapper"/>や
		/// <see cref="INumericWrapper"/>の場合、値から取得した長さ、有効桁数、小数点以下桁数が
		/// 使用されます。指定されている場合や、値がnullの場合、パラメータの値が使用されます。
		/// </remarks>
		public static BindValue Create<T>(string identity, T value, int size = -1,
					byte precision = 0, byte scale = 0)
				=> InnerCreate<T>(identity, value, size, precision, scale);

		/// <summary>
		/// <see cref="INumericWrapper"/>インスタンスを値として指定してインスタンスを
		/// 生成します。<see cref="INumericWrapper"/>を指定することで、桁数等を毎回
		/// 指定する必要がありません。
		/// </summary>
		/// <typeparam name="T">
		/// 値のタイプ。<see cref="INumericWrapper"/>を実装するタイプであることが必要です。
		/// </typeparam>
		/// <param name="identity">パラメタの識別子</param>
		/// <param name="value">値</param>
		/// <returns>生成したインスタンス</returns>
		/// <seealso cref="INumericWrapper"/>
		/// <seealso cref="DecimalType"/>
		public static BindValue Numeric<T>(string identity, T value) where T: INumericWrapper {
			var precision = PickPrecision(value, 0);
			var scale = PickScale(value, 0);
			return InnerCreate<T>(identity, value, -1, precision, scale);
		}

		/// <summary>
		/// 値のフォーマットを指定してインスタンスを生成します。
		/// </summary>
		/// <param name="identity">パラメタの識別子</param>
		/// <param name="format">値のフォーマット</param>
		/// <param name="value">値</param>
		/// <param name="size">値のサイズ</param>
		/// <returns>生成したインスタンス</returns>
		/// <remarks>
		/// このメソッドは、<see cref="WithFormat{T}(string,string,T,int)"/>の型指定のないものです。
		/// </remarks>
		public static BindValue WithFormat(string identity, string format,
						string value, int size = -1) {
			var meta = new BindMetaDataImpl {
				DataType = typeof(string),
				Size = size,
			};
			var ret = new BindValue(identity) {
				RawValue = value,
				BindMetaData = meta,
				Format = format,
			};
			return ret;
		}

		/// <summary>
		/// 値のフォーマットを指定してインスタンスを生成します。
		/// </summary>
		/// <typeparam name="T">
		/// 値のタイプ。<see cref="IStringDataWrapper"/>を実装するタイプであることが必要です。
		/// </typeparam>
		/// <param name="identity">パラメタの識別子</param>
		/// <param name="format">値のフォーマット</param>
		/// <param name="value">値</param>
		/// <param name="size">値のサイズ</param>
		/// <returns>生成したインスタンス</returns>
		/// <remarks>
		/// 長さは、<paramref name="value"/>がnullの場合、または、<paramref name="size"/>
		/// が指定されている場合にはその値が、その他の場合には、<paramref name="value"/>
		/// の<see cref="IStringDataWrapper.Size"/>プロパティが使用されます。
		/// </remarks>
		public static BindValue WithFormat<T>(string identity, string format,
						T value, int size = -1) where T: IStringDataWrapper {
			var meta = new BindMetaDataImpl {
				DataType = typeof(T),
				Size = PickSize(value, size),
			};
			var ret = new BindValue(identity) {
				RawValue = value,
				BindMetaData = meta,
				Format = format,
			};
			return ret;
		}

		/// <summary>
		/// 値のフォーマットを指定してインスタンスを生成します。
		/// </summary>
		/// <param name="identity">パラメタの識別子</param>
		/// <param name="format">値のフォーマット</param>
		/// <param name="value">値</param>
		/// <param name="escape">エスケープ文字</param>
		/// <param name="size">値のサイズ</param>
		/// <returns>生成したインスタンス</returns>
		/// <remarks>
		/// このメソッドは、<see cref="WithFormat{T}(string,string,T,char,int)"/>
		/// の型指定のないものです。
		/// </remarks>
		public static BindValue WithFormat(string identity, string format,
			string value, char escape, int size = -1) {
			var meta = new BindMetaDataImpl {
				DataType = typeof(string),
				Size = size,
			};
			var ret = new BindValue(identity) {
				RawValue = value,
				BindMetaData = meta,
				Format = format,
				Escape = escape
			};
			return ret;
		}

		/// <summary>
		/// 値のフォーマットを指定してインスタンスを生成します。
		/// </summary>
		/// <typeparam name="T">
		/// 値のタイプ。<see cref="IStringDataWrapper"/>を実装するタイプであることが必要です。
		/// </typeparam>
		/// <param name="identity">パラメタの識別子</param>
		/// <param name="format">値のフォーマット</param>
		/// <param name="value">値</param>
		/// <param name="escape">エスケープ文字</param>
		/// <param name="size">値のサイズ</param>
		/// <returns>生成したインスタンス</returns>
		/// <remarks>
		/// 長さは、<paramref name="value"/>がnullの場合、または、<paramref name="size"/>
		/// が指定されている場合にはその値が、その他の場合には、<paramref name="value"/>
		/// の<see cref="IStringDataWrapper.Size"/>プロパティが使用されます。
		/// </remarks>
		public static BindValue WithFormat<T>(string identity, string format,
			T value, char escape, int size = -1) where T: IStringDataWrapper {
			var meta = new BindMetaDataImpl {
				DataType = typeof(T),
				Size = PickSize(value, size),
			};
			var ret = new BindValue(identity) {
				RawValue = value,
				BindMetaData = meta,
				Format = format,
				Escape = escape,
			};
			return ret;
		}
		#endregion

		#region Overrides of Object
		/// <inheritdoc />
		public override bool Equals(object obj) {
			if (ReferenceEquals(this, obj)) {
				return true;
			}
			if (ReferenceEquals(obj, null)) {
				return false;
			}
			var rhs = obj as BindValue;
			if (ReferenceEquals(rhs, null)) {
				return false;
			}
			var meta = BindMetaData;
			var rmeta = rhs.BindMetaData;
			var ret = string.Equals(Identity, rhs.Identity)
						&& AreSame(value, rhs.value)
						&& string.Equals(Format, rhs.Format)
						&& meta.DataType == rmeta.DataType
						&& meta.Size == rmeta.Size
						&& meta.Precision == rmeta.Precision
						&& meta.Scale == rmeta.Scale;
			return ret;
		}

		private static bool AreSame(object lhs, object rhs) {
			if (ReferenceEquals(lhs, null)) {
				lhs = DBNull.Value;
			}
			if (ReferenceEquals(rhs, null)) {
				rhs = DBNull.Value;
			}
			return lhs.Equals(rhs);
		}

		/// <inheritdoc />
		public override int GetHashCode() {
			return Identity.GetHashCode()
						^ (ReferenceEquals(Value, null) ? 7777
							: Value.GetHashCode());
		}
		#endregion

		/// <summary>
		/// 値の同一性を取得します。
		/// </summary>
		/// <param name="lhs">左辺</param>
		/// <param name="rhs">右辺</param>
		/// <returns>同じ内容ならばtrue</returns>
		public static bool operator ==(BindValue lhs, BindValue rhs) {
			return !ReferenceEquals(lhs, null) ? lhs.Equals(rhs)
						: ReferenceEquals(rhs, null);
		}

		/// <summary>
		/// 値の非同一性を取得します。
		/// </summary>
		/// <param name="lhs">左辺</param>
		/// <param name="rhs">右辺</param>
		/// <returns>異なる内容ならばtrue</returns>
		public static bool operator !=(BindValue lhs, BindValue rhs) {
			return !(lhs == rhs);
		}

		/// <summary>
		/// 値とパラメータから採用する長さを決定する。
		/// </summary>
		/// <param name="value">値</param>
		/// <param name="parameterValue">パラメータで与えられた長さ</param>
		/// <returns>採用する長さ</returns>
		internal static int PickSize(object value, int parameterValue) {
			var strValue = value as IStringDataWrapper;
			return (strValue == null) ? parameterValue
					: PickSpecific(-1, strValue.Size, parameterValue);
		}

		/// <summary>
		/// 値とパラメータから採用する有効桁数を決定する。
		/// </summary>
		/// <param name="value">値</param>
		/// <param name="parameterValue">パラメータで与えられた有効桁数</param>
		/// <returns>採用する有効桁数</returns>
		internal static byte PickPrecision(object value, byte parameterValue) {
			var numValue = value as INumericWrapper;
			return (numValue == null) ? parameterValue
				: PickSpecific((byte)0, numValue.Precision, parameterValue);
		}

		/// <summary>
		/// 値とパラメータから採用する小数点以下桁数を決定する。
		/// </summary>
		/// <param name="value">値</param>
		/// <param name="parameterValue">パラメータで与えられた小数点以下桁数</param>
		/// <returns>採用する小数点以下桁数</returns>
		internal static byte PickScale(object value, byte parameterValue) {
			var numValue = value as INumericWrapper;
			return (numValue == null) ? parameterValue
				: PickSpecific((byte)0, numValue.Scale, parameterValue);
		}

		private static byte PickSpecific(byte initialValue,
						byte typeValue, byte parameterValue) {
			return parameterValue != initialValue ? parameterValue : typeValue;
		}

		private static int PickSpecific(int initialValue,
			int typeValue, int parameterValue) {
			return parameterValue != initialValue ? parameterValue : typeValue;
		}
	}
}
