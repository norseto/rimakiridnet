// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data {
	/// <inheritdoc />
	/// <summary>
	/// AND結合条件演算子です。一般的には2項演算子ですが、本フレームワーク
	/// では多項演算子となっています。
	/// </summary>
	/// <seealso cref="NNaryCondition"/>
	/// <seealso cref="CondUtils"/>
	public class AndOp : INNaryOperator {
		/// <summary>
		/// <see cref="AndOp"/>クラスのシングルトンインスタンスです。
		/// </summary>
		public static readonly AndOp Singleton = new AndOp();

		private AndOp() {
			// Empty
		}

		/// <inheritdoc />
		public string ToOpString(IFragmentContext ctx) {
			return " AND ";
		}
	}

	/// <inheritdoc />
	/// <summary>
	/// OR結合条件演算子です。一般的には2項演算子ですが、本システム
	/// では多項演算子となっています。
	/// </summary>
	public class OrOp : INNaryOperator {
		/// <summary>
		/// <see cref="OrOp"/>クラスのシングルトンインスタンスです。
		/// </summary>
		public static readonly OrOp Singleton = new OrOp();

		private OrOp() {
			// Empty
		}

		/// <inheritdoc />
		public string ToOpString(IFragmentContext ctx) {
			return " OR ";
		}
	}
}
