﻿// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Collections.Generic;

namespace Net.Rimakiri.Data {
	/// <summary>
	/// カスタムフォーマッタ。フォーマットのプレイスメントに指定されたパラメタ名
	/// を保存します。
	/// </summary>
	/// <inheritdoc cref="IFormatProvider"/>
	/// <inheritdoc cref="ICustomFormatter"/>
	internal class CustomStatementFormatter : IFormatProvider, ICustomFormatter {
		private readonly HashSet<string> bound = new HashSet<string>();

		/// <summary>
		/// 指定のパラメータ名がフォーマットに含まれていたかを取得します。
		/// </summary>
		/// <param name="parameterName">パラメータ名</param>
		/// <returns>含まれていた場合true</returns>
		public bool Contains(string parameterName) {
			return bound.Contains(parameterName);
		}

		#region Implementation of IFormatProvider
		/// <inheritdoc />
		public object GetFormat(Type formatType) {
			return formatType == typeof(ICustomFormatter) ? this : null;
		}
		#endregion

		#region Implementation of ICustomFormatter
		/// <inheritdoc />
		public string Format(string format, object arg, IFormatProvider formatProvider) {
			var value = arg?.ToString();
			if (value == null) {
				return "null";
			}
			if (!bound.Contains(value)) {
				bound.Add(value);
			}
			return value;
		}
		#endregion
	}
}
