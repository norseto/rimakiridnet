﻿// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Collections.Generic;
using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data {
	/// <summary>
	/// バインドする値を持たない条件式を示す<see cref="ICondition"/>を実装するクラスです。
	/// </summary>
	/// <remarks>
	/// <see cref="SimpleFragment"/>と同じですが、<see cref="ICondition"/>
	/// インターフェイスを実装しているため、<see cref="CondUtils"/>のメソッドに
	/// より結合することができます。
	/// </remarks>
	/// <seealso cref="BasicCondition"/>
	public class SimpleCondition : SimpleFragment, ICondition {
		/// <inheritdoc />
		public SimpleCondition() {
		}

		/// <inheritdoc />
		public SimpleCondition(string text)
			: base(text) {
		}

		#region Implementation of ICondition
		/// <inheritdoc />
		public IEnumerable<ICondition> Flatten(IConditionOperator parentOperator) {
			yield return this;
		}
		#endregion
	}
}
