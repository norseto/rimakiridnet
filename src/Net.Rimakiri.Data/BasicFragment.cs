// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Collections.Generic;
using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data {
	/// <summary>
	/// フォーマットおよびバインドする値をもつ<see cref="IFragment"/>クラスです。
	/// </summary>
	/// <inheritdoc cref="IFragment" />
	/// <inheritdoc cref="IBindingSupport{T}" />
	/// <seealso cref="BasicCondition"/>
	/// <seealso cref="SimpleFragment"/>
	/// <seealso cref="BindValue"/>
	/// <seealso cref="ValueBinderBuilder"/>
	public class BasicFragment : IFragment, IBindingSupport<BasicFragment> {
		private readonly BindingEngine engine;

		/// <inheritdoc />
		public bool Enabled => engine.Enabled;

		/// <inheritdoc />
		public FragmentFilter ExcludeFilter {
			get => engine.ExcludeFilter;
			set => engine.ExcludeFilter = value;
		}

		/// <summary>
		/// テキストフォーマットと値を指定してインスタンスを初期化します。
		/// </summary>
		/// <param name="format">テキストのフォーマット。{0}は最初の値にマッピングされます</param>
		/// <param name="values">値リスト</param>
		public BasicFragment(string format, params IBindValue[] values) {
			engine = new BindingEngine(format, values);
		}

		/// <inheritdoc />
		public BasicFragment AddValues(params IBindValue[] valuesToAdd) {
			engine.AddValues(valuesToAdd);
			return this;
		}

		/// <inheritdoc />
		public BasicFragment ReplaceValue(IBindValue value) {
			engine.ReplaceValue(value);
			return this;
		}

		#region Implementation of IFragment
		/// <inheritdoc />
		/// <returns>
		/// フォーマットと値からSQL文（の一部）となる文字列を作成した結果
		/// </returns>
		public string ToLiteralString(IFragmentContext ctx) {
			return engine.ToLiteralString(ctx);
		}
		#endregion

		#region Implementation of IFragment
		/// <inheritdoc />
		public IEnumerable<IFragment> ToEnumerable() {
			yield return this;
		}
		#endregion
	}
}
