// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Collections.Generic;
using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data {
	/// <summary>
	/// 指定のフォーマットで<see cref="IFragment"/>を整形する
	/// <see cref="IFragment"/>です。
	/// </summary>
	public class FormattedFragment : IFragment {
		/// <summary>
		/// 子要素を設定または取得します。
		/// </summary>
		public IFragment Child { get; set; }

		/// <summary>
		/// フォーマットを設定または取得します。
		/// </summary>
		public string Format { get; set; }

		#region Implementation of IFragment
		/// <inheritdoc />
		public bool Enabled => !string.IsNullOrEmpty(Format)
									&& Child != null && Child.Enabled;

		/// <inheritdoc />
		/// <summary>
		/// 有効な<see cref="Child"/>が存在する場合、<see cref="Format"/>の
		/// <c>{0}</c>に <see cref="Child"/>の<see cref="IFragment.ToLiteralString"/>
		/// の結果を連結したものを返却します。存在しない場合、自要素も含めて
		/// 何も返却しません。
		/// </summary>
		public string ToLiteralString(IFragmentContext ctx) {
			if (!Enabled) {
				return null;
			}
			var text = Child.ToLiteralString(ctx);
			return string.IsNullOrEmpty(text) ? null : string.Format(Format, text);
		}

		/// <inheritdoc />
		public IEnumerable<IFragment> ToEnumerable() {
			yield return this;
		}
		#endregion
	}
}
