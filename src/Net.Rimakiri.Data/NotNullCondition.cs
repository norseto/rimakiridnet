// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data {
	/// <summary>
	/// 全てのパラメータがNULLではない場合に有効となる<see cref="BasicCondition"/>
	/// クラスです。
	/// </summary>
	/// <remarks>
	/// このクラスは、<see cref="BasicCondition"/>の<see cref="BasicCondition.ExcludeFilter"/>
	/// に<see cref="Filters.NotNull"/>を指定したものです。
	/// </remarks>
	/// <inheritdoc />
	public class NotNullCondition : BasicCondition {
		/// <inheritdoc />
		public NotNullCondition(string format, params IBindValue[] values)
				: base(format, values) {
			ExcludeFilter = Filters.NotNull;
		}
	}
}
