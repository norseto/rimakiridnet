// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Collections.Generic;
using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data {
	/// <summary>
	/// バインドする値を持たない単純な文字列の<see cref="IFragment"/>実装クラスです。
	/// </summary>
	/// <inheritdoc />
	/// <seealso cref="BasicFragment"/>
	public class SimpleFragment : IFragment {
		/// <summary>
		/// 条件テキストを取得または設定します。
		/// </summary>
		public string Text { get; set; }

		/// <summary>
		/// 条件テキストを設定せずにインスタンスを初期化します。
		/// </summary>
		public SimpleFragment() {
		}

		/// <summary>
		/// 条件テキストを設定してインスタンスを初期化します。
		/// </summary>
		/// <param name="text">条件テキスト</param>
		/// <inheritdoc />
		public SimpleFragment(string text) : this() {
			Text = text;
		}

		#region Implementation of IFragment
		/// <summary>
		/// 有効か否かを取得します。
		/// </summary>
		/// <inheritdoc />
		public bool Enabled => !string.IsNullOrEmpty(Text);

		/// <inheritdoc />
		public string ToLiteralString(IFragmentContext ctx) {
			return string.IsNullOrEmpty(Text) ? null : Text;
		}

		/// <inheritdoc />
		public IEnumerable<IFragment> ToEnumerable() {
			yield return this;
		}
		#endregion
	}
}
