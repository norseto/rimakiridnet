// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Data;
using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data {
	/// <summary>
	/// デフォルトで用意されている<see cref="IBoundValueVerifier"/>のファクトリ
	/// クラスです。
	/// </summary>
	public static class BoundValueVerifierFactory {
		/// <summary>
		/// デフォルトで用意されている<see cref="ShortSizePolicy"/>に対応した
		/// <see cref="IBoundValueVerifier"/>インスタンスを取得します。
		/// </summary>
		/// <param name="policy">ポリシー</param>
		/// <returns>ポリシーに対応した<see cref="IBoundValueVerifier"/></returns>
		public static IBoundValueVerifier GetBoundValueVerifier(ShortSizePolicy policy) {
			switch (policy) {
			case ShortSizePolicy.Fit:
				return FittingVerifier.Singleton;
			case ShortSizePolicy.Throw:
				return ThrowingVerifier.Singleton;
			case ShortSizePolicy.Ignore:
				return null;
			default:
				return null;
			}
		}
	}

	internal abstract class SizeVerifier : IBoundValueVerifier {
		protected bool IsSizeShort(IDbDataParameter param, int declaredSize) {
			if (declaredSize < 1) {
				return false;
			}
			var strValue = param.Value as string;
			if (strValue == null) {
				return false;
			}

			return strValue.Length > declaredSize;
		}

		#region Implementation of IBoundValueVerifier
		public abstract void Verify(IDbDataParameter parameter, IBindValue value);
		#endregion
	}

	internal class ThrowingVerifier : SizeVerifier {
		public static readonly ThrowingVerifier Singleton = new ThrowingVerifier();

		private ThrowingVerifier() {
		}

		public override void Verify(IDbDataParameter param, IBindValue value) {
			var declaredSize = value.BindMetaData.Size;
			if (!IsSizeShort(param, declaredSize)) {
				return;
			}

			var strValue = param.Value as string;
			throw new ArgumentException($"Invalid size value: {declaredSize} for value {strValue}");
		}
	}

	internal class FittingVerifier : SizeVerifier {
		public static readonly FittingVerifier Singleton = new FittingVerifier();

		private FittingVerifier() {
		}

		public override void Verify(IDbDataParameter param, IBindValue value) {
			var declaredSize = value.BindMetaData.Size;
			if (!IsSizeShort(param, declaredSize)) {
				return;
			}

			if (param.Value is string strValue) {
				param.Size = strValue.Length;
			}
		}
	}
}
