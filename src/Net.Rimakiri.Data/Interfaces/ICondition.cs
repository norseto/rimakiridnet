// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Collections.Generic;

namespace Net.Rimakiri.Data.Interfaces {
	/// <summary>
	/// SQL条件構文式が実装すべき動作を定義したインターフェイスです。
	/// </summary>
	/// <remarks>
	/// <see cref="ICondition"/>は、<see cref="IFragment"/>とは異なり、
	/// 特定の演算子（<see cref="IConditionOperator"/>）で結合することが
	/// できます。結合された構文式は時にネストすることがあります。例えば、
	/// <code>
	/// A=1 AND (B=1 OR C=1)
	/// </code>
	/// などです。これは、A=1 を示す条件式と、 B=1 と C=1 がOR結合された条件式が
	/// AND結合した状態（構文木）です。結合は、<see cref="CondUtils"/>または
	/// <see cref="ConditionExtension"/> で行います。
	/// </remarks>
	/// <seealso cref="IFragment"/>
	/// <seealso cref="CondUtils"/>
	/// <seealso cref="ConditionExtension"/>
	/// <seealso cref="NNaryCondition"/>
	public interface ICondition : IFragment {
		/// <summary>
		/// 構文のネストを平坦化します。
		/// </summary>
		/// <param name="parentOperator">親演算子</param>
		/// <returns>平坦化可能ならば、そこに含まれる条件。平坦化不能ならばnull</returns>
		/// <remarks>
		/// 平坦化とは、以下のような構文を
		/// <code>A = B AND (A = C AND C = D)</code>
		/// 以下のように構文木の階層を減らすことを示します。
		/// <code>A = B AND A = C AND C = D</code>
		/// </remarks>
		IEnumerable<ICondition> Flatten(IConditionOperator parentOperator);
	}
}

