// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;

namespace Net.Rimakiri.Data.Interfaces {
	/// <summary>
	/// SQL文にバインドされる値の情報を取得するインターフェイスです。
	/// </summary>
	public interface IBindMetaData {
		/// <summary>
		/// 最大長を取得します。
		/// </summary>
		int Size { get; }

		/// <summary>
		/// 小数点以下桁数を取得します。
		/// </summary>
		byte Scale { get; }

		/// <summary>
		/// 有効桁数を取得します。
		/// </summary>
		byte Precision { get; }

		/// <summary>
		/// 値のデータタイプを取得します。
		/// </summary>
		/// <remarks>
		/// 値のデータタイプがnull許容型の場合には、クローズジェネリック
		/// タイプが返却されます。<see cref="IDataWrapper"/>の場合、
		/// そのクラスの型になります。
		/// </remarks>
		/// <remarks>
		/// ここで返却した型に対するデータベースの型が何が使用され
		/// るかについては、<see cref="IDatabaseDialect"/>により
		/// 決定されます。
		/// </remarks>
		Type DataType { get; }

		/// <summary>
		/// NULLが許容されるかを取得します。
		/// </summary>
		bool Nullable { get; }
	}
}
