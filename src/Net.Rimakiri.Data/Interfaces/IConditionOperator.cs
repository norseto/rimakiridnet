// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

namespace Net.Rimakiri.Data.Interfaces {
	/// <summary>
	/// 演算子を示すインターフェイスです。
	/// </summary>
	public interface IConditionOperator {
		/// <summary>
		/// 文字列の値を取得します。
		/// </summary>
		/// <param name="ctx">使用するコンテキスト</param>
		/// <returns>文字列値</returns>
		string ToOpString(IFragmentContext ctx);
	}

//	/// <inheritdoc />
//	/// <summary>
//	/// 2項演算子を示すインターフェイスです。
//	/// </summary>
//	public interface IBinaryOperator : IConditionOperator {
//		// Empty
//	}

	/// <inheritdoc />
	/// <summary>
	/// 多項演算子を示すインターフェイスです。
	/// </summary>
	public interface INNaryOperator : IConditionOperator {
		// Empty
	}
}
