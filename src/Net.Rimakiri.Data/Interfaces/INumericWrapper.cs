﻿// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.
namespace Net.Rimakiri.Data.Interfaces {
	/// <summary>
	/// 有効桁数、小数点以下桁数を持つ<see cref="IDataWrapper"/>インターフェイスです。
	/// </summary>
	/// <remarks>
	/// このインターフェイスを実装したラッパーを使用することで、毎回有効桁数と
	/// 小数点以下桁数を指定する必要がなくなります。
	/// </remarks>
	public interface INumericWrapper : IDataWrapper {
		/// <summary>
		/// 有効桁数を取得します。
		/// </summary>
		byte Precision { get; }

		/// <summary>
		/// 小数点以下桁数を取得します。
		/// </summary>
		byte Scale { get; }
	}
}
