﻿// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using Net.Rimakiri.Data.Util;

namespace Net.Rimakiri.Data.Interfaces {
	/// <summary>
	/// .NETの型とデータベースの型のマッピングを行うクラスのインターフェイスです。
	/// </summary>
	/// <typeparam name="T">データベースの型のタイプ</typeparam>
	/// <seealso cref="TypeMapDatabaseDialect{T}"/>
	public interface IDatabaseTypeMap<out T> {
		/// <summary>
		/// パラメータに対応するデータベースの型を取得します。
		/// </summary>
		/// <param name="parameterName">パラメータ名</param>
		/// <param name="value">パラメータ値情報</param>
		/// <returns>データベースの型</returns>
		T GetDatabaseType(string parameterName, IBindValue value);
		
	}
}
