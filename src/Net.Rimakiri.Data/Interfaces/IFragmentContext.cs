// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

namespace Net.Rimakiri.Data.Interfaces {
	/// <summary>
	/// 文字列化を行う際のコンテキストインターフェイスです。
	/// </summary>
	public interface IFragmentContext {
		/// <summary>
		/// <see cref="IDatabaseDialect"/>を取得します。
		/// </summary>
		IDatabaseDialect DatabaseDialect { get; }

		/// <summary>
		/// 条件階層の深さを取得します。
		/// </summary>
		int Depth { get; }

		/// <summary>
		/// 条件階層の深さをインクリメントします。
		/// </summary>
		void IncrementDepth();

		/// <summary>
		/// 条件階層の深さをデクリメントします。
		/// </summary>
		void DecrementDepth();

		/// <summary>
		/// 呼び出し元の演算子を設定または取得します。
		/// </summary>
		IConditionOperator CondOperator { get; set; }

		/// <summary>
		/// バインドされる値情報を通知します。
		/// </summary>
		/// <param name="value">バインドされる値</param>
		void NotifyWillBound(IBindValue value);
	}
}
