// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Data;

namespace Net.Rimakiri.Data.Interfaces {
	/// <summary>
	/// バインドされた実際の値の文字列長が指定されたサイズよりも短い場合の扱い
	/// を表します。NVarChar以外の場合に使用することは推奨されません。
	/// </summary>
	public enum ShortSizePolicy {
		/// <summary>
		/// 指定されていません
		/// </summary>
		Unspecified = 0,

		/// <summary>
		/// 無視します
		/// </summary>
		Ignore,

		/// <summary>
		/// 長さを調節します
		/// </summary>
		Fit,
		
		/// <summary>
		/// 例外を送出します
		/// </summary>
		Throw
	}

	/// <summary>
	/// データベース製品による差異に関するダイアレクトインターフェイスです。
	/// </summary>
	public interface IDatabaseDialect {
		/// <summary>
		/// 長さが足りない場合のポリシーを取得します。
		/// </summary>
		/// <remarks>
		/// ここで取得されたポリシーに対応する<see cref="IBoundValueVerifier"/>
		/// が使用されることが想定されています。
		/// </remarks>
		/// <seealso cref="BoundValueVerifier"/>
		/// <seealso cref="BoundValueVerifierFactory"/>
		ShortSizePolicy ShortSizePolicy { get; }

		/// <summary>
		/// バインドした値を検証する<see cref="IBoundValueVerifier"/>を
		/// 取得します。
		/// </summary>
		/// <remarks>
		/// ここで<see cref="IBoundValueVerifier"/>が取得できない場合、
		/// <see cref="ShortSizePolicy"/>で取得されたポリシーに対応する
		/// 既定の<see cref="IBoundValueVerifier"/>が使用されることが想定
		/// されています。
		/// </remarks>
		/// <seealso cref="ShortSizePolicy"/>
		/// <seealso cref="BoundValueVerifierFactory"/>
		IBoundValueVerifier BoundValueVerifier { get; }

		/// <summary>
		/// パラメタ名のプレフィクスを取得します。
		/// データベースにより異なる名前付パラメータの違いを吸収します。
		/// </summary>
		/// <seealso cref="NameFor"/>
		string ParameterPrefix { get; }

		/// <summary>
		/// ステートメントパラメータ名を生成します。
		/// 例えば、識別子<c>user</c>に対して、SQL Serverでのパラメータ名は
		/// <c>@user</c>、ORACLEでは、<c>:user</c>などです。
		/// </summary>
		/// <param name="identifier">パラメータ識別子</param>
		/// <returns>クエリに指定するパラメータ名</returns>
		/// <remarks>
		/// <see cref="ParameterPrefix"/>に<paramref name="identifier"/>
		/// を連結した結果が返却されます。
		/// </remarks>
		/// <seealso cref="ParameterPrefix"/>
		string NameFor(string identifier);

		/// <summary>
		/// <see cref="IBindValue"/>パラメータ情報をもとに、
		/// <see cref="IDbDataParameter"/>インスタンスに実際のパラメータを設定します。
		/// </summary>
		/// <param name="target">対象パラメータ</param>
		/// <param name="value">バインド値</param>
		/// <remarks>
		/// 対象パラメータの以下の内容は設定された状態で呼び出されます。
		/// つまり、このメソッドを呼出す場合、事前にこれらのパラメータは設定してから
		/// でなければなりません。
		/// <ul>
		/// <li><see cref="IDataParameter.ParameterName"/></li>
		/// <li><see cref="IDataParameter.Value"/></li>
		/// <li><see cref="IDbDataParameter.Size"/></li>
		/// <li><see cref="IDbDataParameter.Scale"/></li>
		/// <li><see cref="IDbDataParameter.Precision"/></li>
		/// </ul>
		/// </remarks>
		void SetupParameter(IDbDataParameter target, IBindValue value);

		/// <summary>
		/// 現在のタイムスタンプを取得するためのSQLリテラルを取得します。
		/// </summary>
		/// <remarks>
		/// データベースが動作しているタイムゾーンでの時間を取得するためのリテラルです。
		/// </remarks>
		string CurrentTimestampLiteral { get; }

		/// <summary>
		/// 現在のUTCタイムスタンプを取得するためのSQLリテラルを取得します。
		/// </summary>
		/// <remarks>
		/// データベースが動作しているタイムゾーンとは関係なく、UTCでの時間を取得するためのリテラルです。
		/// </remarks>
		string CurrentUtcTimestampLiteral { get; }

		/// <summary>
		/// ダミーテーブルの名称を取得します。ダミーテーブルが存在しない場合、
		/// <see cref="string.Empty"/>を返却します。
		/// </summary>
		string DummyTable { get; }

		/// <summary>
		/// FROMを連結した状態のダミーテーブルの名称を取得します。
		/// ダミーテーブルが存在しない場合、<see cref="string.Empty"/>
		/// を返却します。
		/// </summary>
		string DummyTableWithFrom { get; }
	}
}
