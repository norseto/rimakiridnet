// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Collections.Generic;

namespace Net.Rimakiri.Data.Interfaces {
	/// <summary>
	/// SQL文のパーツを表すインターフェイスです。
	/// </summary>
	/// <remarks>
	/// <see cref="FragmentExtension.Concat"/>により、連結が可能です。
	/// </remarks>
	/// <seealso cref="ICondition"/>
	/// <seealso cref="BasicFragment"/>
	/// <seealso cref="WhereFragment"/>
	public interface IFragment {
		/// <summary>
		/// 有効か否かを取得します。
		/// </summary>
		bool Enabled { get; }

		/// <summary>
		/// 文を生成します。
		/// </summary>
		/// <param name="ctx">使用するコンテキスト</param>
		/// <returns>生成した文</returns>
		string ToLiteralString(IFragmentContext ctx);

		/// <summary>
		/// 内容のイテレーションを取得します。
		/// </summary>
		/// <returns>内容のイテレーション</returns>
		IEnumerable<IFragment> ToEnumerable();
	}
}
