// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

namespace Net.Rimakiri.Data.Interfaces {
	/// <summary>
	/// SQLにバインドされる値を表すインターフェイスです。
	/// </summary>
	public interface IBindValue {
		/// <summary>
		/// 識別子を取得します。
		/// </summary>
		/// <remarks>
		/// ここで返却した型に対するSQL内のパラメータとして何が使用され
		/// るかについては、<see cref="IDatabaseDialect"/>により
		/// 決定されます。
		/// </remarks>
		string Identity { get; }

		/// <summary>
		/// フォーマット済の値を取得します。
		/// </summary>
		object Value { get; }

		/// <summary>
		/// 値を取得します。
		/// </summary>
		object RawValue { get; }

		/// <summary>
		/// 値のフォーマットを取得します。
		/// </summary>
		string Format { get; }

		/// <summary>
		/// エスケープ文字を取得します。
		/// </summary>
		char? Escape { get; }

		/// <summary>
		/// パラメータ情報を取得します。
		/// </summary>
		IBindMetaData BindMetaData { get; }	
	}
}
