// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Data;

namespace Net.Rimakiri.Data.Interfaces {
	/// <summary>
	/// バインドされた値の検証を行うクラスのインターフェイスです。
	/// </summary>
	public interface IBoundValueVerifier {
		/// <summary>
		/// パラメータの検証を行います。
		/// </summary>
		/// <param name="parameter">対象パラメータ</param>
		/// <param name="value">バインドした値情報</param>
		void Verify(IDbDataParameter parameter, IBindValue value);
	}

	/// <summary>
	/// <see cref="IBoundValueVerifier"/>の拡張クラスです。
	/// </summary>
	public static class BoundVerifierEx {
		/// <summary>
		/// パラメータの検証を行います。
		/// </summary>
		/// <param name="verifier">対象<see cref="IBoundValueVerifier"/></param>
		/// <param name="parameter">対象パラメータ</param>
		/// <param name="value">バインドした値情報</param>
		public static void VerifyParameter(this IBoundValueVerifier verifier,
			IDbDataParameter parameter, IBindValue value) {
			if (verifier == null) {
				return;
			}
			verifier.Verify(parameter, value);
		}
	}
}
