// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Data;

namespace Net.Rimakiri.Data.Interfaces {
	/// <summary>
	/// <see cref="IDbCommand"/>に対してパラメータをバインドします。
	/// </summary>
	/// <seealso cref="ValueBinderBuilder.Build"/>
	public interface IValueBinder {
		/// <summary>
		/// 実際に設定されるSQL文を取得します。
		/// </summary>
		string Statement { get; }

		/// <summary>
		/// 実際に設定されるパラメータの数を取得します。
		/// </summary>
		int Count { get; }

		/// <summary>
		/// ステートメントを設定し、パラメータを設定します。
		/// </summary>
		/// <param name="command">対象コマンド</param>
		/// <returns>対象コマンド</returns>
		/// <remarks>
		/// すでに設定されているパラメータはクリアされます。
		/// </remarks>
		IDbCommand Bind(IDbCommand command);

		/// <summary>
		/// 格納されている値を変更します。
		/// </summary>
		/// <param name="identity">値を変更するパラメータ識別子</param>
		/// <param name="value">変更する値</param>
		/// <returns>このインスタンス</returns>
		/// <remarks>
		/// このメソッドを使用して値以外のもの（型や長さ等）を変更すること
		/// はできません。
		/// </remarks>
		/// <seealso cref="BasicCondition.ReplaceValue"/>
		/// <seealso cref="BasicFragment.ReplaceValue"/>
		IValueBinder Replace(string identity, object value);
	}
}
