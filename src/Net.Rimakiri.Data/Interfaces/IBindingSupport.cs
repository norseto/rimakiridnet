// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

namespace Net.Rimakiri.Data.Interfaces {
	/// <summary>
	/// <see cref="IFragment"/>の<see cref="IBindValue"/>によるフィルタです。
	/// </summary>
	/// <param name="value">対象値</param>
	/// <returns>対象値が有効な場合True</returns>
	public delegate bool FragmentFilter(IBindValue value);

	/// <summary>
	/// 値のバインドが可能な<see cref="IFragment"/>です。
	/// </summary>
	/// <typeparam name="T">このインターフェイスを実装するクラスの型</typeparam>
	/// <seealso cref="BasicCondition"/>
	/// <seealso cref="BasicFragment"/>
	public interface IBindingSupport<out T> {
		/// <summary>
		/// 指定の値が含まれている場合、このインスタンスを最終的な条件から
		/// 除外することを示す<see cref="FragmentFilter"/>
		/// フィルタを設定または取得します。
		/// </summary>
		/// <remarks>
		/// ここで指定したフィルタがTrueを返却する値が含まれていた場合、最終的な出力に
		/// このインスタンスは含まれなくなります。例えば、以下の例では、変数<c>name</c>
		/// の値が<c>null</c>でない場合、最終的なSQL文は、
		/// <code>SELECT * FROM T_SAMPLE WHERE ID = @id AND NAME = @name</code>
		/// となりますが、<c>null</c>の場合、最終的なSQL文は、
		/// <code>SELECT * FROM T_SAMPLE WHERE ID = @id</code>
		/// となります。つまり、2番目の<see cref="BasicCondition"/>は出力結果から
		/// 除外されます。
		/// <example>
		/// <code language="C#">
		/// int id = 100;
		/// string name = ...;
		/// var fragment = new SimpleFragment("SELECT * FROM T_SAMPLE WHERE");
		/// var condA = new BasicCondition("ID = {0}", BindValue.Create("id", id));
		/// var condB = new BasicCondition("NAME = {0}", BindValue.Create("name", name, 20)) {
		/// 	ExcludeFilter = v => v.Value == null
		/// };
		/// var statement = fragment.Concat(condA.And(condB));
		/// </code>
		/// </example>
		/// </remarks>
		/// <seealso cref="NotNullCondition"/>
		/// <seealso cref="ValueBinderBuilder"/>
		/// <seealso cref="IValueBinder"/>
		/// <seealso cref="Filters"/>
		FragmentFilter ExcludeFilter { get; set; }

		/// <summary>
		/// パラメータを追加します。
		/// </summary>
		/// <param name="valuesToAdd">追加するパラメータ</param>
		/// <returns>このインスタンス</returns>
		T AddValues(params IBindValue[] valuesToAdd);

		/// <summary>
		/// パラメータを変更します。
		/// </summary>
		/// <param name="value">変更するパラメータ</param>
		/// <returns>このインスタンス</returns>
		T ReplaceValue(IBindValue value);
	}
}
