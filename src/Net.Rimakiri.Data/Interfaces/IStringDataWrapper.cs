// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

namespace Net.Rimakiri.Data.Interfaces {
	/// <summary>
	/// 文字列型を保持する<see cref="IDataWrapper"/>インターフェイスです。
	/// </summary>
	public interface IStringDataWrapper : IDataWrapper {
		/// <summary>
		/// 指定された文字列値を持つインスタンスを生成します。
		/// </summary>
		/// <param name="value">文字列値</param>
		/// <returns>インスタンス</returns>
		object FromString(string value);

		/// <summary>
		/// 文字列長を取得します。
		/// </summary>
		int Size { get; }
	}
}
