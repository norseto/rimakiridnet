// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Collections.Generic;
using System.Linq;
using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data {
	/// <summary>
	/// パラメータと値のバインドを行うエンジンクラスです。
	/// </summary>
	/// <inheritdoc />
	internal class BindingEngine : IBindingSupport<object> {
		private readonly IList<IBindValue> values = new List<IBindValue>();
		private readonly string format;
		private string text;
		private bool modified;
		private bool enabled = true;

		/// <summary>
		/// 有効か否かを取得します。
		/// </summary>
		public bool Enabled {
			get {
				if (!modified) {
					return enabled;
				}
				modified = false;
				enabled = CheckEnabled();
				return enabled;
			}
		}

		/// <inheritdoc />
		public FragmentFilter ExcludeFilter { get; set; }

		/// <summary>
		/// テキストフォーマットと値を指定してインスタンスを初期化します。
		/// </summary>
		/// <param name="format">テキストのフォーマット。{0}は最初の値にマッピングされます</param>
		/// <param name="values">値リスト</param>
		public BindingEngine(string format, params IBindValue[] values) {
			this.format = format;
			if (values == null) {
				return;
			}
			AddParamValues(values);
		}

		/// <inheritdoc />
		public object AddValues(params IBindValue[] valuesToAdd) {
			AddParamValues(valuesToAdd);
			return this;
		}

		/// <inheritdoc />
		public object ReplaceValue(IBindValue value) {
			if (value == null) {
				return this;
			}
			var index = -1;
			var identity = value.Identity;
			for (int i = 0, l = values.Count; i < l; i++) {
				if (!string.Equals(values[i].Identity, identity)) {
					continue;
				}
				index = i;
				break;
			}
			if (index >= 0) {
				values[index] = new BindValue(value);
				InitializeState();
			}
			return this;
		}

		private void InitializeState() {
			modified = true;
			text = null;
		}

		/// <summary>
		/// 有効か否かを判定します。有効でない場合には、条件の出力が行われません。
		/// </summary>
		/// <returns>有効な場合true</returns>
		private bool CheckEnabled() {
			var filter = ExcludeFilter;
			if (filter == null) {
				return true;
			}
			var formatter = new CustomStatementFormatter();
			// ReSharper disable once ReturnValueOfPureMethodIsNotUsed
			string.Format(formatter, format,
							values.Select(v => v.Identity)
								.Cast<object>().ToArray());

			foreach (var bindValue in values) {
				if (!formatter.Contains(bindValue.Identity)) {
					continue;
				}
				if (ExcludeFilter(bindValue)) {
					return false;
				}
			}
			return true;
		}

		private void AddParamValues(params IBindValue[] valuesToAdd) {
			var changed = false;
			foreach (var value in valuesToAdd.Where(c => c != null)) {
				values.Add(value);
				changed = true;
			}
			if (changed) {
				InitializeState();
			}
		}

		public string ToLiteralString(IFragmentContext ctx) {
			if (!Enabled) {
				return null;
			}
			var dialect = ctx.DatabaseDialect;
			var formatter = new CustomStatementFormatter();
			var result = string.Format(formatter, format,
							values.Select(v => dialect.NameFor(v.Identity))
								.Cast<object>().ToArray());
			text = result;

			foreach (var bindValue in values) {
				if (formatter.Contains(dialect.NameFor(bindValue.Identity))) {
					ctx.NotifyWillBound(bindValue);
				}
			}
			return text;
		}
	}
}
