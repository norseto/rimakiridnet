// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Collections.Generic;
using System.Linq;
using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data {
	/// <summary>
	/// 条件構文生成時のコンテキストです。
	/// </summary>
	/// <inheritdoc />
	internal class FragmentContextImpl : IFragmentContext {
		private readonly IList<IBindValue> values = new List<IBindValue>();

		/// <summary>
		/// バインドされる値を取得します。
		/// </summary>
		public IEnumerable<IBindValue> BindValues => values.AsEnumerable();

		#region Implementation of IFragmentContext
		/// <summary>
		/// <see cref="IDatabaseDialect"/>を取得または設定します。
		/// </summary>
		/// <inheritdoc />
		public IDatabaseDialect DatabaseDialect { get; set; }

		/// <summary>
		/// 条件階層の深さを取得します。
		/// </summary>
		/// <inheritdoc />
		public int Depth { get; private set; }

		/// <summary>
		/// 条件階層の深さをインクリメントします。
		/// </summary>
		/// <inheritdoc />
		public void IncrementDepth() {
			Depth++;
		}

		/// <summary>
		/// 条件階層の深さをデクリメントします。
		/// </summary>
		/// <inheritdoc />
		public void DecrementDepth() {
			if (Depth > 0) {
				Depth--;
			}
		}

		/// <inheritdoc />
		public IConditionOperator CondOperator { get; set; }

		/// <summary>
		/// バインドされる値情報を通知します。
		/// </summary>
		/// <param name="value">バインドされる値</param>
		/// <inheritdoc />
		public void NotifyWillBound(IBindValue value) {
			values.Add(value);
		}
		#endregion
	}
}
