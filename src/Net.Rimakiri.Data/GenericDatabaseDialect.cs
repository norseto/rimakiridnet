// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Data;
using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data {
	/// <summary>
	/// 一般的なデータベースに対する<see cref="IDatabaseDialect"/>クラスです。
	/// </summary>
	/// <inheritdoc />
	/// <seealso cref="GenericDbTypeMap"/>
	public class GenericDatabaseDialect : IDatabaseDialect {
		private IDatabaseTypeMap<DbType> typeMap = new GenericDbTypeMap();
		private IBoundValueVerifier boundValueVerifier;

		/// <summary>
		/// 使用する<see cref="IDatabaseTypeMap{DbType}"/>を取得または設定します。
		/// </summary>
		public IDatabaseTypeMap<DbType> TypeMap {
			get => typeMap;
			set {
				if (value != null) {
					typeMap = value;
				}
			}
		}

		#region Implementation of IDatabaseDialect
		/// <inheritdoc />
		public ShortSizePolicy ShortSizePolicy { get; set; }

		/// <inheritdoc />
		public IBoundValueVerifier BoundValueVerifier {
			get => boundValueVerifier ?? BoundValueVerifierFactory.GetBoundValueVerifier(ShortSizePolicy);
			set => boundValueVerifier = value;
		}

		/// <inheritdoc />
		public string CurrentTimestampLiteral => "NOW()";

		/// <inheritdoc />
		public string CurrentUtcTimestampLiteral => throw new InvalidOperationException("Not Supported");

		/// <inheritdoc />
		public string DummyTable => string.Empty;

		/// <inheritdoc />
		public string DummyTableWithFrom => string.Empty;

		/// <inheritdoc />
		/// <summary>
		/// このクラスでは"@"が使用されます。
		/// </summary>
		public string ParameterPrefix { get; set; } = "@";

		/// <inheritdoc />
		public string NameFor(string identifier) {
			return ParameterPrefix + identifier;
		}

		/// <inheritdoc />
		public void SetupParameter(IDbDataParameter target, IBindValue value) {
			var parameterName = NameFor(value.Identity);
			var dbType = TypeMap.GetDatabaseType(parameterName, value);
			target.DbType = dbType;
		}
		#endregion
	}
}
