// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Collections.Generic;
using System.Linq;
using System.Text;
using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data {
	/// <inheritdoc cref="ICondition" />
	/// <summary>
	/// 複数の条件構文を指定のオペレータにより多項式結合した<see cref="ICondition"/>です。
	/// </summary>
	/// <seealso cref="INNaryOperator"/>
	/// <seealso cref="CondUtils"/>
	/// <seealso cref="ConditionExtension"/>
	public class NNaryCondition : ICondition, IStructureObserver, IStructureSubject {
		/// <summary>
		/// 結合演算子
		/// </summary>
		private readonly INNaryOperator condOperator;
		private readonly List<ICondition> children = new List<ICondition>();
		private readonly List<ICondition> flatten = new List<ICondition>();

		private IEnumerable<ICondition> Conditions
			=> flatten.Count < 1 ? children : flatten;

		/// <summary>
		/// 条件が有効か否かを取得します。
		/// </summary>
		/// <inheritdoc />
		public bool Enabled {
			get { return Conditions.FirstOrDefault(c => c.Enabled) != null; }
		}

		/// <summary>
		/// 結合演算子を指定して<see cref="NNaryCondition"/>クラスの
		/// インスタンスを初期化します。
		/// </summary>
		/// <param name="condOperator">演算子</param>
		public NNaryCondition(INNaryOperator condOperator) {
			this.condOperator = condOperator;
		}

		/// <summary>
		/// 条件を追加します。
		/// </summary>
		/// <param name="condition">追加する条件</param>
		/// <returns>自身のインスタンス</returns>
		public NNaryCondition Add(ICondition condition) {
			if (condition != null) {
				children.Add(condition);
				if (condition is IStructureSubject subject) {
					subject.StructureObserver = this;
				}
			}
			return this;
		}

		/// <inheritdoc />
		/// <summary>
		/// 条件構文文字列を生成します。
		/// </summary>
		/// <param name="ctx">使用するコンテキスト</param>
		/// <returns>生成されたSQL文の一部(条件文)</returns>
		public string ToLiteralString(IFragmentContext ctx) {
			var cnt = 0;
			StringBuilder sb = new StringBuilder();

			var hasChildren = Conditions.Count(c => c.Enabled) > 1;
			var callerOperator = ctx.CondOperator;
			var needsBrackets = ctx.Depth > 0 && hasChildren && callerOperator != condOperator;

			if (needsBrackets) {
				sb.Append("(");
			}

			if (hasChildren) {
				ctx.IncrementDepth();
				ctx.CondOperator = condOperator;
			}

			try {
				foreach (var condition in Conditions.Where(c => c.Enabled)) {
					if (cnt++ > 0) {
						sb.Append(condOperator.ToOpString(ctx));
					}
					sb.Append(condition.ToLiteralString(ctx));
				}
			}
			finally {
				if (hasChildren) {
					ctx.DecrementDepth();
					ctx.CondOperator = callerOperator;
				}
			}
			if (needsBrackets) {
				sb.Append(")");
			}
			return sb.ToString();
		}

		/// <inheritdoc />
		public IEnumerable<ICondition> Flatten(IConditionOperator parentOperator) {
			var candidate = flatten;
			candidate.Clear();
			for (int i = 0, l = children.Count; i < l; i++) {
				var child = children[i];
				var flattened = child.Flatten(condOperator);
				if (flattened == null) {
					candidate.Add(child);
				}
				else {
					candidate.AddRange(flattened);
				}
			}

			return parentOperator == condOperator ? flatten : ToCondEnumerable();
		}

		private IEnumerable<ICondition> ToCondEnumerable() {
			yield return this;
		}

		#region Implementation of IFragment
		/// <inheritdoc />
		public IEnumerable<IFragment> ToEnumerable() {
			yield return this;
		}
		#endregion

		#region Implementation of IStructureObserver
		/// <inheritdoc />
		public void NotifyStructureMayChanged(IFragment fragment) {
			if (children.FirstOrDefault(f => f == fragment) == null) {
				return;
			}

			Flatten(null);
			StructureObserver?.NotifyStructureMayChanged(this);
		}
		#endregion

		#region Implementation of IStructureSubject
		/// <inheritdoc />
		/// <summary>
		/// 更新の変更通知先を設定します。
		/// </summary>
		public IStructureObserver StructureObserver { private get; set; }
		#endregion
	}
}
