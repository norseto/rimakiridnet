// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Runtime.CompilerServices;
using Net.Rimakiri.Data.Interfaces;
using Net.Rimakiri.Data.Util;

namespace Net.Rimakiri.Data {
	/// <summary>
	/// この名前空間には、データベースに対する処理を行うためのフレームワーク
	/// が含まれています。
	/// </summary>
	[CompilerGenerated]
	internal class NamespaceGroupDoc {
		// Empty
	}

	/// <summary>
	/// この名前空間には、データベースに対する処理を行うためのフレームワーク
	/// が含まれています。
	/// </summary>
	/// <remarks>
	/// 業務ではO/Rマッピングでは対応しにくいデータベースアクセスを実装する
	/// 際により簡易かつ循環的複雑度を高くしないプログラムが書けることを目指して
	/// 作成されています。例えば、以下のような機能です。
	/// <list type="table">
	/// <listheader>  
	/// <term>状況</term>  
	/// <description>このフレームワークでの対応</description>  
	/// </listheader>  
	/// 
	/// <item>
	/// <term>パラメタがnullでないときだけ条件を付ける</term>  
	/// <description>
	/// <see cref="NotNullCondition"/>を使用します。パラメタがnullの場合の
	/// SQLからの除外はフレームワーク側で実施します。
	/// </description>  
	/// </item>
	/// 
	/// <item>
	/// <term>AND条件によってはカッコを付ける必要がある</term>  
	/// <description>
	/// <see cref="ConditionExtension"/>や<see cref="CondUtils"/>を使用します。
	/// 条件の結合状況に従って自動でカッコが付与されます。
	/// </description>  
	/// </item>
	/// 
	/// <item>  
	/// <term>大量にあるバインド変数のタイプを指定する必要がある</term>  
	/// <description>
	/// <see cref="IDatabaseDialect"/>で使用しているデータの型に基づいて
	/// 自動で設定します。
	/// </description>  
	/// </item>
	/// 
	/// <item>  
	/// <term>IN句を作るのが面倒</term>  
	/// <description>
	/// <see cref="InExpander{T}"/>を使用して生成します。
	/// </description>  
	/// </item>
	/// 
	/// <item>  
	/// <term>検索結果からモデルのプロパティに設定するのが面倒</term>  
	/// <description>
	/// <see cref="DbDataReaderMapper{TModel}"/>を使用してモデルを生成し、
	/// プロパティに設定します。
	/// </description>  
	/// </item>
	/// 
	/// </list>
	/// </remarks>
	/// <remarks>
	/// この名前空間のクラスを使用することで、データベース製品に非依存な
	/// SQLの実行を可能とします。以下に例を記載します。
	/// <example>
	/// <code language="C#">
	///	public void DoBind(IDatabaseDialect dialect, DbCommand command,
	///						string name, string address) {
	///		var select = new SimpleFragment("SELECT * FROM SAMPLE");
	///		var cond1 = new BasicCondition("NAME = {0}",
	///						BindValue.Create("name", name, 20));
	///		var cond2 = new NotNullCondition("ADDRESS LIKE {0}",
	///						BindValue.WithFormat("address",
	///							BindValue.ForwardMatch, address, 20));
	///		var statement = select.Concat(new WhereFragment(cond1.And(cond2)));
	///		var builder = new ValueBinderBuilder(statement);
	///		var binder = builder.Build(dialect);
	///
	///		binder.Bind(command);
	///	}
	/// </code>
	/// </example>
	/// ここで、<c>address</c>の内容がnullではない場合には、SQL Serverでは、
	/// <code>
	/// SELECT * FROM SAMPLE WHERE NAME = @name AND ADDRESS LIKE @address
	/// </code>
	/// がSQL文として生成され、ORACLEでは、
	/// <code>
	/// SELECT * FROM SAMPLE WHERE NAME = :name AND ADDRESS LIKE :address
	/// </code>
	/// が生成されて、パラメタとともに設定されます。このようなデータベースの差異は
	/// <see cref="IDatabaseDialect"/>により吸収されます。
	/// さらに、<c>address</c>がnullの場合に生成されるSQL文は、SQL Serverでは、
	/// <code language="SQL Server">
	/// SELECT * FROM SAMPLE WHERE NAME = @name
	/// </code>
	/// となり、ORACLEでは
	/// <code>
	/// SELECT * FROM SAMPLE WHERE NAME = :name
	/// </code>
	/// となります。これは、<c>cond2</c>で<see cref="NotNullCondition"/>が
	/// 使用されているためです。このクラスは値がnullの場合には、SQL文として出力
	/// されません。また、SQLのパラメータとしてもバインドされません。
	/// </remarks>
	/// <seealso cref="IDatabaseDialect"/>
	/// <seealso cref="SimpleFragment"/>
	/// <seealso cref="BasicCondition"/>
	/// <seealso cref="NotNullCondition"/>
	/// <seealso cref="WhereFragment"/>
	/// <seealso cref="ConditionExtension"/>
	/// <seealso cref="FragmentExtension"/>
	/// <seealso cref="BindValue"/>
	/// <seealso cref="ValueBinderBuilder"/>
	/// <seealso cref="IValueBinder"/>
	[CompilerGenerated]
	internal class NamespaceDoc {
		// Empty
	}
}

namespace Net.Rimakiri.Data.Interfaces {
	/// <summary>
	/// この名前空間には、データベースに対する処理を行うためのフレームワーク
	/// のインターフェイスが含まれています。
	/// </summary>
	[CompilerGenerated]
	internal class NamespaceDoc {
		// Empty
	}
}

namespace Net.Rimakiri.Data.Util {
	/// <summary>
	/// この名前空間には、データベースに対する処理を行うためのユーティリティ
	/// クラスが含まれています。
	/// </summary>
	[CompilerGenerated]
	internal class NamespaceDoc {
		// Empty
	}
}

namespace Net.Rimakiri.Data.TypeWrappers {
	/// <summary>
	/// この名前空間には、データベースに対するデータ型を明確に指定するための
	/// 値ラッパークラスが含まれています。
	/// </summary>
	[CompilerGenerated]
	internal class NamespaceDoc {
		// Empty
	}
}
