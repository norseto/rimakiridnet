// ==============================================================================
//     Net.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Collections.Generic;
using System.Linq;
using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Data {
	/// <summary>
	/// 条件構文の結合処理ユーティリティクラスです。
	/// </summary>
	/// <seealso cref="ICondition"/>
	/// <seealso cref="INNaryOperator"/>
	/// <seealso cref="NNaryCondition"/>
	public static class CondUtils {
		/// <summary>
		/// 条件を結合して、新しい条件を生成します。
		/// </summary>
		/// <param name="op">指定された条件の結合条件</param>
		/// <param name="conds">結合する条件</param>
		/// <returns>結合された条件構文。結合対象が存在しなかった場合null</returns>
		public static ICondition Join(INNaryOperator op, IEnumerable<ICondition> conds) {
			if (conds == null) {
				return null;
			}
			var toAdd = conds.Where(it => it != null).ToArray();

			switch (toAdd.Length) {
			case 1:
				return toAdd[0];
			case 0:
				return null;
			}

			var cond = new NNaryCondition(op);
			foreach (var child in toAdd) {
				cond.Add(child);
			}
			cond.Flatten(null);
			return cond;
		}

		/// <summary>
		/// 条件を結合して、新しい条件を生成します。
		/// </summary>
		/// <param name="op">指定された条件の結合条件</param>
		/// <param name="conds">結合する条件</param>
		/// <returns>結合された条件構文。結合対象が存在しなかった場合null</returns>
		/// <seealso cref="AndJoin"/>
		/// <seealso cref="OrJoin"/>
		/// <seealso cref="ConditionExtension.And"/>
		/// <seealso cref="ConditionExtension.Or"/>
		public static ICondition Join(INNaryOperator op, params ICondition[] conds) {
			return conds == null ? null : Join(op, conds.AsEnumerable());
		}

		/// <summary>
		/// AND条件で条件を結合します。
		/// </summary>
		/// <param name="conds">結合する条件</param>
		/// <returns>結合された条件構文。結合対象が存在しなかった場合null</returns>
		/// <remarks>
		/// 第1パラメタを<see cref="AndOp"/>、<paramref name="conds"/>を第2パラメタ
		/// とした、<see cref="Join(INNaryOperator,ICondition[])"/>へのショートカットです。
		/// </remarks>
		/// <seealso cref="Join(INNaryOperator,ICondition[])"/>
		/// <seealso cref="OrJoin"/>
		/// <seealso cref="ConditionExtension.And"/>
		/// <seealso cref="ConditionExtension.Or"/>
		public static ICondition AndJoin(params ICondition[] conds) {
			return Join(AndOp.Singleton, conds);
		}

		/// <summary>
		/// OR条件で条件を結合します
		/// </summary>
		/// <param name="conds">結合する条件</param>
		/// <returns>結合された条件構文。結合対象が存在しなかった場合null</returns>
		/// <remarks>
		/// 第1パラメタを<see cref="OrOp"/>、<paramref name="conds"/>を第2パラメタ
		/// とした、<see cref="Join(INNaryOperator,ICondition[])"/>へのショートカットです。
		/// </remarks>
		/// <seealso cref="Join(INNaryOperator,ICondition[])"/>
		/// <seealso cref="OrJoin"/>
		/// <seealso cref="ConditionExtension.And"/>
		/// <seealso cref="ConditionExtension.Or"/>
		public static ICondition OrJoin(params ICondition[] conds) {
			return Join(OrOp.Singleton, conds);
		}
	}

	/// <summary>
	/// <see cref="ICondition"/>の拡張クラスです。
	/// </summary>
	public static class ConditionExtension {
		private static IEnumerable<ICondition> Merge(ICondition first, params ICondition[] conds) {
			if (first != null) {
				yield return first;
			}
			if (conds == null) {
				yield break;
			}
			foreach (var cond in conds) {
				yield return cond;
			}
		}

		/// <summary>
		/// <see cref="ICondition"/>をAND結合します。
		/// </summary>
		/// <param name="cond">左辺<see cref="ICondition"/></param>
		/// <param name="conds">右辺<see cref="ICondition"/></param>
		/// <returns>結合された条件構文。結合対象が存在しなかった場合null</returns>
		/// <remarks>
		/// このメソッドは、<see cref="CondUtils.AndJoin"/>のショートカットです。
		/// </remarks>
		/// <seealso cref="Or"/>
		/// <seealso cref="CondUtils.Join(INNaryOperator,ICondition[])"/>
		/// <seealso cref="CondUtils.OrJoin"/>
		/// <seealso cref="CondUtils.AndJoin"/>
		public static ICondition And(this ICondition cond, params ICondition[] conds) {
			return CondUtils.Join(AndOp.Singleton, Merge(cond, conds));
		}

		/// <summary>
		/// <see cref="ICondition"/>をOR結合します。
		/// </summary>
		/// <param name="cond">左辺<see cref="ICondition"/></param>
		/// <param name="conds">右辺<see cref="ICondition"/></param>
		/// <returns>結合された条件構文。結合対象が存在しなかった場合null</returns>
		/// <remarks>
		/// 左辺に右辺が順に結合されます（左結合）。
		/// 例のコードでは、<c>stmt1</c>の結果は、<code>(A=1 AND B=2) OR (C=3 AND D=4)</code>
		/// となりますが、<c>stmt2</c>は、<code>((A=1 AND B=2) OR C=3) AND D=4</code>となります。
		/// 分りにくいと思われる場合には、<see cref="CondUtils.AndJoin"/>、
		/// <see cref="CondUtils.OrJoin"/>を明示的に使用するとよいでしょう。
		/// <example>
		/// <code language="C#">
		/// var condA = new BasicCondition("A=1");
		/// var condB = new BasicCondition("B=2");
		/// var condC = new BasicCondition("C=3");
		/// var condD = new BasicCondition("D=4");
		/// 
		/// var stmt1 = condA.And(condB).Or(condC.And(condD)).ToLiteralString(ctx)
		/// var stmt2 = condA.And(condB).Or(condC).And(condD).ToLiteralString(ctx)
		/// </code>
		/// </example>
		/// </remarks>
		/// <remarks>
		/// このメソッドは、<see cref="CondUtils.OrJoin"/>のショートカットです。
		/// </remarks>
		/// <seealso cref="And"/>
		/// <seealso cref="CondUtils.Join(INNaryOperator,ICondition[])"/>
		/// <seealso cref="CondUtils.OrJoin"/>
		/// <seealso cref="CondUtils.AndJoin"/>
		public static ICondition Or(this ICondition cond, params ICondition[] conds) {
			return CondUtils.Join(OrOp.Singleton, Merge(cond, conds));
		}
	}
	
	/// <summary>
	/// <see cref="IFragment"/>の拡張クラスです。
	/// </summary>
	/// <seealso cref="IFragment"/>
	/// <seealso cref="FragmentCollection"/>
	public static class FragmentExtension {
		/// <summary>
		/// 全ての<see cref="IFragment"/>を結合した新しい<see cref="IFragment"/>
		/// を作成します。
		/// </summary>
		/// <param name="lhs">左辺</param>
		/// <param name="rhs">右辺</param>
		/// <returns>結合されたフラグメント。結合対象が存在しなかった場合null</returns>
		/// <remarks>
		/// 結合元の<paramref name="lhs"/>や<paramref name="rhs"/>のインスタンスは何も
		/// 変更されません。
		/// </remarks>
		public static IFragment Concat(this IFragment lhs, params IFragment[] rhs) {
			if (lhs == null && rhs == null) {
				return null;
			}
			var ret = new FragmentCollection();
			if (lhs != null) {
				ret.Append(lhs.ToEnumerable());
			}
			if (rhs != null) {
				foreach (var fragment in rhs.Where(c => c != null)) {
					ret.Append(fragment.ToEnumerable());
				}
			}
			return ret.Count < 1 ? null : ret;
		}
	}
}
