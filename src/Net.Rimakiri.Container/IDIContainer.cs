// ==============================================================================
//     Net.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using Microsoft.Practices.Unity;
using Net.Rimakiri.Container.Extension;

namespace Net.Rimakiri.Container {
	/// <summary>
	/// DIコンテナが提供する機能を定義したインターフェイスです。
	/// </summary>
	/// <remarks>
	/// 現在、Unityを使用していますが、以下の点につき拡張されています。
	/// <list type="bullet">
	/// <item><description>
	/// ビルドアップ時に、<see cref="IDIContainer"/>自体も可能であれば依存性注入
	/// が行われます。
	/// </description></item>
	/// <item><description>
	/// 名称を指定せずにインスタンスを取得した場合、型が合致すれば名称付の
	/// インスタンスも取得されます（デフォルトの
	/// <a href="https://msdn.microsoft.com/en-us/library/ff648512.aspx">
	/// Microsoft Unity Application Block</a>では、名称付きのインスタンスは
	/// 名称を指定する必要があります）。
	/// </description></item>
	/// <item><description>
	/// 名称を指定してインスタンスを取得した場合、その名称のレジストリで定義された
	/// 型が取得時に指定したタイプの派生クラスの場合にもインスタンスとして取得されます
	/// （デフォルトの<a href="https://msdn.microsoft.com/en-us/library/ff648512.aspx">
	/// Microsoft Unity Application Block</a>では、定義された型で取得する必要が
	/// あります）。
	/// </description></item>
	/// </list>
	/// </remarks>
	/// <seealso cref="DIContainerRegistry"/>
	public interface IDIContainer : IDisposable {
		/// <summary>
		/// オブジェクトを取得します。
		/// </summary>
		/// <typeparam name="T">取得するオブジェクトの型</typeparam>
		/// <param name="name">名称</param>
		/// <returns>オブジェクト</returns>
		T Resolve<T>(string name);

		/// <summary>
		/// オブジェクトを取得します。
		/// </summary>
		/// <typeparam name="T">取得するオブジェクトの型</typeparam>
		/// <returns>オブジェクト</returns>
		T Resolve<T>();

		/// <summary>
		/// オブジェクトを取得します。取得に失敗した場合、
		/// 型のデフォルト値を返却します。
		/// </summary>
		/// <typeparam name="T">取得するオブジェクトの型</typeparam>
		/// <param name="name">名称</param>
		/// <returns>オブジェクト</returns>
		T TryResolve<T>(string name);

		/// <summary>
		/// オブジェクトを取得します。取得に失敗した場合、
		/// 型のデフォルト値を返却します。
		/// </summary>
		/// <typeparam name="T">取得するオブジェクトの型</typeparam>
		/// <returns>オブジェクト</returns>
		T TryResolve<T>();

		/// <summary>
		/// 依存性注入を行います。
		/// </summary>
		/// <param name="type">対象オブジェクトの型</param>
		/// <param name="target">対象オブジェクト</param>
		void BuildUp(Type type, object target);
	}

	internal sealed class UnityWrapper : IDIContainer {
		private readonly IUnityContainer origin;
		private readonly ResolverOverride[] overrides;

		#region Constructor(s)
		public UnityWrapper(IUnityContainer container) {
			origin = container;
			container.AddExtension(new TolerantResolveExtension());
			overrides = new ResolverOverride[] { new DIContainerResolveOverride(this) };
		}
		#endregion

		#region Implements IDisposable
		public void Dispose() {
			// Empty
		}
		#endregion

		#region Implements IDIContainer
		public T Resolve<T>(string name) {
			try {
				return origin.Resolve<T>(name);
			}
			catch (Exception e) {
				throw new DIContainerException(e);
			}
		}

		public T Resolve<T>() {
			try {
				return origin.Resolve<T>();
			}
			catch (Exception e) {
				throw new DIContainerException(e);
			}
		}

		public T TryResolve<T>(string name) {
			try {
				return origin.Resolve<T>(name);
			}
			catch (Exception) {
				return default(T);
			}
		}

		public T TryResolve<T>() {
			try {
				return origin.Resolve<T>();
			}
			catch (Exception) {
				return default(T);
			}
		}

		public void BuildUp(Type type, object existing) {
			origin.BuildUp(type, existing, overrides);
		}
		#endregion

		#region Customize Object
		public override int GetHashCode() {
			return (origin != null ? origin.GetHashCode() : 0);
		}
		#endregion
	}
}
