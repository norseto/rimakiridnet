// ==============================================================================
//     Net.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.ObjectBuilder;

namespace Net.Rimakiri.Container {
	/// <summary>
	/// DIコンテナによるインジェクションマーカとなる属性です。
	/// この属性が付属したプロパティはDIコンテナによるビルドアップ
	/// 時に依存性注入の対象となります。
	/// </summary>
	/// <inheritdoc cref="DependencyResolutionAttribute"/>
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Parameter)]
	public class DIDependAttribute : DependencyResolutionAttribute {
		private readonly string name;

		/// <summary>
		/// <see cref="DIDependAttribute"/>クラスのインスタンス
		/// を初期化します。
		/// </summary>
		public DIDependAttribute()
			: this(null) {
			// Empty
		}

		/// <summary>
		/// 名称を指定して<see cref="DIDependAttribute"/>クラスのインス
		/// タンスを初期化します。
		/// </summary>
		/// <param name="name">コンテナに登録された名称</param>
		public DIDependAttribute(string name) {
			this.name = name;
		}

		/// <summary>
		/// リゾルバポリシを取得します。
		/// </summary>
		/// <param name="typeToResolve">解決する型</param>
		/// <returns>解決する型と名称のリゾルバポリシ</returns>
		public override IDependencyResolverPolicy CreateResolver(Type typeToResolve) {
			return new NamedTypeDependencyResolverPolicy(typeToResolve, name);
		}
  }
}
