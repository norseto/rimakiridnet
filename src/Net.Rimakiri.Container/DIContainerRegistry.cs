// ==============================================================================
//     Net.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Unity;
using Net.Rimakiri.Container.Config;
using Net.Rimakiri.Container.Properties;

namespace Net.Rimakiri.Container {
	/// <inheritdoc />
	/// <summary>
	/// レジストリエントリ
	/// </summary>
	internal sealed class ContainerRegistryEntry : IDisposable {
		public IUnityContainer UnityContainer { get; private set; }
		public IDIContainer DIContainer { get; private set; }

		/// <summary>
		/// インスタンスを初期化する
		/// </summary>
		/// <param name="unity">ラップするコンテナ</param>
		public ContainerRegistryEntry(IUnityContainer unity) {
			UnityContainer = unity;
			DIContainer = (unity == null) ? (IDIContainer)new DummyContainer()
									: new UnityWrapper(unity);
		}

		#region Implementation of IDisposable
		public void Dispose() {
			var target = UnityContainer;
			UnityContainer = null;
			DIContainer = null;
			target?.Dispose();
		}
		#endregion
	}

	internal class DummyContainer : IDIContainer {
		#region Implementation of IDIContainer
		public void BuildUp(Type type, object target) {
			// Empty
		}
		public T Resolve<T>() {
			throw new DIContainerException(Resources.NotConfigured,
							new ConfigurationErrorsException(Resources.NotConfigured));
		}
		public T Resolve<T>(string name) {
			throw new DIContainerException(Resources.NotConfigured,
							new ConfigurationErrorsException(Resources.NotConfigured));
		}
		public T TryResolve<T>(string name) {
			throw new DIContainerException(Resources.NotConfigured,
							new ConfigurationErrorsException(Resources.NotConfigured));
		}
		public T TryResolve<T>() {
			throw new DIContainerException(Resources.NotConfigured,
							new ConfigurationErrorsException(Resources.NotConfigured));
		}
		#endregion

		#region Implementation of IDisposable
		public void Dispose() {
			// Empty
		}
		#endregion
	}

	/// <summary>
	/// DIコンテナのレジストリです。設定ファイルに定義された複数のDIコンテナから指定のコンテナを
	/// 取得します。
	/// </summary>
	/// <remarks>
	/// DIコンテナには親子関係を指定でき、親コンテナが指定されていない場合には、
	/// ルートコンテナ(<see cref="RootContainer"/>)が親コンテナとなります。
	/// 子コンテナでは親コンテナの内容をオーバライドします。また、複数の子コンテナは相互に独立して
	/// います。DIコンテナを使用する上での以下のような問題を解消するためにデザインされています。
	/// <list type="table">
	/// <listheader>
	/// <term>問題</term>
	/// <description>フレームワークでの対処</description>
	/// </listheader>
	/// 
	/// <item>
	/// <term>一部の処理だけ設定を変えたい</term>
	/// <description>
	/// 共通のルートコンテナを定義し、一部の処理のみをオーバライドしたコンテナを定義します。
	/// </description>
	/// </item>
	/// 
	/// <item>
	/// <term>複数の開発者が担当ごとに開発する</term>
	/// <description>
	/// 子コンテナは担当者ごとに別の設定ファイルを使用できます。
	/// </description>
	/// </item>
	/// 
	/// <item>
	/// <term>コンテナ設定が肥大化する</term>
	/// <description>
	/// 子コンテナの設定を適宜別ファイルとして分割して定義します。
	/// </description>
	/// </item>
	/// 
	/// <item>
	/// <term>一部の処理だけ設定を変えた、同じ設定のコンテナが複数存在する</term>
	/// <description>
	/// 一つの子コンテナに対するエイリアスとしてその他のコンテナを定義します。
	/// </description>
	/// </item>
	/// </list>
	/// 具体的な設定方法については、<see cref="ConfigSection"/>を参照してください。
	/// </remarks>
	/// <seealso cref="ConfigSection"/>
	/// <seealso cref="IDIContainer"/>
	public sealed class DIContainerRegistry : IDisposable {
		/// <summary>
		/// 使用する<see cref="ConfigurationLoader"/>を取得または設定します。
		/// </summary>
		public IConfigurationLoader ConfigurationLoader { get; set; }

		private ContainerRegistryEntry Root { get; set; }
		private readonly IDictionary<string, ContainerRegistryEntry> containerDic
					= new Dictionary<string, ContainerRegistryEntry>();

		/// <summary>
		/// ルートコンテナを取得します。
		/// </summary>
		public IDIContainer RootContainer => Root?.DIContainer;

		/// <summary>
		/// 全ての設定をロードして、<see cref="DIContainerRegistry"/>クラス
		/// のインスタンスを生成します。
		/// </summary>
		/// <param name="config">設定ファイル</param>
		/// <param name="loader">使用するローダ</param>
		/// <returns>生成されたインスタンス</returns>
		public static List<DIContainerRegistry> LoadAll(string config = null,
						IConfigurationLoader loader = null) {
			return LoadSeq(config, loader).ToList();
		}

		/// <summary>
		/// 最初の設定をロードして、<see cref="DIContainerRegistry"/>クラス
		/// のインスタンスを生成します。
		/// </summary>
		/// <param name="config">設定ファイル</param>
		/// <param name="loader">使用するローダ</param>
		/// <returns>生成されたインスタンス</returns>
		public static DIContainerRegistry Load(string config = null,
						IConfigurationLoader loader = null) {
			return LoadSeq(config, loader).FirstOrDefault();
		}

		private static IEnumerable<DIContainerRegistry> LoadSeq(string config = null,
							IConfigurationLoader loader = null) {
			var configLoader = loader ?? new AppConfigurationLoader();
			foreach (var section in UnityContainerFactory.LoadOfType<ConfigSection>(config, configLoader)) {
				yield return new DIContainerRegistry { ConfigurationLoader = configLoader }
									.Configure(section, config);
			}
		}

		/// <summary>
		/// <see cref="DIContainerRegistry"/>クラスのインスタンスを初期化します。
		/// </summary>
		public DIContainerRegistry() {
			InitRoot();
		}

		/// <summary>
		/// 設定を行います。
		/// </summary>
		/// <param name="configuration">設定</param>
		/// <returns>このインスタンス</returns>
		public DIContainerRegistry Configure(Configuration configuration) {
			var sections = configuration.Sections.GetEnumerator();
			Configure(sections.ToEnumerable<ConfigSection>().FirstOrDefault(),
				configuration.HasFile ? configuration.FilePath : null);
			return this;
		}

		/// <summary>
		/// 設定を行います。
		/// </summary>
		/// <param name="config">設定セクション</param>
		/// <param name="target">設定ファイル</param>
		/// <returns>このインスタンス</returns>
		private DIContainerRegistry Configure(ConfigSection config, string target) {
			if (config?.Root == null || string.IsNullOrEmpty(config.Root.Config)) {
				var file = target ?? Resources.DefaultConfig;
				throw new NoContainerConfigurationException(string.Format(
							Resources.NoConfigurationFound, file));
			}
			config.CheckDuplicates();
			Cleanup();
			var loader = ConfigurationLoader ??
						(ConfigurationLoader = new AppConfigurationLoader());
			var rootFactory = new UnityContainerFactory {
				ContainerDefinition = config.Root,
			};
			Root = new ContainerRegistryEntry(rootFactory.Load(loader));

			var builder = new UnityContainerFactoryBuilder {
				RootFactory = rootFactory
			}.Initialize(config.Containers);

			var orphan = builder.Orphans;
			if (orphan.Length > 0) {
				throw new ConfigurationErrorsException(string.Format(
							Resources.InvalidParent, orphan.Select(o => o.Parent).JoinStrings(", ")));
			}


			foreach (var container in config.Containers.All()) {
				var factory = builder.Build(container.Name);
				var unityContainer = factory?.Load(loader);
				if (unityContainer == null) {
					continue;
				}
				var entry = new ContainerRegistryEntry(unityContainer);
				containerDic.Add(container.Name, entry);
				container.Aliases.All().ForEach(a => containerDic.Add(a.Name, entry));
			}

			return this;
		}

		private void InitRoot() {
			Root = new ContainerRegistryEntry(null);
		}

		private void Cleanup() {
			foreach (var entry in containerDic.Values) {
				entry.Dispose();
			}
			containerDic.Clear();

			var root = Root;
			root?.Dispose();
			InitRoot();
		}

		#region Indexers
		/// <summary>
		/// DIコンテナを取得します。
		/// </summary>
		/// <param name="name">DIコンテナ名</param>
		/// <returns>
		/// DIコンテナ。登録されていなければ<see cref="RootContainer"/>
		/// </returns>
		public IDIContainer this[string name]
			=> (containerDic.ContainsKey(name)
				? containerDic[name] : Root).DIContainer;
		#endregion

		#region Public methods
		/// <summary>
		/// DIコンテナを取得します。
		/// </summary>
		/// <param name="candidates">DIコンテナ名候補</param>
		/// <returns>
		/// DIコンテナ名候補のうち、最初に見つかったDIコンテナ。
		/// 登録されていなければ<see cref="RootContainer"/>
		/// </returns>
		public IDIContainer Find(IEnumerable<string> candidates) {
			IDIContainer container = null;
			if (candidates.FirstOrDefault(s => TryGetContainer(s, out container)) == null) {
				container = RootContainer;
			}

			return container;
		}

		/// <summary>
		/// 依存性注入を行います。
		/// </summary>
		/// <param name="target">対象オブジェクト</param>
		/// <param name="containerName">使用するDIコンテナ名</param>
		public void Buildup(object target, string containerName) {
			if (target == null) {
				return;
			}
			this[containerName].BuildUp(target.GetType(), target);
		}

		/// <summary>
		/// 依存性注入を行います。
		/// </summary>
		/// <param name="target">対象オブジェクト</param>
		/// <param name="containerNames">使用するDIコンテナ名候補</param>
		public void Buildup(object target, IEnumerable<string> containerNames) {
			if (target == null) {
				return;
			}
			Find(containerNames).BuildUp(target.GetType(), target);
		}
		#endregion

		#region Implementation of IDisposable
		/// <inheritdoc cref="IDisposable.Dispose"/>
		public void Dispose() {
			Cleanup();
		}
		#endregion

		/// <summary>
		/// DIコンテナを取得する。
		/// </summary>
		/// <param name="name">コンテナ名</param>
		/// <param name="container">取得したDIコンテナ。登録されていなければNull</param>
		private bool TryGetContainer(string name, out IDIContainer container) {
			var ret = containerDic.TryGetValue(name, out var entry);
			container = ret ? entry.DIContainer : null;
			return ret;
		}
	}
}
