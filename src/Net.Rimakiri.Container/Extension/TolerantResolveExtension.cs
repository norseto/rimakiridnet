// ==============================================================================
//     Net.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Linq;
using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.ObjectBuilder;

namespace Net.Rimakiri.Container.Extension {
	/// <inheritdoc />
	/// <summary>
	///	型に対して緩やかなマッチングを行うUnity Application Block拡張クラスです。
	/// </summary>
	/// <remarks>
	/// 以下のような拡張を行います。
	/// <ol>
	/// <li>
	/// 名称が指定されていない場合に、名称が指定されているエントリも
	/// 検索の対象とします。
	/// </li>
	/// <li>
	/// 名称が指定されている場合、設定のタイプが要求されたものでない
	/// 場合でも、代入可能な場合（設定されているタイプが要求時に指定された
	/// タイプのサブクラスであるような場合）には結果の対象とします。
	/// </li>
	/// <li>
	/// マッピングクラスが指定されていない場合でも、タイプに具象クラス
	/// が指定されている場合はマッピングにタイプのクラスが指定されて
	/// いるものとして扱います。
	/// </li>
	/// <li>
	/// 上記の拡張された検索につき、該当するものが複数ある場合には、
	/// 定義されている順でかつ、クラス階層の一番深いエントリが選択
	/// されます。
	/// </li>
	/// </ol>
	///</remarks>
	public class TolerantResolveExtension : UnityContainerExtension {
		/// <summary>
		/// 初期化を行います。
		/// </summary>
		protected override void Initialize() {
			Context.Strategies.Add(
				new TolerantMappingStrategy(Context.Container) {
					KeyFilter = key => key.Name != null,
					Matches = (name, type) => reg => reg.Name != null && type.IsAssignableFrom(reg.RegisteredType),
				},
				UnityBuildStage.TypeMapping);
			Context.Strategies.Add(
				new TolerantMappingStrategy(Context.Container) {
					KeyFilter = key => key.Name == null,
					Matches = (name, type) => reg => reg.Name == name && type.IsAssignableFrom(reg.RegisteredType),
				},
				UnityBuildStage.TypeMapping);
		}
	}

	internal sealed class TolerantMappingStrategy : BuilderStrategy {
		private readonly IUnityContainer container;

		public TolerantMappingStrategy(IUnityContainer container) {
			this.container = container;
		}

		#region Properties
		/// <summary>
		/// 検索キーのフィルタ。このフィルタに合致するキーは対象外とされる。
		/// </summary>
		public Func<NamedTypeBuildKey, bool> KeyFilter { private get; set; }

		/// <summary>
		/// 検索対象。この条件に合致するものを対象候補とする。
		/// </summary>
		public Func<string, Type, Func<ContainerRegistration, bool>> Matches { private get; set; }
		#endregion

		#region Customization
		/// <inheritdoc />
		public override void PreBuildUp(IBuilderContext context) {
			var buildKey = context.BuildKey;

			if (KeyFilter(buildKey)) {
				return;
			}
			NamedTypeBuildKey key = null;
			var keyMappingPolicy = context.Policies.Get<IBuildKeyMappingPolicy>(buildKey);
			if (keyMappingPolicy == null) {
				key = FindRegistry(buildKey.Name, buildKey.Type);
				if (key != null) {
					keyMappingPolicy = context.Policies.Get<IBuildKeyMappingPolicy>(key);
				}
			}

			var result = keyMappingPolicy != null
				? keyMappingPolicy.Map(buildKey, context)
				: key != null && !key.Type.IsInterface
					? key
					: null;
			if (result != null) {
				context.BuildKey = result;
			}
		} 
		#endregion

		private NamedTypeBuildKey FindRegistry(string name, Type type) {
			ContainerRegistration matched = null;

			container.Registrations.Where(Matches(name, type)).ForEach(reg => {
				if (matched == null || (reg.RegisteredType != matched.RegisteredType
						&& reg.RegisteredType.IsAssignableFrom(matched.RegisteredType))) {
					matched = reg;
				}
			});

			return (matched == null) ? null
						: new NamedTypeBuildKey(matched.RegisteredType, matched.Name);
		}
	}
}
