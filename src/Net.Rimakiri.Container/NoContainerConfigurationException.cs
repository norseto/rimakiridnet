// ==============================================================================
//     Net.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Configuration;

namespace Net.Rimakiri.Container {
	/// <summary>
	/// コンテナ設定が存在しないことを示す例外
	/// </summary>
	/// <inheritdoc cref="ConfigurationErrorsException"/>
	[Serializable]
	public class NoContainerConfigurationException : ConfigurationErrorsException {
		/// <inheritdoc />
		/// <summary>
		/// メッセージを指定してインスタンスを初期化します。
		/// </summary>
		/// <param name="message">メッセージ</param>
		public NoContainerConfigurationException(string message) : base(message) {
			// Empty
		}
	}
}
