// ==============================================================================
//     Net.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.ComponentModel;
using System.Globalization;
using System.Text;
using Net.Rimakiri.Container.Properties;

namespace Net.Rimakiri.Container.Converters {
	/// <inheritdoc />
	/// <summary>
	/// 文字列から<see cref="Encoding"/>の特定の値にコンバートします。
	/// </summary>
	public class TextEncodingConverter : TypeConverter {
		/// <inheritdoc />
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value) {
			var name = value?.ToString();
			if (string.IsNullOrEmpty(name)) {
				throw new ArgumentException(Resources.ConverterNullValue);
			}
			return Encoding.GetEncoding(name);
		}
	}
}
