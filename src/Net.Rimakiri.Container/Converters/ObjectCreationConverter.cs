// ==============================================================================
//     Net.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.ComponentModel;
using System.Globalization;

namespace Net.Rimakiri.Container.Converters {
	/// <inheritdoc />
	/// <summary>
	/// 文字列で指定されたクラスのデフォルトコンストラクタで生成した
	/// オブジェクトを返却するコンバータです。
	/// </summary>
	/// <typeparam name="T">作成するオブジェクトの型</typeparam>
	public class ObjectCreationConverter<T> : TypeConverter {
		/// <inheritdoc />
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value) {
			var ctor = typeof(T).GetConstructor(new Type[0]);
			if (ctor == null) {
				throw new NotSupportedException();
			}
			return (T) ctor.Invoke(new object[0]);
		}
	}
}
