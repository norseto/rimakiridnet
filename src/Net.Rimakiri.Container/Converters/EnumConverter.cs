// ==============================================================================
//     Net.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.ComponentModel;
using System.Globalization;
using Net.Rimakiri.Container.Properties;

namespace Net.Rimakiri.Container.Converters {
	/// <inheritdoc />
	/// <summary>
	/// 文字列から<see cref="Enum"/>の特定の値にコンバートします。
	/// </summary>
	/// <typeparam name="T">コンバート先<see cref="Enum"/></typeparam>
	public class EnumConverter<T> : TypeConverter where T : struct {
		/// <inheritdoc />
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value) {
			var strValue = value?.ToString();
			if (string.IsNullOrEmpty(strValue)) {
				throw new ArgumentException(Resources.ConverterNullValue);
			}

			if (Enum.TryParse(strValue, out T result)) {
				return result;
			}

			// Search with ignore case.
			foreach (var evalue in Enum.GetValues(typeof(T))) {
				if (strValue.Equals(evalue.ToString(), StringComparison.InvariantCultureIgnoreCase)) {
					return evalue;
				}
			}
			return result;
		}
	}
}
