// ==============================================================================
//     Net.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.ObjectBuilder2;
using Net.Rimakiri.Container.Config;

namespace Net.Rimakiri.Container {
	/// <summary>
	/// <see cref="UnityContainerFactory"/>を生成するビルダクラスです。
	/// </summary>
	internal class UnityContainerFactoryBuilder {
		private class DefinitionEntry {
			public DefinitionEntry Parent { get; }
			public ContainerDefinition Definition { get; }

			public DefinitionEntry(DefinitionEntry parent, ContainerDefinition definition) {
				Parent = parent;
				Definition = definition;
			}
		}

		private readonly IDictionary<string, DefinitionEntry> containers
				= new Dictionary<string, DefinitionEntry>();

		/// <summary>
		/// ルートコンテナのファクトリを設定または取得します。
		/// </summary>
		public UnityContainerFactory RootFactory { private get; set; }

		/// <summary>
		/// 親子関係が断絶しているコンテナ設定を取得します。
		/// </summary>
		public ContainerElement[] Orphans { get; private set; }

		/// <summary>
		/// 初期化を行います。
		/// </summary>
		/// <param name="collection">コンテナ設定</param>
		/// <returns>このインスタンス</returns>
		public UnityContainerFactoryBuilder Initialize(ContainersElementCollection collection) {
			collection.All().Where(c => string.IsNullOrEmpty(c.Parent)).ForEach(c => { BuildLinks(null, c, collection); });
			Orphans = collection.All().Where(c => !containers.ContainsKey(c.Name)).ToArray();
			return this;
		}

		/// <summary>
		/// ファクトリをビルドします。
		/// </summary>
		/// <param name="name">ビルドするコンテナの名称</param>
		/// <returns>ビルドしたファクトリ</returns>
		public UnityContainerFactory Build(string name) {
			return containers.TryGetValue(name, out var entry)
						? BuildFactory(entry) : null;
		}

		private UnityContainerFactory BuildFactory(DefinitionEntry entry) {
			if (entry == null) {
				return RootFactory;
			}

			var parent = BuildFactory(entry.Parent);
			return new UnityContainerFactory {
				Parent = parent,
				ContainerDefinition = entry.Definition
			};
		}

		private void BuildLinks(DefinitionEntry parent, ContainerElement definition,
						ContainersElementCollection collection) {
			var entry = new DefinitionEntry(parent, definition);
			containers[definition.Name] = entry;
			var names = new HashSet<string> {
				definition.Name,
			};

			definition.Aliases.All().Select(a => a.Name).ForEach(n => names.Add(n));
			collection.All().Where(c => names.Contains(c.Parent)).ForEach(c => { BuildLinks(entry, c, collection);});
		}
	}
}
