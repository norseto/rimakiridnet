using System;
using System.Security;

[assembly:CLSCompliant(true)]
[assembly:SecurityTransparent]
#if DEBUG
[assembly: SecurityRules(SecurityRuleSet.Level1, SkipVerificationInFullTrust = true)]
#endif
