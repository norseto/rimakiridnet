// ==============================================================================
//     Net.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Configuration;

namespace Net.Rimakiri.Container.Config {
	/// <inheritdoc />
	/// <summary>
	/// コンテナ定義を行う抽象要素クラスです。
	/// </summary>
	public abstract class ContainerDefinition : BaseElement {
		private const string AttributeNameConf = "include";
		private const string AttributeNameContainerName = "containername";

		/// <summary>
		/// Unity Application Blockの設定ファイルを取得または設定します。
		/// </summary>
		[ConfigurationProperty(AttributeNameConf, IsRequired = true)]
		public string Config {
			get => (string)this[AttributeNameConf];
			set => this[AttributeNameConf] = value;
		}

		/// <summary>
		/// コンテナ名を取得または設定します。
		/// </summary>
		[ConfigurationProperty(AttributeNameContainerName, IsRequired = false, DefaultValue = "")]
		public string ContainerName {
			get => (string)this[AttributeNameContainerName];
			set => this[AttributeNameContainerName] = value;
		}
	}
}
