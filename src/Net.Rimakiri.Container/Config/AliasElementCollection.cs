// ==============================================================================
//     Net.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Configuration;

namespace Net.Rimakiri.Container.Config {
	/// <inheritdoc />
	/// <summary>
	/// <see cref="AliasElement"/>のコレクション要素です。
	/// </summary>
	[ConfigurationCollection(typeof(AliasElement),
		AddItemName = ContainerElement.PropertyNameAlias)]
	public class AliasElementCollection : BaseElementCollection<AliasElement> {
		#region Overrides of ConfigurationElementCollection
		/// <summary>
		/// 要素キーを取得します。
		/// </summary>
		/// <param name="element">対象要素</param>
		/// <returns><see cref="AliasElement.Name"/>属性の値</returns>
		protected override object GetElementKey(ConfigurationElement element) {
			return ((AliasElement)element).Name;
		}
		#endregion
	}
}
