// ==============================================================================
//     Net.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Configuration;
using Net.Rimakiri.Container.Properties;

namespace Net.Rimakiri.Container.Config {
	/// <inheritdoc />
	/// <summary>
	/// <see cref="ContainerElement"/>コレクションクラスです。
	/// </summary>
	[ConfigurationCollection(typeof(ContainerElement),
		AddItemName = ConfigSection.PropertyNameContainer)]
	public class ContainersElementCollection : BaseElementCollection<ContainerElement> {
		#region Overrides of ConfigurationElementCollection
		/// <summary>
		/// 要素キーを取得します。
		/// </summary>
		/// <param name="element">対象要素</param>
		/// <returns><see cref="ContainerElement.Name"/>属性の値</returns>
		protected override object GetElementKey(ConfigurationElement element) {
			return ((ContainerElement)element).Name;
		}

		/// <summary>
		/// 要素を追加します。
		/// </summary>
		/// <param name="element">追加対象</param>
		/// <param name="throwIfExists">trueの場合、存在していると例外を送出します</param>
		public void Add(ContainerElement element, bool throwIfExists) {
			var source = CurrentConfiguration.FilePath;
			element.Source = source;
			var idx = BaseIndexOf(element);
			if (idx < 0) {
				BaseAdd(element, throwIfExists);
				return;
			}

			var current = BaseGet(idx) as ContainerElement;
			if (current != null && current.Source == source) {
				throw new ConfigurationErrorsException(string.Format(
					Resources.DuplicateContainer, current.Name));
			}
			if (current != null && throwIfExists) {
				throw new ConfigurationErrorsException(string.Format(
					Resources.DuplicateContainerOverride, current.Name));
			}
			BaseAdd(element, throwIfExists);
		}

		#endregion
	}
}
