// ==============================================================================
//     Net.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

namespace Net.Rimakiri.Container.Config {
	/// <inheritdoc />
	/// <summary>
	/// ルートコンテナを定義する要素です。
	/// </summary>
	/// <remarks>
	/// include属性でUnity Application Blockの設定ファイルを指定します。
	/// containername属性で指定されたファイルのコンテナ名を指定します。
	/// containername属性が指定されない場合、無名コンテナが指定されたもの
	/// として扱われます。
	/// 例えば、以下のような場合、Test00.configファイルで定義された無名の
	/// コンテナがルートコンテナとして使用されます。
	/// <code>
	///  &lt;configSections&gt;
	///    &lt;section name="dicontainer" type="Net.Rimakiri.Container.ConfigSection, Net.Rimakiri.Container" /&gt;
	///  &lt;/configSections&gt;
	///
	///  &lt;dicontainer xmlns="http://rimakiri.net/dotnet/dicontainer/1.1"&gt;
	///    &lt;root include="Test00.config" /&gt;
	///    &lt;container include="Test00.config" name="Test01" containername="Test01"&gt;
	///      &lt;alias name="hoge" /&gt;
	///    &lt;/container&gt;
	///  &lt;/dicontainer&gt;
	/// </code>
	/// </remarks>
	/// <seealso cref="DIContainerRegistry.RootContainer"/>
	// ReSharper disable once ClassNeverInstantiated.Global
	public class RootElement : ContainerDefinition {
	}
}
