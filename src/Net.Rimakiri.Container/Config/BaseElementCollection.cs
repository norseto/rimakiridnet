// ==============================================================================
//     Net.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Collections.Generic;
using System.Configuration;

namespace Net.Rimakiri.Container.Config {
	/// <inheritdoc />
	/// <summary>
	/// 要素コレクションの共通ベースクラスです。
	/// </summary>
	/// <typeparam name="T">要素の型</typeparam>
	public abstract class BaseElementCollection<T> : ConfigurationElementCollection
			where T : ConfigurationElement {
		#region Overrides of ConfigurationElementCollection
		/// <summary>
		/// 新規要素を作成します。
		/// </summary>
		/// <remarks>
		/// 作成される要素にはデフォルトコンストラクタが必要です。
		/// </remarks>
		/// <returns>作成した要素</returns>
		protected override ConfigurationElement CreateNewElement() {
			return Activator.CreateInstance<T>();
		}
		#endregion

		/// <summary>
		/// 要素を追加します。
		/// </summary>
		/// <param name="element">追加対象</param>
		public void Add(T element) {
			BaseAdd(element, true);
		}

		/// <summary>
		/// 全要素を取得します。
		/// </summary>
		/// <returns>全要素</returns>
		public IEnumerable<T> All() {
			return GetEnumerator().ToEnumerable<T>();
		}
	}
}
