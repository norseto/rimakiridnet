// ==============================================================================
//     Net.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Configuration;
using System.Xml;

namespace Net.Rimakiri.Container.Config {
	/// <summary>
	/// コンテナを定義する要素です。
	/// </summary>
	/// <remarks>
	/// include属性でUnity Application Blockの設定ファイルを指定します。
	/// containername属性で指定されたファイルのコンテナ名を指定します。
	/// containername属性が指定されない場合、無名コンテナが指定されたもの
	/// として扱われます。
	/// 例えば、以下のような場合、Test00.configファイルで定義されたTest01
	/// またはTest02のUnityコンテナがコンテナとして使用されます。
	/// また、コンテナTest02の親コンテナとしてTest01が使用されます。
	/// コンテナTest01の親コンテナはルートコンテナです。
	/// <code>
	///  &lt;configSections&gt;
	///    &lt;section name="dicontainer" type="Net.Rimakiri.Container.ConfigSection, Net.Rimakiri.Container" /&gt;
	///  &lt;/configSections&gt;
	///
	///  &lt;dicontainer xmlns="http://rimakiri.net/dotnet/dicontainer/1.1"&gt;
	///    &lt;root include="Test00.config" /&gt;
	///    &lt;container include="Test00.config" name="Test01" containername="Test01"&gt;
	///      &lt;alias name="hoge" /&gt;
	///    &lt;/container&gt;
	///    &lt;container include="Test00.config" name="Test02" containername="Test02" parent="Test01" &gt;
	///      &lt;alias name="fuga" /&gt;
	///    &lt;/container&gt;
	///  &lt;/dicontainer&gt;
	/// </code>
	/// このコンテナを取得するには、<see cref="DIContainerRegistry.this"/>に、Test01を
	/// 指定します。
	/// また、このコンテナの別名を示す複数の&lt;alias&gt;要素(<see cref="AliasElement"/>)
	/// を子要素として指定可能です。上記の例では、<see cref="DIContainerRegistry.this"/>に、
	/// hogeを指定しても同じコンテナを取得することができます。
	/// </remarks>
	/// <remarks>
	/// name属性の値は、別名を含め一意でなければなりません。
	/// </remarks>
	/// <seealso cref="DIContainerRegistry.this"/>
	/// <inheritdoc />
	public class ContainerElement : ContainerDefinition {
		private const string PropertyNameAliases = "aliases";
		/// <summary>
		/// Alias要素のタグ名
		/// </summary>
		public const string PropertyNameAlias = "alias";
		private const string AttributeNameName = "name";
		private const string AttributeNameParent = "parent";

		private string source;

		/// <summary>
		/// この要素を定義した設定ファイルパスを取得または設定します。
		/// </summary>
		internal string Source {
			get => source;
			set {
				if (source == null) {
					source = value;
				}
			}
		}

		/// <summary>
		/// 継承した内容か否かを取得します。
		/// </summary>
		public bool Inherited { get; private set; }

		/// <summary>
		/// この要素が継承により追加されたかを検証します。
		/// </summary>
		/// <param name="filePath">設定ファイルのパス</param>
		internal void VerifyInherition(string filePath) {
			Inherited = Source != filePath;
		}

		/// <summary>
		/// コンテナの名称を取得または設定します。
		/// </summary>
		[ConfigurationProperty(AttributeNameName, IsKey = true, IsRequired = true)]
		public string Name {
			get => (string)this[AttributeNameName];
			set => this[AttributeNameName] = value;
		}

		/// <summary>
		/// 親コンテナの名称を取得または設定します。
		/// </summary>
		[ConfigurationProperty(AttributeNameParent, IsKey = false, IsRequired = false, DefaultValue = null)]
		public string Parent {
			get => (string)this[AttributeNameParent];
			set => this[AttributeNameParent] = value;
		}

		/// <summary>
		/// コンテナのエイリアスを取得または設定します。
		/// </summary>
		[ConfigurationProperty(PropertyNameAliases, IsDefaultCollection = true)]
		public AliasElementCollection Aliases {
			get => (AliasElementCollection)this[PropertyNameAliases];
			set => this[PropertyNameAliases] = value;
		}

		#region Overrides of ConfigurationElement
		/// <inheritdoc />
		protected override bool OnDeserializeUnrecognizedElement(string elementName, XmlReader reader) {
			if (elementName != PropertyNameAlias) {
				return base.OnDeserializeUnrecognizedElement(elementName, reader);
			}
			var elm = new AliasElement();
			elm.Deserialize(reader);
			Aliases.Add(elm);
			return true;
		}
		#endregion
	}
}
