﻿// ==============================================================================
//     Net.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Collections;
using System.Collections.Generic;

namespace Net.Rimakiri.Container.Config {
	/// <summary>
	/// 拡張機能ユーティリティです。
	/// </summary>
	public static class ExtensionUtils {
		/// <summary>
		/// IEnumeratorをIEnumerableに変換します。
		/// </summary>
		/// <typeparam name="T">要素の型</typeparam>
		/// <param name="enumer">対象<see cref="IEnumerator"/></param>
		/// <returns>変換された<see cref="IEnumerable{T}"/></returns>
		public static IEnumerable<T> ToEnumerable<T>(this IEnumerator enumer) where T: class {
			while (enumer.MoveNext()) {
				if (!(enumer.Current is T e)) {
					continue;
				}
				yield return e;
			}
		}
	}
}
