// ==============================================================================
//     Net.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Configuration;

namespace Net.Rimakiri.Container.Config {
	/// <inheritdoc />
	/// <summary>
	/// 子コンテナの別名を定義する要素です。
	/// </summary>
	/// <remarks>
	/// name属性で別名を指定します。
	/// 例えば、以下のような場合、Test00.configファイルで定義されたTest01の
	/// コンテナの別名としてhogeが登録されます。
	/// <code>
	///  &lt;configSections&gt;
	///    &lt;section name="dicontainer" type="Net.Rimakiri.Container.ConfigSection, Net.Rimakiri.Container" /&gt;
	///  &lt;/configSections&gt;
	///
	///  &lt;dicontainer xmlns="http://rimakiri.net/dotnet/dicontainer/1.1"&gt;
	///    &lt;root include="Test00.config" /&gt;
	///    &lt;container include="Test00.config" name="Test01" containername="Test01"&gt;
	///      &lt;alias name="hoge" /&gt;
	///    &lt;/container&gt;
	///  &lt;/dicontainer&gt;
	/// </code>
	/// 上記の例では、<see cref="DIContainerRegistry.this"/>に、
	/// hogeを指定しても同じコンテナを取得することができます。
	/// </remarks>
	/// <remarks>
	/// name属性の値は、別名を含め一意でなければなりません。
	/// </remarks>
	public class AliasElement : BaseElement {
		private const string AttributeNameName = "name";

		/// <summary>
		/// 別名を取得または設定します。
		/// </summary>
		[ConfigurationProperty(AttributeNameName, IsKey = true, IsRequired = true)]
		public string Name {
			get => (string)this[AttributeNameName];
			set => this[AttributeNameName] = value;
		}
	}
}
