// ==============================================================================
//     Net.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Configuration;

namespace Net.Rimakiri.Container {
	/// <summary>
	/// 設定ファイルを読込むローダが実装すべきインターフェイスです。
	/// </summary>
	/// <seealso cref="DIContainerRegistry.ConfigurationLoader"/>
	public interface IConfigurationLoader {
		/// <summary>
		/// 設定ファイルをロードします。
		/// </summary>
		/// <param name="name">対象ファイル</param>
		/// <returns>設定</returns>
		Configuration LoadConfiguration(string name = null);
	}

	/// <summary>
	/// アプリケーション設定ファイルをロードする
	/// <see cref="IConfigurationLoader"/>実装です。
	/// </summary>
	/// <inheritdoc />
	public class AppConfigurationLoader : IConfigurationLoader {
		#region Implementation of IConfigurationLoader

		/// <inheritdoc />
		public Configuration LoadConfiguration(string name = null) {
			if (name == null) {
				return ConfigurationManager.OpenExeConfiguration(
							ConfigurationUserLevel.None);
			}
			var configMap = new ExeConfigurationFileMap {
				ExeConfigFilename = name,
			};
			return ConfigurationManager.OpenMappedExeConfiguration(
						configMap, ConfigurationUserLevel.None);
		}

		#endregion
	}
}
