// ==============================================================================
//     Net.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Runtime.CompilerServices;

namespace Net.Rimakiri.Container {
	/// <summary>
	/// この名前空間には、DI(Dependency Injection)コンテナを使用して
	/// オブジェクトの生成・ビルドアップ(オブジェクトのプロパティに
	/// 生成したオブジェクトを設定します)を実行するためのフレームワーク
	/// が含まれています。
	/// <a href="https://msdn.microsoft.com/en-us/library/ff648512.aspx">
	/// Microsoft Unity Application Block</a>が実装として使用されて
	/// いますが、この名前空間のクラスですべて隠蔽されています。
	/// </summary>
	/// <seealso cref="DIContainerRegistry"/>
	[CompilerGenerated]
	internal class NamespaceGroupDoc {
		// Empty
	}

	/// <summary>
	/// コンテナ名前空間には、DIコンテナを使用するためのクラスが
	/// 含まれています。
	/// </summary>
	[CompilerGenerated]
	internal class NamespaceDoc {
		// Empty
	}
}

namespace Net.Rimakiri.Container.Config {
	/// <summary>
	/// コンテナ設定名前空間には、DIコンテナの設定ファイルを処理
	/// するためのクラスが含まれています。
	/// </summary>
	[CompilerGenerated]
	internal class NamespaceDoc {
		// Empty
	}
}

namespace Net.Rimakiri.Container.Converters {
	/// <summary>
	/// コンテナコンバータ名前空間には、DIコンテナの設定ファイルで
	/// 使用することを想定した各種コンバータが含まれています。
	/// </summary>
	[CompilerGenerated]
	internal class NamespaceDoc {
		// Empty
	}
}

namespace Net.Rimakiri.Container.Extension {
	/// <summary>
	/// コンテナ拡張名前空間には、Microsoft Unity Application Block
	/// をこのフレームワークで想定する挙動とするための拡張クラスが
	/// 含まれています。
	/// </summary>
	[CompilerGenerated]
	internal class NamespaceDoc {
		// Empty
	}
}
