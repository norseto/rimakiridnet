// ==============================================================================
//     Net.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Unity;

namespace Net.Rimakiri.Container {
	/// <summary>
	/// DIコンテナ自身をビルドアップ時に注入するUnity Application
	/// Blockの拡張クラスです。
	/// </summary>
	/// <inheritdoc cref="ResolverOverride" />
	public class DIContainerResolveOverride : ResolverOverride, IDependencyResolverPolicy {
		private readonly IDIContainer container;

		/// <summary>
		/// <see cref="DIContainerResolveOverride"/>クラスのインスタンス
		/// を初期化します。
		/// </summary>
		/// <param name="container">注入するDIコンテナ</param>
		public DIContainerResolveOverride(IDIContainer container) {
			this.container = container;
		}

		#region Customize ResolveOverride
		/// <summary>
		/// 型に対するリゾルバポリシを取得します。
		/// </summary>
		/// <param name="context">コンテキスト</param>
		/// <param name="dependencyType">取得するオブジェクトの型</param>
		/// <returns>要求された型が<see cref="IDIContainer"/>の場合は自身、その他の場合はnull</returns>
		public override IDependencyResolverPolicy GetResolver(IBuilderContext context, Type dependencyType) {
			return typeof(IDIContainer).IsAssignableFrom(dependencyType) ? this : null;
		}
		#endregion

		#region Implements IDependencyResolverPolicy
		/// <inheritdoc />
		/// <summary>
		/// 自身をビルドアップします。
		/// </summary>
		/// <param name="context">対象コンテキスト</param>
		/// <returns>コンテナ自身</returns>
		public object Resolve(IBuilderContext context) {
			return container;
		} 
		#endregion
	}
}
