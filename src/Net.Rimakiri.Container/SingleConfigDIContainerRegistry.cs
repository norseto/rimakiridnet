// ==============================================================================
//     Net.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;

namespace Net.Rimakiri.Container {
	/// <summary>
	/// DIコンテナレジストリの試験実装です。
	/// このクラスは使用しないでください。
	/// </summary>
	/// <inheritdoc />
	[Obsolete("Use class DIContainerRegistry")]
	public class SingleConfigDIContainerRegistry : IDisposable {
		#region Constants
		private const string DefaultSection = "unity";
		#endregion

		#region Fields
		private bool rootLoaded;
		private IUnityContainer rootContainer;
		private readonly Dictionary<string, IUnityContainer> containers =
			new Dictionary<string, IUnityContainer>();
		private UnityConfigurationSection configSection;
		private string section;
		private string config;
		#endregion

		#region WrapperFields
		private IDIContainer rootWrapper;
		private readonly Dictionary<string, IDIContainer> children =
			new Dictionary<string, IDIContainer>();
		#endregion

		#region Public Properties
		/// <summary>
		/// 設定ファイルを取得または設定します。
		/// 指定がなければアプリケーション構成ファイルが使用されます。
		/// </summary>
		public string Config {
			get => config;
			set {
				if (rootLoaded) {
					throw new InvalidOperationException("Cannot change configuration.");
				}
				config = value;
				configSection = null;
			}
		}

		/// <summary>
		/// 設定ファイルが指定された場合のセクション名を取得または設定します。
		/// </summary>
		public string Section {
			get => section ?? DefaultSection;
			set {
				if (rootLoaded) {
					throw new InvalidOperationException("Cannot change configuration.");
				}
				section = value;
				configSection = null;
			}
		}

		/// <summary>
		/// ルートコンテナを取得します。存在しなければnullを返却します。
		/// </summary>
		public IDIContainer Root => rootWrapper;
		#endregion

		#region Indexers
		/// <summary>
		/// DIコンテナを取得します。
		/// </summary>
		/// <param name="name">DIコンテナ名</param>
		/// <returns>DIコンテナ。登録されていなければ<see cref="Root"/>コンテナ</returns>
		public IDIContainer this[string name]
			=> children.ContainsKey(name) ? children[name] : Root;
		#endregion

		#region Public methods

		/// <summary>
		/// 全てのコンテナを読込みます。
		/// </summary>
		/// <param name="configuration">対象コンフィグレーション</param>
		public void LoadAll(Configuration configuration = null) {
			var conf = configuration ?? OpenConfiguration();
			var hasRoot = false;
			var childNames = new List<string>();

			foreach (ConfigurationSection sec in conf.Sections) {
				var unitySection = sec as UnityConfigurationSection;
				if (unitySection == null) {
					continue;
				}
				var name = sec.SectionInformation.Name;
				if (name != Section) {
					continue;
				}

				configSection = unitySection;
				foreach (var container in unitySection.Containers) {
					var cname = container.Name;
					if (string.IsNullOrEmpty(cname)) {
						hasRoot = true;
						continue;
					}
					childNames.Add(cname);
				}
				break;
			}
			foreach (var childName in childNames) {
				LoadConfiguration(childName);
			}
			if (childNames.Count < 1 && hasRoot) {
				LoadConfiguration(null);
			}
		}

		/// <summary>
		/// 設定を読込みます。
		/// </summary>
		/// <param name="name">設定名称</param>
		/// <returns>自身のインスタンス</returns>
		public SingleConfigDIContainerRegistry LoadConfiguration(string name) {
			lock (this) {
				if (name == null && rootLoaded) {
					return this;
				}
				if (name != null && containers.ContainsKey(name)) {
					return this;
				}
				LoadChildConfiguration(name);
				return this;
			}
		}

		/// <summary>
		/// 依存性注入を行います。
		/// </summary>
		/// <param name="target">対象オブジェクト</param>
		/// <param name="containerName">使用するコンテナ名</param>
		public void Buildup(object target, string containerName) {
			if (target == null) {
				return;
			}
			var container = (children.ContainsKey(containerName))
				? children[containerName]
				: rootWrapper;
			container?.BuildUp(target.GetType(), target);
		}
		#endregion

		#region Implements IDisposable
		/// <inheritdoc />
		public void Dispose() {
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// リソースを開放します。
		/// </summary>
		/// <param name="disposing">マネージリソースを開放する場合true</param>
		protected virtual void Dispose(bool disposing) {
			if (!disposing) {
				return;
			}
			try {
				DisposeContainer(rootContainer);
				foreach (var container in containers.Values) {
					DisposeContainer(container);
				}

				rootWrapper?.Dispose();
			}
			finally {
				rootLoaded = false;
				rootContainer = null;
				rootWrapper = null;
				containers.Clear();
				children.Clear();
			}
		}
		#endregion

		#region Implementations
		private IUnityContainer LoadUnityConfiguration(IUnityContainer container, string name) {
			if (Config == null && configSection == null) {
				return (name == null) ? container.LoadConfiguration()
							: container.LoadConfiguration(name);
			}
			var unityConfigSection = GetConfigSection();
			return (name == null)
				? unityConfigSection.Configure(container)
				: unityConfigSection.Configure(container, name);
		}

		private UnityConfigurationSection GetConfigSection() {
			if (configSection != null) {
				return configSection;
			}

			var target = OpenConfiguration();
			configSection = (UnityConfigurationSection)target.GetSection(Section);
			return configSection;
		}

		private Configuration OpenConfiguration() {
			if (Config == null) {
				return ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
			}

			var configMap = new ExeConfigurationFileMap {
				ExeConfigFilename = Config,
			};
			return ConfigurationManager.OpenMappedExeConfiguration(configMap,
				ConfigurationUserLevel.None);
		}

		/// <summary>
		/// 無名コンテナをルートとして読込みます。
		/// </summary>
		private void SetupRootContainer() {
			if (rootLoaded) {
				return;
			}
			rootLoaded = true;
			var root = CreateRoot();
			rootContainer = root;
			if (root != null) {
				rootWrapper = new UnityWrapper(rootContainer);
			}
		}

		private IUnityContainer NewContainer(IUnityContainer parent) {
			var container = (parent != null) ? parent.CreateChildContainer() : new UnityContainer();
			return container;
		}

		private IUnityContainer CreateRoot() {
			try {
				return LoadUnityConfiguration(NewContainer(null), null);
			}
			catch (ArgumentException) {
				// Root notfound.
			}
			return null;
		}

		private void LoadChildConfiguration(string name) {
			SetupRootContainer();
			if (name == null) {
				return;
			}
			var root = rootContainer;
			var container = NewContainer((root == null) ? null : CreateRoot());

			LoadUnityConfiguration(container, name);

			containers.Add(name, container);
			children.Add(name, new UnityWrapper(container));
		}

		private static void DisposeContainer(IUnityContainer target) {
			if (target == null) {
				return;
			}
			try {
				if (target.Parent != null) {
					target = target.Parent;
				}
				target.Dispose();
			}
			catch (Exception) {
				// Ignore.
			}
		}
		#endregion
	}
}
