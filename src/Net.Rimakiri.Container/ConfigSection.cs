// ==============================================================================
//     Net.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Xml;
using Microsoft.Practices.ObjectBuilder2;
using Net.Rimakiri.Container.Config;
using Net.Rimakiri.Container.Properties;

namespace Net.Rimakiri.Container {
	/// <summary>
	/// <see cref="DIContainerRegistry"/>クラスで使用する
	/// DIコンテナの設定用セクションです。
	/// </summary>
	/// <remarks>
	/// 以下のように、アプリケーション設定ファイルでこのセクション
	/// を定義して使用します。
	/// <code>
	///  &lt;configSections&gt;
	///    &lt;section name="dicontainer" type="Net.Rimakiri.Container.ConfigSection, Net.Rimakiri.Container" /&gt;
	///  &lt;/configSections&gt;
	/// </code>
	/// また、指定可能な子要素は、ルートコンテナを示すひとつの&lt;root&gt;要素
	/// (<see cref="RootElement"/>)およびコンテナを示す複数の&lt;container&gt;要素
	/// (<see cref="ContainerElement"/>)です。コンテナには&lt;parent&gt;属性を指定
	/// して親子関係を指定することが可能です。親が指定されていないコンテナは、ルート
	/// コンテナが親となります。
	/// <code>
	///  &lt;configSections&gt;
	///    &lt;section name="dicontainer" type="Net.Rimakiri.Container.ConfigSection, Net.Rimakiri.Container" /&gt;
	///  &lt;/configSections&gt;
	///
	///  &lt;dicontainer xmlns="http://rimakiri.net/dotnet/dicontainer/1.1"&gt;
	///    &lt;root include="Test00.config" /&gt;
	///    &lt;container include="Test00.config" name="Test01" containername="Test01"&gt;
	///      &lt;alias name="hoge" /&gt;
	///    &lt;/container&gt;
	///    &lt;container include="Test00.config" name="Test02" containername="Test02" parent="hoge"&gt;
	///      &lt;alias name="fuga" /&gt;
	///    &lt;/container&gt;
	///  &lt;/dicontainer&gt;
	/// </code>
	/// </remarks>
	/// <seealso cref="DIContainerRegistry"/>
	/// <seealso cref="ContainersElementCollection"/>
	/// <seealso cref="ContainerElement"/>
	/// <seealso cref="RootElement"/>
	public class ConfigSection : ConfigurationSection {
		internal const string PropertyNameContainer = "container";
		private const string PropertyNameRootContainer = "root";
		private const string PropertyNameContainerCollection = "containers";
		private const string PropertyNameAllowOverride = "allowOverride";
		private bool dupChecked;

		/// <summary>
		/// XML名前空間を指定します。
		/// </summary>
		[ConfigurationProperty("xmlns", IsRequired = false)]
		public string XmlNs { get; set; }

		/// <summary>
		/// ルートコンテナ設定を取得または設定します。
		/// </summary>
		[ConfigurationProperty(PropertyNameRootContainer, IsRequired = true)]
		public RootElement Root {
			get => (RootElement)this[PropertyNameRootContainer];
			set => this[PropertyNameRootContainer] = value;
		}

		/// <summary>
		/// コンテナ設定コレクションを取得または設定します。
		/// </summary>
		[ConfigurationProperty(PropertyNameContainerCollection, IsDefaultCollection = true)]
		public ContainersElementCollection Containers {
			get => (ContainersElementCollection)this[PropertyNameContainerCollection];
			set => this[PropertyNameContainerCollection] = value;
		}

		/// <summary>
		/// オーバライド可能かを指定します。IISなどで設定ファイルが継承されている
		/// ような場合を想定しています。
		/// </summary>
		[ConfigurationProperty("allowOverride", IsRequired = false)]
		public bool AllowOverride {
			get => (bool)this[PropertyNameAllowOverride];
			set => this[PropertyNameAllowOverride] = value;
		}

		#region Overrides of ConfigurationElement
		/// <inheritdoc />
		protected override bool OnDeserializeUnrecognizedElement(string elementName, XmlReader reader) {
			if (elementName != PropertyNameContainer) {
				return base.OnDeserializeUnrecognizedElement(elementName, reader);
			}
			var elm = new ContainerElement();
			elm.Deserialize(reader);
			Containers.Add(elm, !AllowOverride);
			return true;
		}
		#endregion

		/// <summary>
		/// 継承により追加されているセクションがあるかを検証します。
		/// </summary>
		/// <param name="filePath">設定ファイルのパス</param>
		internal void VerifyInheritance(string filePath) {
			if (!AllowOverride) {
				return;
			}
			Containers.All().ForEach(con => { con.VerifyInherition(filePath); });
		}

		/// <summary>
		/// 重複した名称・エイリアスが存在するかをチェックします。
		/// 重複が見つかった場合には例外を送出します。
		/// </summary>
		/// <returns>このインスタンス</returns>
		internal ConfigSection CheckDuplicates() {
			if (dupChecked) {
				return this;
			}

			dupChecked = true;
			var names = Containers.All().Select(c => c.Name);
			var aliases = Containers.All().SelectMany(c => c.Aliases.All().Select(a => a.Name));

			var duplicates = names.Concat(aliases).GroupBy(n => n)
								.Where(g => g.Count() > 1).Select(g => g.Key);

			var dupNames = duplicates.ToArray();
			if (dupNames.Length < 1) {
				return this;
			}

			throw new ConfigurationErrorsException(string.Format(Resources.DuplicateName,
							dupNames.JoinStrings(", ")));
		}

		/// <summary>
		/// 指定された設定からすべての<see cref="ConfigSection"/>のセクション
		/// を取得します。
		/// </summary>
		/// <param name="config">取得元設定</param>
		/// <returns>すべてのセクション</returns>
		public static IEnumerable<ConfigSection> GetAll(Configuration config) {
			return	config.Sections.GetEnumerator()
						.ToEnumerable<ConfigSection>()
							.Select(it => it.CheckDuplicates());
		}
	}
}
