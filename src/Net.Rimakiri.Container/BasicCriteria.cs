// ==============================================================================
//     Net.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Net.Rimakiri.Container {
	/// <summary>
	/// 指定された条件から範囲を広げるコンテナ名を指定するクラスです。
	/// </summary>
	/// <seealso cref="DIContainerRegistry.Find"/>
	/// <seealso cref="DIContainerRegistry.Buildup(object,IEnumerable{string})"/>
	public class BasicCriteria : IEnumerable<string> {
		/// <summary>
		/// セパレータを設定または取得します。
		/// </summary>
		public string Separator { set; get; } = "_";

		/// <summary>
		/// 各レベルの名称を設定または取得します。
		/// </summary>
		public string[] LevelNames { set; get; } = {};

		/// <summary>
		/// インスタンスを初期化します。
		/// </summary>
		public BasicCriteria() {
		}

		/// <summary>
		/// インスタンスを初期化します。
		/// </summary>
		/// <param name="levelNames">各レベルの名称</param>
		public BasicCriteria(params string[] levelNames) {
			LevelNames = levelNames.ToArray();
		}

		#region Implementation of IEnumerable
		/// <inheritdoc />
		public IEnumerator<string> GetEnumerator() {
			var targets = LevelNames.Where(s => !string.IsNullOrEmpty(s)).ToArray();
			for (var i = targets.Length; i > 0; i--) {
				yield return string.Join(Separator, targets, 0, i);
			}
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return GetEnumerator();
		}
		#endregion
	}
}
