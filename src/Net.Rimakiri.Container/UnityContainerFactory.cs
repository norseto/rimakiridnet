// ==============================================================================
//     Net.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Net.Rimakiri.Container.Config;
using Net.Rimakiri.Container.Properties;
using ContainerElement = Net.Rimakiri.Container.Config.ContainerElement;

namespace Net.Rimakiri.Container {
	/// <summary>
	/// <see cref="IUnityContainer"/>のファクトリクラスです。
	/// </summary>
	internal class UnityContainerFactory {
		public UnityContainerFactory Parent { get; set; }
		public ContainerDefinition ContainerDefinition { get; set; }

		/// <summary>
		/// DIコンテナをロードする。
		/// </summary>
		/// <param name="loader">使用するローダ</param>
		/// <returns>ロードしたDIコンテナ</returns>
		public IUnityContainer Load(IConfigurationLoader loader) {
			if (Parent == null) {
				return LoadUnity(null, ContainerDefinition, loader);
			}

			var parentContainer = Parent.Load(loader);
			return parentContainer == null ? null : LoadUnity(parentContainer, ContainerDefinition, loader);
		}

		/// <summary>
		/// コンテナをロードする
		/// </summary>
		/// <param name="parent">親コンテナ</param>
		/// <param name="element">設定要素</param>
		/// <param name="loader">使用するローダ</param>
		/// <returns>ロードしたコンテナ</returns>
		private static IUnityContainer LoadUnity(IUnityContainer parent,
							ContainerDefinition element, IConfigurationLoader loader) {
			var unityConfig = LoadOfType<UnityConfigurationSection>(
									element.Config, loader).FirstOrDefault();
			if (unityConfig == null) {
				throw new ConfigurationErrorsException(string.Format(
								Resources.NoUnityConfig, element.Config));
			}

			// 継承により追加されているコンテナ定義の場合、Unityの設定ファイルには
			// 定義が存在しない場合があるため、それを検証する
			if (element is ContainerElement containerElement
					&& parent != null && containerElement.Inherited
					&& unityConfig.Containers.Count(
							c => c.Name == element.ContainerName) < 1) {
				return null;
			}

			var container = (parent != null) ? parent.CreateChildContainer()
									: new UnityContainer();
			try {
				unityConfig.Configure(container, element.ContainerName);
			}
			catch (Exception ex) {
				throw new ConfigurationErrorsException(string.Format(
							Resources.UnityConfigError, element.Config), ex);
			}

			return container;
		}

		/// <summary>
		/// 設定ファイルをロードして特定のセクションを取得する。
		/// </summary>
		/// <typeparam name="T">取得するセクションのタイプ</typeparam>
		/// <param name="config">ロードするファイル</param>
		/// <param name="loader">試用するローダ</param>
		/// <returns>指定されたタイプのセクション</returns>
		internal static IEnumerable<T> LoadOfType<T>(string config, IConfigurationLoader loader)
				where T : ConfigurationSection {
			var configuration = loader.LoadConfiguration(config);
			if (configuration == null || !configuration.HasFile) {
				var path = configuration?.FilePath;
				var confFile = (path == null) ? config : config + "[" + path + "]";
				throw new ConfigurationErrorsException(string.Format(
								Resources.NoConfigurationFound, confFile));
			}
			configuration.Sections.GetEnumerator().ToEnumerable<ConfigurationSection>()
				.OfType<ConfigSection>().ForEach(
							c => c.VerifyInheritance(configuration.FilePath));
			return configuration.Sections.GetEnumerator().ToEnumerable<T>();
		}
	}
}
