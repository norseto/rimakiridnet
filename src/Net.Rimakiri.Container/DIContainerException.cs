﻿// ==============================================================================
//     Net.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;

namespace Net.Rimakiri.Container {
	/// <summary>
	/// DIコンテナで発生した例外です。原因となった例外を保持します。
	/// </summary>
	/// <seealso cref="IDIContainer"/>
	[Serializable]
	public class DIContainerException : Exception {
		/// <summary>
		/// <see cref="DIContainerException"/>クラスのインスタンス
		/// を初期化します。
		/// </summary>
		/// <param name="message">例外メッセージ</param>
		/// <param name="cause">原因となった例外</param>
		public DIContainerException(string message, Exception cause)
			: base(message, cause) {
		}

		/// <summary>
		/// <see cref="DIContainerException"/>クラスのインスタンス
		/// を初期化します。
		/// </summary>
		/// <param name="cause">原因となった例外</param>
		public DIContainerException(Exception cause)
			: base(cause.Message, cause) {
		}
	}
}
