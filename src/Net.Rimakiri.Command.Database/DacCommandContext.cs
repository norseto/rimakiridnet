// ==============================================================================
//     Net.Rimakiri.Command.Database
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Command.Database {
	/// <summary>
	/// データベース処理を行うコマンド用の<see cref="CommandContext"/>派生クラスです。
	/// </summary>
	/// <remarks>
	/// 設定した<see cref="IConnectionFactory"/>からのデータベース接続の取得
	/// やローカルトランザクション自動開始など、データベース処理に有用な機能を追加
	/// した<see cref="CommandContext"/>クラスです。このコンテキストを使用する
	/// コマンドを使用する場合、<see cref="DacCommandServiceBuilder{T}"/>を
	/// 使用してコマンドサービスをビルドしてください。
	/// </remarks>
	/// <inheritdoc />
	/// <seealso cref="DacCommandServiceBuilder{T}"/>
	public class DacCommandContext : CommandContext {
		private readonly Dictionary<string, IConnectionFactory> factories
				= new Dictionary<string, IConnectionFactory>();

		private const string ConnectionKey = "__ConnectionKey";

		/// <summary>
		/// デフォルトの<see cref="IConnectionFactory"/>インスタンスを
		/// 設定または取得します。
		/// </summary>
		public IConnectionFactory ConnectionFactory { get; set; }

		/// <summary>
		/// 使用する<see cref="IConnectionFactory"/>インスタンスを設定します。
		/// <see cref="ConnectionFactory"/>が設定されていない場合、最初のインスタンス
		/// が<see cref="ConnectionFactory"/>として設定されます。
		/// </summary>
		public IConnectionFactory[] ConnectionFactories {
			set {
				if (value == null) {
					return;
				}
				if (factories.Count > 0) {
					throw new InvalidOperationException();
				}
				for (int i = 0, l = value.Length; i < l; i++) {
					var factory = value[i];
					factories[factory.Name] = factory;
					if (i == 0 && ConnectionFactory == null) {
						ConnectionFactory = factory;
					}
				}
			}
		}

		/// <summary>
		/// データベースへの接続を取得します。取得したデータベース接続は
		/// フレームワークでコマンド終了後に解放されます。
		/// </summary>
		/// <param name="name">
		/// 取得する<see cref="DbConnection"/>の<see cref="IConnectionFactory"/>名
		/// </param>
		/// <returns>取得したデータベース接続</returns>
		/// <remarks>
		/// ローカルトランザクションの場合にはフレームワークでトランザクションが
		/// 管理されません。トランザクションをフレームワークで管理する場合には、
		/// <see cref="GetConnection"/>を使用してください。
		/// </remarks>
		protected DbConnection GetRawConnection(string name = null) {
			return OpenConnection(GetConnectionFactory(name), false);
		}

		private DbConnection OpenConnection(IConnectionFactory factory, bool transactional) {
			var cache = GetTxAttribute<DisposableDictionary<string, DbConnection>>(ConnectionKey);
			if (cache == null) {
				cache = new DisposableDictionary<string, DbConnection>();
				SetTxAttribute(ConnectionKey, cache);
			}

			var firstTime = false;
			var name = factory.Name ?? string.Empty;
			if (!cache.TryGetValue(name, out var con)) {
				con = factory.NewConnection(SupportsGlobalTx, TxIsolation.ToDbIsolationLevel());
				cache[name] = con;
				firstTime = true;
			}
			if (con.State == ConnectionState.Closed) {
				con.Open();
			}
			if (transactional && firstTime && !SupportsGlobalTx) {
				SetIsolationLevelMapping(factory.IsolationLevelMapping);
				EnlistTx(con);
			}
			return con;
		}

		/// <summary>
		/// データベースへの接続を取得します。取得したデータベース接続は
		/// フレームワークでコマンド終了後に解放されます。
		/// </summary>
		/// <param name="name">
		/// 取得する<see cref="DbConnection"/>の<see cref="IConnectionFactory"/>名
		/// </param>
		/// <returns>取得したデータベース接続</returns>
		/// <remarks>
		/// ローカルトランザクションの場合、トランザクションが開始します。
		/// トランザクションをフレームワークで管理しない場合には、
		/// <see cref="GetRawConnection"/>を使用してください。
		/// </remarks>
		protected DbConnection GetConnection(string name = null) {
			return OpenConnection(GetConnectionFactory(name), true);
		}

		/// <summary>
		/// 指定の名称の<see cref="IConnectionFactory"/>を取得します。
		/// </summary>
		/// <param name="name">名称。指定のない場合にはデフォルトを返却します</param>
		/// <returns>
		/// 指定された名称の<see cref="IConnectionFactory"/>。
		/// 指定がない場合、デフォルトの<see cref="ConnectionFactory"/>
		/// </returns>
		public IConnectionFactory GetConnectionFactory(string name = null) {
			return name == null || name == ConnectionFactory.Name
						? ConnectionFactory : factories[name];
		}

		/// <summary>
		/// 指定された名称の<see cref="IConnectionFactory"/>から<see cref="DbCommand"/>
		/// を作成します。
		/// </summary>
		/// <param name="name">
		/// 使用する<see cref="DbConnection"/>の<see cref="IConnectionFactory"/>名。
		/// 指定しない場合、デフォルトの<see cref="ConnectionFactory"/>の接続が使用されます。
		/// </param>
		/// <returns>作成された<see cref="DbCommand"/>インスタンス</returns>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:")]
		public DbCommand CreateDbCommand(string name = null) {
			CheckTimeout("CreateDbCommand");
			var con = GetConnection(name);
			DbCommand cmd = null;
			try {
				cmd = NewDbCommand(con);
				if (cmd.Connection == null) {
					cmd.Connection = con;
				}

				SetCommandTimeoutWithRemain(cmd);
				cmd.Transaction = GetDbTransaction();
				return cmd;
			}
			catch (Exception) {
				cmd?.Dispose();

				throw;
			}
		}

		/// <summary>
		/// データベース接続に対する<see cref="IDbCommand"/>を作成します。
		/// </summary>
		/// <param name="con">対象データベース接続</param>
		/// <returns>作成した<see cref="IDbCommand"/>インスタンス</returns>
		protected virtual DbCommand NewDbCommand(DbConnection con) {
			return con.CreateCommand();
		}

		/// <summary>
		/// トランザクションタイムアウトの残時間をコマンドタイムアウトとして設定します。
		/// </summary>
		/// <param name="cmd">対象コマンド</param>
		public virtual void SetCommandTimeoutWithRemain(DbCommand cmd) {
			var remain = GetTimeoutRemainTicks();
			if (remain >= 0) {
				cmd.CommandTimeout = (int)(remain / TimeSpan.TicksPerSecond) + 1;
			}
		}
	}
}
