// ==============================================================================
//     Net.Rimakiri.Command.Database
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Command.Database {
	/// <summary>
	/// <see cref="DacCommandContext"/>をコンテキストとして使用する
	/// <see cref="ICommandServiceBuilder{T}"/>インターフェイスの実装クラスです。
	/// </summary>
	/// <typeparam name="T">
	/// 使用する<see cref="CommandContext"/>のタイプ。
	/// <see cref="DacCommandContext"/>またはその派生クラスである必要があります。
	/// </typeparam>
	/// <inheritdoc />
	public class DacCommandServiceBuilder<T> : CommandServiceBuilder<T> where T:DacCommandContext {
		private readonly DacCommandContextLifecycleListener<T> dacListener = new DacCommandContextLifecycleListener<T>();

		/// <summary>
		/// 使用する<see cref="IConnectionFactory"/>を指定します。
		/// 設定された内容は、<see cref="DacCommandContext.ConnectionFactory"/>
		/// へと引き継がれます。
		/// </summary>
		public IConnectionFactory ConnectionFactory {
			get => dacListener.ConnectionFactory;
			set => dacListener.ConnectionFactory = value;
		}

		/// <summary>
		/// 使用する<see cref="IConnectionFactory"/>を指定します。
		/// 設定された内容は、<see cref="DacCommandContext.ConnectionFactory"/>
		/// へと引き継がれます。
		/// </summary>
		public IConnectionFactory[] ConnectionFactories {
			get => dacListener.ConnectionFactories;
			set => dacListener.ConnectionFactories = value;
		}

		/// <inheritdoc />
		public override IContextLifecycleListener<T> ContextLifecycleListener {
			get => dacListener;
			set => dacListener.Additional = value;
		}
	}
}
