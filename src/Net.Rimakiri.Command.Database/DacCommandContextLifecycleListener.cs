// ==============================================================================
//     Net.Rimakiri.Command.Database
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using Net.Rimakiri.Data.Interfaces;

namespace Net.Rimakiri.Command.Database {
	/// <inheritdoc />
	/// <summary>
	/// <see cref="DacCommandContext" />に対する<see cref="IContextLifecycleListener{T}" />の
	/// 実装クラスです。<see cref="ConnectionFactory"/>で設定された<see cref="IConnectionFactory"/>
	/// インスタンスを作成された<see cref="DacCommandContext"/>の<see cref="DacCommandContext.ConnectionFactory"/>
	/// として設定します。
	/// </summary>
	/// <typeparam name="T">
	/// 使用する<see cref="CommandContext"/>のタイプ。
	/// <see cref="DacCommandContext"/>またはその派生クラスである必要があります。
	/// </typeparam>
	internal class DacCommandContextLifecycleListener<T> : IContextLifecycleListener<T>
			where T : DacCommandContext {
		private IConnectionFactory connectionFactory;
		private IConnectionFactory[] connectionFactories;

		/// <summary>
		/// 追加リスナを取得または設定します。
		/// </summary>
		public IContextLifecycleListener<T> Additional { get; set; }

		/// <summary>
		/// 使用する<see cref="IConnectionFactory"/>を設定します。
		/// 設定済みの場合には何も行いません。
		/// </summary>
		public IConnectionFactory ConnectionFactory {
			get => connectionFactory;
			set {
				if (connectionFactory == null && value != null) {
					connectionFactory = value;
				}
			}
		}

		private static IConnectionFactory[] Copy(IConnectionFactory[] source) {
			if (source == null) {
				return null;
			}
			var dest = new IConnectionFactory[source.Length];
			Array.Copy(source, dest, dest.Length);
			return dest;
		}

		/// <summary>
		/// 使用する<see cref="IConnectionFactory"/>を設定します。
		/// 設定済みの場合には何も行いません。
		/// </summary>
		public IConnectionFactory[] ConnectionFactories {
			get => Copy(connectionFactories);
			set {
				if (connectionFactories == null && value != null) {
					connectionFactories = Copy(value);
				}
			}
		}

		#region Implementation of IContextLifecycleListener<in T>
		/// <summary>
		/// コンテキスト生成後のコールバックです。
		/// <see cref="ConnectionFactory"/>を生成したコンテキストの
		/// <see cref="DacCommandContext.ConnectionFactory"/>に設定します。
		/// </summary>
		/// <param name="context">作成されたコンテキスト</param>
		public void OnPostContextCreate(T context) {
			context.ConnectionFactory = ConnectionFactory;
			context.ConnectionFactories = ConnectionFactories;
			var additional = Additional;
			additional?.OnPostContextCreate(context);
		}

		/// <summary>
		/// コンテキスト破棄前のコールバックです。このクラスでは何もしません。
		/// </summary>
		/// <param name="context">破棄されるコンテキスト</param>
		public void OnPreContextDestroy(T context) {
			var additional = Additional;
			additional?.OnPreContextDestroy(context);
		}
		#endregion
	}
}
