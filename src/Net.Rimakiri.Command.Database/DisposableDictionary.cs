// ==============================================================================
//     Net.Rimakiri.Command.Database
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Collections.Generic;

namespace Net.Rimakiri.Command.Database {
	/// <inheritdoc cref="Dictionary{TKey,TValue}" />
	/// <summary>
	/// <see cref="IDisposable" />を実装した<see cref="Dictionary{TKey,TValue}" />
	/// クラスです。
	/// </summary>
	/// <typeparam name="TKey">キーのタイプ</typeparam>
	/// <typeparam name="TValue">値のタイプ</typeparam>
	internal class DisposableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, IDisposable {
		#region Implementation of IDisposable
		/// <inheritdoc />
		/// <summary>
		/// 値が<see cref="T:System.IDisposable" />を実装している場合、<see cref="M:System.IDisposable.Dispose" />
		/// を呼び出します。
		/// </summary>
		public void Dispose() {
			Exception error = null;
			foreach (var value in Values) {
				try {
					if (value is IDisposable target) {
						target.Dispose();
					}
				}
				catch (Exception ex) {
					error = ex;
				}
			}
			if (error != null) {
				throw error;
			}
		}
		#endregion
	}
}
