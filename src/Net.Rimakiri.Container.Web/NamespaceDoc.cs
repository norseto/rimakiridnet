// ==============================================================================
//     Net.Rimakiri.Container.Web
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Runtime.CompilerServices;

namespace Net.Rimakiri.Container.Web
{
	/// <summary>
	/// この名前空間には、DI(Dependency Injection)コンテナ
	/// フレームワークをWeb環境で使用するためのクラスが含まれ
	/// ています。
	/// </summary>
	[CompilerGenerated]
	internal class NamespaceGroupDoc {
		// Empty
	}

	/// <summary>
	/// この名前空間には、DI(Dependency Injection)コンテナ
	/// フレームワークをWeb環境で使用するためのクラスが含まれ
	/// ています。
	/// </summary>
	/// <remarks>
	/// このフレームワークをASP.NET WebFormsで使用するには以下のような手順で
	/// 行います。なお、<see cref="Net.Rimakiri.Command"/>
	/// の説明で作成したクラスを使用するものとしています。<br/><br/>
	/// <ol>
	/// <li>
	/// DIコンテナの設定ファイルを作成します。ここでは、sample.configと
	/// いう名前のファイルとします。この設定ファイルは、
	/// Unity Application Blockの設定ファイルです。
	/// <code source="DocExampleCSWeb/sample.config" language="XML" />
	/// </li>
	/// <li>
	/// Web.configに以下のような行を追加します。
	/// <code source="DocExampleCSWeb/sampleweb.config" language="XML" />
	/// </li>
	/// <li>
	/// グローバルアプリケーションファイルに以下のような内容を追加します。
	/// 追加した内容では、各ページのクラス名のコンテナでページオブジェクトを
	/// ビルドアップしています。
	/// <example>
	/// <code source="DocExampleCSWeb/Global.asax.cs" region="GlovalAsax" language="C#" />
	/// <code source="DocExampleVBWeb/Global.asax.vb" region="GlovalAsax" language="VB" />
	/// </example>
	/// </li>
	/// <li>
	/// 以下のような内容をDefault.aspxのクラスに追加します。DIDepend属性が
	/// 付加されているプロパティはページ実行前にDIコンテナにより値が設定
	/// されています。
	/// <example>
	/// <code source="DocExampleCSWeb/Default.aspx.cs" region="DefaultAspx" language="C#" />
	/// <code source="DocExampleVBWeb/Default.aspx.vb" region="DefaultAspx" language="VB" />
	/// </example>
	/// </li>
	/// </ol>
	/// </remarks>
	/// 
	/// <remarks>
	/// このフレームワークをASP.NET MVCで使用するには以下のような手順で行います。
	/// 設定ファイルその他についてはすべて上記のASP.NET WebFormsで使用する場合の例と同様
	/// のものが設定されている必要があります。<br/><br/>
	/// <ol>
	/// <li>
	/// 以下のようなカスタムコントローラファクトリクラスを作成します。
	/// このファクトリクラスはコントローラ生成後、コントローラ名_アクション名 のコンテナ
	/// を使用してコントローラに依存性を注入します。
	/// <example>
	/// <code language="C#">
	///	public class CustomControllerFactory : DefaultControllerFactory {
	///		#region Overrides of DefaultControllerFactory
	///		/// <inheritdoc />
	///		public override IController CreateController(RequestContext requestContext, string controllerName) {
	///			var controller = base.CreateController(requestContext, controllerName);
	///			object action;
	///			var sb = new StringBuilder().Append(controllerName);
	///			if (requestContext.RouteData.Values.TryGetValue("action", out action)) {
	///				sb.Append("_").Append(action.ToString());
	///			}
	///			RegistryCache.BuildUp(requestContext.HttpContext, controller, sb.ToString());
	///			return controller;
	///		}
	///		#endregion
	///	}
	/// </code>
	/// </example>
	/// </li>
	/// <li>
	/// 次にGlobal.asaxのソースを開き、MvcApplicationの内容を以下のようにして作成したコントローラ
	/// ファクトリが使用されるようにします。
	/// <example>
	/// <code language="C#">
	///	public class MvcApplication : RegistryCacheSupport {
	///		protected void Application_Start() {
	///			OnApplicationStart(null, null);
	///			AreaRegistration.RegisterAllAreas();
	///			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
	///			RouteConfig.RegisterRoutes(RouteTable.Routes);
	///			BundleConfig.RegisterBundles(BundleTable.Bundles);
	///			ControllerBuilder.Current.SetControllerFactory(new CustomControllerFactory());
	///		}
	///
	///		protected void Application_End() {
	///			OnApplicationEnd(null, null);
	///		}
	///	}
	/// </code>
	/// </example>
	/// </li>
	/// </ol>
	/// </remarks>
	[CompilerGenerated]
	internal class NamespaceDoc {
		// Empty
	}
}
