// ==============================================================================
//     Net.Rimakiri.Container.Web
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Diagnostics.CodeAnalysis;
using System.Web;

namespace Net.Rimakiri.Container.Web {
	/// <summary>
	/// <see cref="DIContainerRegistry"/>のキャッシュをサポートする
	/// <see cref="HttpApplication"/>クラスです。複数のモジュールで
	/// DIコンテナを使用する場合、このクラスを使用することで初期起動が速く
	/// なり、メモリ効率もよくなります。
	/// </summary>
	/// <remarks>
	/// 以下のような内容のファイルをGlobal.asaxとして配置することで、このクラス
	/// が使用されます。
	/// <code>
	/// &lt;%@ Application Inherits="Net.Rimakiri.Container.Web.RegistryCacheSupport" Language="C#" %&gt;
	/// </code>
	/// </remarks>
	/// <seealso cref="RegistryCache" />
	[SuppressMessage("ReSharper", "ArrangeTypeMemberModifiers")]
	[SuppressMessage("ReSharper", "UnusedMember.Local")]
	public class RegistryCacheSupport : HttpApplication {
		/// <summary>
		/// アプリケーション開始時コールバックです。継承する場合、
		/// <see cref="OnApplicationEnd"/>をオーバライドしてください。
		/// </summary>
		/// <param name="sender">イベントソース</param>
		/// <param name="e">イベントパラメータ</param>
		void Application_Start(object sender, EventArgs e) {
			OnApplicationStart(new HttpApplicationStateWrapper(Application));
		}

		/// <summary>
		/// アプリケーション終了時コールバックです。継承する場合、
		/// <see cref="OnApplicationEnd"/>をオーバライドしてください。
		/// </summary>
		/// <param name="sender">イベントソース</param>
		/// <param name="e">イベントパラメータ</param>
		void Application_End(object sender, EventArgs e) {
			OnApplicationEnd(new HttpApplicationStateWrapper(Application));
		}

		/// <summary>
		/// アプリケーション開始時に<see cref="DIContainerRegistry"/>インスタンス
		/// を取得し、アプリケーション全体でキャッシュするよう保存します。
		/// </summary>
		/// <param name="application">アプリケーションステート</param>
		protected virtual void OnApplicationStart(HttpApplicationStateBase application) {
			try {
				var registry = DIContainerRegistry.Load(null, new WebConfigurationLoader());
				RegistryCache.SetDIContainerRegistry(application, registry);
			}
			catch (NoContainerConfigurationException) {
				// Dummy
				RegistryCache.SetDIContainerRegistry(application, null);
			}
		}

		/// <summary>
		/// アプリケーション終了時に保存されている<see cref="DIContainerRegistry"/>
		/// インスタンスを解放します。
		/// </summary>
		/// <param name="application">アプリケーションステート</param>
		protected virtual void OnApplicationEnd(HttpApplicationStateBase application) {
			RegistryCache.SetDIContainerRegistry(application, null);
		}
	}
}
