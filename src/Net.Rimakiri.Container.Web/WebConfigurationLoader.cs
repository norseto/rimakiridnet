// ==============================================================================
//     Net.Rimakiri.Container.Web
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Configuration;
using System.Web.Configuration;
using System.Web.Hosting;

namespace Net.Rimakiri.Container.Web {
	/// <summary>
	/// Webアプリケーション設定ファイルをロードする
	/// <see cref="IConfigurationLoader"/>実装です。
	/// </summary>
	/// <inheritdoc />
	public class WebConfigurationLoader : IConfigurationLoader {
		private const string Home = "~";

		#region Implementation of IConfigurationLoader

		/// <inheritdoc />
		/// <summary>
		/// Webホスト環境の設定ファイルをロードします。
		/// </summary>
		/// <param name="name">対象ファイル</param>
		/// <returns>設定</returns>
		public Configuration LoadConfiguration(string name = null) {
			var virtualPath = name == null ? Home : Home + "/" + name;

			if (virtualPath == Home) {
				return WebConfigurationManager.OpenWebConfiguration(Home);
			}

			var configMap = new ExeConfigurationFileMap {
				ExeConfigFilename = HostingEnvironment.MapPath(virtualPath),
			};
			return ConfigurationManager.OpenMappedExeConfiguration(
						configMap, ConfigurationUserLevel.None);
		}

		#endregion
	}
}
