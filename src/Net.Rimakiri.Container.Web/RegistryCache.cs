// ==============================================================================
//     Net.Rimakiri.Container.Web
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Web;
using System.Web.Routing;

namespace Net.Rimakiri.Container.Web {
	/// <summary>
	/// <see cref="RegistryCacheSupport"/>でキャッシュされた
	/// DIコンテナを使用するためのクラスです。。
	/// </summary>
	/// <seealso cref="RegistryCacheSupport"/>
	[SuppressMessage("ReSharper", "UnusedMember.Global")]
	public static class RegistryCache {
		private const string RegistryKey = "__##DIContainerRegistry";

		/// <summary>
		/// キャッシュされている<see cref="DIContainerRegistry"/>を取得します。
		/// </summary>
		/// <param name="context">コンテキスト</param>
		/// <returns>
		/// キャッシュされている<see cref="DIContainerRegistry"/>。存在しない場合null
		/// </returns>
		public static DIContainerRegistry GetDIContainerRegistry(HttpContextBase context) {
			var app = context.Application;
			return app[RegistryKey] as DIContainerRegistry;
		}

		/// <summary>
		/// キャッシュされている<see cref="DIContainerRegistry"/>を取得します。
		/// </summary>
		/// <param name="application">アプリケーション</param>
		/// <returns>
		/// キャッシュされている<see cref="DIContainerRegistry"/>。存在しない場合null
		/// </returns>
		public static DIContainerRegistry GetDIContainerRegistry(HttpApplicationStateBase application) {
			return application[RegistryKey] as DIContainerRegistry;
		}

		/// <summary>
		/// <see cref="DIContainerRegistry"/>を保存します。
		/// </summary>
		/// <param name="application">アプリケーションステート</param>
		/// <param name="registry">
		/// キャッシュする<see cref="DIContainerRegistry"/>。
		/// </param>
		public static void SetDIContainerRegistry(HttpApplicationStateBase application, DIContainerRegistry registry) {
			var current = application[RegistryKey] as DIContainerRegistry;
			if (registry == null) {
				application[RegistryKey] = string.Empty;
			}
			else {
				application[RegistryKey] = registry;
			}

			current?.Dispose();
		}

		/// <summary>
		/// キャッシュされているレジストリから依存性注入を行います。
		/// </summary>
		/// <param name="context">コンテキスト</param>
		/// <param name="instance">依存性注入対象</param>
		/// <param name="name">コンテナ名称</param>
		public static void BuildUp(HttpContextBase context, object instance, string name) {
			if (instance == null) {
				return;
			}
			var reg = GetDIContainerRegistry(context);
			reg?.Buildup(instance, name);
		}

		/// <summary>
		/// キャッシュされているレジストリから依存性注入を行います。
		/// </summary>
		/// <param name="context">コンテキスト</param>
		/// <param name="instance">依存性注入対象</param>
		/// <param name="names">コンテナ名称候補</param>
		public static void BuildUp(HttpContextBase context, object instance, IEnumerable<string> names) {
			if (instance == null) {
				return;
			}
			var reg = GetDIContainerRegistry(context);
			reg?.Buildup(instance, names);
		}

		/// <summary>
		/// キャッシュされているレジストリから依存性注入を行います。
		/// </summary>
		/// <param name="context">コンテキスト</param>
		/// <param name="instance">依存性注入対象</param>
		/// <param name="name">コンテナ名称</param>
		public static void BuildUp(RequestContext context, object instance, string name) {
			BuildUp(context.HttpContext, instance, name);
		}

		/// <summary>
		/// キャッシュされているレジストリから依存性注入を行います。
		/// </summary>
		/// <param name="context">コンテキスト</param>
		/// <param name="instance">依存性注入対象</param>
		/// <param name="names">コンテナ名称候補</param>
		public static void BuildUp(RequestContext context, object instance, IEnumerable<string> names) {
			BuildUp(context.HttpContext, instance, names);
		}
	}
}
