﻿// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Collections.Generic;

namespace Net.Rimakiri.Command {
	/// <summary>
	/// コマンドビジターインターフェイスです。
	/// </summary>
	public interface ICommandVisitor {
		/// <summary>
		/// 子ビジターを作成します。
		/// </summary>
		/// <returns>作成したビジター</returns>
		/// <remarks>
		/// このメソッドを直接使用しないでください。
		/// <see cref="CommandVisitorExtension.PropagateChild{T}"/>または
		/// <see cref="CommandVisitorExtension.PropagateChildren{T}"/>を
		/// 使用してください。
		/// </remarks>
		ICommandVisitor CreateChildVisitor();

		/// <summary>
		/// 対象コマンドを通知します。
		/// </summary>
		/// <param name="cmd">対象コマンド</param>
		/// <typeparam name="T">コンテキストの型</typeparam>
		void Visit<T>(ICommand<T> cmd) where T: CommandContext;
	}

	/// <summary>
	/// <see cref="ICommandVisitor"/>の拡張機能を提供します。
	/// </summary>
	public static class CommandVisitorExtension {
		/// <summary>
		/// 子コマンドの<see cref="ICommandVisitAcceptor.AcceptVisitor"/>を
		/// 適切に呼び出します。
		/// </summary>
		/// <typeparam name="T">コンテキストの型</typeparam>
		/// <param name="visitor">ビジター</param>
		/// <param name="child">子コマンド</param>
		public static void PropagateChild<T>(this ICommandVisitor visitor, ICommand<T> child) where T: CommandContext {
			if (visitor == null) {
				return;
			}

			if (child is ICommandVisitAcceptor acceptor) {
				acceptor.AcceptVisitor(visitor.CreateChildVisitor());
			}
		}

		/// <summary>
		/// 全ての子コマンドの<see cref="ICommandVisitAcceptor.AcceptVisitor"/>
		/// を適切に呼び出します。
		/// </summary>
		/// <typeparam name="T">コンテキストの型</typeparam>
		/// <param name="visitor">ビジター</param>
		/// <param name="children">子コマンド</param>
		public static void PropagateChildren<T>(this ICommandVisitor visitor,
					IEnumerable<ICommand<T>> children) where T : CommandContext {
			if (visitor == null || children == null) {
				return;
			}
			foreach (var command in children) {
				PropagateChild(visitor, command);
			}
		}
	}
}
