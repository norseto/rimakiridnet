// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Threading;

namespace Net.Rimakiri.Command {
	/// <summary>
	/// <see cref="IRetryPolicy"/>の基底クラスとして
	/// 使用可能なクラスです。
	/// </summary>
	public class RetryPolicyBase {
		private readonly Random random = new Random();

		/// <summary>
		/// 固定遅延ミリ秒を設定または取得します。
		/// </summary>
		public int FixedDelayMs { get; set; } = -1;

		/// <summary>
		/// 可変遅延ミリ秒最小値を設定または取得します。
		/// </summary>
		public int MinDelayMs { get; set; } = 79;

		/// <summary>
		/// 可変遅延ミリ秒最大値を設定または取得します。
		/// </summary>
		public int MaxDelayMs { get; set; } = 157;

		/// <summary>
		/// 遅延時間を取得します。
		/// </summary>
		/// <param name="tryCount">試行回数</param>
		/// <returns>遅延時間</returns>
		protected virtual TimeSpan GetDelay(int tryCount) {
			if (FixedDelayMs > 0) {
				return TimeSpan.FromMilliseconds(FixedDelayMs << tryCount);
			}

			if (MinDelayMs > MaxDelayMs) {
				return TimeSpan.FromMilliseconds(MinDelayMs << tryCount);
			}

			return TimeSpan.FromMilliseconds(
					random.Next(MinDelayMs << tryCount, MaxDelayMs << tryCount));
		}

		/// <summary>
		/// 遅延を行います。
		/// </summary>
		/// <param name="tryCount">試行回数</param>
		protected void Delay(int tryCount) {
			Thread.Sleep(GetDelay(tryCount));
		}
	}
}
