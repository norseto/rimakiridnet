﻿// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

namespace Net.Rimakiri.Command {
	/// <summary>
	/// コマンドの処理のトランザクションスコープを示します。
	/// コマンドのトランザクションスコープは、プロパティ
	/// <see cref="ICommand{T}.TxScope"/>にて示されます。
	/// </summary>
	/// <remarks>
	/// <p>
	/// 未指定値<see cref="Unspecified"/>をデフォルトの値に丸めるには、
	/// <see cref="TxScopeExtension.ToSpecificValue"/>
	/// を使用します。
	/// </p>
	/// </remarks>
	/// <seealso cref="ICommand{T}"/>
	public enum TxScope {
		/// <summary>
		/// 未指定 - デフォルト値を使用します。
		/// </summary>
		Unspecified = 0,

		/// <summary>
		/// トランザクションが必要。既に開始している場合には
		/// 何もしません。
		/// </summary>
		Required = 1,

		/// <summary>
		/// 新規トランザクションが開始されます。
		/// </summary>
		RequiresNew = 3,

		/// <summary>
		/// トランザクションから除外されます。
		/// </summary>
		Suppress = 7,

		/// <summary>
		/// 何もしません。トランザクションが開始している場合には、
		/// そのトランザクション内で実行されます。
		/// </summary>
		Supports = 8,

		/// <summary>
		/// トランザクション内が必須です。
		/// トランザクション外で実行される場合には例外が発生します。
		/// </summary>
		Mandatory = 24,
	}

	/// <summary>
	/// <see cref="TxScope"/>の拡張機能を提供します。
	/// </summary>
	public static class TxScopeExtension {
		/// <summary>
		/// 未指定値をデフォルト値に丸めます。
		/// </summary>
		/// <param name="scope">対象スコープ</param>
		/// <returns>
		/// 対象スコープが未指定値の場合はデフォルト値、
		/// その他の場合にはそのままの値
		/// </returns>
		public static TxScope ToSpecificValue(this TxScope scope) {
			return (scope == TxScope.Unspecified)
				? TxScope.Supports
				: scope;
		}
	}
}
