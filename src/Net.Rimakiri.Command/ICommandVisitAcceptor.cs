﻿// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

namespace Net.Rimakiri.Command {
	/// <summary>
	/// <see cref="ICommandVisitor"/>のアクセプターインターフェースです。
	/// </summary>
	public interface ICommandVisitAcceptor {
		/// <summary>
		/// ビジターを受け入れます。
		/// </summary>
		/// <param name="visitor">通知対象</param>
		/// <remarks>
		/// 通常、自オブジェクトをパラメタとして、<paramref name="visitor"/>
		/// の<see cref="ICommandVisitor.Visit{T}"/>メソッドを呼び出します。
		/// さらに、コマンドから別のコマンドを呼び出している場合には、
		/// <see cref="CommandVisitorExtension.PropagateChild{T}"/>または
		/// <see cref="CommandVisitorExtension.PropagateChildren{T}"/>
		/// を呼出して、別のコマンドの通知処理を呼出す必要があります。
		/// </remarks>
		void AcceptVisitor(ICommandVisitor visitor);
	}
}
