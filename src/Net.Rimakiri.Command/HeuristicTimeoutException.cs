// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using Net.Rimakiri.Command.Properties;

namespace Net.Rimakiri.Command {
	/// <inheritdoc />
	/// <summary>
	/// コマンドフレームワークでトランザクションのタイムアウトの
	/// 発生を自己診断的に検出した場合に送出される例外です。
	/// </summary>
	/// <seealso cref="CommandContext.CheckTimeout"/>
	[Serializable]
	public class HeuristicTimeoutException : TimeoutException {
		/// <inheritdoc />
		/// <summary>
		/// インスタンスを初期化します。
		/// </summary>
		/// <param name="checkPoint">チェックポイント</param>
		/// <param name="timeoutTick">トランザクションのタイムアウト(Tick)</param>
		/// <param name="elapsedTick">処理経過時間(Tick)</param>
		public HeuristicTimeoutException(object checkPoint, long timeoutTick, long elapsedTick)
			: base(string.Format(Resources.HeuristicTimeout, checkPoint,
				TimeSpan.FromTicks(timeoutTick).TotalMilliseconds,
				TimeSpan.FromTicks(elapsedTick).TotalMilliseconds)) {
		}
	}
}
