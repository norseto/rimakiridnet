// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using Net.Rimakiri.Command.Spi;

namespace Net.Rimakiri.Command {
	/// <summary>
	/// 冪等なコマンドから発生した例外からリトライするか否かを
	/// 判定するクラスが実装すべきインターフェイスです。
	/// </summary>
	/// <seealso cref="ICommand{T}.Idempotent"/>
	/// <seealso cref="ICommandServiceSpi.MaxRetry"/>
	/// <seealso cref="ICommandServiceSpi.RetryPolicy"/>
	public interface IRetryPolicy {
		/// <summary>
		/// 発生した例外がリトライ可能かを判定します。
		/// </summary>
		/// <param name="error">発生した例外</param>
		/// <param name="tryCount">試行回数(初回:0)</param>
		/// <returns>リトライ可能な場合、true</returns>
		bool IsRetryable(Exception error, int tryCount);
	}

	/// <summary>
	/// <see cref="IRetryPolicy"/>関連の拡張機能を提供します。
	/// </summary>
	public static class RetryPolicyExtension {
		/// <summary>
		/// 発生した例外からコマンド実行のリトライが可能かを判定します。
		/// </summary>
		/// <param name="policy">対象ポリシー</param>
		/// <param name="tryCount">試行回数(初回:0)</param>
		/// <param name="error">発生した例外</param>
		/// <returns><paramref name="policy"/>がnullか、リトライ可能な場合、true</returns>
		public static bool IsRetryableError(this IRetryPolicy policy, Exception error, int tryCount) {
			return policy != null && policy.IsRetryable(error, tryCount);
		}

		/// <summary>
		/// 例外から指定したタイプの原因例外を取得します。
		/// </summary>
		/// <typeparam name="T">取得する原因例外のタイプ</typeparam>
		/// <param name="ex">発生した例外</param>
		/// <returns>原因例外</returns>
		public static T FindCause<T>(this Exception ex) where T : Exception {
			var candidate = ex;
			T ret = null;
			while ((ret == null) && (candidate != null)) {
				ret = candidate as T;
				candidate = candidate.InnerException;
			}
			return ret;
		}
	}
}
