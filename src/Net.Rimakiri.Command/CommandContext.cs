// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Data;
using System.Data.Common;
using Net.Rimakiri.Command.Spi;

namespace Net.Rimakiri.Command {
	/// <summary>
	/// コマンドの実行状態を保持するコンテキストです。
	/// 主にコマンドから別のコマンドを実行する（コマンドチェイン）際に使用します。
	/// </summary>
	/// <remarks>
	/// <see cref="ICommandService"/>により最初にコマンドが実行される時に
	/// 生成され、そのコマンド中で別のコマンドを実行した場合でも、同じ
	/// インスタンスが使用されます。このライフサイクルを利用して、コマンドの
	/// 呼出しに親子関係が存在する場合に、親子のコマンド間で何らかの情報を属性
	/// としてやり取りすることが可能です（<see cref="SetAttribute"/>、
	/// <see cref="GetAttribute{T}"/>）。
	/// </remarks>
	/// <remarks>
	/// 継承することで、システム特有の情報を保持させることを想定しています。
	/// また、そのような場合には<see cref="IContextLifecycleListener"/>
	/// を使用してシステム特有の情報を初期化することが可能です。
	/// また、システム特有の情報のリソースなどを解放する場合には、
	/// <see cref="Dispose(bool)"/>をオーバーライドしてください。
	/// <see cref="Dispose()"/>の呼出しはフレームワーク側で行われます。
	/// </remarks>
	/// <inheritdoc />
	public class CommandContext : IDisposable {
		/// <summary>
		/// コンテキストSPI
		/// </summary>
		internal ICommandContextSpi ContextSpi { get; set; }

		/// <summary>
		/// 別のコマンドを実行するためのコマンドサービスを取得します。
		/// </summary>
		/// <remarks>
		/// このコマンドのコンテキストは、このサービスで実行されるコマンド
		/// のコンテキストと同じオブジェクトとなります。
		/// </remarks>
		public ICommandService Service => ContextSpi.Service;

		/// <summary>
		/// 現在のトランザクションのロールバックをマークします。
		/// ロールバックをマークした場合、トランザクションは
		/// コミットされません。
		/// </summary>
		public void MarkRollbackOnly() {
			ContextSpi.MarkRollbackOnly();
		}

		/// <summary>
		/// 現在のトランザクションのロールバックがマークされているか
		/// を取得します。
		/// </summary>
		public bool IsMarkedRollback => ContextSpi.IsMarkedRollback();

		/// <summary>
		/// トランザクションのタイムアウトをチェックします。
		/// </summary>
		/// <param name="checkPoint">
		/// タイムアウト発生時に例外のメッセージに出力されるチェックポイント名
		/// </param>
		/// <remarks>
		/// トランザクションのタイムアウト時間を経過しているにも拘わらず（ロール
		/// バックされる）不要な処理を行わないために、このメソッドを呼出してチェックする
		/// ことが可能です。
		/// <paramref name="checkPoint" />は任意のものを指定可能です。タイムアウト
		/// が発生している場合にここで指定したチェックポイントが例外のメッセージ
		/// の一部としてして使用されます。
		/// </remarks>
		public void CheckTimeout(string checkPoint) {
			ContextSpi.CheckTimeout(checkPoint);
		}

		/// <summary>
		/// トランザクションタイムアウトまでの残Tick数を取得します。
		/// </summary>
		/// <returns>
		/// タイムアウトまでの残Tick数。ただし、
		/// タイムアウト設定が存在しない場合：-1、
		/// タイムアウトが経過している場合：0
		/// </returns>
		/// <remarks>
		/// <see cref="IDbCommand"/>のタイムアウト設定などの設定値の算出などに
		/// 使用することを想定しています。
		/// </remarks>
		public long GetTimeoutRemainTicks() {
			return ContextSpi.GetTimeoutRemainTicks();
		}

		/// <summary>
		/// 属性を設定します。同じ名前の属性が存在する場合には上書きされます。
		/// </summary>
		/// <param name="key">キー</param>
		/// <param name="value">属性の値</param>
		/// <seealso cref="GetAttribute{T}"/>
		/// <seealso cref="RemoveAttribute"/>
		/// <remarks>
		/// 属性はコマンドから別のコマンドを呼出す場合に、複数のコマンド間で何らかの
		/// 情報をやり取りする際に使用するためのものとして用意されています。
		/// したがって、コマンド実行中に例外が発生した場合、属性の内容はコマンド実行
		/// 開始時の状態にロールバックされます。例外が発生しなかった場合、属性は蓄積
		/// されていきます。
		/// </remarks>
		public void SetAttribute(string key, object value) {
			ContextSpi.SetAttribute(key, value);
		}

		/// <summary>
		/// 属性を取得します。型が異なる場合はデフォルト値が返却されます。
		/// </summary>
		/// <typeparam name="T">属性の型</typeparam>
		/// <param name="key">キー</param>
		/// <returns>属性の値</returns>
		/// <seealso cref="SetAttribute"/>
		/// <seealso cref="RemoveAttribute"/>
		public T GetAttribute<T>(string key) {
			return ContextSpi.GetAttribute<T>(key);
		}

		/// <summary>
		/// 属性を削除します。
		/// </summary>
		/// <param name="key">キー</param>
		/// <seealso cref="SetAttribute"/>
		/// <seealso cref="RemoveAttribute"/>
		public void RemoveAttribute(string key) {
			ContextSpi.RemoveAttribute(key);
		}

		/// <summary>
		/// コマンドトランザクション属性を設定します。同じ名前の属性に異なる値を設定した場合には
		/// 例外が送出されます。
		/// </summary>
		/// <param name="key">キー</param>
		/// <param name="value">属性の値</param>
		/// <seealso cref="GetTxAttribute{T}"/>
		/// <seealso cref="RemoveTxAttribute"/>
		/// <remarks>
		/// コマンドトランザクション属性とは現在実行中のコマンドトランザクションに紐づいた属性です。
		/// これらはトランザクション終了時(コマンドの実行終了時とは限りません)に全て破棄されます。
		/// その際、設定されている属性値が<see cref="IDisposable"/>を実装している場合、
		/// <see cref="IDisposable.Dispose"/>が自動的に呼び出されます。
		/// <see cref="IDbConnection"/>等を安全にキャッシュすることを可能とするため
		/// に用意されています。
		/// </remarks>
		/// <seealso cref="ICommandTxFactory.Create{T}"/>
		public void SetTxAttribute(string key, object value) {
			ContextSpi.SetScopeAttribute(key, value);
		}

		/// <summary>
		/// コマンドトランザクション属性を取得します。型が異なる場合はデフォルト値が返却されます。
		/// </summary>
		/// <typeparam name="T">属性の型</typeparam>
		/// <param name="key">キー</param>
		/// <returns>属性の値</returns>
		/// <seealso cref="SetTxAttribute"/>
		/// <seealso cref="RemoveTxAttribute"/>
		public T GetTxAttribute<T>(string key) {
			return ContextSpi.GetScopeAttribute<T>(key);
		}

		/// <summary>
		/// コマンドトランザクション属性を削除します。
		/// </summary>
		/// <param name="key">キー</param>
		/// <seealso cref="SetTxAttribute"/>
		/// <seealso cref="RemoveTxAttribute"/>
		/// <returns>属性の値</returns>
		public object RemoveTxAttribute(string key) {
			return ContextSpi.RemoveScopeAttribute(key);
		}

		/// <summary>
		/// 対象のコネクションのローカルデータベーストランザクションを開始します。
		/// </summary>
		/// <param name="connection">対象となるコネクション</param>
		/// <remarks>
		/// コミット・ロールバックはフレームワークで自動で行われます。
		/// </remarks>
		protected void EnlistTx(DbConnection connection) {
			ContextSpi.EnlistTx(connection);
		}

		/// <summary>
		/// 現在の<see cref="TxIsolation"/>を取得します。
		/// </summary>
		public TxIsolation TxIsolation => ContextSpi.TxIsolation;

		/// <summary>
		/// <see cref="IsolationLevel"/>のマッピングを設定します。
		/// </summary>
		/// <remarks>
		/// 特定のデータベースで、サポートしていないトランザクション分離レベルを別のものに
		/// 変えるために使用します。
		/// </remarks>
		/// <param name="mapping"><see cref="IsolationLevel"/>のマッピング</param>
		/// <seealso cref="ICommandTx.IsolationLevelMapping"/>
		protected void SetIsolationLevelMapping(Func<IsolationLevel, IsolationLevel> mapping) {
			ContextSpi.SetIsolationLevelMapping(mapping);
		}

		/// <summary>
		/// 現在のトランザクションがグローバルトランザクションをサポート
		/// しているかを取得します。
		/// </summary>
		protected bool SupportsGlobalTx => ContextSpi.SupportsGlobalTx;

		/// <summary>
		/// ローカルデータベーストランザクションを取得します。
		/// </summary>
		/// <returns>
		/// <see cref="EnlistTx"/>によって開始されたローカルデータベース
		/// トランザクション。開始していない場合や、ローカルデータベース
		/// トランザクションを使用していない場合にはnull
		/// </returns>
		/// <remarks>
		/// コミット・ロールバックはフレームワークで自動で行われます。
		/// </remarks>
		protected DbTransaction GetDbTransaction() {
			return ContextSpi.GetDbTransaction();
		}

		#region IDisposable
		/// <summary>
		/// インスタンスのリソースを解放します。
		/// </summary>
		/// <param name="disposing">明示的な開放の場合True</param>
		protected virtual void Dispose(bool disposing) {
			if (!disposing) {
				return;
			}
			ContextSpi.Dispose();
		}

		/// <inheritdoc />
		public void Dispose() {
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// インスタンスのリソースを解放します。
		/// </summary>
		~CommandContext() {
			Dispose(false);
		}
		#endregion
	}
}
