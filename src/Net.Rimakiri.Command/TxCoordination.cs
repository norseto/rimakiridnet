﻿// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

namespace Net.Rimakiri.Command {
	/// <summary>
	/// トランザクション分離レベルの調整方法を示します。
	/// </summary>
	/// <seealso cref="CommandServiceBuilder{T}.TxCoordination"/>
	/// <seealso cref="IIsolationCoodinatee.TxIntention"/>
	public enum TxCoordination {
		/// <summary>
		/// 調整を行いません。
		/// </summary>
		None,

		/// <summary>
		/// 楽観的調整を行います。
		/// </summary>
		Optimistic,

		/// <summary>
		/// 悲観的調整を行います。
		/// </summary>
		Pessimistic,

		/// <summary>
		/// 厳格な調整を行います。用途の内、最も分離レベルの高いものが
		/// 採用されます。
		/// </summary>
		Strict,

		/// <summary>
		/// デフォルトの調整（楽観的調整）を行います。
		/// </summary>
		Normal,
	}
}
