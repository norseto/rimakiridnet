// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using Net.Rimakiri.Command.Properties;
using Net.Rimakiri.Command.Spi;

namespace Net.Rimakiri.Command {
	/// <inheritdoc />
	/// <summary>
	/// コマンドサービスの実装クラスです（ファサード）。
	/// しかし、コマンドの実際の実行は<see cref="ICommandServiceSpi"/>に、
	/// コンテキストの管理は<see cref="ICommandContextSpi"/>に移譲されます。
	/// </summary>
	/// <remarks>
	/// このクラスのインスタンスを直接生成するのではなく、
	/// <see cref="CommandServiceBuilder"/>または
	/// <see cref="CommandServiceBuilder{T}"/>を使用してください。
	/// </remarks>
	/// <typeparam name="TC">コンテキストの型</typeparam>
	public class CommandServiceFacade<TC> : ICommandService where TC: CommandContext {
		/// <summary>
		/// コンテキストの管理に使用される<see cref="ICommandContextSpi"/>
		/// を生成する<see cref="ICommandContextSpiFactory"/>を取得または設定します。
		/// </summary>
		public ICommandContextSpiFactory ContextSpiFactory { get; set; }

		/// <summary>
		/// コマンドの実行に使用される<see cref="ICommandServiceSpi"/>
		/// を取得または設定します。
		/// </summary>
		public ICommandServiceSpi ServiceSpi { get; set; }

		/// <summary>
		/// ライフサイクルリスナを設定します。
		/// </summary>
		/// <remarks>
		/// 実際には、指定されたリスナの呼出として
		/// <see cref="PostContextCreate"/>および
		/// <see cref="PreContextDestroy"/>のコールバックが
		/// 設定されます。
		/// </remarks>
		/// <seealso cref="PostContextCreate"/>
		/// <seealso cref="PreContextDestroy"/>
		public IContextLifecycleListener<TC> LifecycleListener {
			set {
				if (value == null) {
					PostContextCreate = null;
					PreContextDestroy = null;
					return;
				}
				PostContextCreate = value.OnPostContextCreate;
				PreContextDestroy = value.OnPreContextDestroy;
			}
		}

		/// <summary>
		/// コンテキストが生成されたときに実行されるコールバックアクションを
		/// 取得または設定します。
		/// </summary>
		public Action<TC> PostContextCreate { get; set; }

		/// <summary>
		/// コンテキストが破棄されるときに実行されるコールバックアクションを
		/// 取得または設定します。
		/// </summary>
		public Action<TC> PreContextDestroy { get; set; }

		/// <summary>
		/// 使用されるコンテキストの型を取得または設定します。
		/// 設定されていない場合には、最初に実行するコマンドが要求する型が
		/// 使用されます。
		/// </summary>
		public Type ContextType { get; set; }

		#region Implementation of ICommandService

		/// <inheritdoc cref="ICommandService.ExecuteCommand{T}"/>
		public void ExecuteCommand<T>(ICommand<T> command) where T: CommandContext {
			CheckSpi();

			var serviceSpi = ServiceSpi;
			var contextSpiFactory = ContextSpiFactory;

			var service = new ThinCommandServiceImpl {
				ServiceSpi = serviceSpi
			};

			CommandContext context;
			try {
				context = CreateContext<T>();
			}
			catch (Exception ex) {
				throw new InvalidOperationException(string.Format(
					Resources.ContextFactoryNullCreation, typeof(T).FullName), ex);
			}
			using (context) {
				context.ContextSpi = contextSpiFactory.Create(service);
				service.Context = context;
				FireCallback(PostContextCreate, context);
				service.ExecuteCommand(command);
				FireCallback(PreContextDestroy, context);
			}
		}

		private void CheckSpi() {
			if (ServiceSpi == null) {
				throw new InvalidOperationException(Resources.NullServiceSpi);
			}
			if (ContextSpiFactory == null) {
				throw new InvalidOperationException(Resources.NullContextSpiFactory);
			}
		}

		private static void FireCallback(Action<TC> action, CommandContext context) {
			if (action == null) {
				return;
			}
			var ctx = context as TC;
			ctx?.ContextSpi.FireLifecycleCallback(action, ctx);
		}

		private CommandContext CreateContext<T>() where T : CommandContext {
			var type = ContextType;
			if (type == null) {
				return Activator.CreateInstance<T>();
			}

			if (!typeof(CommandContext).IsAssignableFrom(type)) {
				throw new InvalidOperationException(string.Format(
					Resources.InvalidContextType,
						typeof(T).FullName, type.FullName));
			}
			return Activator.CreateInstance(type) as CommandContext;
		}

		#endregion

		/// <summary>
		/// コマンドサービスの実装
		/// </summary>
		private sealed class ThinCommandServiceImpl : ICommandService {
			/// <summary>
			/// サービスSPI
			/// </summary>
			public ICommandServiceSpi ServiceSpi { private get; set; }

			/// <summary>
			/// 使用されるコンテキスト
			/// </summary>
			public CommandContext Context { private get; set; }

			/// <inheritdoc cref="ICommandService.ExecuteCommand{T}"/>
			public void ExecuteCommand<T>(ICommand<T> command) where T: CommandContext {
				var ctx = Context;
				var context = ctx as T;
				if (context == null) {
					throw new InvalidOperationException(
							string.Format(Resources.InvalidContextType,
								typeof(T).FullName, Context.GetType().FullName));
				}
				ServiceSpi.ExecuteCommand(context, command);
			}
		}
	}

	/// <summary>
	/// <see cref="CommandContext"/>をコンテキストの型とした
	/// <see cref="CommandServiceFacade{TC}"/>クラスです。
	/// </summary>
	public class CommandServiceFacade : CommandServiceFacade<CommandContext> {
		// Empty		
	}
}
