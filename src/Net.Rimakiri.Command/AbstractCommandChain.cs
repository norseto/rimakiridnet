// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Collections.Generic;
using System.Linq;

namespace Net.Rimakiri.Command {
	/// <summary>
	/// 複数のコマンドを順次実行する<see cref="ICommand{T}"/>実装クラスです。
	/// </summary>
	/// <typeparam name="T">コンテキストの型</typeparam>
	/// <inheritdoc cref="ICommand{T}" />
	public abstract class AbstractCommandChain<T> : ICommand<T>, IIsolationCoodinatee
			where T: CommandContext {
		private readonly List<ICommand<T>> commands = new List<ICommand<T>>();

		/// <summary>
		/// 追加されたコマンドを取得します。
		/// </summary>
		protected List<ICommand<T>> Commands => commands;

		/// <summary>
		/// <seealso cref="AbstractCommandChain{T}"/>クラスのインスタンスを
		/// 初期化します。
		/// </summary>
		protected AbstractCommandChain() {
		}

		/// <summary>
		/// 実行するコマンドを指定して<seealso cref="AbstractCommandChain{T}"/>
		/// クラスのインスタンスを初期化します。
		/// </summary>
		/// <param name="commands">順次実行されるコマンド</param>
		protected AbstractCommandChain(params ICommand<T>[] commands) {
			AddCommands(commands);
		}

		/// <summary>
		/// 実行するコマンドを追加します。
		/// </summary>
		/// <param name="targets">追加するコマンド</param>
		public void Add(params ICommand<T>[] targets) {
			AddCommands(targets);
		}

		/// <summary>
		/// 指定した順番のコマンドを取得します。
		/// </summary>
		/// <param name="index">取得するコマンドのインデックス</param>
		/// <returns>指定したインデックスのコマンド</returns>
		public ICommand<T> this[int index]
			=> index < 0 || commands.Count <= index ? null : commands[index];

		private void AddCommands(ICommand<T>[] targets) {
			if (targets == null) {
				return;
			}
			commands.AddRange(targets.Where(it => it != null));
		}

		/// <summary>
		/// 指定されたコマンドを実行対象から削除します。
		/// </summary>
		/// <param name="targets">削除するコマンド</param>
		public void Remove(params ICommand<T>[] targets) {
			if (targets == null) {
				return;
			}
			foreach (var command in targets.Where(it => it != null)) {
				commands.Remove(command);
			}
		}

		#region Implementation of ICommand<in T>
		/// <inheritdoc />
		public abstract Idempotent Idempotent { get; set; }
		/// <inheritdoc />
		public abstract TxScope TxScope { get; set; }

		/// <inheritdoc />
		/// <remarks>
		/// 追加されたコマンドを順次実行します。
		/// </remarks>
		public virtual void Execute(T context) {
			var service = context.Service;
			foreach (var command in commands) {
				service.ExecuteCommand(command);
			}
		}

		/// <inheritdoc />
		/// <remarks>
		/// このクラスでは何もしません。
		/// </remarks>
		public virtual void PostExecute(T context) {
			// Nothing to do
		}

		/// <inheritdoc />
		/// <remarks>
		/// このクラスでは何もしません。
		/// </remarks>
		public virtual void PreExecute(T context) {
			// Nothing to do
		}
		#endregion

		#region Implementation of IIsolationCodinatee
		/// <inheritdoc />
		public TxIntention TxIntention => TxIntention.UnSpecified;

		/// <inheritdoc />
		public void AcceptVisitor(ICommandVisitor visitor) {
			visitor.Visit(this);
			visitor.PropagateChildren(commands);
		}
		#endregion
	}
}
