// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Linq;

namespace Net.Rimakiri.Command {
	/// <inheritdoc />
	/// <summary>
	/// 複数の<see cref="IRetryPolicy" />のコンポジットポリシークラスです。
	/// </summary>
	public class CompositeRetryPolicy : IRetryPolicy {
		/// <summary>
		/// 複合する<see cref="IRetryPolicy"/>を設定します。
		/// </summary>
		public IRetryPolicy[] RetryPolicies { set; private get; }

		#region Implementation of IRetryPolicy
		/// <inheritdoc />
		/// <summary>
		/// <see cref="RetryPolicies"/>で指定したポリシーのいずれかが
		/// リトライ可能と判断した場合、リトライ可能と判断します。
		/// </summary>
		public bool IsRetryable(Exception error, int tryCount) {
			var policies = RetryPolicies;
			return policies != null && policies.Any(
						p => p != null && p.IsRetryable(error, tryCount));
		}
		#endregion
	}
}
