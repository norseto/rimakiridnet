// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using Net.Rimakiri.Command.Spi;

namespace Net.Rimakiri.Command {
	/// <inheritdoc />
	/// <summary>
	/// <see cref="ICommandServiceBuilder{T}"/>を実装したコマンドサービスを生成する
	/// ビルダクラスです。実際には <see cref="CommandServiceFacade"/>クラスの
	/// インスタンスが生成されます。
	/// </summary>
	/// <typeparam name="T">コンテキストの型</typeparam>
	/// <remarks>
	/// <see cref="CommandServiceBuilder"/>クラスでは、コンテキストの型は最初に
	/// 実行されるコマンドが要求する型になりますが、コンテキストの型を指定する
	/// ことで、常に指定された型のコンテキストを使用することができます。
	/// </remarks>
	/// <remarks>
	/// 以下のようなカスタマイズが可能です。
	/// <list type="table">
	/// <listheader>
	/// <term>内容</term>  
	/// <description>方法</description>  
	/// </listheader>
	/// 
	/// <item>
	/// <term>トランザクションを常にロールバックしたい</term>
	/// <description>
	/// <see cref="NoCommit"/>プロパティにtrueを設定します（デフォルトfalse）。
	/// ただし、<see cref="ICommand{T}.TxScope"/>プロパティが
	/// <see cref="TxScope.Required"/>か<see cref="TxScope.RequiresNew"/>
	/// であるコマンドから実行されたトランザクションのみが管理（ロールバック）されます。
	/// </description>
	/// </item>
	/// 
	/// <item>
	/// <term>トランザクションタイムアウトを変更したい</term>
	/// <description><see cref="Timeout"/>プロパティを設定します（デフォルト60秒）</description>
	/// </item>
	/// 
	/// <item>
	/// <term>トランザクション分離レベルを変更したい</term>
	/// <description>
	/// <see cref="TxIsolation"/>プロパティを設定します（デフォルトREAD_COMMITTED）
	/// </description>
	/// </item>
	/// 
	/// <item>
	/// <term>グローバル（分散）トランザクションを使用したい</term>
	/// <description>
	/// <see cref="TransactionFactory"/>プロパティに
	/// 別途提供されている、<c>ScopeRuntimeTxScopeFactory</c>を設定します
	/// （デフォルトローカル（データベース）トランザクション）
	/// </description>
	/// </item>
	/// 
	/// <item>
	/// <term>デッドロックが発生した場合リトライしたい</term>
	/// <description>
	/// <see cref="RetryPolicy"/>に使用するデータベースに合わせた
	/// <see cref="IRetryPolicy"/>を設定します（デフォルトリトライなし）。
	/// ただし、実行されるコマンドの<see cref="ICommand{T}.Idempotent"/>プロパティが
	/// <see cref="Idempotent.Transactional"/>でなければなりません。
	/// </description>
	/// </item>
	/// 
	/// <item>
	/// <term><see cref="CommandContext"/>に共通の特定の値を設定したい</term>
	/// <description>
	/// <see cref="IContextLifecycleListener{T}"/>を派生したクラスを作成し、
	/// <see cref="IContextLifecycleListener{T}.OnPostContextCreate"/>で特定の
	/// 値を設定します。
	/// </description>
	/// </item>
	/// 
	/// <item>
	/// <term><see cref="CommandContext"/>にコマンドを実行する毎に異なる特定の値を
	/// 設定したい</term>
	/// <description>
	/// <see cref="Build"/>メソッドの<c>postCreate</c>パラメータに特定の値を設定す
	/// るデリゲートを指定します。
	/// </description>
	/// </item>
	/// 
	/// </list>
	/// </remarks>
	/// <remarks>
	/// このクラスは、最初の<see cref="Build"/>呼出し時に内部の初期化を行います。
	/// シングルトンとして使用するような場合には、インスタンス生成後に
	/// <see cref="InitializeForSingleton"/>を呼出すか、<see cref="Build"/>
	/// で生成した<see cref="ICommandService"/>をシングルトンとして使用してください。
	/// 生成された<see cref="ICommandService"/>は内部にステートを保持していないため、
	/// 安全にシングルトンとして使用可能です。
	/// </remarks>
	/// <seealso cref="ICommandService"/>
	public class CommandServiceBuilder<T> : ICommandServiceBuilder<T> where T : CommandContext {
		private class DumbRetryPolicy : IRetryPolicy {
			internal static readonly DumbRetryPolicy Instance = new DumbRetryPolicy();

			private DumbRetryPolicy() {
				// Empty
			}
			#region Implementation of IRetryPolicy
			public bool IsRetryable(Exception error, int tryCount) {
				throw new NotImplementedException();
			}
			#endregion
		}

		private bool initialized;

		/// <summary>
		/// トランザクションでコミットを行わないかを設定または取得します。
		/// </summary>
		public bool NoCommit { get; set; }

		/// <summary>
		/// 使用する<see cref="ICommandContextSpiFactory"/>を取得または設定します。
		/// 通常は設定する必要はありません。
		/// </summary>
		/// <remarks>
		/// 設定されない場合、
		/// <see cref="DefaultContextSpiFactoryImpl"/>のインスタンス
		/// が設定されたものとして扱われます。
		/// </remarks>
		public ICommandContextSpiFactory ContextSpiFactory { get; set; }

		/// <summary>
		/// 使用する<see cref="ICommandServiceSpi"/>を取得または設定します。
		/// 通常は設定する必要はありません。
		/// </summary>
		/// <remarks>
		/// 設定されない場合、<see cref="DefaultServiceSpiImpl"/>のインスタンス
		/// が設定されたものとして扱われます。
		/// </remarks>
		public ICommandServiceSpi ServiceSpi { get; set; }

		/// <summary>
		/// 使用する<see cref="ICommandTxFactory"/>を取得または設定します。
		/// </summary>
		/// <seealso cref="LocalCommandTxFactory"/>
		public ICommandTxFactory TransactionFactory { get; set; }

		/// <summary>
		/// デフォルトのトランザクション分離レベルを取得または設定します。
		/// </summary>
		/// <seealso cref="ICommandTxFactory.IsolationLevel"/>
		public TxIsolation IsolationLevel { get; set; } = DefaultTxConstants.TxIsolation;

		/// <summary>
		/// トランザクション分離レベルの調整方法を取得または設定します。
		/// </summary>
		public TxCoordination TxCoordination { get; set; } = TxCoordination.Normal;

		/// <summary>
		/// トランザクションタイムアウトを取得または設定します。
		/// </summary>
		public TimeSpan Timeout { get; set; } = DefaultTxConstants.TxTimeout;

		/// <summary>
		/// 最大リトライ数を取得または設定します。
		/// </summary>
		/// <remarks>
		/// コマンドは最大ここで指定したリトライ回数+1回実行されます。
		/// </remarks>
		public int MaxRetry { get; set; } = -1;

		/// <summary>
		/// リトライポリシーを設定します。
		/// 設定されたリトライポリシーの判定結果により、リトライを行います。
		/// 設定されていない場合はリトライはされません。
		/// </summary>
		/// <seealso cref="MaxRetry"/>
		/// <seealso cref="IRetryPolicy"/>
		public IRetryPolicy RetryPolicy { get; set; } = DumbRetryPolicy.Instance;

		/// <summary>
		/// コンテキストライフサイクルリスナを取得または設定します。
		/// </summary>
		/// <remarks>
		/// <see cref="Build"/>実行時にコールバックが指定されない場合、
		/// ここで設定されているリスナが使用されます。
		/// </remarks>
		public virtual IContextLifecycleListener<T> ContextLifecycleListener { get; set; }

		/// <summary>
		/// 初期化を行います。シングルトンで使用する場合には、このメソッドを
		/// 呼び出す必要があります。
		/// </summary>
		public void InitializeForSingleton() {
			InnerInitialize();
			initialized = true;
		}

		private void InnerInitialize() {
			if (ServiceSpi == null) {
				ServiceSpi = new DefaultServiceSpiImpl();
			}
			if (MaxRetry >= 0) {
				ServiceSpi.MaxRetry = MaxRetry;
			}
			if (RetryPolicy != DumbRetryPolicy.Instance) {
				ServiceSpi.RetryPolicy = RetryPolicy;
			}

			if (ServiceSpi.CommandTxFactory == null) {
				ServiceSpi.CommandTxFactory = TransactionFactory ?? new LocalCommandTxFactory();
			}

			var factory = ServiceSpi.CommandTxFactory;
			factory.TxCoordination = TxCoordination;
			factory.IsolationLevel = IsolationLevel;
			factory.Timeout = Timeout;
			factory.NoCommit = NoCommit;

			if (ContextSpiFactory == null) {
				ContextSpiFactory = new DefaultContextSpiFactoryImpl();
			}
		}

		/// <summary>
		/// <see cref="ICommandService"/>を実装したコマンドサービスを生成します。
		/// </summary>
		/// <param name="type">コンテキストの型</param>
		/// <param name="postCreate">コンテキスト生成時コールバック</param>
		/// <param name="preDestroy">コンテキスト破棄時コールバック</param>
		/// <returns>生成したコマンドサービス</returns>
		protected ICommandService InnerBuild(Type type, Action<T> postCreate = null,
						Action<T> preDestroy = null) {
			if (!initialized) {
				InnerInitialize();
			}
			var facade = new CommandServiceFacade<T> {
				ContextSpiFactory = ContextSpiFactory,
				ServiceSpi = ServiceSpi,
				ContextType = type,
				LifecycleListener = ContextLifecycleListener,
			};

			facade.PostContextCreate = MergeAction(facade.PostContextCreate, postCreate);
			facade.PreContextDestroy = MergeAction(facade.PreContextDestroy, preDestroy);

			return facade;
		}

		private static Action<T> MergeAction(Action<T> current, Action<T> addition) {
			return (addition == null) ? current
					: (current == null) ? addition
					: ctx => {
						current(ctx);
						addition(ctx);
					};
		}

		/// <inheritdoc />
		public virtual ICommandService Build(Action<T> postCreate = null,
			Action<T> preDestroy = null) {
			return InnerBuild(typeof(T), postCreate, preDestroy);
		}
	}

	/// <inheritdoc cref="CommandServiceBuilder{T}" />
	/// <summary>
	/// <see cref="ICommandServiceBuilder"/>を実装したコマンドサービスを生成する
	/// ビルダクラスです。実際には <see cref="CommandServiceFacade"/>クラスの
	/// インスタンスが生成されます。
	/// </summary>
	/// <seealso cref="ICommandService"/>
	public class CommandServiceBuilder : CommandServiceBuilder<CommandContext>, ICommandServiceBuilder {

		#region Overrides of CommandServiceBuilder<CommandContext>

		/// <inheritdoc cref="CommandServiceBuilder{T}.Build"/>
		public override ICommandService Build(Action<CommandContext> postCreate = null,
							Action<CommandContext> preDestroy = null) {
			return InnerBuild(null, postCreate, preDestroy);
		}

		#endregion
	}
}
