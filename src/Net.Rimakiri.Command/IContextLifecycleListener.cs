﻿// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

namespace Net.Rimakiri.Command {
	/// <summary>
	/// コマンドで使用されるコンテキストのライフサイクルコールバック
	/// が実装すべきインターフェイスです。
	/// </summary>
	/// <typeparam name="T">コンテキストの型</typeparam>
	public interface IContextLifecycleListener<in T> where T: CommandContext {
		/// <summary>
		/// コンテキストが生成され、使用可能になった時点でコールバックされます。
		/// </summary>
		/// <param name="context">対象コンテキスト</param>
		void OnPostContextCreate(T context);

		/// <summary>
		/// コンテキストが破棄される前にコールバックされます。
		/// </summary>
		/// <param name="context">対象コンテキスト</param>
		void OnPreContextDestroy(T context);
	}

	/// <summary>
	///	<see cref="IContextLifecycleListener{T}"/>のコンテキストの型が
	/// <see cref="CommandContext"/>の場合のインターフェイスです。
	/// </summary>
	public interface IContextLifecycleListener : IContextLifecycleListener<CommandContext> {
		// Empty
	}
}
