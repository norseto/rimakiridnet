// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

namespace Net.Rimakiri.Command.Spi {
	/// <summary>
	/// デフォルトの<see cref="IIsolationCoordinatorFactory"/>の実装クラスです。
	/// </summary>
	/// <seealso cref="ICommandTxFactory"/>
	/// <inheritdoc cref="IIsolationCoordinatorFactory"/>
	public class DefaultCoordinatorFactoryImpl : IIsolationCoordinatorFactory {
		#region Implementation of IIsolationCoordinatorFactory
		/// <inheritdoc />
		/// <remarks>
		/// パラメタにより、<see cref="IIsolationCoordinator.Coordinate"/>の際に、
		/// 以下の挙動の<see cref="IIsolationCoordinator"/>実装が返却されます。
		/// <ul>
		/// <li>
		/// <paramref name="coordination"/>が<see cref="TxCoordination.Strict"/>
		/// の場合<br/>
		/// 全ての<see cref="TxIntention"/>を以下の<see cref="TxIsolation"/>に
		/// マッピングし、<see cref="TxIsolationExtension.ToDbIsolationLevel"/>で
		/// 定義された順番で検出された分離レベルが使用されます。
		/// <ol>
		/// <li>
		/// <see cref="TxIntention.Insert"/>または<see cref="TxIntention.Delete"/>
		/// が含まれる場合、<see cref="TxIsolation.Serializable"/>
		/// </li>
		/// <li>
		/// <see cref="TxIntention.Update"/>が含まれる場合、
		/// <see cref="TxIsolation.RepeatableRead"/>
		/// </li>
		/// <li>
		/// <see cref="TxIntention.Select"/>または
		/// <see cref="TxIntention.SelectForUpdate"/>
		/// が含まれる場合、<see cref="TxIsolation.ReadCommitted"/>
		/// </li>
		/// <li>
		/// その他の場合、<see cref="TxIsolation.Unspecified"/>
		/// </li>
		/// </ol>
		/// 上記の結果が、<see cref="TxIsolation.ReadCommitted"/>
		/// または<see cref="TxIsolation.Unspecified"/>の場合、
		/// <see cref="TxIntention.Select"/>もしくは
		/// <see cref="TxIntention.SelectForUpdate"/>が複数回存在する場合(
		/// ただし、全て<see cref="TxIntention.SelectForUpdate"/>の場合は除く)、
		/// <see cref="TxIsolation.RepeatableRead"/>
		/// </li>
		/// 
		/// <li>
		/// <paramref name="coordination"/>が<see cref="TxCoordination.Pessimistic"/>
		/// の場合<br/>
		/// 参照系(<see cref="TxIntention.Select"/>、<see cref="TxIntention.SelectForUpdate"/>)
		/// だけの場合もしくは更新系(<see cref="TxIntention.Update"/>、
		/// <see cref="TxIntention.Delete"/>、<see cref="TxIntention.Insert"/>、
		/// <see cref="TxIntention.SelectForUpdate"/>)だけの場合、
		/// デフォルト値(<see cref="TxIntention.UnSpecified"/>)。
		/// その他の場合は、
		/// <paramref name="coordination"/>が<see cref="TxCoordination.Strict"/>
		/// の場合と同様。ただし、<see cref="TxIntention.Select"/>、
		/// <see cref="TxIntention.SelectForUpdate"/>の複数回判定はありません。
		/// </li>
		/// 
		/// <li>
		/// <paramref name="coordination"/>が<see cref="TxCoordination.Optimistic"/>
		/// の場合<br/>
		/// 最初に更新(<see cref="TxIntention.Update"/>、<see cref="TxIntention.SelectForUpdate"/>)
		/// 系が実行される場合には、デフォルト値(<see cref="TxIntention.UnSpecified"/>)。
		/// その他の場合は、
		/// <paramref name="coordination"/>が<see cref="TxCoordination.Pessimistic"/>
		/// の場合と同様。
		/// </li>
		/// 
		/// <li>
		/// <paramref name="coordination"/>が<see cref="TxCoordination.Normal"/>
		/// の場合<br/>
		/// <paramref name="coordination"/>が<see cref="TxCoordination.Optimistic"/>
		/// の場合と同様。
		/// </li>
		/// 
		/// </ul>
		/// </remarks>
		public IIsolationCoordinator CreateCoordinator(TxCoordination coordination) {
			return (coordination == TxCoordination.Optimistic) ? new OptimisticIsolationCoordinator()
					: (coordination == TxCoordination.Pessimistic) ? new PessimisticIsolationCoordinator()
					: (coordination == TxCoordination.Strict) ? new StrictIsolationCoordinator()
					: (IIsolationCoordinator)new OptimisticIsolationCoordinator();
		}
		#endregion
	}
}
