// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Data;
using System.Data.Common;
using Net.Rimakiri.Command.Properties;

namespace Net.Rimakiri.Command.Spi {
	/// <summary>
	/// <see cref="DbTransaction"/>を使用する<see cref="ICommandTx"/>
	/// を生成するファクトリクラスです。
	/// </summary>
	/// <remarks>
	/// このクラスが生成したインスタンスに対しては<see cref="ICommandTx.Enlist"/>の
	/// 呼出しは必須です。
	/// </remarks>
	/// <seealso cref="ICommandServiceSpi"/>
	/// <inheritdoc />
	public class LocalCommandTxFactory : AbstractCommandTxFactory {
		#region Overrides of AbstractCommandTxFactory
		/// <inheritdoc />
		public override ICommandTx Create<T>(ICommand<T> command, TxScope currentScope) {
			var inTran = currentScope == TxScope.Required || currentScope == TxScope.RequiresNew;
			var scope = command.TxScope;
			var specificScope = scope.ToSpecificValue();
			if (!inTran && specificScope == TxScope.Mandatory) {
				throw new InvalidOperationException(Resources.MandatoryNeedsTx);
			}

			var nextScope = RequestedTxScope(specificScope);
			if (nextScope == null || inTran && nextScope == TxScope.Required
					|| nextScope == TxScope.Suppress && currentScope == nextScope) {
				return null;
			}

			var isolation = GetRuntimeIsolation(command, specificScope);

			var ret = new DbTransactionWrapper(isolation, NoCommit);
			return ret;
		}
		#endregion

		private static TxScope? RequestedTxScope(TxScope scope) {
			return (scope == TxScope.Required || scope == TxScope.RequiresNew
					|| scope == TxScope.Suppress)
				? scope : (TxScope?)null;
		}
	}

	internal sealed class DbTransactionWrapper : ICommandTx {
		private readonly bool noCommit;
		private readonly TxIsolation isolation;
		private DbTransaction Tran { get; set; }

		public DbTransactionWrapper(TxIsolation isolation, bool noCommit) {
			this.isolation = isolation.Simplify();
			this.noCommit = noCommit;
		}

		#region Implementation of ICommandTx
		/// <inheritdoc />
		public TxIsolation TxIsolation {
			get { return isolation; }
		}

		/// <inheritdoc />
		public bool SupportsGlobal {
			get { return false; }
		}

		/// <inheritdoc />
		public DbTransaction DbTransaction {
			get { return Tran; }
		}

		/// <inheritdoc />
		public void Complete() {
			var tx = Tran;
			if (tx != null && !noCommit) {
				tx.Commit();
			}
		}

		/// <inheritdoc />
		public void Enlist(DbConnection connection) {
			if (Tran == null || Tran.Connection == null) {
				var origin = isolation.ToDbIsolationLevel();
				var mapping = IsolationLevelMapping;
				var converted = (mapping != null) ? mapping(origin) : origin;
				Tran = connection.BeginTransaction(converted);
				return;
			}
			if (Tran.Connection != connection) {
				throw	new InvalidOperationException(Resources.LocalMultiConTx);
			}
		}

		/// <inheritdoc />
		public Func<IsolationLevel, IsolationLevel> IsolationLevelMapping { get; set; }
		#endregion

		#region Implementation of IDisposable
		/// <inheritdoc />
		public void Dispose() {
			var tx = Tran;
			Tran = null;
			DbConnection connection = null;
			if (tx != null) {
				connection = tx.Connection;
				tx.Dispose();
			}
			if (connection != null) {
				try {
					connection.Dispose();
				}
				catch (Exception) {
					// Ignore.
				}
			}
		}
		#endregion
	}
}
