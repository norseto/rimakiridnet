// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace Net.Rimakiri.Command.Spi {
	/// <summary>
	/// デフォルトの<see cref="ICommandContextSpi"/>実装です。
	/// </summary>
	/// <seealso cref="DefaultContextSpiFactoryImpl"/>
	/// <inheritdoc cref="ICommandContextSpi"/>
	public class DefaultContextSpiImpl : ICommandContextSpi {
		private readonly AttributeHolder attributes = new AttributeHolder();
		private readonly Stack<ContextScopeStatus> status = new Stack<ContextScopeStatus>(3);
		private bool areIdempotent = true;

		#region Implementation of ICommandContextSpi
		/// <inheritdoc />
		public ICommandService Service { get; }

		/// <inheritdoc />
		public int TxNestCount => status.Count;

		/// <inheritdoc />
		public TxScope TxScope => status.Peek().TxScope;
		#endregion

		/// <summary>
		/// <see cref="DefaultContextSpiImpl"/>クラスのインスタンスを初期化します。
		/// </summary>
		/// <param name="service">サービス</param>
		public DefaultContextSpiImpl(ICommandService service) {
			Service = service;
			PushNewScopeStatus(TxScope.Supports, null);
		}

		#region Implementation of ICommandContextSpi
		/// <inheritdoc />
		public TxIsolation TxIsolation => status.Peek().TxIsolation;

		/// <inheritdoc />
		public bool SupportsGlobalTx => status.Peek().SupportsGlobal;

		/// <inheritdoc cref="ICommandContextSpi.CheckTimeout"/>
		public void CheckTimeout(string checkPoint) {
			status.Peek().CheckTimeout(checkPoint);
		}

		/// <inheritdoc />
		public long GetTimeoutRemainTicks() {
			return status.Peek().GetTimeoutRemainTicks();
		}

		/// <summary>
		/// コマンドスタックが全て冪等性のあるものであるかを判定します。
		/// </summary>
		/// <returns>全て冪等性がある</returns>
		public bool WereIdempotent() {
			return areIdempotent;
		}

		/// <inheritdoc cref="ICommandContextSpi.OnPreCallExecute{T}"/>
		public void OnPreCallExecute<T>(ICommand<T> command, long timeoutTick)
						where T : CommandContext {
			attributes.Begin();
			status.Peek().RegisterTimeout(timeoutTick);
		}

		/// <inheritdoc />
		/// <remarks>
		/// このクラスでは、<see cref="ICommandContextSpi.CheckTimeout"/>
		/// を呼び出す。
		/// </remarks>
		public void OnPostCallExecute<T>(ICommand<T> command) where T : CommandContext {
			attributes.Commit();
			UpdateIdempotentState(command);
			CheckTimeout(command.GetType().FullName);
		}

		/// <inheritdoc />
		public void OnExecuteError<T>(ICommand<T> command) where T : CommandContext {
			attributes.Rollback();
			UpdateIdempotentState(command);
		}

		/// <summary>
		/// 冪等の結果を更新します。冪等ではないコマンドを実行した場合、リトライ不可とマークします。
		/// </summary>
		/// <typeparam name="T">コマンドのコンテキストのタイプ</typeparam>
		/// <param name="command">実行したコマンド</param>
		private void UpdateIdempotentState<T>(ICommand<T> command)
						where T : CommandContext {
			if (areIdempotent) {
				areIdempotent = (command.Idempotent.ToSpecificValue() != Idempotent.None);
			}
		}

		/// <inheritdoc />
		/// <remarks>
		/// 現在有効なトランザクションスコープに対してロールバック
		/// がマークされているかを判定します。
		/// </remarks>
		public bool IsMarkedRollback() {
			var current = status.Peek();
			return current.IsRoledBack;
		}

		/// <inheritdoc />
		/// <remarks>
		/// 現在有効なトランザクションスコープに対してロールバック
		/// をマークします。
		/// </remarks>
		public void MarkRollbackOnly() {
			var current = status.Peek();
			current.IsRoledBack = true;
		}

		/// <inheritdoc />
		public void OnPostStartScope(TxScope requested, ICommandTx scope) {
			PushNewScopeStatus(requested, scope);
		}

		/// <inheritdoc />
		/// <summary>
		/// トランザクションを終了します。トランザクション内冪等コマンドを実行した場合、
		/// 正常終了（コミット済）の場合にはリトライ不可とマークします。
		/// </summary>
		public void OnPreEndScope(bool stat) {
			var current = status.Pop();
			if (current == null) {
				return;
			}

			current.Dispose();
			if (stat && current.TxScope != TxScope.Supports) {
				areIdempotent = false;
			}
		}

		/// <inheritdoc />
		public T GetAttribute<T>(string key) {
			return attributes.Get<T>(key);
		}

		/// <inheritdoc />
		public void SetAttribute(string key, object value) {
			attributes.Add(key, value);
		}

		/// <inheritdoc />
		public void RemoveAttribute(string key) {
			attributes.Remove(key);
		}

		/// <inheritdoc />
		public T GetScopeAttribute<T>(string key) {
			var current = status.Peek();
			return current != null ? current.GetAttribute<T>(key) : default(T);
		}

		/// <inheritdoc />
		public void SetScopeAttribute(string key, object value) {
			var current = status.Peek();
			current?.SetAttribute(key, value);
		}

		/// <inheritdoc />
		public object RemoveScopeAttribute(string key) {
			var current = status.Peek();
			return current?.RemoveAttribute(key);
		}

		/// <inheritdoc />
		public void FireLifecycleCallback<T>(Action<T> action, T context) where T : CommandContext {
			try {
				attributes.Begin();
				action(context);
				attributes.Commit();
			}
			catch (Exception) {
				attributes.Rollback();
				throw;
			}
		}

		/// <inheritdoc />
		public void EnlistTx(DbConnection connection) {
			status.Peek().Enlist(connection);
		}

		/// <inheritdoc />
		public DbTransaction GetDbTransaction() {
			return status.Peek().DbTransaction;
		}

		/// <inheritdoc />
		public void SetIsolationLevelMapping(Func<IsolationLevel, IsolationLevel> mapping) {
			status.Peek().SetIsolationLevelMapping(mapping);
		}
		#endregion

		private void PushNewScopeStatus(TxScope requested, ICommandTx scope) {
			// スコープが、Suppressの場合、スコープは作成されるが、
			// トランザクションがないため、タイムアウト管理は行わない。
			// その他、Required、RequiresNew、Suppress以外はそもそもスコープが
			// 作成されない。ただし、最初のスコープはSupportsで作成される
			var specificScope = requested.ToSpecificValue();
			status.Push(new ContextScopeStatus(specificScope, scope));
		}

		#region IDisposable
		/// <summary>
		/// リソースを破棄します。
		/// </summary>
		/// <param name="disposing">マネージリソースを解放する場合true</param>
		protected virtual void Dispose(bool disposing) {
			if (!disposing) {
				return;
			}
			while (status.Count > 0) {
				var target = status.Pop();
				target?.Dispose();
			}
		}

		/// <inheritdoc />
		public void Dispose() {
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// インスタンスのリソースを解放します。
		/// </summary>
		~DefaultContextSpiImpl() {
			Dispose(false);
		}
		#endregion
	}
}
