﻿// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

namespace Net.Rimakiri.Command.Spi {
	/// <summary>
	/// トランザクション分離レベルを調整するクラスが実装するインターフェイスです。
	/// </summary>
	public interface IIsolationCoordinator {
		/// <summary>
		/// コマンドのトランザクション用途を通知します。
		/// </summary>
		/// <param name="intention">コマンドのトランザクション用途</param>
		void NotifyIntention(TxIntention intention);

		/// <summary>
		/// トランザクション分離レベルを調整します。
		/// </summary>
		/// <returns>調整後分離レベル</returns>
		/// <remarks>
		/// <see cref="NotifyIntention"/>で通知されたすべての
		/// <see cref="IIsolationCoodinatee"/>を実装したコマンドが
		/// 要求するトランザクション分離レベルから最適なものを探します。
		/// また、この結果が<see cref="TxIsolation.Snapshot"/>または
		/// <see cref="TxIsolation.ReadUncommitted"/>になることは
		/// ありません。これらを使用する場合には、デフォルト値として設定
		/// する必要があります。
		/// </remarks>
		/// <seealso cref="CommandServiceBuilder{T}.IsolationLevel"/>
		/// <seealso cref="ICommandServiceSpi.CommandTxFactory"/>
		/// <seealso cref="ICommandTxFactory.IsolationLevel"/>
		TxIsolation Coordinate();
	}

	/// <summary>
	/// <see cref="IIsolationCoordinator"/>のインスタンスを生成するファクトリ
	/// クラスが実装するインターフェイスです。
	/// </summary>
	public interface IIsolationCoordinatorFactory {
		/// <summary>
		/// <see cref="IIsolationCoordinator"/>クラスのインスタンスを
		/// 生成します。
		/// </summary>
		/// <param name="coordination">調整方法</param>
		/// <returns>生成したインスタンス</returns>
		/// <remarks>
		/// <paramref name="coordination"/>が<see cref="TxCoordination.None"/>
		/// で呼び出されることはありません。
		/// </remarks>
		IIsolationCoordinator CreateCoordinator(TxCoordination coordination);
	}
}
