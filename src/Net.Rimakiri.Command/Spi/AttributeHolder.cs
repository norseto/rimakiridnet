﻿// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Collections.Generic;

namespace Net.Rimakiri.Command.Spi {
	/// <summary>
	/// 属性を保持するクラスです。
	/// </summary>
	internal class AttributeHolder {
		private readonly object removeMarker = new object();
		private readonly Stack<Dictionary<string,object>> stack
					= new Stack<Dictionary<string, object>>();

		private bool IsRoot => stack.Count < 1;

		private Dictionary<string, object> Parent => IsRoot ? null : stack.Peek();

		private Dictionary<string,object> temporary
					= new Dictionary<string, object>();

		/// <summary>
		/// Begin transaction.
		/// </summary>
		public void Begin() {
			stack.Push(temporary);
			temporary = new Dictionary<string, object>(Parent);
		}

		/// <summary>
		/// Commit transaction - update committed attribute.
		/// </summary>
		public void Commit() {
			var committed = Parent;
			if (committed == null) {
				return;
			}
			foreach (var key in temporary.Keys) {
				var value = temporary[key];
				if (committed.ContainsKey(key)) {
					committed.Remove(key);
				}
				if (value != removeMarker) {
					committed.Add(key, value);
				}
			}
			temporary = stack.Pop();
		}

		/// <summary>
		/// Rollback transaction.
		/// </summary>
		public void Rollback() {
			temporary = stack.Pop();
		}

		/// <summary>
		/// Add attribute value.
		/// </summary>
		/// <param name="key">Key</param>
		/// <param name="value">Value</param>
		public void Add(string key, object value) {
			if (value == null) {
				Remove(key);
				return;
			}
			SetValue(temporary, key, value);
		}

		/// <summary>
		/// Remove value.
		/// </summary>
		/// <param name="key">Key</param>
		public void Remove(string key) {
			var parent = Parent;
			if (parent != null && parent.ContainsKey(key)) {
				SetValue(temporary, key, removeMarker);
				return;
			}
			SetValue(temporary, key, null);
		}

		private static void SetValue(Dictionary<string, object> target,
								string key, object value) {
			if (target == null) {
				return;
			}
			if (target.ContainsKey(key)) {
				target.Remove(key);
			}
			if (value != null) {
				target.Add(key, value);
			}
		}

		/// <summary>
		/// Get attribute value.
		/// </summary>
		/// <typeparam name="T">Type of the value</typeparam>
		/// <param name="key">Key</param>
		/// <returns>Value for the key</returns>
		public T Get<T>(string key) {
			var value = GetValue(key);
			if (value is T) {
				return (T)value;
			}
			return default(T);
		}

		/// <summary>
		/// Get attribute value.
		/// </summary>
		/// <param name="key">Key</param>
		/// <returns>Value for the key</returns>
		private object GetValue(string key) {
			object ret;
			if (temporary.TryGetValue(key, out ret)) {
				return ret == removeMarker ? null : ret;
			}
			return null;
		}
	}
}
