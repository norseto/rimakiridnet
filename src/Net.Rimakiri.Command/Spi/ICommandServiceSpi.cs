// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

namespace Net.Rimakiri.Command.Spi {
	/// <summary>
	/// コマンドサービスがコマンドを実行するにあたり提供すべき
	/// 必須機能SPI(Service Provider Interface)です。
	/// </summary>
	/// <seealso cref="CommandServiceFacade"/>
	public interface ICommandServiceSpi {
		/// <summary>
		/// 実行時トランザクションスコープファクトリを設定まはた取得します。
		/// </summary>
		ICommandTxFactory CommandTxFactory { set; get; }

		/// <summary>
		/// 冪等コマンドのリトライ回数を設定または取得します。
		/// </summary>
		/// <remarks>
		/// 冪等コマンドの場合、最大ここで指定したリトライ回数+1回実行されます。
		/// </remarks>
		/// <seealso cref="RetryPolicy"/>
		int MaxRetry { get; set; }

		/// <summary>
		/// リトライポリシーを取得または設定します。
		/// 設定されたリトライポリシーの判定結果により、リトライを行います。
		/// 設定されていない場合はリトライはされません。
		/// </summary>
		/// <seealso cref="MaxRetry"/>
		/// <seealso cref="IRetryPolicy"/>
		IRetryPolicy RetryPolicy { get; set; }

		/// <summary>
		/// コマンドを指定のコンテキストで実行します。
		/// </summary>
		/// <param name="context">コンテキスト</param>
		/// <param name="command">実行するコマンド</param>
		/// <typeparam name="T">コンテキストの型</typeparam>
		void ExecuteCommand<T>(T context, ICommand<T> command) where T: CommandContext;
	}
}
