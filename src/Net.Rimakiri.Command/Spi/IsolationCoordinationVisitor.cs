// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Collections.Generic;

namespace Net.Rimakiri.Command.Spi {
	/// <inheritdoc />
	/// <summary>
	/// トランザクション分離レベルを調整するためのビジタークラスです。
	/// </summary>
	internal class IsolationCoordinationVisitor : ICommandVisitor {
		private TxScope Scope { get; set; }
		private TxIntention Intention { get; set; }
		private readonly IIsolationCoordinatorFactory factory;
		private readonly TxCoordination txCoordination;
		private readonly List<IsolationCoordinationVisitor> children
			= new List<IsolationCoordinationVisitor>();

		/// <summary>
		/// トランザクション分離レベルの調整方法を指定してインスタンスを初期化します。
		/// </summary>
		/// <param name="txCoordination">調整方法</param>
		/// <param name="factory"><see cref="IIsolationCoordinatorFactory"/>のインスタンス</param>
		public IsolationCoordinationVisitor(TxCoordination txCoordination,
					IIsolationCoordinatorFactory factory) {
			this.txCoordination = txCoordination;
			this.factory = factory;
		}

		#region Implementation of ICommandVisitor
		/// <inheritdoc />
		public ICommandVisitor CreateChildVisitor() {
			var ret = new IsolationCoordinationVisitor(txCoordination, factory);
			children.Add(ret);
			return ret;
		}

		/// <inheritdoc />
		public void Visit<T>(ICommand<T> cmd) where T : CommandContext {
			if (cmd == null) {
				return;
			}
			Scope = cmd.TxScope;
			var coordinatee = cmd as IIsolationCoodinatee;
			if (coordinatee == null) {
				return;
			}
			Intention |= coordinatee.TxIntention;
		}
		#endregion

		/// <summary>
		/// トランザクション分離レベルを調整します。
		/// </summary>
		/// <param name="isolation">調整前分離レベル</param>
		/// <returns>調整後分離レベル</returns>
		/// <remarks>
		/// <see cref="Visit{T}"/>で通知されたすべての
		/// <see cref="IIsolationCoodinatee"/>を実装したコマンドが
		/// 要求するトランザクション分離レベルから最適なものを探します。
		/// ただし、<see cref="ICommand{T}.TxScope"/>プロパティが、
		/// <see cref="TxScope.Suppress"/>、<see cref="TxScope.RequiresNew"/>
		/// のものに関しては無視されます。
		/// </remarks>
		public TxIsolation Coordinate(TxIsolation isolation) {
			var coordinator = factory.CreateCoordinator(txCoordination);
			NotifyIntentions(coordinator);
			return isolation | coordinator.Coordinate();
		}

		private void NotifyIntentions(IIsolationCoordinator coordinator) {
			coordinator.NotifyIntention(Intention);
			foreach (var child in children) {
				var scope = child.Scope;
				if (scope == TxScope.Suppress || scope == TxScope.RequiresNew) {
					continue;
				}
				child.NotifyIntentions(coordinator);
			}
		}

	}
}
