// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;

namespace Net.Rimakiri.Command.Spi {
	/// <summary>
	/// 抽象<see cref="ICommandTxFactory"/>クラスです。
	/// </summary>
	/// <inheritdoc />
	public abstract class AbstractCommandTxFactory : ICommandTxFactory {
		private IIsolationCoordinatorFactory coordinatorFactory
			= new DefaultCoordinatorFactoryImpl();

		/// <summary>
		/// 常にコミットを行わないかを設定します。
		/// </summary>
		public bool NoCommit { get; set; }

		/// <inheritdoc />
		public IIsolationCoordinatorFactory CoordinatorFactory {
			get => coordinatorFactory;
			set {
				if (value != null) {
					coordinatorFactory = value;
				}
			}
		}

		/// <inheritdoc />
		public TimeSpan Timeout { get; set; } = DefaultTxConstants.TxTimeout;

		/// <inheritdoc />
		public TxIsolation IsolationLevel { get; set; } = DefaultTxConstants.TxIsolation;

		/// <inheritdoc />
		public TxCoordination TxCoordination { get; set; } = TxCoordination.Normal;

		/// <inheritdoc />
		public abstract ICommandTx Create<T>(ICommand<T> command, TxScope currentScope)
				where T : CommandContext;

		/// <summary>
		/// 実際に必要なトランザクション分離レベルを取得します。
		/// </summary>
		/// <typeparam name="T">コンテキストの型</typeparam>
		/// <param name="command">実行するコマンド</param>
		/// <param name="scope">コマンドのスコープ</param>
		/// <returns>実行時のトランザクション分離レベル</returns>
		protected TxIsolation GetRuntimeIsolation<T>(ICommand<T> command, TxScope scope)
				where T: CommandContext {
			var isolation = IsolationLevel;
			if (TxCoordination == TxCoordination.None || scope == TxScope.Suppress) {
				return isolation;
			}

			if (!(command is ICommandVisitAcceptor coordinatee)) {
				return isolation;
			}
			var visitor = new IsolationCoordinationVisitor(TxCoordination, CoordinatorFactory);
			coordinatee.AcceptVisitor(visitor);
			isolation |= visitor.Coordinate(isolation);
			return isolation;
		}
	}
}
