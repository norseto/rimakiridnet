﻿// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;

namespace Net.Rimakiri.Command.Spi {
	internal abstract class IsolationCoordinatorBase : IIsolationCoordinator {
		protected TxIntention Intention { get; set; }

		#region Implementation of IIsolationCoordinator
		public virtual void NotifyIntention(TxIntention intention) {
			Intention |= intention;
		}
		public abstract TxIsolation Coordinate();
		#endregion

		/// <summary>
		/// もっとも厳格なトランザクション分離レベルを取得する。
		/// </summary>
		/// <returns>もっとも厳格なトランザクション分離レベル</returns>
		protected TxIsolation GetMostStrictIsolation() {
			var isolation = TxIsolation.Unspecified;
			TxIntention[] intentions = {
				TxIntention.Select,
				TxIntention.SelectForUpdate,
				TxIntention.Update,
				TxIntention.Delete,
				TxIntention.Insert,
			};
			TxIsolation[] isolations = {
				TxIsolation.ReadCommitted,
				TxIsolation.ReadCommitted,
				TxIsolation.RepeatableRead,
				TxIsolation.Serializable,
				TxIsolation.Serializable,
			};

			for (int i = 0, l = Math.Min(intentions.Length, isolations.Length); i < l; i++) {
				if (Intention.HasFlag(intentions[i])) {
					isolation |= isolations[i];
				}
			}
			return isolation;
		}
	}

	internal class StrictIsolationCoordinator : IsolationCoordinatorBase {
		private int selectCount;
		private int select4UpdtCount;

		#region Overrides of IsolationCoordinatorBase
		/// <inheritdoc />
		public override void NotifyIntention(TxIntention intention) {
			base.NotifyIntention(intention);
			if (intention.HasFlag(TxIntention.Select)) {
				selectCount++;
			}
			if (intention.HasFlag(TxIntention.SelectForUpdate)) {
				select4UpdtCount++;
			}
		}

		/// <inheritdoc />
		public override TxIsolation Coordinate() {
			var isolation = GetMostStrictIsolation();
			return (isolation == TxIsolation.ReadCommitted
						|| isolation == TxIsolation.Unspecified)
						&& selectCount > 0 && selectCount + select4UpdtCount > 1
					? TxIsolation.RepeatableRead : isolation;
		}
		#endregion
	}

	internal class PessimisticIsolationCoordinator : IsolationCoordinatorBase {
		#region Overrides of IsolationCoordinatorBase
		/// <inheritdoc />
		public override TxIsolation Coordinate() {
			const TxIntention implicitLock = TxIntention.SelectForUpdate | TxIntention.Update
											| TxIntention.Delete | TxIntention.Insert;
			const TxIntention selection = TxIntention.Select | TxIntention.SelectForUpdate;
			var intention = Intention;
			if (intention == TxIntention.UnSpecified
					|| intention.HasOnlyAnyFlag(implicitLock)
					|| intention.HasOnlyAnyFlag(selection)) {
				return TxIsolation.Unspecified;
			}
			return GetMostStrictIsolation();
		}
		#endregion
	}

	internal class OptimisticIsolationCoordinator : PessimisticIsolationCoordinator {
		private bool updateFirst;

		#region Overrides of IsolationCoordinatorBase
		/// <inheritdoc />
		public override void NotifyIntention(TxIntention intention) {
			const TxIntention updateLock = TxIntention.SelectForUpdate | TxIntention.Update;

			if (Intention == TxIntention.UnSpecified
					&& intention.HasOnlyAnyFlag(updateLock)) {
				updateFirst = true;
			}
			base.NotifyIntention(intention);
		}
		#endregion

		#region Overrides of PessimisticIsolationCoordinator
		/// <inheritdoc />
		public override TxIsolation Coordinate() {
			return updateFirst ? TxIsolation.Unspecified : base.Coordinate();
		}
		#endregion
	}

}
