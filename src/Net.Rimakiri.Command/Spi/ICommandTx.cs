// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Data;
using System.Data.Common;

namespace Net.Rimakiri.Command.Spi {
	/// <inheritdoc />
	/// <summary>
	/// 実行時のトランザクションスコープを表すインターフェイスです。
	/// </summary>
	/// <remarks>
	/// デフォルトでは、<see cref="DbTransaction"/>を使用した
	/// トランザクション管理が行われますが、
	/// <see cref="ICommandTxFactory"/>に
	/// 別のものを指定することで、用途に合わせたトランザクションの管理を
	/// フレームワークに行わせることが可能です。
	/// デフォルトの場合、データーベースコネクションを取得後、
	/// <see cref="Enlist"/>にてトランザクションを開始し、
	/// <see cref="CommandContext.SetTxAttribute"/>で
	/// コネクションを保存し、再利用します。
	/// <see cref="IDbCommand.Transaction"/>に
	/// <see cref="CommandContext.GetDbTransaction"/>
	/// の結果を設定します。
	/// </remarks>
	/// <seealso cref="LocalCommandTxFactory"/>
	public interface ICommandTx : IDisposable {
		/// <summary>
		/// トランザクションの成功を通知します。
		/// </summary>
		void Complete();

		/// <summary>
		/// ローカルデータベーストランザクションを開始します。
		/// </summary>
		/// <param name="connection">対象コネクション</param>
		/// <remarks>
		/// 使用している<see cref="ICommandTxFactory"/>が
		/// ローカルデータベーストランザクションに対応している必要があります。
		/// 対応していない場合、なにも行いません。
		/// </remarks>
		void Enlist(DbConnection connection);

		/// <summary>
		/// ローカルデータベーストランザクションを取得します。
		/// </summary>
		/// <remarks>
		/// 使用している<see cref="ICommandTxFactory"/>が
		/// ローカルデータベーストランザクションに対応している必要があります。
		/// 対応していない場合、nullが返却されます。
		/// </remarks>
		DbTransaction DbTransaction { get; }

		/// <summary>
		/// トランザクション分離レベルを取得します。
		/// </summary>
		TxIsolation TxIsolation { get; }

		/// <summary>
		/// グローバルトランザクションに対応しているかを取得します。
		/// </summary>
		bool SupportsGlobal { get; }

		/// <summary>
		/// <see cref="IsolationLevel"/>のマッピングを設定または取得します。
		/// </summary>
		/// <remarks>
		/// 特定のデータベースで、サポートしていないトランザクション分離レベルを別のものに
		/// 変えるために使用します。
		/// </remarks>
		Func<IsolationLevel, IsolationLevel> IsolationLevelMapping { get; set; }
	}

	/// <summary>
	/// <see cref="ICommandTx"/>を生成するファクトリクラスが実装するインターフェイスです。
	/// </summary>
	/// <seealso cref="CommandServiceBuilder{T}.TransactionFactory"/>
	public interface ICommandTxFactory {
		/// <summary>
		/// トランザクションをコミットしないかを設定または取得します。
		/// </summary>
		bool NoCommit { get; set; }

		/// <summary>
		/// 使用する<see cref="IIsolationCoordinatorFactory"/>を取得または設定します。
		/// </summary>
		IIsolationCoordinatorFactory CoordinatorFactory { get; set; }

		/// <summary>
		/// トランザクションのタイムアウトを取得または設定します。
		/// </summary>
		TimeSpan Timeout { get; set; }

		/// <summary>
		/// デフォルトのトランザクションの分離レベルを取得または設定します。
		/// </summary>
		/// <remarks>
		/// <see cref="IIsolationCoodinatee"/>を実装したコマンドを実行
		/// する場合、トランザクション分離レベルはここで指定した分離レベルより
		/// 厳格なものに調整される場合があります。
		/// </remarks>
		TxIsolation IsolationLevel { get; set; }

		/// <summary>
		/// トランザクション分離レベルの調整方法を取得または設定します。
		/// </summary>
		TxCoordination TxCoordination { get; set; }

		/// <summary>
		/// トランザクションスコープを生成します。
		/// </summary>
		/// <param name="command">対象コマンド</param>
		/// <param name="currentScope">現在のトランザクションスコープ</param>
		/// <typeparam name="T">コマンドコンテキストの型</typeparam>
		/// <exception cref="InvalidOperationException">
		/// 対象コマンドのスコープが<see cref="TxScope.Mandatory"/>で
		/// 現在トランザクションが開始していない場合。
		/// </exception>
		/// <returns>生成したトランザクションスコープ。不要な場合はnull</returns>
		/// <remarks>
		/// アンビエントトランザクションが存在する状態で、<see cref="TxScope.Required"/>
		/// が指定された場合、スコープは生成されませんが、<see cref="TxScope.RequiresNew"/>
		/// または<see cref="TxScope.Suppress"/>が指定された場合には生成されます。
		/// </remarks>
		ICommandTx Create<T>(ICommand<T> command, TxScope currentScope) where T : CommandContext;
	}
}
