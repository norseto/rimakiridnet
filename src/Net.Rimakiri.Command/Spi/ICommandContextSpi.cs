// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Data;
using System.Data.Common;

namespace Net.Rimakiri.Command.Spi {
	/// <summary>
	/// コマンドのコンテキストを管理するクラスが実装すべきインターフェイスです。
	/// </summary>
	/// <seealso cref="CommandContext" />
	/// <inheritdoc />
	public interface ICommandContextSpi : IDisposable {
		/// <summary>
		/// 使用するサービスを取得します。
		/// </summary>
		ICommandService Service { get; }

		/// <summary>
		/// トランザクションのロールバックをマークします。
		/// ロールバックがマークされたトランザクションは
		/// サービス側でコミットされません。
		/// </summary>
		void MarkRollbackOnly();

		/// <summary>
		/// トランザクションのロールバックがマークされているかを判定します。
		/// </summary>
		/// <returns>トランザクションがロールバックするようマークされている場合True</returns>
		bool IsMarkedRollback();

		/// <summary>
		/// 新規トランザクションスコープが開始されたときに呼び出されます。
		/// </summary>
		/// <remarks>
		/// トランザクションが開始しない<see cref="F:Net.Rimakiri.Command.TxScope.Mandatory"/>や
		/// <see cref="F:Net.Rimakiri.Command.TxScope.Supports"/>をパラメタに呼び出されることはありません。
		/// </remarks>
		/// <param name="requested">コマンドの要求するスコープ</param>
		/// <param name="scope">開始したトランザクションのスコープ</param>
		void OnPostStartScope(TxScope requested, ICommandTx scope);

		/// <summary>
		/// トランザクションスコープが終了されたときに呼び出されます。
		/// </summary>
		/// <param name="stat">正常終了の場合true</param>
		void OnPreEndScope(bool stat);

		/// <summary>
		/// コマンドの呼出開始を通知します。
		/// コマンドの呼出し前に呼び出されます。
		/// </summary>
		/// <param name="command">呼出すコマンド</param>
		/// <param name="timeoutTick">タイムアウト(Tick)</param>
		/// <typeparam name="T">コンテキストの型</typeparam>
		void OnPreCallExecute<T>(ICommand<T> command, long timeoutTick) where T : CommandContext;

		/// <summary>
		/// コマンドの呼出終了を通知します。
		/// コマンドの呼出しが正常に終了した後に呼び出されます。
		/// </summary>
		/// <param name="command">呼出したコマンド</param>
		/// <typeparam name="T">コンテキストの型</typeparam>
		void OnPostCallExecute<T>(ICommand<T> command) where T : CommandContext;

		/// <summary>
		/// コマンドの呼出終了を通知します。
		/// コマンドの呼出しで例外が発生した場合に呼び出されます。
		/// </summary>
		/// <param name="command">呼出したコマンド</param>
		/// <typeparam name="T">コンテキストの型</typeparam>
		void OnExecuteError<T>(ICommand<T> command) where T : CommandContext;

		/// <summary>
		/// 現在のトランザクションのタイムアウトをチェックします。
		/// </summary>
		/// <param name="checkPoint">タイムアウト発生時に送出される例外
		/// に含まれるチェックポイント名</param>
		/// <exception cref="HeuristicTimeoutException">
		/// タイムアウト時間を経過してる場合
		/// </exception>
		/// <remarks>
		/// フレームワーク側ではコマンドの実行後に呼び出しますが、 コマンドの
		/// 処理中に呼び出してもかまいません。時間がかかる処理の前後で呼び出す
		/// ことで、タイムアウトのチェックを厳密に行うことができます。
		/// </remarks>
		void CheckTimeout(string checkPoint);

		/// <summary>
		/// タイムアウトまでの残Tick数を取得します。
		/// </summary>
		/// <returns>
		/// タイムアウトまでの残Tick数。ただし、
		/// タイムアウト設定が存在しない場合：-1、
		/// タイムアウトが経過している場合：0
		/// </returns>
		long GetTimeoutRemainTicks();

		/// <summary>
		/// 実行が終了したコマンド全てが冪等かを判定します。
		/// </summary>
		/// <returns>全て冪等であった場合true</returns>
		bool WereIdempotent();

		/// <summary>
		/// トランザクションスコープのネスト数を取得します。
		/// </summary>
		/// <remarks>
		/// Requiredスコープのコマンドから、さらにRequiredスコープの
		/// コマンドを呼出してもネスト数は増加しません（同一スコープ）。
		/// </remarks>
		int TxNestCount { get; }

		/// <summary>
		/// 現在の<see cref="TxScope"/>を取得します。
		/// </summary>
		TxScope TxScope { get; }

		/// <summary>
		/// 現在の<see cref="Command.TxIsolation"/>を取得します。
		/// </summary>
		TxIsolation TxIsolation { get; }

		/// <summary>
		/// 現在のトランザクションがグローバルトランザクションをサポート
		/// しているかを取得します。
		/// </summary>
		bool SupportsGlobalTx { get; }

		/// <summary>
		/// 属性を設定します。同じ名前の属性が存在する場合には上書きされます。
		/// </summary>
		/// <param name="key">キー</param>
		/// <param name="value">属性の値</param>
		void SetAttribute(string key, object value);

		/// <summary>
		/// 属性を削除します。
		/// </summary>
		/// <param name="key">キー</param>
		void RemoveAttribute(string key);

		/// <summary>
		/// 属性を取得します。型が異なる場合はデフォルト値が返却されます。
		/// </summary>
		/// <typeparam name="T">属性の型</typeparam>
		/// <param name="key">キー</param>
		/// <returns>属性の値</returns>
		T GetAttribute<T>(string key);

		/// <summary>
		/// スコープ属性を設定します。同じ名前の属性が存在する場合には例外が発生します。
		/// </summary>
		/// <param name="key">キー</param>
		/// <param name="value">属性の値</param>
		/// <remarks>
		/// スコープ属性として設定されたオブジェクトが<see cref="IDisposable"/>
		/// を実装している場合、スコープ終了時に<see cref="IDisposable.Dispose"/>
		/// が呼び出されます。
		/// </remarks>
		void SetScopeAttribute(string key, object value);

		/// <summary>
		/// スコープ属性を削除します。
		/// </summary>
		/// <param name="key">キー</param>
		/// <returns>削除した属性の値</returns>
		object RemoveScopeAttribute(string key);

		/// <summary>
		/// スコープ属性を取得します。型が異なる場合はデフォルト値が返却されます。
		/// </summary>
		/// <typeparam name="T">属性の型</typeparam>
		/// <param name="key">キー</param>
		/// <returns>属性の値</returns>
		T GetScopeAttribute<T>(string key);

		/// <summary>
		/// ライフサイクルコールバックを実行します。
		/// </summary>
		/// <typeparam name="T">コンテキストの型</typeparam>
		/// <param name="action">実行するコールバック</param>
		/// <param name="context">コールバックへのコンテキスト</param>
		void FireLifecycleCallback<T>(Action<T> action, T context) where T : CommandContext;

		/// <summary>
		/// 対象のコネクションのトランザクションを開始します。
		/// </summary>
		/// <param name="connection">トランザクションを開始するコネクション</param>
		void EnlistTx(DbConnection connection);

		/// <summary>
		/// <see cref="IsolationLevel"/>のマッピングを設定します。
		/// </summary>
		/// <remarks>
		/// 特定のデータベースで、サポートしていないトランザクション分離レベルを別のものに
		/// 変えるために使用します。
		/// </remarks>
		/// <param name="mapping"><see cref="IsolationLevel"/>のマッピング</param>
		void SetIsolationLevelMapping(Func<IsolationLevel, IsolationLevel> mapping);

		/// <summary>
		/// データベーストランザクションを取得します。
		/// </summary>
		/// <returns>データベースのトランザクション</returns>
		DbTransaction GetDbTransaction();
	}

	/// <summary>
	/// <see cref="ICommandContextSpi"/>を生成するファクトリクラスです。
	/// </summary>
	/// <remarks>
	/// コンテキストの必須機能の動作をデフォルトの
	/// <see cref="DefaultContextSpiImpl"/>
	/// から変更したい場合に使用します。
	/// </remarks>
	public interface ICommandContextSpiFactory {
		/// <summary>
		/// コンテキストSPIを生成します。
		/// </summary>
		/// <param name="service">使用されるサービス</param>
		/// <returns>生成したSPI</returns>
		/// <seealso cref="CommandServiceFacade"/>
		ICommandContextSpi Create(ICommandService service);
	}
}
