// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;

namespace Net.Rimakiri.Command.Spi {
	/// <summary>
	/// デフォルトの<see cref="ICommandServiceSpi"/>実装です。
	/// </summary>
	/// <inheritdoc />
	public class DefaultServiceSpiImpl : ICommandServiceSpi {
		#region Implementation of ICommandServiceSpi

		/// <summary>
		/// トランザクションスコープファクトリを取得または設定します。
		/// </summary>
		/// <seealso cref="CommandTxFactory"/>
		public ICommandTxFactory CommandTxFactory { get; set; }

		/// <inheritdoc />
		/// <remarks>
		/// デフォルト値は2（最大3回実行）です。
		/// </remarks>
		public int MaxRetry { get; set; } = 2;

		/// <inheritdoc />
		/// <remarks>
		///	デフォルトでは何も設定されていません。
		/// </remarks>
		public IRetryPolicy RetryPolicy { get; set; }

		/// <inheritdoc cref="ICommandServiceSpi.ExecuteCommand{T}"/>
		public virtual void ExecuteCommand<T>(T context, ICommand<T> command) where T: CommandContext {
			command.PreExecute(context);
			DoExecute(context, command);
			command.PostExecute(context);
		}

		#endregion

		/// <summary>
		/// 冪等なコマンドのリトライを含め、コマンドを実行する。
		/// </summary>
		/// <typeparam name="T">コンテキストの型</typeparam>
		/// <param name="context">コンテキスト</param>
		/// <param name="command">実行するコマンド</param>
		private void DoExecute<T>(T context, ICommand<T> command)
			where T : CommandContext {
			Exception error = null;
			var retry = MaxRetry;
			if (retry < 0) {
				retry = 0;
			}

			for (var i = 0; i < retry + 1; i++) {
				try {
					error = null;
					ExecuteWithTx(context, command, i);
					break;
				}
				catch (ExecutionException ee) {
					error = ee.InnerException;
					var current = context.ContextSpi.TxNestCount;

					// 現在のスタックが初期状態の場合か、子のトランザクションで
					// 例外が発生した場合にはリトライする
					// (子のトランザクションはロールバックされている)
					if (current <= 1 || current < ee.StackCount
							|| command.Idempotent.ToSpecificValue() == Idempotent.Anytime) {
						continue;
					}

					// 同一トランザクション内で例外が発生した場合には、例外を送出し、
					// 呼出元（親のトランザクションスコープ）でロールバックさせ、
					// その後リトライの判定を行う
					throw;
				}
			}

			if (error != null) {
				throw error;
			}
		}

		/// <summary>
		/// 必要に応じてトランザクションスコープを生成し、コマンドを実行する。
		/// </summary>
		/// <typeparam name="T">コンテキストの型</typeparam>
		/// <param name="context">コンテキスト</param>
		/// <param name="command">実行するコマンド</param>
		/// <param name="tryCount">試行回数(初回:0)</param>
		private void ExecuteWithTx<T>(T context, ICommand<T> command, int tryCount) where T : CommandContext {
			var requestedScope = command.TxScope;

			var factory = CommandTxFactory ??
						(CommandTxFactory = new LocalCommandTxFactory());

			var toTicks = factory.Timeout.Ticks;
			var spi = context.ContextSpi;
			var scope = factory.Create(command, spi.TxScope);
			if (scope == null) {
				InnerExecute(context, command, toTicks, tryCount);
				return;
			}
			using (scope) {
				var completed = false;
				spi.OnPostStartScope(requestedScope, scope);
				try {
					InnerExecute(context, command, toTicks, tryCount);
					completed = true;
					if (!context.IsMarkedRollback) {
						scope.Complete();
					}
				}
				finally {
					spi.OnPreEndScope(completed);
				}
			}
		}

		/// <summary>
		/// コマンドを実行する。
		/// </summary>
		/// <remarks>
		/// リトライ可能な例外が発生した場合には、<see cref="ExecutionException"/>
		/// でラップして送出される。
		/// </remarks>
		/// <typeparam name="T">コンテキストの型</typeparam>
		/// <param name="context">コンテキスト</param>
		/// <param name="command">実行するコマンド</param>
		/// <param name="timeoutTicks">トランザクションタイムアウト（Tick）</param>
		/// <param name="tryCount">試行回数(初回:0)</param>
		/// <exception cref="ExecutionException">コマンドが冪等の場合に発生した例外</exception>
		private void InnerExecute<T>(T context, ICommand<T> command, long timeoutTicks, int tryCount)
			where T : CommandContext {

			var spi = context.ContextSpi;
			spi.OnPreCallExecute(command, timeoutTicks);
			try {
				command.Execute(context);
			}
			catch (ExecutionException) {
				throw;
			}
			catch (Exception ex) {
				spi.OnExecuteError(command);
				if (!spi.WereIdempotent() || !RetryPolicy.IsRetryableError(ex, tryCount)) {
					throw;
				}
				throw ExecutionException.Generate(ex, spi.TxNestCount);
			}
			spi.OnPostCallExecute(command);
		}
	}
}
