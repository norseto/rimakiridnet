// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;

namespace Net.Rimakiri.Command.Spi {
	/// <summary>
	/// トランザクションのデフォルト値が定義されています。
	/// </summary>
	public static class DefaultTxConstants {
		/// <summary>
		/// トランザクションタイムアウトのデフォルト値です。
		/// </summary>
		public static readonly TimeSpan TxTimeout = TimeSpan.FromSeconds(60);

		/// <summary>
		/// トランザクション分離レベルのデフォルト値です。
		/// </summary>
		public static readonly TxIsolation TxIsolation = TxIsolation.ReadCommitted;
	}
}
