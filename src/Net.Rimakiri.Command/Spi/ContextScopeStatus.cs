// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading;

namespace Net.Rimakiri.Command.Spi {
	/// <summary>
	/// コンテキストのトランザクションスコープのステータスを保持する
	/// </summary>
	/// <inheritdoc cref="IDisposable"/>
	internal sealed class ContextScopeStatus : IDisposable {
		private const long InitialValue = -1L;
		private readonly ICommandTx commandTx;
		private readonly bool manageTimeout;
		private long txTimeoutTick = InitialValue;
		private long start = InitialValue;
		private readonly Dictionary<string, object> attributes
				= new Dictionary<string, object>();

		/// <summary>
		/// このインスタンスの<see cref="TxScope"/>を取得します。
		/// </summary>
		public TxScope TxScope { get; }

		/// <summary>
		/// このインスタンスの<see cref="IDbTransaction"/>を取得します。
		/// </summary>
		public DbTransaction DbTransaction => commandTx?.DbTransaction;

		/// <summary>
		/// このインスタンスの<see cref="TxIsolation"/>を取得します。
		/// </summary>
		public TxIsolation TxIsolation => commandTx.TxIsolation;

		/// <summary>
		/// このインスタンスがグローバルトランザクションをサポートしているかを取得します。
		/// </summary>
		public bool SupportsGlobal => commandTx.SupportsGlobal;

		/// <summary>
		/// インスタンスを初期化する
		/// </summary>
		/// <param name="requested">要求されたスコープ</param>
		/// <param name="commandTx">コマンドトランザクション</param>
		public ContextScopeStatus(TxScope requested, ICommandTx commandTx) {
			manageTimeout = requested == TxScope.Required || requested == TxScope.RequiresNew;
			TxScope = requested;
			this.commandTx = commandTx;
		}

		/// <summary>
		/// ロールバックがマークされたか否かを設定または取得します。
		/// </summary>
		public bool IsRoledBack { get; set; }

		/// <summary>
		/// タイムアウトをチェックします。
		/// </summary>
		/// <param name="checkPoint">チェックポイント</param>
		/// <exception cref="HeuristicTimeoutException">
		/// タイムアウト時間を経過してる場合
		/// </exception>
		public void CheckTimeout(string checkPoint) {
			var timeout = Interlocked.Read(ref txTimeoutTick);
			if (timeout < 0) {
				return;
			}
			var elapsed = DateTime.Now.Ticks - start;
			if (elapsed > timeout) {
				throw new HeuristicTimeoutException(checkPoint, timeout, elapsed);
			}
		}

		/// <summary>
		/// タイムアウトまでの残Tick数を取得します。
		/// </summary>
		/// <returns>
		/// タイムアウトまでの残Tick数。ただし、
		/// タイムアウト設定が存在しない場合：-1、
		/// タイムアウトが経過している場合：0
		/// </returns>
		public long GetTimeoutRemainTicks() {
			var timeout = Interlocked.Read(ref txTimeoutTick);
			if (timeout < 0) {
				return -1;
			}
			var elapsed = DateTime.Now.Ticks - start;
			return (elapsed > timeout) ? 0 : timeout - elapsed;
		}

		/// <summary>
		/// タイムアウトを登録します。
		/// </summary>
		/// <remarks>
		/// タイムアウト管理が行われていない場合もしくはすでに登録されている
		/// 場合には登録されません。
		/// </remarks>
		/// <param name="timeoutTick">タイムアウトTick</param>
		public void RegisterTimeout(long timeoutTick) {
			if (!manageTimeout) {
				return;
			}
			if (Interlocked.CompareExchange(ref txTimeoutTick, timeoutTick,
					InitialValue) == InitialValue) {
				start = DateTime.Now.Ticks;
			}
		}

		public void SetAttribute(string key, object value) {
			attributes[key] = value;
		}

		public object RemoveAttribute(string key) {
			object value;
			if (attributes.TryGetValue(key, out value)) {
				attributes.Remove(key);
			}
			return value;
		}

		public T GetAttribute<T>(string key) {
			if (attributes.TryGetValue(key, out var value) && value is T variable) {
				return variable;
			}
			return default(T);
		}

		public void Enlist(DbConnection connection) {
			commandTx?.Enlist(connection);
		}

		public void SetIsolationLevelMapping(Func<IsolationLevel, IsolationLevel> mapping) {
			if (commandTx != null) {
				commandTx.IsolationLevelMapping = mapping;
			}
		}


		#region Implementation of IDisposable
		public void Dispose() {
			var targets = new Dictionary<string, object>(attributes);
			attributes.Clear();
			foreach (var value in targets.Values) {
				if (value is IDisposable disposable) {
					disposable.Dispose();
				}
			}
		}
		#endregion
	}
}
