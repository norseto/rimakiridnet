// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;

namespace Net.Rimakiri.Command.Spi {
	/// <summary>
	/// コマンド実行例外ラッパーです。
	/// </summary>
	/// <remarks>
	/// リトライを判定するため、コマンドで発生した例外をラップします。
	/// </remarks>
	/// <inheritdoc />
	[Serializable]
	internal class ExecutionException : Exception {
		public int StackCount { get; }

		/// <inheritdoc />
		/// <summary>
		/// インスタンスを初期化します。
		/// </summary>
		/// <param name="innerException">発生した例外</param>
		/// <param name="stackCount">発生時のトランザクションスタックカウント</param>
		private ExecutionException(Exception innerException, int stackCount)
				: base("Command execution failed.", innerException) {
			StackCount = stackCount;
		}

		/// <summary>
		/// インスタンスを生成する、このクラスのファクトリメソッドです。
		/// </summary>
		/// <param name="inner">発生した例外</param>
		/// <param name="stackCount">発生時のトランザクションスタック数</param>
		public static ExecutionException Generate(Exception inner, int stackCount) {
			return new ExecutionException(inner, stackCount);
		}
	}
}
