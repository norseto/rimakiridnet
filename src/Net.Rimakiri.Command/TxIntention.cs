// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;

namespace Net.Rimakiri.Command {
	/// <summary>
	/// コマンドの処理のトランザクション用途を示します。
	/// </summary>
	/// <remarks>
	/// トランザクション分離レベルの調整の際に使用されます。
	/// 実際には、<see cref="TxIsolation"/>に変換された後に調整
	/// されます。
	/// </remarks>
	/// <seealso cref="TxIsolation"/>
	/// <seealso cref="IIsolationCoodinatee"/>
	[Flags]
	public enum TxIntention {
		/// <summary>
		/// 未指定
		/// </summary>
		UnSpecified = 0,

		/// <summary>
		/// コマンドでSELECTを実行することを示します。
		/// </summary>
		Select = 1,

		/// <summary>
		/// コマンドでSELECT for Updateを実行することを示します。
		/// </summary>
		SelectForUpdate = 2,

		/// <summary>
		/// コマンドでUPDATEを実行することを示します。
		/// </summary>
		Update = 4,

		/// <summary>
		/// コマンドでINSERTを実行することを示します。
		/// </summary>
		Insert = 8,

		/// <summary>
		/// コマンドでDELETEを実行することを示します。
		/// </summary>
		Delete = 16,
	}

	/// <summary>
	/// <see cref="TxIntention"/>の拡張機能を提供します。
	/// </summary>
	public static class TxIntentionExtension {
		private const TxIntention All
			= TxIntention.Select | TxIntention.SelectForUpdate
				| TxIntention.Insert | TxIntention.Delete
				| TxIntention.Update;

		/// <summary>
		/// 指定のトランザクション用途が指定した用途のいずれかのみがセット
		/// されているかを判定します。
		/// </summary>
		/// <param name="intention">対象トランザクション用途</param>
		/// <param name="flags">検査するトランザクション用途</param>
		/// <returns>
		/// 対象トランザクションが検査するトランザクション用途のいずれ
		/// かがセットされており、その他がセットされていない場合、True
		/// </returns>
		public static bool HasOnlyAnyFlag(this TxIntention intention, TxIntention flags) {
			return (intention & flags) != 0 && (intention & (All ^ flags)) == 0;
		}
	}
}
