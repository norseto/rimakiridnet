﻿// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Data;
using Net.Rimakiri.Command.Spi;

namespace Net.Rimakiri.Command {
	/// <summary>
	/// コマンドの処理のトランザクション分離レベルを示します。
	/// </summary>
	/// <remarks>
	/// トランザクション分離レベルの調整の際に使用されます。
	/// 指定された分離レベルよりも厳格なものが使用される場合があります。
	/// </remarks>
	/// <seealso cref="IIsolationCoodinatee"/>
	/// <seealso cref="ICommandTxFactory"/>
	/// <seealso cref="CommandServiceBuilder{T}"/>
	[Flags]
	public enum TxIsolation {
		/// <summary>
		/// 未指定 - デフォルト値を使用
		/// </summary>
		Unspecified = 0,

		/// <summary>
		/// SNAPSHOT - スナップショットを使用
		/// </summary>
		Snapshot = 1,

		/// <summary>
		/// READ UNCOMMITTED - ダーティリードを許可
		/// </summary>
		ReadUncommitted = 8,

		/// <summary>
		/// READ COMMITTED - コミット済最新読取り
		/// </summary>
		ReadCommitted = 16,

		/// <summary>
		/// REPEATABLE READ - 一貫性読取り
		/// </summary>
		RepeatableRead = 32,

		/// <summary>
		/// SERIALIZABLE - 直列化可能
		/// </summary>
		Serializable = 64,
	}

	/// <summary>
	/// <see cref="TxIsolation"/>の拡張機能を提供します。
	/// </summary>
	public static class TxIsolationExtension {
		/// <summary>
		/// 対応する<see cref="IsolationLevel"/>の値を返却します。
		/// </summary>
		/// <param name="isolation">対象<see cref="TxIsolation"/></param>
		/// <returns><see cref="System.Data.IsolationLevel"/>の値</returns>
		/// <remarks>
		/// 複数の値が指定されている場合、以下の順序で決定されます。
		/// <ol>
		/// <li><see cref="TxIsolation.Snapshot"/> - <see cref="IsolationLevel.Snapshot"/></li>
		/// <li><see cref="TxIsolation.Serializable"/> - <see cref="IsolationLevel.Serializable"/></li>
		/// <li><see cref="TxIsolation.RepeatableRead"/> - <see cref="IsolationLevel.RepeatableRead"/></li>
		/// <li><see cref="TxIsolation.ReadUncommitted"/> - <see cref="IsolationLevel.ReadUncommitted"/></li>
		/// <li><see cref="TxIsolation.ReadCommitted"/> - <see cref="IsolationLevel.ReadCommitted"/></li>
		/// </ol>
		/// 対応する値が存在しない場合、<see cref="System.Data.IsolationLevel.ReadCommitted"/>
		/// を返却します。
		/// </remarks>
		public static IsolationLevel ToDbIsolationLevel(this TxIsolation isolation) {
			TxIsolation[] flags = {
				TxIsolation.Snapshot,
				TxIsolation.Serializable,
				TxIsolation.RepeatableRead,
				TxIsolation.ReadUncommitted,
				TxIsolation.ReadCommitted,
			};
			IsolationLevel[] levels = {
				IsolationLevel.Snapshot,
				IsolationLevel.Serializable,
				IsolationLevel.RepeatableRead,
				IsolationLevel.ReadUncommitted,
				IsolationLevel.ReadCommitted,
			};
			for (int i = 0, l = Math.Min(flags.Length, levels.Length); i < l; i++) {
				if (isolation.HasFlag(flags[i])) {
					return levels[i];
				}
			}
			return IsolationLevel.ReadCommitted;
		}

		/// <summary>
		/// 複数の<see cref="TxIsolation"/>をひとつにまとめます。
		/// </summary>
		/// <param name="isolation">対象<see cref="TxIsolation"/></param>
		/// <returns>まとめられた値</returns>
		/// <remarks>
		/// 複数の値が指定されている場合、以下の順序で決定されます。
		/// <ol>
		/// <li><see cref="TxIsolation.Snapshot"/></li>
		/// <li><see cref="TxIsolation.Serializable"/></li>
		/// <li><see cref="TxIsolation.RepeatableRead"/></li>
		/// <li><see cref="TxIsolation.ReadUncommitted"/></li>
		/// <li><see cref="TxIsolation.ReadCommitted"/></li>
		/// </ol>
		/// 対応する値が存在しない場合、<see cref="System.Data.IsolationLevel.ReadCommitted"/>
		/// を返却します。
		/// </remarks>
		public static TxIsolation Simplify(this TxIsolation isolation) {
			TxIsolation[] flags = {
				TxIsolation.Snapshot,
				TxIsolation.Serializable,
				TxIsolation.RepeatableRead,
				TxIsolation.ReadUncommitted,
				TxIsolation.ReadCommitted,
			};
			foreach (var flag in flags) {
				if (isolation.HasFlag(flag)) {
					return flag;
				}
			}
			return TxIsolation.ReadCommitted;
		}
	}
}
