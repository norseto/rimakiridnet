// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;

namespace Net.Rimakiri.Command {
	/// <summary>
	/// <see cref="ICommandService"/>インターフェイスを実装した
	/// コマンドサービスクラスのインスタンスを生成するビルダクラスです。
	/// </summary>
	/// <typeparam name="T">コンテキストの型</typeparam>
	/// <seealso cref="CommandServiceBuilder{T}"/>
	public interface ICommandServiceBuilder<out T> where T : CommandContext {
		/// <summary>
		/// <see cref="ICommandService"/>インターフェイスを実装
		/// したコマンドサービスのインスタンスを生成します。
		/// </summary>
		/// <param name="postCreate">コンテキスト生成時に呼出されるコールバックアクション</param>
		/// <param name="preDestroy">コンテキスト破棄時に呼出されるコールバックアクション</param>
		/// <returns>生成したコマンドサービス</returns>
		/// <remarks>
		/// <paramref name="postCreate"/>および<paramref name="preDestroy"/>で指定したコールバック
		/// は生成したサービスでのみ有効となります。
		/// </remarks>
		ICommandService Build(Action<T> postCreate = null,
			Action<T> preDestroy = null);
	}

	/// <summary>
	/// コンテキストに<see cref="CommandContext"/>クラスを使用する、
	/// <see cref="ICommandService"/>インターフェイスを実装した
	/// コマンドサービスクラスのインスタンスを生成するビルダクラスです。
	/// </summary>
	/// <seealso cref="CommandServiceBuilder"/>
	public interface ICommandServiceBuilder :  ICommandServiceBuilder<CommandContext> {
		// Empty
	}
}
