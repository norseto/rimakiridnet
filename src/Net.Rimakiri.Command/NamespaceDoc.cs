// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Runtime.CompilerServices;
using Net.Rimakiri.Command.Spi;

namespace Net.Rimakiri.Command {
	/// <summary>
	/// この名前空間には、特定の処理を行うコマンドオブジェクトを
	/// トランザクションやリトライ、タイムアウトなどが管理された
	/// 状態で実行するフレームワークが含まれています。
	/// </summary>
	[CompilerGenerated]
	internal class NamespaceGroupDoc {
		// Empty
	}

	/// <summary>
	/// コマンド名前空間には、コマンドフレームワークを使用して
	/// コマンドを実行するためのAPIが含まれています。
	/// コマンドとは、<see cref="ICommand{T}"/>を実装したクラスのことです。
	/// コマンドの実行は、コマンドサービスと呼ばれる<see cref="ICommandService"/>
	/// を実装したインスタンスを使用します。
	/// このコマンドサービスインスタンスは通常<see cref="CommandServiceBuilder{T}"/>
	/// により生成します。<br/>
	/// </summary>
	/// <remarks>
	/// コマンドは呼出しごとに生成する使い捨てのオブジェクトです。コマンドにプロパティ
	/// を定義してコマンドに対するデータの入出力を行ってください。コマンドからさらに
	/// 別のコマンドを実行することも可能です。<br/>
	/// その他、コマンドフレームワークには以下の機能・概念があります。
	/// <list type="table">
	/// <listheader>
	/// <term>機能・概念</term>
	/// <description>説明</description>
	/// </listheader>
	/// 
	/// <item>
	/// <term>コマンドトランザクション</term>
	/// <description>
	/// コマンドフレームワークにはコマンドトランザクションと呼ばれる抽象化されたトランザクション
	/// の枠組みの概念があります（<see cref="ICommandTx"/>）。
	/// 実行されるコマンドの<see cref="ICommand{T}.TxScope"/>プロパティが返却する値により、
	/// コマンドサービスがコマンドトランザクションを準備します。このコマンドトランザクション
	/// がローカルデータトランザクションとして実行されるか、それともグローバルトランザクション
	/// として実行されるかは、<see cref="CommandServiceBuilder{T}.TransactionFactory"/>
	/// によって指定可能です。デフォルトでは、クラウド等の使用を考慮してローカルデータベース
	/// トランザクションを使用します。どちらのトランザクションを使用するかにかかわらず、コマンド
	/// の実装に違いはなく、コマンド側でコミット・ロールバックを行う必要はありません。
	/// </description>
	/// </item>
	/// 
	/// <item>
	/// <term>冪（べき）等</term>
	/// <description>
	/// このフレームワークでは、コマンドの実行が複数回行われた場合の状態変化の有無を冪等性と呼ん
	/// でいます。例えば、データベースを検索して結果を返すような処理は冪等といえます（返却内容
	/// は異なる場合もありますが）。また、単にファイルにログを追加書込みするような処理は冪等
	/// とはいえない場合もあると思われます。コマンドの冪等性は、
	/// <see cref="ICommand{T}.Idempotent"/>でフレームワークに通知します。コマンドサービス
	/// は、実行するコマンドの冪等性をもとに、例外が発生した場合には再実行を行うことが可能です。
	/// 例えば、デッドロックが発生した場合には、即時その処理を再実行するようにも、トランザクション
	/// がロールバックされた後に再実行するようにも指定できます。ただし、冪等でない処理を行った
	/// 場合には、再実行は行われません。どのような場合にリトライするかについては、
	/// <see cref="CommandServiceBuilder{T}.RetryPolicy"/>で指定します。
	/// </description>
	/// </item>
	/// 
	/// <item>
	/// <term>トランザクション分離レベル</term>
	/// <description>
	/// このフレームワークでは、デフォルトのトランザクション分離レベルを指定することが可能です
	/// （<see cref="CommandServiceBuilder{T}.IsolationLevel"/>）。
	/// また、特定のインターフェイスを実装したコマンドが通知する処理内容（<see cref="TxIntention"/>）
	/// により、トランザクション分離レベルを自動で設定するようにすることも可能です。自動調整
	/// の内容については、<see cref="CommandServiceBuilder{T}.TxCoordination"/>で
	/// 指定することが可能です。
	/// </description>
	/// </item>
	/// 
	/// <item>
	/// <term>タイムアウト</term>
	/// <description>
	/// コマンドトランザクションのタイムアウトを指定することが可能です。特定の処理後に自動で
	/// チェックを行い、タイムアウトを超過している場合、<see cref="HeuristicTimeoutException"/>
	/// を送出します。
	/// </description>
	/// </item>
	/// 
	/// </list>
	/// </remarks>
	/// <remarks>
	/// <p>
	/// このコマンドフレームワークを使用してアプリケーションを
	/// 作成する場合、以下のような手順で行います。
	/// </p>
	/// <ol>
	/// <li>
	/// 実際に使用するコンテキストの型を決定します。コンテキスト
	/// は<see cref="CommandContext"/>を継承して作成し、機能を
	/// 追加します。 例えば、実行中のユーザ情報の取得や設定などです。
	/// <example>
	/// <code source="DocExample.CS/CsSample.cs" region="CommandContext" language="C#" />
	/// <code source="DocExample.VB/VbSample.vb" region="CommandContext" language="VB" />
	/// </example>
	/// </li>
	/// <li>
	/// 作成したコンテクストを使用するコマンドのインターフェイスを
	/// <see cref="ICommand{T}"/>を継承して作成します。
	/// 必ず作成しな ければならないわけではありませんが、
	/// 作成しておくと便利です。
	/// <example>
	/// <code source="DocExample.CS/CsSample.cs" region="ICommand" language="C#" />
	/// <code source="DocExample.VB/VbSample.vb" region="ICommand" language="VB" />
	/// </example>
	/// アプリケーションで使用するコマンドは全てこのインターフェイス
	/// を実装するようにします。
	/// <example>
	/// <code source="DocExample.CS/CsSample.cs" region="CommandImpl" language="C#" />
	/// <code source="DocExample.VB/VbSample.vb" region="CommandImpl" language="VB" />
	/// </example>
	/// </li>
	/// <li>
	/// 作成したコンテクストを使用するコマンドサービスビルダと
	/// インターフェイスを <see cref="CommandServiceBuilder{T}"/>
	/// および<see cref="ICommandServiceBuilder{T}"/>を継承して作成
	/// します。
	/// 作成したビルダの中身は空で問題ありません。
	/// アプリケーションではこのサービスビルダを使用するようにします。
	/// これらのクラスやインターフェイスは必ず作成しな ければならない
	/// わけではありませんが、作成しておくと便利です。
	/// <example>
	/// <code source="DocExample.CS/CsSample.cs" region="CommandServiceBuilder" language="C#" />
	/// <code source="DocExample.VB/VbSample.vb" region="CommandServiceBuilder" language="VB" />
	/// </example>
	/// </li>
	/// <li>
	/// 作成したコマンドビルダを使用してコマンドを実行することができます。
	/// <example>
	/// <code source="DocExample.CS/CsSample.cs" region="BuildExample" language="C#" />
	/// <code source="DocExample.VB/VbSample.vb" region="BuildExample" language="VB" />
	/// </example>
	/// </li>
	/// <li>
	/// 必要に応じて作成したコンテクストのライフサイクルリスナを
	/// <see cref="IContextLifecycleListener{T}"/>を実装して作成します。
	/// <example>
	/// <code source="DocExample.CS/CsSample.cs" region="LifecycleListener" language="C#" />
	/// <code source="DocExample.VB/VbSample.vb" region="LifecycleListener" language="VB" />
	/// </example>
	/// 作成した場合には、コマンドサービスビルダの
	/// <see cref="CommandServiceBuilder{T}.ContextLifecycleListener"/>
	/// に設定して使用するようにします。
	/// <example>
	/// <code source="DocExample.CS/CsSample.cs" region="BuildExampleListener" language="C#" />
	/// <code source="DocExample.VB/VbSample.vb" region="BuildExampleListener" language="VB" />
	/// </example>
	/// </li>
	/// </ol>
	/// <p>
	/// アプリケーション特有のコンテキストが必要でない場合には、そのまま
	/// <see cref="CommandContext"/>、<see cref="ICommand"/>、
	/// <see cref="ICommandServiceBuilder"/>および
	/// <see cref="CommandServiceBuilder"/>を使用することができます。
	/// ただ、アプリケーションの今後の変化の可能性を考えると、
	/// アプリケーション用のコンテキストを作成することを推奨します。
	/// </p>
	/// </remarks>
	/// <seealso cref="CommandContext"/>
	/// <seealso cref="ICommand{T}"/>
	/// <seealso cref="ICommandService"/>
	/// <seealso cref="ICommandServiceBuilder{T}"/>
	/// <seealso cref="CommandServiceBuilder{T}"/>
	[CompilerGenerated]
	internal class NamespaceDoc {
		// Empty
	}
}

namespace Net.Rimakiri.Command.Spi {
	/// <summary>
	/// コマンドSPI(Service Provider Interface)名前空間には、
	/// コマンドフレームワーク自体の挙動をカスタマイズまたは
	/// 作成するためのAPIが含まれています。
	/// </summary>
	[CompilerGenerated]
	internal class NamespaceDoc {
		// Empty
	}
}
