﻿// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

namespace Net.Rimakiri.Command {
	/// <summary>
	/// コマンドを実行するサービスが提供するインターフェイスです。
	/// </summary>
	/// <remarks>
	/// <see cref="ICommandService"/>のインスタンスは、ステート
	/// を持たず、マルチスレッドセーフでかつ再利用可能でなければなりません。
	/// </remarks>
	public interface ICommandService {
		/// <summary>
		/// 指定されたコマンドを実行します。
		/// </summary>
		/// <param name="command">実行するコマンド</param>
		/// <typeparam name="T">使用されるコンテキストの型</typeparam>
		void ExecuteCommand<T>(ICommand<T> command) where T: CommandContext;
	}
}
