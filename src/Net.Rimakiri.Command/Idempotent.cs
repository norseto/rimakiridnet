﻿// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using Net.Rimakiri.Command.Spi;

namespace Net.Rimakiri.Command {
	/// <summary>
	/// コマンドの処理の冪等性を示します。コマンドの冪等性は、
	/// プロパティ
	/// <see cref="ICommand{T}.Idempotent"/>にて示されます。
	/// </summary>
	/// <remarks>
	/// <p>
	/// コマンドの実行時に例外が発生した場合、発生した例外からリトライ
	/// が可能と判断され、なおかつコマンドが冪等である場合にはコマンド
	/// サービスは再度コマンドを実行します。
	/// 発生した例外がリトライ可能かの判定には、
	/// <see cref="ICommandServiceSpi.RetryPolicy"/>
	/// が使用されます。また、最大リトライ回数は、
	/// <see cref="ICommandServiceSpi.MaxRetry"/>で決定されます。
	/// </p>
	/// <p>
	///	冪等性が<see cref="Transactional"/>のコマンドで例外が発生し
	/// た場合、それまでのトランザクションをロールバック後、そのトラ
	/// ンザクションに含まれる全てのコマンドのリトライが行われます
	/// （<see cref="None"/>のコマンドが実行がされていない場合）。
	/// </p>
	/// <p>
	///	冪等性が<see cref="Anytime"/>のコマンドで例外が発生した場合、
	/// 即座にリトライが行われます。別のコマンドから呼び出される場合には、
	/// 呼出元のリトライを考慮すると、想定より多い回数の呼出が行われる
	/// ことがあります。
	/// </p>
	/// <p>
	///	ひとつでも冪等性が<see cref="None"/>のコマンドの実行が終了
	/// （正常終了のほか、例外発生も含まれます）すると、その後に実行
	/// されたコマンドで例外が発生してもリトライは行われません。
	/// なお、冪等性が<see cref="None"/>のコマンドから呼び出された
	/// 冪等性が<see cref="Anytime"/>のコマンドで例外が発生した場合、この
	/// コマンドのリトライは行われます（冪等性が<see cref="None"/>の
	/// コマンドの実行は終了していないため）。
	/// </p>
	/// <p>
	/// 未指定値<see cref="Unspecified"/>をデフォルトの値に丸めるには、
	/// <see cref="IdempotentExtension.ToSpecificValue"/>
	/// を使用します。
	/// </p>
	/// </remarks>
	/// <seealso cref="ICommand{T}"/>
	/// <seealso cref="ICommandServiceSpi"/>
	public enum Idempotent {
		/// <summary>
		/// 未指定 - デフォルト値を使用
		/// </summary>
		Unspecified = 0,

		/// <summary>
		/// 非冪等
		/// </summary>
		None = 1,

		/// <summary>
		/// トランザクションがロールバックされれば冪等
		/// </summary>
		Transactional = 2,

		/// <summary>
		/// 常に冪等（トランザクションのロールバック不要）
		/// </summary>
		Anytime = 4,
	}

	/// <summary>
	/// <see cref="Idempotent"/>の拡張機能を提供します。
	/// </summary>
	public static class IdempotentExtension {
		/// <summary>
		/// 未指定値をデフォルトの値に丸めます。
		/// </summary>
		/// <param name="idempotent">対象冪等性値</param>
		/// <returns>
		/// 対象冪等性値が未指定値の場合はデフォルト値、
		/// その他の場合にはそのままの値
		/// </returns>
		public static Idempotent ToSpecificValue(this Idempotent idempotent) {
			return (idempotent == Idempotent.Unspecified)
				? Idempotent.None
				: idempotent;
		}
	}
}
