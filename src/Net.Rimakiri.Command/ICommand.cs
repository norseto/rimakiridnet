﻿// ==============================================================================
//     Net.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

namespace Net.Rimakiri.Command {
	/// <summary>
	/// <see cref="ICommandService"/>で実行するアクションコマンドが実装
	/// する必要があるインターフェイスです。
	/// </summary>
	/// <typeparam name="T">使用されるコンテキストの型</typeparam>
	/// <seealso cref="IIsolationCoodinatee"/>
	public interface ICommand<in T> where T : CommandContext {
		/// <summary>
		/// このコマンドが動作するトランザクションスコープを取得します。
		/// </summary>
		TxScope TxScope { get; }

		/// <summary>
		/// このコマンドの動作の冪等性を取得します。
		/// </summary>
		Idempotent Idempotent { get; }

		/// <summary>
		/// コマンド実行前のコールバックです。<see cref="Execute"/>がコマンド
		/// サービスにより実行される前に呼び出されます。
		/// </summary>
		/// <remarks>
		/// このコマンドの<see cref="Execute"/>がトランザクション内で呼び
		/// 出された場合を除き、このコールバックはトランザクション「外」で
		/// 呼び出されます。
		/// </remarks>
		/// <param name="context">実行コンテキスト</param>
		void PreExecute(T context);

		/// <summary>
		/// コマンドの処理を実行します。
		/// </summary>
		/// <remarks>
		/// このコールバックは指定したトランザクション「内」で呼び出されます。
		/// <paramref name="context"/>の<see cref="CommandContext.Service"/>
		/// プロパティの<see cref="ICommandService.ExecuteCommand{T}"/>を呼出
		/// すことにより、さらに別のコマンドを実行することができます。
		/// </remarks>
		/// <param name="context">実行コンテキスト</param>
		void Execute(T context);

		/// <summary>
		/// コマンド実行後のコールバックです。<see cref="Execute"/>がコマンド
		/// サービスにより実行された後に呼び出されます。
		/// </summary>
		/// <remarks>
		/// このコマンドの<see cref="Execute"/>がトランザクション内で呼び
		/// 出された場合を除き、このコールバックはトランザクション「外」で
		/// 呼び出されます。
		/// </remarks>
		/// <param name="context">実行コンテキスト</param>
		void PostExecute(T context);
	}

	/// <summary>
	/// デフォルトのコンテキストを使用するコマンドインターフェイスです。
	/// </summary>
	public interface ICommand : ICommand<CommandContext> {
		// Empty
	}
}
