// ==============================================================================
//     Net.Rimakiri.Command.Transactions
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Transactions;

namespace Net.Rimakiri.Command.Transactions {
	/// <summary>
	/// <see cref="TransactionScope"/>に関連する拡張機能を提供します。
	/// </summary>
	public static class TransactionScopeExtension {
		/// <summary>
		/// 対応する<see cref="IsolationLevel"/>の値を返却します。
		/// </summary>
		/// <param name="isolation">対象<see cref="TxIsolation"/></param>
		/// <returns><see cref="IsolationLevel"/>の値</returns>
		/// <remarks>
		/// 複数の値が指定されている場合、以下の順序で決定されます。
		/// <ol>
		/// <li><see cref="TxIsolation.Snapshot"/> - <see cref="IsolationLevel.Snapshot"/></li>
		/// <li><see cref="TxIsolation.Serializable"/> - <see cref="IsolationLevel.Serializable"/></li>
		/// <li><see cref="TxIsolation.RepeatableRead"/> - <see cref="IsolationLevel.RepeatableRead"/></li>
		/// <li><see cref="TxIsolation.ReadUncommitted"/> - <see cref="IsolationLevel.ReadUncommitted"/></li>
		/// <li><see cref="TxIsolation.ReadCommitted"/> - <see cref="IsolationLevel.ReadCommitted"/></li>
		/// </ol>
		/// 対応する値が存在しない場合、<see cref="IsolationLevel.ReadCommitted"/>
		/// を返却します。
		/// </remarks>
		public static IsolationLevel ToIsolationLevel(this TxIsolation isolation) {
			TxIsolation[] flags = {
				TxIsolation.Snapshot,
				TxIsolation.Serializable,
				TxIsolation.RepeatableRead,
				TxIsolation.ReadUncommitted,
				TxIsolation.ReadCommitted,
			};
			IsolationLevel[] levels = {
				IsolationLevel.Snapshot,
				IsolationLevel.Serializable,
				IsolationLevel.RepeatableRead,
				IsolationLevel.ReadUncommitted,
				IsolationLevel.ReadCommitted,
			};
			for (int i = 0, l = Math.Min(flags.Length, levels.Length); i < l; i++) {
				if (isolation.HasFlag(flags[i])) {
					return levels[i];
				}
			}
			return IsolationLevel.ReadCommitted;
		}

		/// <summary>
		/// TxScope値を<see cref="TransactionScopeOption"/>に変換します。
		/// </summary>
		/// <param name="scope">対象スコープ</param>
		/// <returns>変換後の値。対応するものがない場合null</returns>
		public static TransactionScopeOption? ToTransactionScopeOption(this TxScope scope) {
			switch (scope.ToSpecificValue()) {
			case TxScope.Required:
				return TransactionScopeOption.Required;
			case TxScope.RequiresNew:
				return TransactionScopeOption.RequiresNew;
			case TxScope.Suppress:
				return TransactionScopeOption.Suppress;
			default:
				return null;
			}
		}

	}
}
