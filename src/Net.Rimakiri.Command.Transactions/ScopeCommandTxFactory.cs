// ==============================================================================
//     Net.Rimakiri.Command.Transactions
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Data.Common;
using System.Transactions;
using Net.Rimakiri.Command.Spi;
using Net.Rimakiri.Command.Transactions.Properties;
using IsolationLevel = System.Data.IsolationLevel;

namespace Net.Rimakiri.Command.Transactions {
	/// <summary>
	/// <see cref="TransactionScope"/>を使用する<see cref="ICommandTx"/>
	/// を生成するファクトリクラスです。
	/// </summary>
	/// <remarks>
	/// このクラスが生成したインスタンスに対する<see cref="ICommandTx.Enlist"/>の
	/// 呼出しは何も行われません(自動的に行われます)。
	/// </remarks>
	/// <seealso cref="ICommandServiceSpi"/>
	/// <inheritdoc cref="AbstractCommandTxFactory"/>
	public class ScopeCommandTxFactory : AbstractCommandTxFactory {
		/// <summary>
		/// トランザクションスコープを生成します。
		/// </summary>
		/// <param name="command">対象コマンド</param>
		/// <param name="currentScope">現在のトランザクションスコープ</param>
		/// <typeparam name="T">コマンドコンテキストの型</typeparam>
		/// <exception cref="InvalidOperationException">
		/// 対象コマンドのスコープが<see cref="TxScope.Mandatory"/>で
		/// 現在トランザクションが開始していない場合。
		/// </exception>
		/// <returns>生成したトランザクションスコープ。不要な場合はnull</returns>
		/// <remarks>
		/// アンビエントトランザクションが存在する状態で、<see cref="TxScope.Required"/>
		/// が指定された場合、スコープは生成されませんが、<see cref="TxScope.RequiresNew"/>
		/// または<see cref="TxScope.Suppress"/>が指定された場合には生成されます。
		/// </remarks>
		public override ICommandTx Create<T>(ICommand<T> command, TxScope currentScope) {
			var inTran = currentScope == TxScope.Required || currentScope == TxScope.RequiresNew;
			var scope = command.TxScope;
			var specificScope = scope.ToSpecificValue();
			if (!inTran && specificScope == TxScope.Mandatory) {
				throw new InvalidOperationException(Resources.MandatoryNeedsTx);
			}

			var option = specificScope.ToTransactionScopeOption();
			if (option == null || inTran && option == TransactionScopeOption.Required
				|| (specificScope == TxScope.Suppress && specificScope == currentScope)) {
				return null;
			}

			var isolation = GetRuntimeIsolation(command, specificScope);
			var txOptions = new TransactionOptions {
				Timeout = Timeout,
				IsolationLevel = isolation.ToIsolationLevel(),
			};

#if (NET40 || NET45)
			var ret = new TransactionWrapper(isolation,
					new TransactionScope(option.Value, txOptions), NoCommit);
#else
			var ret = new TransactionWrapper(isolation,
						new TransactionScope(option.Value, txOptions,
								TransactionScopeAsyncFlowOption.Enabled), NoCommit);

#endif

			return ret;
		}
	}

	internal sealed class TransactionWrapper : ICommandTx {
		private readonly TransactionScope scope;
		private readonly bool noCommit;

		public TransactionWrapper(TxIsolation isolation, TransactionScope scope, bool noCommit) {
			this.scope = scope;
			this.noCommit = noCommit;
			TxIsolation = isolation.Simplify();
		}

		#region Implementation of ICommandTx
		/// <inheritdoc />
		public DbTransaction DbTransaction => null;

		/// <inheritdoc />
		public TxIsolation TxIsolation { get; }

		/// <inheritdoc />
		public bool SupportsGlobal => true;

		/// <inheritdoc />
		public void Complete() {
			if (!noCommit) {
				scope.Complete();
			}
		}

		/// <inheritdoc />
		public void Enlist(DbConnection connection) {
			// Nothing to do in this class.
		}

		/// <inheritdoc />
		public Func<IsolationLevel, IsolationLevel> IsolationLevelMapping { get; set; }
		#endregion

		#region Implementation of IDisposable
		/// <inheritdoc />
		public void Dispose() {
			scope.Dispose();
		}
		#endregion
	}
}
