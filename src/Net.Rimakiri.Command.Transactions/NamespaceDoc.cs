﻿// ==============================================================================
//     Net.Rimakiri.Command.Transactions
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Runtime.CompilerServices;
using System.Transactions;
using Net.Rimakiri.Command.Spi;

namespace Net.Rimakiri.Command.Transactions {
	/// <summary>
	/// この名前空間には、<see cref="TransactionScope"/>
	/// を使用した拡張クラスが含まれています。
	/// </summary>
	[CompilerGenerated]
	internal class NamespaceGroupDoc {
		// Empty
	}

	/// <summary>
	/// この名前空間には、<see cref="TransactionScope"/>
	/// を使用した拡張クラスが含まれています。
	/// </summary>
	/// <seealso cref="ICommandService"/>
	/// <seealso cref="ICommandServiceBuilder{T}"/>
	/// <seealso cref="CommandServiceBuilder{T}"/>
	/// <seealso cref="LocalCommandTxFactory"/>
	[CompilerGenerated]
	internal class NamespaceDoc {
		// Empty
	}
}
