// ==============================================================================
//     Test.Rimakiri.Data.Rdbms
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

#if (NET40 || NET45 || NET451 || NET452 || NET46 || NET461 || NET462 || NET47 || NET471 || NET472)
#define NETFRAMEWORK
#endif

using System;
using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;
using Net.Rimakiri.Command;
using Net.Rimakiri.Command.Database;
using Net.Rimakiri.Command.Spi;
using Net.Rimakiri.Data.DB2;
using Net.Rimakiri.Data.Interfaces;
using Test.Net.Rimakiri.Data.Common;
#if (NETFRAMEWORK)
using IBM.Data.DB2;
#else
using IBM.Data.DB2.Core;
#endif

namespace Test.Rimakiri.Data.Rdbms {
	[ExcludeFromCodeCoverage]
	internal class DB2RetryPolicyWithCount : DB2RetryPolicy {
		private int retryCount;
		private int errorCount;

		public int RetryCount {
			get { return retryCount; }
			set { retryCount = value; }
		}

		public int ErrorCount {
			get { return errorCount; }
			set { errorCount = value; }
		}

		public int LastError { get; private set; }

		#region Overrides of DB2RetryPolicy
		public override bool IsRetryable(Exception error, int tryCount) {
			errorCount++;
			if (base.IsRetryable(error, tryCount)) {
				var cause = error.FindCause<DB2Exception>();
				LastError = cause.ErrorCode;
				retryCount++;
				return true;
			}
			return false;
		}
		#endregion
	}

	/// <summary>
	/// SQL Server用のテスト
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestDB2 : RdbmsTestBase {
		private IDatabaseDialect Dialect { get; set; }

		#region Overrides of RdbmsTestBase
		protected override IConnectionFactory GetConnectionFactory() {
			return new DB2ConnectionFactory {
				ConnectionStringName = "DB2",
				DatabaseDialect = Dialect,
			};
		}
		#endregion

		protected DacCommandServiceBuilder<TestDacContext> MakeServiceBuilder() {
#if (NETFRAMEWORK)
			return base.MakeServiceBuilder();
#else
			return base.MakeServiceBuilder(false);
#endif
		}

		[Test]
		[Category("Database")]
		[Category("DB2")]
		public void TestConnect() {
			DoTestConnect();
		}

		[Test]
		[Category("Database")]
		[Category("DB2")]
		public void TestSimple() {
			var service = MakeServiceBuilder().Build();
			DoInitialize(service);
			DoTestSimple(service);
		}

		[Test]
		[Category("Database")]
		[Category("DB2")]
		public void TestSelectLike() {
			var builder = MakeServiceBuilder();
			var service = builder.Build();
			DoSelectLike(service);
		}

		[Test]
		[Category("Database")]
		[Category("DB2")]
		public void TestRollback() {
			var service = MakeServiceBuilder().Build();
			DoInitialize(service);
			Assert.AreEqual(3, DoTestSimple(service));
			DoClean(service);
			Assert.AreEqual(0, DoTestSimple(service));
			DoInitialize(service);
			Assert.AreEqual(3, DoTestSimple(service));

			var builder = MakeServiceBuilder();
			builder.NoCommit = true;
			service = builder.Build();
			DoClean(service);
			Assert.AreEqual(3, DoTestSimple(service));
		}

		[Test]
		[Category("Database")]
		[Category("DB2")]
		public void TestSimpleLocal() {
			var builder = MakeServiceBuilder();
			builder.TransactionFactory = new LocalCommandTxFactory();
			var service = builder.Build();
			DoInitialize(service);
			DoTestSimple(service);
		}

		[Test]
		[Category("Database")]
		[Category("DB2")]
		public void TestRollbackLocal() {
			var builder = MakeServiceBuilder();
			builder.TransactionFactory = new LocalCommandTxFactory();
			var service = builder.Build();
			DoInitialize(service);
			Assert.AreEqual(3, DoTestSimple(service));
			DoClean(service);
			Assert.AreEqual(0, DoTestSimple(service));
			DoInitialize(service);
			Assert.AreEqual(3, DoTestSimple(service));

			builder = MakeServiceBuilder();
			builder.NoCommit = true;
			builder.TransactionFactory = new LocalCommandTxFactory();
			service = builder.Build();
			DoClean(service);
			Assert.AreEqual(3, DoTestSimple(service));
		}

		[Test]
		[Category("Database")]
		[Category("DB2")]
		public void TestDeadLock() {
			var builder = MakeServiceBuilder();
			var policy = new DB2RetryPolicyWithCount();
			builder.RetryPolicy = policy;

			var service = builder.Build();
			DoDeadLockRecovery(service, builder.ConnectionFactory.DatabaseDialect);
			Assert.AreEqual(1, policy.RetryCount);
			Assert.AreEqual(1, policy.ErrorCount);
			Dialect = null;
		}

		[Test]
		[Category("Database")]
		[Category("DB2")]
		public void TestUnretryableError() {
			var builder = MakeServiceBuilder();
			var policy = new DB2RetryPolicyWithCount();
			builder.RetryPolicy = policy;

			var service = builder.Build();
			try {
				DoThrowError(service);
				Assert.IsTrue(false, "Should thrown an exception");
			}
			catch (NullReferenceException) {
				Assert.IsTrue(true);
			}
			Assert.AreEqual(0, policy.RetryCount);
			Assert.AreEqual(1, policy.ErrorCount);
		}

		[Test]
		[Category("Database")]
		[Category("DB2")]
		public void TestSerialization() {
			var builder = MakeServiceBuilder();
			var policy = new DB2RetryPolicyWithCount();
			builder.Timeout = TimeSpan.FromSeconds(15);
			builder.RetryPolicy = policy;
			builder.IsolationLevel = TxIsolation.RepeatableRead;

			var service = builder.Build();
			DoSerializationRecovery(service, builder.ConnectionFactory.DatabaseDialect);

// I don't know why these changes are occurred. Anyway, it does succeed.
#if (NETFRAMEWORK)
			Assert.AreEqual(0, policy.RetryCount);
			Assert.AreEqual(0, policy.ErrorCount);
#else
			Assert.AreEqual(1, policy.RetryCount);
			Assert.AreEqual(1, policy.ErrorCount);
#endif
		}

		[Test]
		[Category("Database")]
		[Category("DB2")]
		public void TestSerializationSnapshot() {
			var builder = MakeServiceBuilder();
			var policy = new DB2RetryPolicyWithCount();
			builder.Timeout = TimeSpan.FromSeconds(15);
			builder.RetryPolicy = policy;
			builder.IsolationLevel = TxIsolation.Snapshot;

			var service = builder.Build();
			try {
				DoSerializationRecovery(service, builder.ConnectionFactory.DatabaseDialect);
				Assert.AreEqual(1, policy.RetryCount);
				Assert.AreEqual(1, policy.ErrorCount);
			}
			catch (InvalidOperationException) {
				Assert.IsTrue(true);
			}
		}

		[Test]
		[Category("Database")]
		[Category("DB2")]
		public void TestLocalSerialization() {
			var builder = MakeServiceBuilder();
			var policy = new DB2RetryPolicyWithCount();
			builder.RetryPolicy = policy;
			builder.IsolationLevel = TxIsolation.Snapshot;
			builder.TransactionFactory = new LocalCommandTxFactory();

			var service = builder.Build();
			DoSerializationRecovery(service, builder.ConnectionFactory.DatabaseDialect);
			Assert.AreEqual(1, policy.RetryCount);
			Assert.AreEqual(1, policy.ErrorCount);
		}

		[Test]
		[Category("Database")]
		[Category("DB2")]
		public void TestBindNumeric() {
			DoBindNumericValues<DB2ParameterCollection>((col) => {
				var i = 0;
				Assert.AreEqual(DB2Type.SmallInt, col[i++].DB2Type);
				Assert.AreEqual(DB2Type.SmallInt, col[i++].DB2Type);
				Assert.AreEqual(DB2Type.SmallInt, col[i++].DB2Type);
				Assert.AreEqual(DB2Type.Integer, col[i++].DB2Type);
				Assert.AreEqual(DB2Type.Integer, col[i++].DB2Type);
				Assert.AreEqual(DB2Type.BigInt, col[i++].DB2Type);
				Assert.AreEqual(DB2Type.BigInt, col[i++].DB2Type);
				Assert.AreEqual(DB2Type.Decimal, col[i++].DB2Type);
				Assert.AreEqual(DB2Type.Real, col[i++].DB2Type);
				Assert.AreEqual(DB2Type.Double, col[i++].DB2Type);
				Assert.AreEqual(DB2Type.Decimal, col[i++].DB2Type);
				
				Assert.AreEqual(DB2Type.SmallInt, col[i++].DB2Type);
				Assert.AreEqual(DB2Type.SmallInt, col[i++].DB2Type);
				Assert.AreEqual(DB2Type.SmallInt, col[i++].DB2Type);
				Assert.AreEqual(DB2Type.Integer, col[i++].DB2Type);
				Assert.AreEqual(DB2Type.Integer, col[i++].DB2Type);
				Assert.AreEqual(DB2Type.BigInt, col[i++].DB2Type);
				Assert.AreEqual(DB2Type.BigInt, col[i++].DB2Type);
				Assert.AreEqual(DB2Type.Decimal, col[i++].DB2Type);
				Assert.AreEqual(DB2Type.Real, col[i++].DB2Type);
				Assert.AreEqual(DB2Type.Double, col[i++].DB2Type);
				Assert.AreEqual(DB2Type.Decimal, col[i++].DB2Type);
			});
		}

		[Test]
		[Category("Database")]
		[Category("DB2")]
		public void TestBindWrapper() {
			DoBindWrapper<DB2ParameterCollection>((col) => {
				var i = 0;
				Assert.AreEqual(DB2Type.Char, col[i++].DB2Type);
				Assert.AreEqual(DB2Type.Char, col[i++].DB2Type);
				Assert.AreEqual(DB2Type.VarChar, col[i++].DB2Type);
				Assert.AreEqual(DB2Type.VarChar, col[i++].DB2Type);
				Assert.AreEqual(DB2Type.Date, col[i++].DB2Type);
				Assert.AreEqual(DB2Type.Timestamp, col[i++].DB2Type);
				Assert.AreEqual(DB2Type.Decimal, col[i].DB2Type);
				Assert.AreEqual(10, col[i].Precision);
				Assert.AreEqual(3, col[i++].Scale);
			});
		}

		[Test]
		[Category("Database")]
		[Category("DB2")]
		public void TestBindString() {
			DoBindString<DB2ParameterCollection>((col) => {
				var i = 0;
				Assert.AreEqual(DB2Type.VarChar, col[i].DB2Type);
				Assert.AreEqual(10, col[i++].Size);
				Assert.AreEqual(DB2Type.Char, col[i++].DB2Type);
				Assert.AreEqual(DB2Type.Timestamp, col[i++].DB2Type);
			});
		}

		[Test]
		[Category("Database")]
		[Category("DB2")]
		public void TestBindUnknown() {
			try {
				DoBindUnknown<DB2ParameterCollection>((col) => {});
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (InvalidOperationException) {
				Assert.IsTrue(true);
			}
		}

		[Test]
		[Category("Database")]
		[Category("DB2")]
		public void TestSelectIn() {
			var builder = MakeServiceBuilder();
			builder.TransactionFactory = new LocalCommandTxFactory();
			DoSelectIn(builder.Build());
		}

		[Test]
		[Category("Database")]
		[Category("DB2")]
		public void TestSelectNotIn() {
			var builder = MakeServiceBuilder();
			builder.TransactionFactory = new LocalCommandTxFactory();
			DoSelectNotIn(builder.Build());
		}

		[Test]
		[Category("Database")]
		[Category("DB2")]
		public void TestDataReader() {
			var builder = MakeServiceBuilder();
			DoGetByType(builder.Build());
		}

		[Test]
		[Category("Database")]
		[Category("DB2")]
		public void TestDataReaderAll() {
			var builder = MakeServiceBuilder();
			DoGetByTypeAll(builder.Build());
		}

		[Test]
		[Category("Database")]
		[Category("DB2")]
		public void TestGetCurrentTime() {
			var offset = TimeZoneInfo.FindSystemTimeZoneById("Tokyo Standard Time")
					.GetUtcOffset(DateTime.UtcNow);
			var builder = MakeServiceBuilder();
			DoGetCurrentTime(builder.Build(), "SYSIBM.SYSDUMMY1", offset);
			DoGetCurrentUtcTime(builder.Build(), "SYSIBM.SYSDUMMY1", TimeSpan.Zero);
		}
	}
}
