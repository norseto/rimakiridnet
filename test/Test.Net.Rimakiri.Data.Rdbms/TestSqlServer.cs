// ==============================================================================
//     Test.Rimakiri.Data.Rdbms
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;
using Net.Rimakiri.Command;
using Net.Rimakiri.Command.Database;
using Net.Rimakiri.Command.Spi;
using Net.Rimakiri.Data.Interfaces;
using Net.Rimakiri.Data.SqlServer;
using Test.Net.Rimakiri.Data.Common;

namespace Test.Rimakiri.Data.Rdbms {
	[ExcludeFromCodeCoverage]
	internal class SqlRetryPolicyWithCount : SqlServerRetryPolicy {
		private int retryCount;
		private int errorCount;

		public int RetryCount {
			get { return retryCount; }
			set { retryCount = value; }
		}

		public int ErrorCount {
			get { return errorCount; }
			set { errorCount = value; }
		}

		public int LastError { get; private set; }

		#region Overrides of SqlServerRetryPolicy
		public override bool IsRetryable(Exception error, int tryCount) {
			errorCount++;
			if (base.IsRetryable(error, tryCount)) {
				var cause = error.FindCause<SqlException>();
				LastError = cause.Number;
				retryCount++;
				return true;
			}
			return false;
		}
		#endregion
	}

	/// <summary>
	/// SQL Server用のテスト
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestSqlServer : RdbmsTestBase {
		private IDatabaseDialect Dialect { get; set; }
		private bool setAbort = false;

		#region Overrides of RdbmsTestBase
		protected override IConnectionFactory GetConnectionFactory() {
			setAbort = !setAbort;
			return new SqlServerConnectionFactory {
				ConnectionStringName = "SQLServer",
				DatabaseDialect = Dialect,
				XactAbort = setAbort,
			};
		}
		#endregion

		[Test]
		public void TestConnectNullNamedFactory() {
			var factory = new SqlServerConnectionFactory {
				ConnectionString =
						ConfigurationManager.ConnectionStrings["SQLServer"].ConnectionString,
				DatabaseDialect = Dialect,
			};
			var service = new DacCommandServiceBuilder<DacCommandContext> {
				ConnectionFactory = factory,
			}.Build();
			var cmd = new DelegateDacCommand(ctx => {
				using (var dbcmd = ctx.CreateDbCommand()) {
					using (var child = ctx.CreateDbCommand()) {
						Assert.AreSame(dbcmd.Connection, child.Connection);
					}
				}
			});
			service.ExecuteCommand(cmd);
		}

		[Test]
		[Category("Database")]
		public void TestConnect() {
			DoTestConnect();
		}

		[Test]
		[Category("Database")]
		public void TestSimple() {
			var service = MakeServiceBuilder().Build();
			DoInitialize(service);
			DoTestSimple(service);
		}

		[Test]
		[Category("Database")]
		public void TestSelectLike() {
			var builder = MakeServiceBuilder();
			var service = builder.Build();
			DoSelectLike(service);
		}

		[Test]
		[Category("Database")]
		public void TestRollback() {
			var service = MakeServiceBuilder().Build();
			DoInitialize(service);
			Assert.AreEqual(3, DoTestSimple(service));
			DoClean(service);
			Assert.AreEqual(0, DoTestSimple(service));
			DoInitialize(service);
			Assert.AreEqual(3, DoTestSimple(service));

			var builder = MakeServiceBuilder();
			builder.NoCommit = true;
			service = builder.Build();
			DoClean(service);
			Assert.AreEqual(3, DoTestSimple(service));
		}

		[Test]
		[Category("Database")]
		public void TestSimpleLocal() {
			var builder = MakeServiceBuilder();
			builder.TransactionFactory = new LocalCommandTxFactory();
			var service = builder.Build();
			DoInitialize(service);
			DoTestSimple(service);
		}

		[Test]
		[Category("Database")]
		public void TestRollbackLocal() {
			var builder = MakeServiceBuilder();
			builder.TransactionFactory = new LocalCommandTxFactory();
			var service = builder.Build();
			DoInitialize(service);
			Assert.AreEqual(3, DoTestSimple(service));
			DoClean(service);
			Assert.AreEqual(0, DoTestSimple(service));
			DoInitialize(service);
			Assert.AreEqual(3, DoTestSimple(service));

			builder = MakeServiceBuilder();
			builder.NoCommit = true;
			builder.TransactionFactory = new LocalCommandTxFactory();
			service = builder.Build();
			DoClean(service);
			Assert.AreEqual(3, DoTestSimple(service));
		}

		[Test]
		[Category("Database")]
		public void TestDeadLock() {
			Dialect = new SqlServerVarCharDialect();
			var builder = MakeServiceBuilder();
			var policy = new SqlRetryPolicyWithCount();
			builder.RetryPolicy = policy;

			var service = builder.Build();
			DoDeadLockRecovery(service, builder.ConnectionFactory.DatabaseDialect);
			Assert.AreEqual(1, policy.RetryCount);
			Assert.AreEqual(1, policy.ErrorCount);
			Assert.AreEqual(1205, policy.LastError);
			Dialect = null;
		}

		[Test]
		[Category("Database")]
		public void TestUnretryableError() {
			var builder = MakeServiceBuilder();
			var policy = new SqlRetryPolicyWithCount();
			builder.RetryPolicy = policy;

			var service = builder.Build();
			try {
				DoThrowError(service);
				Assert.IsTrue(false, "Should thrown an exception");
			}
			catch (NullReferenceException) {
				Assert.IsTrue(true);
			}
			Assert.AreEqual(0, policy.RetryCount);
			Assert.AreEqual(1, policy.ErrorCount);
		}

		[Test]
		[Category("Database")]
		public void TestSerialization() {
			var builder = MakeServiceBuilder();
			var policy = new SqlRetryPolicyWithCount();
			builder.Timeout = TimeSpan.FromSeconds(15);
			builder.RetryPolicy = policy;
			builder.IsolationLevel = TxIsolation.RepeatableRead;

			var service = builder.Build();
			DoSerializationRecovery(service, builder.ConnectionFactory.DatabaseDialect);
			Assert.AreEqual(1, policy.RetryCount);
			Assert.AreEqual(1, policy.ErrorCount);
			Assert.AreEqual(1205, policy.LastError);
		}

		[Test]
		[Category("Database")]
		public void TestSerializationSnapshot() {
			var builder = MakeServiceBuilder();
			var policy = new SqlRetryPolicyWithCount();
			builder.Timeout = TimeSpan.FromSeconds(15);
			builder.RetryPolicy = policy;
			builder.IsolationLevel = TxIsolation.Snapshot;

			var service = builder.Build();
			DoSerializationRecovery(service, builder.ConnectionFactory.DatabaseDialect);
			Assert.AreEqual(1, policy.RetryCount);
			Assert.AreEqual(1, policy.ErrorCount);
			Assert.AreEqual(3960, policy.LastError);
		}

		[Test]
		[Category("Database")]
		public void TestLocalSerialization() {
			var builder = MakeServiceBuilder();
			var policy = new SqlRetryPolicyWithCount();
			builder.RetryPolicy = policy;
			builder.IsolationLevel = TxIsolation.Snapshot;
			builder.TransactionFactory = new LocalCommandTxFactory();

			var service = builder.Build();
			DoSerializationRecovery(service, builder.ConnectionFactory.DatabaseDialect);
			Assert.AreEqual(1, policy.RetryCount);
			Assert.AreEqual(1, policy.ErrorCount);
			Assert.AreEqual(3960, policy.LastError);
		}

		[Test]
		[Category("Database")]
		public void TestBindNumeric() {
			DoBindNumericValues<SqlParameterCollection>((col) => {
				var i = 0;
				Assert.AreEqual(SqlDbType.SmallInt, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.TinyInt, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.SmallInt, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.Int, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.Int, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.BigInt, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.BigInt, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.Decimal, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.Float, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.Real, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.Decimal, col[i++].SqlDbType);
				
				Assert.AreEqual(SqlDbType.SmallInt, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.TinyInt, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.SmallInt, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.Int, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.Int, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.BigInt, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.BigInt, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.Decimal, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.Float, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.Real, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.Decimal, col[i++].SqlDbType);
			});
		}

		[Test]
		[Category("Database")]
		public void TestBindWrapper() {
			DoBindWrapper<SqlParameterCollection>((col) => {
				var i = 0;
				Assert.AreEqual(SqlDbType.Char, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.NChar, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.VarChar, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.NVarChar, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.Date, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.DateTime, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.Decimal, col[i].SqlDbType);
				Assert.AreEqual(10, col[i].Precision);
				Assert.AreEqual(3, col[i++].Scale);
			});
		}

		[Test]
		[Category("Database")]
		public void TestBindString() {
			Dialect = new SqlServerDialect();
			DoBindString<SqlParameterCollection>((col) => {
				var i = 0;
				Assert.AreEqual(SqlDbType.NVarChar, col[i].SqlDbType);
				Assert.AreEqual(10, col[i++].Size);
				Assert.AreEqual(SqlDbType.NChar, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.DateTime, col[i++].SqlDbType);
			});
			
			Dialect = new SqlServerVarCharDialect();
			DoBindString<SqlParameterCollection>((col) => {
				var i = 0;
				Assert.AreEqual(SqlDbType.VarChar, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.Char, col[i++].SqlDbType);
				Assert.AreEqual(SqlDbType.DateTime, col[i++].SqlDbType);
			});
		}

		[Test]
		[Category("Database")]
		public void TestBindUnknown() {
			try {
				DoBindUnknown<SqlParameterCollection>((col) => {});
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (InvalidOperationException) {
				Assert.IsTrue(true);
			}
		}

		[Test]
		[Category("Database")]
		public void TestSelectIn() {
			var builder = MakeServiceBuilder();
			builder.TransactionFactory = new LocalCommandTxFactory();
			DoSelectIn(builder.Build());
		}

		[Test]
		[Category("Database")]
		public void TestSelectNotIn() {
			var builder = MakeServiceBuilder();
			builder.TransactionFactory = new LocalCommandTxFactory();
			DoSelectNotIn(builder.Build());
		}

		[Test]
		[Category("Database")]
		public void TestDataReader() {
			var builder = MakeServiceBuilder();
			DoGetByType(builder.Build());
		}

		[Test]
		[Category("Database")]
		public void TestDataReaderAll() {
			var builder = MakeServiceBuilder();
			DoGetByTypeAll(builder.Build());
		}

		[Test]
		[Category("Database")]
		public void TestGetCurrentTime() {
			var offset = TimeZoneInfo.FindSystemTimeZoneById("Tokyo Standard Time")
					.GetUtcOffset(DateTime.UtcNow);
			var builder = MakeServiceBuilder();
			DoGetCurrentTime(builder.Build(), "", offset);
			DoGetCurrentUtcTime(builder.Build(), "", TimeSpan.Zero);
		}
	}
}
