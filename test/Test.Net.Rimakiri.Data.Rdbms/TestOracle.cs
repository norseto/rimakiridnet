// ==============================================================================
//     Test.Rimakiri.Data.Rdbms
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

//#if (NET40 || NET45 || NET451 || NET452 || NET46 || NET461 || NET462 || NET47 || NET471 || NET472)
//#define NETFRAMEWORK
//#endif
//
//#if (NETFRAMEWORK)
using System;
using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;
using Net.Rimakiri.Command;
using Net.Rimakiri.Command.Spi;
using Net.Rimakiri.Data.Interfaces;
using Net.Rimakiri.Data.Oracle;
using Oracle.ManagedDataAccess.Client;
using Test.Net.Rimakiri.Data.Common;

namespace Test.Rimakiri.Data.Rdbms {
	[ExcludeFromCodeCoverage]
	internal class OracleRetryPolicyWithCount : OracleRetryPolicy {
		private int retryCount;
		private int errorCount;

		public int RetryCount {
			get { return retryCount; }
			set { retryCount = value; }
		}

		public int ErrorCount {
			get { return errorCount; }
			set { errorCount = value; }
		}

		public int LastError { get; private set; }

		#region Overrides of SqlServerRetryPolicy
		public override bool IsRetryable(Exception error, int tryCount) {
			errorCount++;
			if (base.IsRetryable(error, tryCount)) {
				var cause = error.FindCause<OracleException>();
				LastError = cause.Number;
				retryCount++;
				return true;
			}
			return false;
		}
		#endregion
	}

	/// <summary>
	/// SQL Server用のテスト
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestOracle : RdbmsTestBase {
		#region Overrides of RdbmsTestBase
		protected override IConnectionFactory GetConnectionFactory() {
			return new OracleConnectionFactory { ConnectionStringName = "Oracle" };
		}
		#endregion

		[Test]
		[Category("Database")]
		[Category("Oracle")]
		public void TestConnect() {
			DoTestConnect();
		}

		[Test]
		[Category("Database")]
		[Category("Oracle")]
		public void TestSimple() {
			var service = MakeServiceBuilder().Build();
			DoInitialize(service);
			DoTestSimple(service);
		}

		[Test]
		[Category("Database")]
		[Category("Oracle")]
		public void TestSelectLike() {
			var builder = MakeServiceBuilder();
			var service = builder.Build();
			DoSelectLike(service);
		}

		[Test]
		[Category("Database")]
		[Category("Oracle")]
		public void TestRollback() {
			var service = MakeServiceBuilder().Build();
			DoInitialize(service);
			Assert.AreEqual(3, DoTestSimple(service));
			DoClean(service);
			Assert.AreEqual(0, DoTestSimple(service));
			DoInitialize(service);
			Assert.AreEqual(3, DoTestSimple(service));

			var builder = MakeServiceBuilder();
			builder.NoCommit = true;
			service = builder.Build();
			DoClean(service);
			Assert.AreEqual(3, DoTestSimple(service));
		}

		[Test]
		[Category("Database")]
		[Category("Oracle")]
		public void TestSimpleLocal() {
			var builder = MakeServiceBuilder();
			builder.TransactionFactory = new LocalCommandTxFactory();
			var service = builder.Build();
			DoInitialize(service);
			DoTestSimple(service);
		}

		[Test]
		[Category("Database")]
		[Category("Oracle")]
		public void TestRollbackLocal() {
			var builder = MakeServiceBuilder();
			builder.TransactionFactory = new LocalCommandTxFactory();
			var service = builder.Build();
			DoInitialize(service);
			Assert.AreEqual(3, DoTestSimple(service));
			DoClean(service);
			Assert.AreEqual(0, DoTestSimple(service));
			DoInitialize(service);
			Assert.AreEqual(3, DoTestSimple(service));

			builder = MakeServiceBuilder();
			builder.NoCommit = true;
			builder.TransactionFactory = new LocalCommandTxFactory();
			service = builder.Build();
			DoClean(service);
			Assert.AreEqual(3, DoTestSimple(service));
		}

		[Test]
		[Category("Database")]
		[Category("Oracle")]
		public void TestDeadLock() {
			var builder = MakeServiceBuilder();
			var policy = new OracleRetryPolicyWithCount();
			builder.RetryPolicy = policy;

			var service = builder.Build();
			DoDeadLockRecovery(service, builder.ConnectionFactory.DatabaseDialect);
			Assert.AreEqual(1, policy.RetryCount);
			Assert.AreEqual(1, policy.ErrorCount);
			Assert.AreEqual(60, policy.LastError);
		}

		[Test]
		[Category("Database")]
		[Category("Oracle")]
		public void TestUnretryableError() {
			var builder = MakeServiceBuilder();
			var policy = new OracleRetryPolicyWithCount();
			builder.RetryPolicy = policy;

			var service = builder.Build();
			try {
				DoThrowError(service);
				Assert.IsTrue(false, "Should thrown an exception");
			}
			catch (NullReferenceException) {
				Assert.IsTrue(true);
			}
			Assert.AreEqual(0, policy.RetryCount);
			Assert.AreEqual(1, policy.ErrorCount);
		}

		[Test]
		[Category("Database")]
		[Category("Oracle")]
		public void TestSerialization() {
			var builder = MakeServiceBuilder();
			var policy = new OracleRetryPolicyWithCount();
			builder.RetryPolicy = policy;
			builder.IsolationLevel = TxIsolation.Serializable;

			var service = builder.Build();
			DoSerializationRecovery(service, builder.ConnectionFactory.DatabaseDialect);
			Assert.AreEqual(1, policy.RetryCount);
			Assert.AreEqual(1, policy.ErrorCount);
			Assert.AreEqual(8177, policy.LastError);
		}

		[Test]
		[Category("Database")]
		[Category("Oracle")]
		public void TestBadSerialization() {
			var builder = MakeServiceBuilder();
			((OracleConnectionFactory)builder.ConnectionFactory).SetSerialize = false;
			var policy = new OracleRetryPolicyWithCount();
			builder.RetryPolicy = policy;
			builder.IsolationLevel = TxIsolation.Serializable;

			var service = builder.Build();
			DoSerializationRecovery(service, builder.ConnectionFactory.DatabaseDialect);

			Assert.AreEqual(0, policy.RetryCount);
			Assert.AreEqual(0, policy.ErrorCount);
		}

		[Test]
		[Category("Database")]
		[Category("Oracle")]
		public void TestLocalSerialization() {
			var builder = MakeServiceBuilder();
			var policy = new OracleRetryPolicyWithCount();
			builder.RetryPolicy = policy;
			builder.IsolationLevel = TxIsolation.RepeatableRead;
			builder.TransactionFactory = new LocalCommandTxFactory();

			var service = builder.Build();
			DoSerializationRecovery(service, builder.ConnectionFactory.DatabaseDialect);
			Assert.AreEqual(1, policy.RetryCount);
			Assert.AreEqual(1, policy.ErrorCount);
			Assert.AreEqual(8177, policy.LastError);
		}

		[Test]
		[Category("Database")]
		[Category("Oracle")]
		public void TestBindNumeric() {
			DoBindNumericValuesOra<OracleParameterCollection>((col) => {
				var i = 0;
				Assert.AreEqual(OracleDbType.Byte, col[i++].OracleDbType);
				Assert.AreEqual(OracleDbType.Int16, col[i++].OracleDbType);
				Assert.AreEqual(OracleDbType.Int32, col[i++].OracleDbType);
				Assert.AreEqual(OracleDbType.Int64, col[i++].OracleDbType);
				Assert.AreEqual(OracleDbType.Single, col[i++].OracleDbType);
				Assert.AreEqual(OracleDbType.Double, col[i++].OracleDbType);
				Assert.AreEqual(OracleDbType.Decimal, col[i++].OracleDbType);
			});
		}

		[Test]
		[Category("Database")]
		[Category("Oracle")]
		public void TestBindWrapper() {
			DoBindWrapper<OracleParameterCollection>((col) => {
				var i = 0;
				Assert.AreEqual(OracleDbType.Char, col[i++].OracleDbType);
				Assert.AreEqual(OracleDbType.NChar, col[i++].OracleDbType);
				Assert.AreEqual(OracleDbType.Varchar2, col[i++].OracleDbType);
				Assert.AreEqual(OracleDbType.NVarchar2, col[i++].OracleDbType);
				Assert.AreEqual(OracleDbType.Date, col[i++].OracleDbType);
				Assert.AreEqual(OracleDbType.TimeStamp, col[i++].OracleDbType);
				Assert.AreEqual(OracleDbType.Decimal, col[i].OracleDbType);
				Assert.AreEqual(10, col[i].Precision);
				Assert.AreEqual(3, col[i++].Scale);
			});
		}

		[Test]
		[Category("Database")]
		[Category("Oracle")]
		public void TestBindString() {
			DoBindString<OracleParameterCollection>((col) => {
				var i = 0;
				Assert.AreEqual(OracleDbType.NVarchar2, col[i].OracleDbType);
				Assert.AreEqual(10, col[i++].Size);
				Assert.AreEqual(OracleDbType.NChar, col[i++].OracleDbType);
				Assert.AreEqual(OracleDbType.TimeStamp, col[i++].OracleDbType);
			});
		}

		[Test]
		[Category("Database")]
		[Category("Oracle")]
		public void TestBindUnknown() {
			try {
				DoBindUnknown<OracleParameterCollection>((col) => {});
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (InvalidOperationException) {
				Assert.IsTrue(true);
			}
		}

		[Test]
		[Category("Database")]
		[Category("Oracle")]
		public void TestSelectIn() {
			var builder = MakeServiceBuilder();
			builder.TransactionFactory = new LocalCommandTxFactory();
			DoSelectIn(builder.Build());
		}

		[Test]
		[Category("Database")]
		[Category("Oracle")]
		public void TestSelectNotIn() {
			var builder = MakeServiceBuilder();
			builder.TransactionFactory = new LocalCommandTxFactory();
			DoSelectNotIn(builder.Build());
		}

		[Test]
		[Category("Database")]
		[Category("Oracle")]
		public void TestDataReader() {
			var builder = MakeServiceBuilder();
			DoGetByType(builder.Build());
		}

		[Test]
		[Category("Database")]
		[Category("Oracle")]
		public void TestDataReaderAll() {
			var builder = MakeServiceBuilder();
			DoGetByTypeAll(builder.Build());
		}

		[Test]
		[Category("Database")]
		[Category("Oracle")]
		public void TestGetCurrentTime() {
			var offset = TimeZoneInfo.FindSystemTimeZoneById("Tokyo Standard Time")
					.GetUtcOffset(DateTime.UtcNow);
			var builder = MakeServiceBuilder();
			DoGetCurrentTime(builder.Build(), "DUAL", offset);
			DoGetCurrentUtcTime(builder.Build(), "DUAL", TimeSpan.Zero);
		}
	}
}
//#endif
