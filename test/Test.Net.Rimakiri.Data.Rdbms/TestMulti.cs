// ==============================================================================
//     Test.Rimakiri.Data.Rdbms
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

#if (NET40 || NET45 || NET451 || NET452 || NET46 || NET461 || NET462 || NET47 || NET471 || NET472)
#define NETFRAMEWORK
#endif

using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;
using Net.Rimakiri.Command;
using Net.Rimakiri.Command.Database;
using Net.Rimakiri.Data;
using Net.Rimakiri.Data.DB2;
using Net.Rimakiri.Data.Interfaces;
#if (NETFRAMEWORK)
using System;
using Net.Rimakiri.Data.Oracle;
using Net.Rimakiri.Command.Transactions;
#endif
using Net.Rimakiri.Data.SqlServer;
using Test.Net.Rimakiri.Data.Common;

namespace Test.Rimakiri.Data.Rdbms {
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestMulti {
		private IRetryPolicy GetPolicy() {
			var policy = new CompositeRetryPolicy();
			policy.RetryPolicies = new IRetryPolicy[] {
				new SqlRetryPolicyWithCount(),
				new DB2RetryPolicyWithCount(),
#if (NETFRAMEWORK)
				new OracleRetryPolicyWithCount()
#endif
			};
			return policy;
		}

		private IConnectionFactory[] GetConnectionFactories() {
			return new IConnectionFactory[] {
				new SqlServerConnectionFactory {
					Name = "MSSQL",
					ConnectionStringName = "SQLServer"
				},
#if (NETFRAMEWORK)
				new OracleConnectionFactory {
					Name = "Oracle",
					ConnectionStringName = "Oracle"
				},
#endif
				new DB2ConnectionFactory {
					Name = "DB2",
					ConnectionStringName = "DB2"
				},
			};
		}

#if (NETFRAMEWORK)
		[Test]
		[Category("Database")]
		[Category("Oracle")]
		public void TestSimpleLocalTxOra() {
			var builder = new ValueBinderBuilder("SELECT * FROM T_TX_TEST WHERE ID = {0}",
				BindValue.Create("id", 1));
			var cmd = new ICommand<DacCommandContext>[] {
				new BinderQueryDacCommand(builder) {
					ConnectionFactoryName = "Oracle"
				},
				new DelegateDacCommand(ctx => {
					Assert.IsNotNull(ctx.GetConnectionFactory());
				}),
			};
			var chain = new SimpleCommandChain(cmd) {
				TxScope = TxScope.Required,
				Idempotent = Idempotent.Transactional,
			};

			var serviceBuilder = new DacCommandServiceBuilder<DacCommandContext> {
				ConnectionFactories = GetConnectionFactories(),
				RetryPolicy = GetPolicy(),
			};
			Assert.IsNull(serviceBuilder.ConnectionFactory);
			Assert.IsNotNull(serviceBuilder.ConnectionFactories);
			Assert.AreEqual(3, serviceBuilder.ConnectionFactories.Length);
			serviceBuilder.Build().ExecuteCommand(chain);
		}
#endif

		[Test]
		[Category("Database")]
		public void TestSimpleLocalTxMsSql() {
			var builder = new ValueBinderBuilder("SELECT * FROM T_TX_TEST WHERE ID = {0}",
				BindValue.Create("id", 1));
			var cmd = new[] {
				new BinderQueryDacCommand(builder) {
					ConnectionFactoryName = "MSSQL"
				},
			};
			var chain = new SimpleCommandChain(cmd) {
				TxScope = TxScope.Required,
				Idempotent = Idempotent.Transactional,
			};

			var serviceBuilder = new DacCommandServiceBuilder<DacCommandContext> {
				ConnectionFactories = GetConnectionFactories(),
				RetryPolicy = GetPolicy(),
			};
			serviceBuilder.Build().ExecuteCommand(chain);
		}

		[Test]
		[Category("Database")]
		[Category("DB2")]
		public void TestSimpleLocalTxDB2() {
			var builder = new ValueBinderBuilder("SELECT * FROM T_TX_TEST WHERE ID = {0}",
				BindValue.Create("id", 1));
			var cmd = new[] {
				new BinderQueryDacCommand(builder) {
					ConnectionFactoryName = "DB2"
				},
			};
			var chain = new SimpleCommandChain(cmd) {
				TxScope = TxScope.Required,
				Idempotent = Idempotent.Transactional,
			};

			var serviceBuilder = new DacCommandServiceBuilder<DacCommandContext> {
				ConnectionFactories = GetConnectionFactories(),
				RetryPolicy = GetPolicy(),
			};
			serviceBuilder.Build().ExecuteCommand(chain);
		}

#if (NETFRAMEWORK)
		[Test]
		[Category("Database")]
		[Category("Oracle")]
		[Category("DB2")]
		public void TestMultipleWithTransactionLocalTx() {
			var builder = new ValueBinderBuilder("SELECT * FROM T_TX_TEST WHERE ID = {0}",
							BindValue.Create("id", 1));
			var cmd = new[] {
				new BinderQueryDacCommand(builder) {
					ConnectionFactoryName = "Oracle"
				},
				new BinderQueryDacCommand(builder) {
					ConnectionFactoryName = "DB2"
				},
				new BinderQueryDacCommand(builder) {
					ConnectionFactoryName = "MSSQL"
				}
			};
			var chain = new SimpleCommandChain(cmd) {
				TxScope = TxScope.Required,
				Idempotent = Idempotent.Transactional,
			};

			try {
				var serviceBuilder = new DacCommandServiceBuilder<DacCommandContext> {
					ConnectionFactories = GetConnectionFactories(),
					RetryPolicy = GetPolicy(),
				};
				serviceBuilder.Build().ExecuteCommand(chain);
				Assert.IsTrue(false, "Should throw an exception");
			}
			catch (InvalidOperationException) {
				Assert.IsTrue(true);
			}
		}

		[Test]
		[Category("Database")]
		[Category("Oracle")]
		[Category("DB2")]
		public void TestMultipleWithTransactionTxScopeOraAndDB2() {
			var builder = new ValueBinderBuilder("SELECT * FROM T_TX_TEST WHERE ID = {0}",
				BindValue.Create("id", 1));
			var cmd = new[] {
				new BinderQueryDacCommand(builder) {
					ConnectionFactoryName = "Oracle"
				},
				new BinderQueryDacCommand(builder) {
					ConnectionFactoryName = "DB2"
				},
			};
			var chain = new SimpleCommandChain(cmd) {
				TxScope = TxScope.Required,
				Idempotent = Idempotent.Transactional,
			};

			var serviceBuilder = new DacCommandServiceBuilder<DacCommandContext> {
				ConnectionFactories = GetConnectionFactories(),
				RetryPolicy = GetPolicy(),
				TransactionFactory = new ScopeCommandTxFactory(),
			};
			serviceBuilder.Build().ExecuteCommand(chain);
		}

		[Test]
		[Category("MSDTC")]
		[Category("Database")]
		[Category("Oracle")]
		[Category("DB2")]
		// This test will faile with MS SQLServer Linux. Needs Windows MSDTC
		public void TestMultipleWithTransactionTxScopeFull() {
			var builder = new ValueBinderBuilder("SELECT * FROM T_TX_TEST WHERE ID = {0}",
				BindValue.Create("id", 1));
			var cmd = new ICommand<DacCommandContext>[] {
				new BinderQueryDacCommand(builder) {
					ConnectionFactoryName = "MSSQL"
				},
				new BinderQueryDacCommand(builder) {
					ConnectionFactoryName = "Oracle"
				},
				new BinderQueryDacCommand(builder) {
					ConnectionFactoryName = "DB2"
				},
			};
			var chain = new SimpleCommandChain(cmd) {
				TxScope = TxScope.Required,
				Idempotent = Idempotent.Transactional,
			};

			var serviceBuilder = new DacCommandServiceBuilder<DacCommandContext> {
				ConnectionFactories = GetConnectionFactories(),
				RetryPolicy = GetPolicy(),
				TransactionFactory = new ScopeCommandTxFactory(),
			};
			serviceBuilder.Build().ExecuteCommand(chain);
		}
#endif
	}
}
