// ==============================================================================
//     Test.Rimakiri.Data.Rdbms
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;
using Net.Rimakiri.Command.Spi;
using Net.Rimakiri.Data;
using Net.Rimakiri.Data.Interfaces;
using Test.Net.Rimakiri.Data.Common;

namespace Test.Rimakiri.Data.x86 {
	/// <summary>
	/// SQL Server用のテスト
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestAccess : RdbmsTestBase {
		#region Overrides of RdbmsTestBase
		protected override IConnectionFactory GetConnectionFactory() {
			return new OleDbConnectionFactory { ConnectionStringName = "Access" };
		}
		#endregion

		[Test]
		[Category("Database")]
		public void TestConnect() {
			DoTestConnect();
		}

//		[Test]
//		[Category("Database")]
//		public void TestSimple() {
//			var service = MakeServiceBuilder().Build();
//			DoInitialize(service);
//			DoTestSimple(service);
//		}
//
//		[Test]
//		[Category("Database")]
//		public void TestRollback() {
//			var service = MakeServiceBuilder().Build();
//			DoInitialize(service);
//			Assert.AreEqual(3, DoTestSimple(service));
//			DoClean(service);
//			Assert.AreEqual(0, DoTestSimple(service));
//			DoInitialize(service);
//			Assert.AreEqual(3, DoTestSimple(service));
//
//			var builder = MakeServiceBuilder();
//			builder.NoCommit = true;
//			service = builder.Build();
//			DoClean(service);
//			Assert.AreEqual(3, DoTestSimple(service));
//		}

		[Test]
		public void TestSimpleLocal() {
			var builder = MakeServiceBuilder();
			builder.TransactionFactory = new LocalCommandTxFactory();
			var service = builder.Build();
			DoInitialize(service);
			DoTestSimple(service);
		}

		[Test]
		public void TestSelectLike() {
			var builder = MakeServiceBuilder();
			builder.TransactionFactory = new LocalCommandTxFactory();
			var service = builder.Build();
			DoSelectLike(service);
		}

		[Test]
		public void TestRollbackLocal() {
			var builder = MakeServiceBuilder();
			builder.TransactionFactory = new LocalCommandTxFactory();
			var service = builder.Build();
			DoInitialize(service);
			Assert.AreEqual(3, DoTestSimple(service));
			DoClean(service);
			Assert.AreEqual(0, DoTestSimple(service));
			DoInitialize(service);
			Assert.AreEqual(3, DoTestSimple(service));

			builder = MakeServiceBuilder();
			builder.NoCommit = true;
			builder.TransactionFactory = new LocalCommandTxFactory();
			service = builder.Build();
			DoClean(service);
			Assert.AreEqual(3, DoTestSimple(service));
		}

		[Test]
		public void TestBindNumeric() {
			DoBindNumericValues<OleDbParameterCollection>((col) => {
				var i = 0;
				Assert.AreEqual(DbType.SByte, col[i++].DbType);
				Assert.AreEqual(DbType.Byte, col[i++].DbType);
				Assert.AreEqual(DbType.Int16, col[i++].DbType);
				Assert.AreEqual(DbType.UInt16, col[i++].DbType);
				Assert.AreEqual(DbType.Int32, col[i++].DbType);
				Assert.AreEqual(DbType.UInt32, col[i++].DbType);
				Assert.AreEqual(DbType.Int64, col[i++].DbType);
				Assert.AreEqual(DbType.UInt64, col[i++].DbType);
				Assert.AreEqual(DbType.Single, col[i++].DbType);
				Assert.AreEqual(DbType.Double, col[i++].DbType);
				Assert.AreEqual(DbType.Decimal, col[i++].DbType);
				
				Assert.AreEqual(DbType.SByte, col[i++].DbType);
				Assert.AreEqual(DbType.Byte, col[i++].DbType);
				Assert.AreEqual(DbType.Int16, col[i++].DbType);
				Assert.AreEqual(DbType.UInt16, col[i++].DbType);
				Assert.AreEqual(DbType.Int32, col[i++].DbType);
				Assert.AreEqual(DbType.UInt32, col[i++].DbType);
				Assert.AreEqual(DbType.Int64, col[i++].DbType);
				Assert.AreEqual(DbType.UInt64, col[i++].DbType);
				Assert.AreEqual(DbType.Single, col[i++].DbType);
				Assert.AreEqual(DbType.Double, col[i++].DbType);
				Assert.AreEqual(DbType.Decimal, col[i++].DbType);
			});
		}

		[Test]
		public void TestBindWrapper() {
			DoBindWrapper<OleDbParameterCollection>((col) => {
				var i = 0;
				Assert.AreEqual(DbType.String, col[i++].DbType);
				Assert.AreEqual(DbType.String, col[i++].DbType);
				Assert.AreEqual(DbType.String, col[i++].DbType);
				Assert.AreEqual(DbType.String, col[i++].DbType);
				Assert.AreEqual(DbType.Date, col[i++].DbType);
				Assert.AreEqual(DbType.DateTime, col[i++].DbType);
				Assert.AreEqual(DbType.Decimal, col[i].DbType);
				Assert.AreEqual(10, col[i].Precision);
				Assert.AreEqual(3, col[i++].Scale);
			});
		}

		[Test]
		public void TestBindString() {
			DoBindString<OleDbParameterCollection>((col) => {
				var i = 0;
				Assert.AreEqual(DbType.String, col[i].DbType);
				Assert.AreEqual(10, col[i++].Size);
				Assert.AreEqual(DbType.String, col[i++].DbType);
				Assert.AreEqual(DbType.DateTime, col[i++].DbType);
			});
		}

		[Test]
		public void TestBindUnknown() {
			try {
				DoBindUnknown<OleDbParameterCollection>((col) => {});
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (InvalidOperationException) {
				Assert.IsTrue(true);
			}
		}

		[Test]
		public void TestSelectIn() {
			var builder = MakeServiceBuilder();
			builder.TransactionFactory = new LocalCommandTxFactory();
			DoSelectIn(builder.Build());
		}

		[Test]
		public void TestSelectNotIn() {
			var builder = MakeServiceBuilder();
			builder.TransactionFactory = new LocalCommandTxFactory();
			DoSelectNotIn(builder.Build());
		}

		[Test]
		public void TestGetCurrentTime() {
			var builder = MakeServiceBuilder();
			builder.TransactionFactory = new LocalCommandTxFactory();
			DoGetCurrentTime(builder.Build(), "", TimeZoneInfo.Local.BaseUtcOffset);
			try {
				DoGetCurrentUtcTime(builder.Build(), "", TimeSpan.Zero);
			}
			catch (InvalidOperationException) {
				Assert.IsTrue(true);
			}
		}
	}
}
