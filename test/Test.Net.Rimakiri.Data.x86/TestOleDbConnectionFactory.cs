// ==============================================================================
//     Test.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Data;
using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;
using Net.Rimakiri.Data;
using Net.Rimakiri.Data.Interfaces;

namespace Test.Rimakiri.Data.x86 {
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class TestOleDbConnectionFactory {
		private const string ConStr =
				"Provider=Microsoft.JET.OLEDB.4.0;Data Source=DataFile\\Sample.mdb;" +
				"Jet OLEDB:Database Password=xxxx;";

		public static IConnectionFactory MakeOleDFactory(IDatabaseDialect dialect = null) {
			var factory = new OleDbConnectionFactory {
				ConnectionString = ConStr,
				Name = "OleConnection",
				DatabaseDialect = dialect,
			};
			return factory;
		}

		[Test]
		public void TestConnect() {
			var factory = MakeOleDFactory();

			Assert.IsNotNull(factory.DatabaseDialect);
			Assert.AreEqual("OleConnection", factory.Name);
			using (var con = factory.NewConnection(false, IsolationLevel.ReadCommitted)) {
				Assert.IsNotNull(con);
				con.Open();
			}
		}
	}
}
