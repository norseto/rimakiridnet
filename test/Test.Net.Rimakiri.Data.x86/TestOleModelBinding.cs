// ==============================================================================
//     Test.Rimakiri.Data.x86
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;
using Net.Rimakiri.Data;
using Net.Rimakiri.Data.Interfaces;
using Net.Rimakiri.Data.TypeWrappers;
using Net.Rimakiri.Data.Util;

namespace Test.Rimakiri.Data.x86 {
	[ExcludeFromCodeCoverage]
	public class OleModelBindingModel {
		[EntityBinding(Size = 10)]
		public string Name { get; set; }

		[EntityBinding(Size = 20)]
		public NCharType NCharName { get; set; }

		[EntityBinding(Precision = 12, Scale = 3)]
		public decimal PayAmount { get; set; }

		public string NoLengthName { get; set; }
	}

	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestOleModelBinding {
		[Test]
		public void SimpleTest() {
			var binding = new ModelBindingFactory<OleModelBindingModel> { Dynamic = true}.Create(
								new OleModelBindingModel {
									Name = "Hoge",
								});
			var confactory = TestOleDbConnectionFactory.MakeOleDFactory();
			using (var con = confactory.NewConnection(false, IsolationLevel.ReadCommitted)) {
				var format = "SELECT * FROM T_MDB_TEST WHERE ID = {0}";
				var target = new ValueBinderBuilder(format, binding.Create("id", m => m.Name));
				var binder = target.Build(confactory.DatabaseDialect);
				using (var cmd = con.CreateCommand()) {
					binder.Bind(cmd);
					Assert.AreEqual(10, cmd.Parameters[0].Size);
				}
			}
		}

		[Test]
		public void SimpleOverTest() {
			var binding = new ModelBindingFactory<OleModelBindingModel> { Dynamic = true}.Create(
								new OleModelBindingModel {
									Name = "HogeHogeHoge",
								});
			var confactory = TestOleDbConnectionFactory.MakeOleDFactory();
			using (var con = confactory.NewConnection(false, IsolationLevel.ReadCommitted)) {
				var format = "SELECT * FROM T_MDB_TEST WHERE ID = {0}";
				var target = new ValueBinderBuilder(format, binding.Create("id", m => m.Name));
				var binder = target.Build(confactory.DatabaseDialect);
				using (var cmd = con.CreateCommand()) {
					binder.Bind(cmd);
					Assert.AreEqual(10, cmd.Parameters[0].Size);
				}
			}
		}

		[Test]
		public void SimpleNullTest() {
			var binding = new ModelBindingFactory<OleModelBindingModel> { Dynamic = true}.Create(
								new OleModelBindingModel());
			var confactory = TestOleDbConnectionFactory.MakeOleDFactory();
			using (var con = confactory.NewConnection(false, IsolationLevel.ReadCommitted)) {
				var format = "SELECT * FROM T_MDB_TEST WHERE ID = {0}";
				var target = new ValueBinderBuilder(format, binding.Create("id", m => m.Name));
				var binder = target.Build(confactory.DatabaseDialect);
				using (var cmd = con.CreateCommand()) {
					binder.Bind(cmd);
					Assert.AreEqual(10, cmd.Parameters[0].Size);
				}
			}
		}

		[Test]
		public void FittingTest() {
			var binding = new ModelBindingFactory<OleModelBindingModel> { Dynamic = true}.Create(
								new OleModelBindingModel {
									Name = "HogeHogeHoge",
								});
			var dialect = new GenericDatabaseDialect {
				ShortSizePolicy = ShortSizePolicy.Fit,
			};
			var confactory = TestOleDbConnectionFactory.MakeOleDFactory(dialect);
			using (var con = confactory.NewConnection(false, IsolationLevel.ReadCommitted)) {
				var format = "SELECT * FROM T_MDB_TEST WHERE ID = {0}";
				var target = new ValueBinderBuilder(format, binding.Create("id", m => m.Name));
				var binder = target.Build(confactory.DatabaseDialect);
				using (var cmd = con.CreateCommand()) {
					binder.Bind(cmd);
					Assert.AreEqual(12, cmd.Parameters[0].Size);
				}
			}
		}

		[Test]
		public void FittingShortTest() {
			var binding = new ModelBindingFactory<OleModelBindingModel> { Dynamic = true}.Create(
								new OleModelBindingModel {
									Name = "HogeHoge",
								});
			var dialect = new GenericDatabaseDialect {
				ShortSizePolicy = ShortSizePolicy.Fit,
			};
			var confactory = TestOleDbConnectionFactory.MakeOleDFactory(dialect);
			using (var con = confactory.NewConnection(false, IsolationLevel.ReadCommitted)) {
				var format = "SELECT * FROM T_MDB_TEST WHERE ID = {0}";
				var target = new ValueBinderBuilder(format, binding.Create("id", m => m.Name));
				var binder = target.Build(confactory.DatabaseDialect);
				using (var cmd = con.CreateCommand()) {
					binder.Bind(cmd);
					Assert.AreEqual(10, cmd.Parameters[0].Size);
				}
			}
		}

		[Test]
		public void FittingNullTest() {
			var binding = new ModelBindingFactory<OleModelBindingModel> { Dynamic = true}.Create(
									new OleModelBindingModel());
			var dialect = new GenericDatabaseDialect {
				ShortSizePolicy = ShortSizePolicy.Fit,
			};
			var confactory = TestOleDbConnectionFactory.MakeOleDFactory(dialect);
			using (var con = confactory.NewConnection(false, IsolationLevel.ReadCommitted)) {
				var format = "SELECT * FROM T_MDB_TEST WHERE ID = {0}";
				var target = new ValueBinderBuilder(format, binding.Create("id", m => m.Name));
				var binder = target.Build(confactory.DatabaseDialect);
				using (var cmd = con.CreateCommand()) {
					binder.Bind(cmd);
					Assert.AreEqual(10, cmd.Parameters[0].Size);
				}
			}
		}

		[Test]
		public void FittingNoLenTest() {
			var binding = new ModelBindingFactory<OleModelBindingModel> { Dynamic = true}.Create(
									new OleModelBindingModel { NoLengthName = "HogeHoge"});
			var dialect = new GenericDatabaseDialect {
				ShortSizePolicy = ShortSizePolicy.Fit,
			};
			var confactory = TestOleDbConnectionFactory.MakeOleDFactory(dialect);
			using (var con = confactory.NewConnection(false, IsolationLevel.ReadCommitted)) {
				var format = "SELECT * FROM T_MDB_TEST WHERE ID = {0}";
				var target = new ValueBinderBuilder(format, binding.Create("id", m => m.NoLengthName));
				var binder = target.Build(confactory.DatabaseDialect);
				using (var cmd = con.CreateCommand()) {
					binder.Bind(cmd);
					Assert.AreEqual(8, cmd.Parameters[0].Size);
				}
			}
		}

		[Test]
		public void ThrowingTest() {
			var binding = new ModelBindingFactory<OleModelBindingModel> { Dynamic = true}.Create(
								new OleModelBindingModel {
									Name = "HogeHogeHoge",
								});
			var dialect = new GenericDatabaseDialect {
				ShortSizePolicy = ShortSizePolicy.Throw,
			};
			var confactory = TestOleDbConnectionFactory.MakeOleDFactory(dialect);
			using (var con = confactory.NewConnection(false, IsolationLevel.ReadCommitted)) {
				var format = "SELECT * FROM T_MDB_TEST WHERE ID = {0}";
				var target = new ValueBinderBuilder(format, binding.Create("id", m => m.Name));
				var binder = target.Build(confactory.DatabaseDialect);
				using (var cmd = con.CreateCommand()) {
					try {
						binder.Bind(cmd);
						Assert.IsTrue(false, "Should throw an exception.");
					}
					catch (InvalidOperationException ae) {
						Assert.IsTrue(true);
						Assert.IsNotNull(ae.InnerException);
						Assert.IsTrue(ae.InnerException.Message.Contains("HogeHogeHoge"));
						Assert.IsTrue(ae.InnerException.Message.Contains("10"));
					}
				}
			}
		}

		[Test]
		public void ThrowingShortTest() {
			var binding = new ModelBindingFactory<OleModelBindingModel> { Dynamic = true}.Create(
								new OleModelBindingModel {
									Name = "HogeHoge",
								});
			var dialect = new GenericDatabaseDialect {
				ShortSizePolicy = ShortSizePolicy.Throw,
			};
			var confactory = TestOleDbConnectionFactory.MakeOleDFactory(dialect);
			using (var con = confactory.NewConnection(false, IsolationLevel.ReadCommitted)) {
				var format = "SELECT * FROM T_MDB_TEST WHERE ID = {0}";
				var target = new ValueBinderBuilder(format, binding.Create("id", m => m.Name));
				var binder = target.Build(confactory.DatabaseDialect);
				using (var cmd = con.CreateCommand()) {
					binder.Bind(cmd);
					Assert.AreEqual(10, cmd.Parameters[0].Size);
				}
			}
		}

		[Test]
		public void ThrowingNullTest() {
			var binding = new ModelBindingFactory<OleModelBindingModel> { Dynamic = true}.Create(
								new OleModelBindingModel());
			var dialect = new GenericDatabaseDialect {
				ShortSizePolicy = ShortSizePolicy.Throw,
			};
			var confactory = TestOleDbConnectionFactory.MakeOleDFactory(dialect);
			using (var con = confactory.NewConnection(false, IsolationLevel.ReadCommitted)) {
				var format = "SELECT * FROM T_MDB_TEST WHERE ID = {0}";
				var target = new ValueBinderBuilder(format, binding.Create("id", m => m.Name));
				var binder = target.Build(confactory.DatabaseDialect);
				using (var cmd = con.CreateCommand()) {
					binder.Bind(cmd);
					Assert.AreEqual(10, cmd.Parameters[0].Size);
				}
			}
		}
	}
}
