// ==============================================================================
//     Test.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;
using Net.Rimakiri.Data;
using Net.Rimakiri.Data.Interfaces;
using Net.Rimakiri.Data.TypeWrappers;

namespace Test.Rimakiri.Data.x86 {
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class TestValueBinderBuilder {
		[Test]
		public void TestBuild() {
			var confactory = TestOleDbConnectionFactory.MakeOleDFactory();
			using (var con = confactory.NewConnection(false, IsolationLevel.ReadCommitted)) {
				var format = "SELECT * FROM T_MDB_TEST WHERE ID = {0}";
				var target = new ValueBinderBuilder(format, BindValue.Create("id", 1));
				var binder = target.Build(confactory.DatabaseDialect);
				Assert.IsNotNull(binder);
				AssertCommands(con, binder, 1);
			}
		}

		[Test]
		public void TestBuildWithAddCond() {
			var confactory = TestOleDbConnectionFactory.MakeOleDFactory();
			using (var con = confactory.NewConnection(false, IsolationLevel.ReadCommitted)) {
				var format = "SELECT * FROM T_MDB_TEST WHERE ID = {0}";
				var target = new ValueBinderBuilder();
				target.Concat(new BasicCondition(format, BindValue.Create("id", 1)));
				var binder = target.Build(confactory.DatabaseDialect);
				Assert.IsNotNull(binder);
				AssertCommands(con, binder, 1);
			}
		}

		[Test]
		public void TestBuildWithMultiAddCond() {
			var confactory = TestOleDbConnectionFactory.MakeOleDFactory();
			using (var con = confactory.NewConnection(false, IsolationLevel.ReadCommitted)) {
				var format = "ID = {0}";
				var target = new ValueBinderBuilder();
				target.Concat(null)
					.Concat(new SimpleFragment("SELECT * FROM T_MDB_TEST WHERE"), null)
					.Concat(new BasicCondition(format, BindValue.Create("id", 1)));
				var binder = target.Build(confactory.DatabaseDialect);
				Assert.IsNotNull(binder);
				AssertCommands(con, binder, 1);
			}
		}

		[Test]
		public void TestBuildWithReplace() {
			var confactory = TestOleDbConnectionFactory.MakeOleDFactory();
			using (var con = confactory.NewConnection(false, IsolationLevel.ReadCommitted)) {
				var format = "SELECT * FROM T_MDB_TEST WHERE ID = {0}";
				var target = new ValueBinderBuilder();
				target.Concat(new BasicCondition(format, BindValue.Create("id", 1)));
				var binder = target.Build(confactory.DatabaseDialect);
				Assert.IsNotNull(binder);
				AssertCommands(con, binder, 1);

				binder.Replace("id", 100);
				AssertCommands(con, binder, 100);
			}
		}


		[Test]
		public void TestBuildWithWrapper() {
			var confactory = TestOleDbConnectionFactory.MakeOleDFactory();
			var now = DateTime.Now;
			using (var con = confactory.NewConnection(false, IsolationLevel.ReadCommitted)) {
				var format = "SELECT * FROM T_MDB_TEST WHERE ID = {0}";
				var target = new ValueBinderBuilder();
				target.Concat(new BasicCondition(format, BindValue.Create("id", new DateType(now))));
				var binder = target.Build(confactory.DatabaseDialect);
				Assert.IsNotNull(binder);

				if (con.State != ConnectionState.Open) {
					con.Open();
				}

				var cmd = con.CreateCommand();
				binder.Bind(cmd);

				Assert.AreEqual(1, cmd.Parameters.Count);
				var param = cmd.Parameters[0] as IDbDataParameter;
				Assert.IsNotNull(param);
				Assert.AreEqual("@id", param.ParameterName);
				Assert.AreEqual(DbType.Date, param.DbType);
				Assert.AreNotEqual(now, param.Value);
				Assert.AreEqual(now.Date, param.Value);

				binder.Replace("id", null);
				binder.Bind(cmd);
				Assert.AreEqual(1, cmd.Parameters.Count);
				param = cmd.Parameters[0];
				Assert.IsNotNull(param);
				Assert.AreEqual("@id", param.ParameterName);
				Assert.AreEqual(DbType.Date, param.DbType);
				Assert.AreEqual(DBNull.Value, param.Value);

				binder.Replace("id", new TimestampType(null));
				binder.Bind(cmd);
				Assert.AreEqual(1, cmd.Parameters.Count);
				param = cmd.Parameters[0];
				Assert.IsNotNull(param);
				Assert.AreEqual("@id", param.ParameterName);
				Assert.AreEqual(DbType.Date, param.DbType);
				Assert.AreEqual(DBNull.Value, param.Value);
			}
		}

		[Test]
		public void TestMultiBuild() {
			const int id = 100;
			var fragment = new BasicFragment("SELECT * FROM T_SAMPLE WHERE ID = {0}")
							.AddValues(BindValue.Create("id", id));
			var builder = new ValueBinderBuilder(fragment);
			var binder1 = builder.Build(new GenericDatabaseDialect());
			var binder2 = builder.Build(new GenericDatabaseDialect());

			Assert.AreEqual(1, binder1.Count);
			Assert.AreEqual(1, binder2.Count);
		}

		[Test]
		public void TestMultiBuildWithSameValue() {
			const int id = 100;
			var fragment = new SimpleFragment("SELECT * FROM T_SAMPLE WHERE");
			var condA = new BasicCondition("ID = {0}", BindValue.Create("id", id));
			var condB = new BasicCondition("PID = {0}", BindValue.Create("id", id));
			var statement = fragment.Concat(condA.And(condB));
			var builder = new ValueBinderBuilder(statement);
			var binder1 = builder.Build(new GenericDatabaseDialect());
			var binder2 = builder.Build(new GenericDatabaseDialect());

			Assert.AreEqual(1, binder1.Count);
			Assert.AreEqual(1, binder2.Count);
		}

		[Test]
		public void TestMultiBuildWithAnotherValue() {
			var fragment = new SimpleFragment("SELECT * FROM T_SAMPLE WHERE");
			var condA = new BasicCondition("ID = {0}", BindValue.Create("id", 100));
			var condB = new BasicCondition("PID = {0}", BindValue.Create("id", 101));
			var statement = fragment.Concat(condA.And(condB));
			var builder = new ValueBinderBuilder(statement);
			try {
				builder.Build(new GenericDatabaseDialect());
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (ArgumentException) {
				Assert.IsTrue(true);
			}
		}

		private void AssertCommands(IDbConnection con, IValueBinder binder, int value) {
			if (con.State != ConnectionState.Open) {
				con.Open();
			}
			using (var cmd = con.CreateCommand()) {
				binder.Bind(cmd);
				var query = "SELECT * FROM T_MDB_TEST WHERE ID = @id";
				Assert.AreEqual(query, binder.Statement);
				Assert.AreEqual(query, cmd.CommandText);
				Assert.AreEqual(1, cmd.Parameters.Count);
				Assert.AreEqual("@id", ((IDbDataParameter)cmd.Parameters[0]).ParameterName);
				Assert.AreEqual(value, ((IDbDataParameter)cmd.Parameters[0]).Value);
				Assert.AreEqual(DbType.Int32, ((IDbDataParameter)cmd.Parameters[0]).DbType);
			}
		}
	}
}
