// ==============================================================================
//     Test.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Diagnostics.CodeAnalysis;
using Net.Rimakiri.Command;
using NUnit.Framework;

namespace Test.Rimakiri.Command {
	[ExcludeFromCodeCoverage]
	internal class DumbCommandChain : AbstractCommandChain<CommandContext> {
		#region Overrides of AbstractCommandChain<CommandContext>
		public override Idempotent Idempotent {
			get { return Idempotent.Transactional; }
			set { }
		}
		public override TxScope TxScope {
			get { return TxScope.Supports; }
			set { }
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class TestAbstractCommandChain {
		[Test]
		public void ChainTest() {
			var target = new DumbCommandChain();
			var cmd = new AttrSetCommand<string>("key", "value");

			target.Add(cmd);
			Assert.AreSame(cmd, target[0]);

			target.Remove(cmd);
			Assert.AreSame(null, target[0]);

			target.Add(cmd);
			new CommandServiceBuilder().Build().ExecuteCommand(target);
		}

		[Test]
		public void NullSafetyTest() {
			var target = new DumbCommandChain();
			var cmd = new AttrSetCommand<string>("key", "value");

			target.Add(cmd);
			target.Remove(null);
			Assert.AreSame(cmd, target[0]);

			target.Remove(cmd, null);
			Assert.AreSame(null, target[0]);

			target.Add(null);
			target.Add(cmd, null);
			new CommandServiceBuilder().Build().ExecuteCommand(target);
		}
	}
}
