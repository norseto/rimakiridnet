// ==============================================================================
//     Test.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using Net.Rimakiri.Command;
using Net.Rimakiri.Data.SqlServer;
using NUnit.Framework;

namespace Test.Rimakiri.Command {
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class TestPracticalDatabase {
		[Test]
		[Category("Database")]
		public void ConcurrentInsertByKeyTest() {
			const int idValue = 4;
			const string tableName = "T_TX_TEST";
			const string nameValue = "HogeHoge";

			var builder = new DbCommandServiceBuilder {
				MaxRetry = 0,
				TxCoordination = TxCoordination.None,
			};
			var service = builder.Build();
			service.ExecuteCommand(DbCommandUtils.CreateDeleteCommand(tableName, idValue));

			var cmd1 = DbCommandUtils.CreateSafeInsertCommand(tableName, idValue, nameValue);
			var cmd2 = DbCommandUtils.CreateSafeInsertCommand(tableName, idValue, nameValue);
			var barrier = new Barrier(2);
			var executer = new ParallelExecution<DbCommandContext> {
				Builder = builder,
			};
			executer.Add(new CommandChain(new BarrierCommand(barrier), cmd1));
			executer.Add(new CommandChain(new BarrierCommand(barrier), cmd2));

			executer.Execute();
			Assert.IsNull(executer.ErrorFor(cmd1));
			Assert.IsNull(executer.ErrorFor(cmd2));

			service.ExecuteCommand(DbCommandUtils.CreateDeleteCommand("T_TX_TEST", idValue));
		}

		[Test]
		[Category("Database")]
		public void RemoveIfNoChildTest() {
			const int idValue = 8;
			const string parentTable = "T_TX_TEST";
			const string childTable = "T_TX_TEST2";
			const string nameValue = "HogeHoge";

			var builder = new DbCommandServiceBuilder {
				MaxRetry = 0,
				TxCoordination = TxCoordination.None,
			};
			var service = builder.Build();
			var setup1 = DbCommandUtils.CreateSafeInsertCommand(parentTable, idValue, nameValue);
			var setup2 = DbCommandUtils.CreateDeleteCommand(childTable, idValue);
			service.ExecuteCommand(new CommandChain(setup1, setup2));

			var cmd1 = DbCommandUtils.CreateSelectSingleCommand(childTable, idValue);
			var cmd2 = new SelectionCountFilter(1);
			var cmd3 = DbCommandUtils.CreateDeleteCommand(parentTable, idValue);
			var chain1 = new CommandChain(cmd1, cmd2, cmd3);

			service.ExecuteCommand(chain1);
			Assert.AreEqual(0, cmd1.ResultCount);
			Assert.AreEqual(1, cmd3.Affected);
		}

		[Test]
		[Category("Database")]
		public void InsertIfHasParentTest() {
			const int idValue = 8;
			const string parentTable = "T_TX_TEST";
			const string childTable = "T_TX_TEST2";
			const string nameValue = "HogeHoge";

			var builder = new DbCommandServiceBuilder {
				MaxRetry = 0,
				TxCoordination = TxCoordination.None,
			};
			var service = builder.Build();
			var setup1 = DbCommandUtils.CreateSafeInsertCommand(parentTable, idValue, nameValue);
			var setup2 = DbCommandUtils.CreateDeleteCommand(childTable, idValue);
			service.ExecuteCommand(new CommandChain(setup1, setup2));

			var cmd1 = DbCommandUtils.CreateSelectSingleCommand(parentTable, idValue);
			var cmd2 = new SelectionCountFilter(0);
			var cmd3 = DbCommandUtils.CreateDumbInsertCommand(childTable, idValue, nameValue);
			var chain1 = new CommandChain(cmd1, cmd2, cmd3);

			service.ExecuteCommand(chain1);
			Assert.AreEqual(1, cmd1.ResultCount);
			Assert.AreEqual(1, cmd3.Affected);

			var cleanup1 = DbCommandUtils.CreateDeleteCommand(parentTable, idValue);
			var cleanup2 = DbCommandUtils.CreateDeleteCommand(childTable, idValue);
			service.ExecuteCommand(new CommandChain(cleanup1, cleanup2));
		}

		[Test]
		[Category("Database")]
		// Concurrent RemoveIfNoChildTest and InsertIfHasParentTest
		// Must be corrupted.
		public void ConcurrentParentAndChildTest() {
			const int idValue = 8;
			const string parentTable = "T_TX_TEST";
			const string childTable = "T_TX_TEST2";
			const string nameValue = "HogeHoge";

			var builder = new DbCommandServiceBuilder {
				MaxRetry = 0,
				TxCoordination = TxCoordination.None,
			};
			var service = builder.Build();
			var setup1 = DbCommandUtils.CreateSafeInsertCommand(parentTable, idValue, nameValue);
			var setup2 = DbCommandUtils.CreateDeleteCommand(childTable, idValue);
			service.ExecuteCommand(new CommandChain(setup1, setup2));

			var barrier = new Barrier(2);

			// RemoveIfNoChild
			var cmd1 = DbCommandUtils.CreateSelectSingleCommand(childTable, idValue);
			var cmd2 = new SelectionCountFilter(1);
			var cmd3 = DbCommandUtils.CreateDeleteCommand(parentTable, idValue);
			var chain1 = new CommandChain(cmd1, new BarrierCommand(barrier), cmd2, cmd3);

			// InsertIfHasParent
			var cmd4 = DbCommandUtils.CreateSelectSingleCommand(parentTable, idValue);
			var cmd5 = new SelectionCountFilter(0);
			var cmd6 = DbCommandUtils.CreateDumbInsertCommand(childTable, idValue, nameValue);
			var chain2 = new CommandChain(cmd4, new BarrierCommand(barrier), cmd5, cmd6);

			var executer = new ParallelExecution<DbCommandContext> {
				Builder = builder,
			};
			executer.Add(chain1, chain2);
			executer.Execute();

			// Corrupt result
			Assert.AreEqual(1, cmd3.Affected);
			Assert.AreEqual(1, cmd6.Affected);
			Assert.IsNull(executer.ErrorFor(chain1));
			Assert.IsNull(executer.ErrorFor(chain2));

			// Cleanup
			var cleanup1 = DbCommandUtils.CreateDeleteCommand(parentTable, idValue);
			var cleanup2 = DbCommandUtils.CreateDeleteCommand(childTable, idValue);
			service.ExecuteCommand(new CommandChain(cleanup1, cleanup2));
		}

		[Test]
		[Category("Database")]
		// Concurrent RemoveIfNoChildTest and InsertIfHasParentTest
		// Must NOT be corrupted.
		public void CoordinatedConcurrentParentAndChildTest() {
			const int idValue = 8;
			const string parentTable = "T_TX_TEST";
			const string childTable = "T_TX_TEST2";
			const string nameValue = "HogeHoge";

			var builder = new DbCommandServiceBuilder {
				MaxRetry = 0,
				TxCoordination = TxCoordination.Normal,
			};
			var service = builder.Build();
			var setup1 = DbCommandUtils.CreateSafeInsertCommand(parentTable, idValue, nameValue);
			var setup2 = DbCommandUtils.CreateDeleteCommand(childTable, idValue);
			service.ExecuteCommand(new CommandChain(setup1, setup2));

			var barrier = new Barrier(2);

			// RemoveIfNoChild
			var cmd1 = DbCommandUtils.CreateSelectSingleCommand(childTable, idValue);
			var cmd2 = new SelectionCountFilter(1);
			var cmd3 = DbCommandUtils.CreateDeleteCommand(parentTable, idValue);
			var chain1 = new CommandChain(cmd1, new BarrierCommand(barrier), cmd2, cmd3);

			// InsertIfHasParent
			var cmd4 = DbCommandUtils.CreateSelectSingleCommand(parentTable, idValue);
			var cmd5 = new SelectionCountFilter(0);
			var cmd6 = DbCommandUtils.CreateDumbInsertCommand(childTable, idValue, nameValue);
			var chain2 = new CommandChain(cmd4, new BarrierCommand(barrier), cmd5, cmd6);

			var executer = new ParallelExecution<DbCommandContext> {
				Builder = builder,
			};
			executer.Add(chain1, chain2);
			executer.Execute();

			// Consistent result
			Assert.AreEqual(1, cmd3.Affected + cmd6.Affected);
			Assert.IsTrue(executer.ErrorFor(chain1) == null
						|| executer.ErrorFor(chain2) == null);
			Assert.IsTrue(executer.ErrorFor(chain1) != null
						|| executer.ErrorFor(chain2) != null);
			Assert.IsFalse(executer.ErrorFor(chain1) == null
						&& executer.ErrorFor(chain2) == null);
			Assert.IsFalse(executer.ErrorFor(chain1) != null
						&& executer.ErrorFor(chain2) != null);
			var error = executer.ErrorFor(chain1) ?? executer.ErrorFor(chain2);
			// Dead lock
			Assert.IsTrue(error is SqlException);

			// Cleanup
			var cleanup1 = DbCommandUtils.CreateDeleteCommand(parentTable, idValue);
			var cleanup2 = DbCommandUtils.CreateDeleteCommand(childTable, idValue);
			service.ExecuteCommand(new CommandChain(cleanup1, cleanup2));
		}

		[Test]
		[Category("Database")]
		// Concurrent RemoveIfNoChildTest and InsertIfHasParentTest with retry.
		// Must NOT be corrupted.
		public void CoordinatedConcurrentParentAndChildRetryTest() {
			const int idValue = 8;
			const string parentTable = "T_TX_TEST";
			const string childTable = "T_TX_TEST2";
			const string nameValue = "HogeHoge";

			var builder = new DbCommandServiceBuilder {
				MaxRetry = 1,
				TxCoordination = TxCoordination.Normal,
				RetryPolicy = new SqlServerRetryPolicy(),
			};
			var service = builder.Build();
			var setup1 = DbCommandUtils.CreateSafeInsertCommand(parentTable, idValue, nameValue);
			var setup2 = DbCommandUtils.CreateDeleteCommand(childTable, idValue);
			service.ExecuteCommand(new CommandChain(setup1, setup2));

			var barrier = new Barrier(2);

			// RemoveIfNoChild
			var cmd1 = DbCommandUtils.CreateSelectSingleCommand(childTable, idValue);
			var cmd2 = new SelectionCountFilter(1);
			var cmd3 = DbCommandUtils.CreateDeleteCommand(parentTable, idValue);
			var chain1 = new CommandChain(cmd1, new BarrierCommand(barrier), cmd2, cmd3);

			// InsertIfHasParent
			var cmd4 = DbCommandUtils.CreateSelectSingleCommand(parentTable, idValue);
			var cmd5 = new SelectionCountFilter(0);
			var cmd6 = DbCommandUtils.CreateDumbInsertCommand(childTable, idValue, nameValue);
			var chain2 = new CommandChain(cmd4, new BarrierCommand(barrier), cmd5, cmd6);

			var executer = new ParallelExecution<DbCommandContext> {
				Builder = builder,
			};
			executer.Add(chain1, chain2);
			executer.Execute();

			// Consistent result
			Assert.AreEqual(1, cmd3.Affected + cmd6.Affected);
			Assert.IsTrue(executer.ErrorFor(chain1) == null
						|| executer.ErrorFor(chain2) == null);
			Assert.IsTrue(executer.ErrorFor(chain1) != null
						|| executer.ErrorFor(chain2) != null);
			Assert.IsFalse(executer.ErrorFor(chain1) == null
							&& executer.ErrorFor(chain2) == null);
			Assert.IsFalse(executer.ErrorFor(chain1) != null
							&& executer.ErrorFor(chain2) != null);
			var error = executer.ErrorFor(chain1) ?? executer.ErrorFor(chain2);
			// Retry and Select count filtered.
			Assert.IsTrue(error is InvalidOperationException);

			// Cleanup
			var cleanup1 = DbCommandUtils.CreateDeleteCommand(parentTable, idValue);
			var cleanup2 = DbCommandUtils.CreateDeleteCommand(childTable, idValue);
			service.ExecuteCommand(new CommandChain(cleanup1, cleanup2));
		}
		[Test]
		[Category("Database")]
		public void ConcurrentDumbInsertTest() {
			const int idValue = 8;
			const string targetTable = "T_TX_TEST";
			const string nameValue = "HogeHoge";

			var builder = new DbCommandServiceBuilder {
				MaxRetry = 0,
				TxCoordination = TxCoordination.None,
			};
			var service = builder.Build();
			var setup1 = DbCommandUtils.CreateDeleteCommand(targetTable, idValue);
			service.ExecuteCommand(new CommandChain(setup1));

			var barrier = new Barrier(2);

			// RemoveIfNoChild
			var cmd1 = DbCommandUtils.CreateSelectSingleCommand(targetTable, idValue);
			var cmd2 = new SelectionCountFilter(1);
			var cmd3 = DbCommandUtils.CreateDumbInsertCommand(targetTable, idValue, nameValue);
			var chain1 = new CommandChain(cmd1, new BarrierCommand(barrier), cmd2, cmd3);

			// InsertIfHasParent
			var cmd4 = DbCommandUtils.CreateSelectSingleCommand(targetTable, idValue);
			var cmd5 = new SelectionCountFilter(1);
			var cmd6 = DbCommandUtils.CreateDumbInsertCommand(targetTable, idValue, nameValue);
			var chain2 = new CommandChain(cmd4, new BarrierCommand(barrier), cmd5, cmd6);

			var executer = new ParallelExecution<DbCommandContext> {
				Builder = builder,
			};
			executer.Add(chain1, chain2);
			executer.Execute();

			// Constraint error
			Assert.AreEqual(1, cmd3.Affected + cmd6.Affected);
			var error = executer.ErrorFor(chain1) ?? executer.ErrorFor(chain2);
			Assert.IsNotNull(error);
			var sqle = error as SqlException;
			Assert.IsNotNull(sqle);
			Assert.AreEqual(2627, sqle.Number);

			// Cleanup
			var cleanup1 = DbCommandUtils.CreateDeleteCommand(targetTable, idValue);
			service.ExecuteCommand(new CommandChain(cleanup1));
		}

		[Test]
		[Category("Database")]
		public void ConcurrentDumbInsertWithRetryTest() {
			const int idValue = 8;
			const string targetTable = "T_TX_TEST";
			const string nameValue = "HogeHoge";

			var builder = new DbCommandServiceBuilder {
				MaxRetry = 1,
				TxCoordination = TxCoordination.Normal,
				RetryPolicy = new SqlServerRetryPolicy(),
			};
			var service = builder.Build();
			var setup1 = DbCommandUtils.CreateDeleteCommand(targetTable, idValue);
			service.ExecuteCommand(new CommandChain(setup1));

			var barrier = new Barrier(2);

			// RemoveIfNoChild
			var cmd1 = DbCommandUtils.CreateSelectSingleCommand(targetTable, idValue);
			var cmd2 = new SelectionCountFilter(1);
			var cmd3 = DbCommandUtils.CreateDumbInsertCommand(targetTable, idValue, nameValue);
			var chain1 = new CommandChain(cmd1, new BarrierCommand(barrier), cmd2, cmd3);

			// InsertIfHasParent
			var cmd4 = DbCommandUtils.CreateSelectSingleCommand(targetTable, idValue);
			var cmd5 = new SelectionCountFilter(1);
			var cmd6 = DbCommandUtils.CreateDumbInsertCommand(targetTable, idValue, nameValue);
			var chain2 = new CommandChain(cmd4, new BarrierCommand(barrier), cmd5, cmd6);

			var executer = new ParallelExecution<DbCommandContext> {
				Builder = builder,
			};
			executer.Add(chain1, chain2);
			executer.Execute();

			// Constraint error
			Assert.AreEqual(1, cmd3.Affected + cmd6.Affected);
			var error = executer.ErrorFor(chain1) ?? executer.ErrorFor(chain2);
			Assert.IsNotNull(error);
			Assert.IsTrue(error is InvalidOperationException);

			// Cleanup
			var cleanup1 = DbCommandUtils.CreateDeleteCommand(targetTable, idValue);
			service.ExecuteCommand(new CommandChain(cleanup1));
		}
	}
}
