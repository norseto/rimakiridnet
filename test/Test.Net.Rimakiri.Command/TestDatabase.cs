// ==============================================================================
//     Test.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Diagnostics.CodeAnalysis;
using System.Threading;
using Net.Rimakiri.Command;
using Net.Rimakiri.Command.Spi;
using Net.Rimakiri.Data.SqlServer;
using NUnit.Framework;

namespace Test.Rimakiri.Command {
	/// <summary>
	/// データベースのテスト
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestDatabase {

		[Test]
		[Category("Database")]
		public void SelectTest() {
			var builder = new DbCommandServiceBuilder();
			var service = builder.Build();
			var chain = new CommandChain();
			var cmd = new SimpleSelectCommand {
				Statement = "SELECT * FROM T_TX_TEST",
			};
			chain.Add(cmd, new BarrierCommand(new Barrier(1)));

			service.ExecuteCommand(chain);
			Assert.AreEqual(3, cmd.ResultCount);
		}

		[Test]
		[Category("Database")]
		public void UpdateTest() {
			var builder = new DbCommandServiceBuilder();
			var service = builder.Build();
			var cmd = new NonQueryCommand {
				Statement = "UPDATE T_TX_TEST SET NAME = @name WHERE ID = @id",
				Id = 1,
				Name = "HogeHoge",
			};

			service.ExecuteCommand(cmd);
			Assert.AreEqual(1, cmd.Affected);
		}

		[Test]
		[Category("Database")]
		public void UpdateWithLocalTest() {
			var builder = new DbCommandServiceBuilder {
				TransactionFactory = new LocalCommandTxFactory(),
			};
			var service = builder.Build();
			var cmd = new NonQueryCommand {
				Statement = "UPDATE T_TX_TEST SET NAME = @name WHERE ID = @id",
				Id = 1,
				Name = "NAME",
			};
			var sel = new SimpleSelectCommand {
				Statement = "SELECT * FROM T_TX_TEST WHERE ID=1"
			};

			var globaService = new DbCommandServiceBuilder().Build();
			globaService.ExecuteCommand(cmd);
			globaService.ExecuteCommand(sel);
			Assert.AreEqual("NAME", sel.Results[0]["NAME"]);

			cmd.Id = 2;
			sel.Statement = "SELECT * FROM T_TX_TEST WHERE ID=2";
			globaService.ExecuteCommand(cmd);
			globaService.ExecuteCommand(sel);
			Assert.AreEqual("NAME", sel.Results[0]["NAME"]);


			cmd.Name = "HogeHoge";
			service.ExecuteCommand(cmd);
			Assert.AreEqual(1, cmd.Affected);

			globaService.ExecuteCommand(sel);
			Assert.AreEqual("HogeHoge", sel.Results[0]["NAME"]);
		}

		[Test]
		[Category("Database")]
		public void RollbackWithLocalTest() {
			var builder = new DbCommandServiceBuilder {
				TransactionFactory = new LocalCommandTxFactory(),
				RetryPolicy = new SqlServerRetryPolicy(),
			};
			var service = builder.Build();
			var cmd1 = new NonQueryCommand {
				Statement = "UPDATE T_TX_TEST SET NAME = @name WHERE ID = @id",
				Id = 1,
				Name = "NAME",
			};
			var cmd2 = new NonQueryCommand {
				Statement = "UPDATE T_TX_TEST SET NAME = @name WHERE ID = @id",
				Id = 2,
				Name = "NAME",
			};
			var sel = new SimpleSelectCommand {
				Statement = "SELECT * FROM T_TX_TEST WHERE ID IN (1, 2)"
			};

			var globaService = new DbCommandServiceBuilder().Build();
			var chain01 = new SimpleCommandChain<DbCommandContext>(TxScope.Required);
			chain01.Add(cmd1, cmd2);
			globaService.ExecuteCommand(chain01);
			globaService.ExecuteCommand(sel);
			Assert.AreEqual("NAME", sel.Results[0]["NAME"]);
			Assert.AreEqual("NAME", sel.Results[1]["NAME"]);

			var chain02 = new SimpleCommandChain<DbCommandContext>(TxScope.Required);
			cmd1.Name = cmd2.Name = "HogeHoge";
			chain02.Add(cmd1, cmd2, new ThrowCommand());
			try {
				service.ExecuteCommand(chain02);
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (SampleException) {
				Assert.IsTrue(true);
			}
			globaService.ExecuteCommand(sel);
			Assert.AreEqual("NAME", sel.Results[0]["NAME"]);
			Assert.AreEqual("NAME", sel.Results[1]["NAME"]);

			var chain03 = new SimpleCommandChain<DbCommandContext>(TxScope.Required);
			chain03.Add(cmd1, cmd2);
			service.ExecuteCommand(chain03);
			globaService.ExecuteCommand(sel);
			Assert.AreEqual("HogeHoge", sel.Results[0]["NAME"]);
			Assert.AreEqual("HogeHoge", sel.Results[1]["NAME"]);
		}


		[Test]
		[Category("Database")]
		public void UpdateMultiTest() {
			var builder = new DbCommandServiceBuilder();
			var service = builder.Build();
			var cmd1 = new NonQueryCommand {
				Statement = "UPDATE T_TX_TEST SET NAME = @name WHERE ID = @id",
				Id = 1,
				Name = "NAME",
			};
			var cmd2 = new NonQueryCommand {
				Statement = "UPDATE T_TX_TEST SET NAME = @name WHERE ID = @id",
				Id = 2,
				Name = "NAME",
			};

			var cmd = new CommandChain(cmd1, cmd2);
			service.ExecuteCommand(cmd);
			Assert.AreEqual(1, cmd1.Affected);
			Assert.AreEqual(1, cmd2.Affected);
		}

		[Test]
		[Category("Database")]
		public void DeadLockTest() {
			var barrier = new Barrier(2);
			var cmd1 = new CommandChain();
			var cmd2 = new CommandChain();
			var executor = new ParallelExecution<DbCommandContext> {
				Builder = new DbCommandServiceBuilder {
					MaxRetry = 0,
				}
			};
			SetupUpdates(barrier, cmd1, cmd2);
			executor.Add(cmd1, cmd2);
			executor.Execute();

			Assert.IsFalse(executor.ErrorFor(cmd1) != null
							&& executor.ErrorFor(cmd2) != null,
				"One of tx should succeed.");
			Assert.IsTrue(executor.ErrorFor(cmd1) != null
						|| executor.ErrorFor(cmd2) != null,
				"Should cause deadlock.");
		}

		[Test]
		[Category("Database")]
		public void DeadLockRecoveryTest() {
			var barrier = new Barrier(2);
			var cmd1 = new CommandChain();
			var cmd2 = new CommandChain();
			var executor = new ParallelExecution<DbCommandContext> {
				Builder = new DbCommandServiceBuilder {
					MaxRetry = 1,
					RetryPolicy = new UnconditionalRetryPolicy(),
				}
			};
			SetupUpdates(barrier, cmd1, cmd2);
			executor.Add(cmd1, cmd2);
			executor.Execute();

			Assert.IsNull(executor.ErrorFor(cmd1), "Should not cause exception.");
			Assert.IsNull(executor.ErrorFor(cmd2), "Should not cause exception.");
		}

		private void SetupUpdates(Barrier barrier,
			AbstractCommandChain<DbCommandContext> cmd1,
			AbstractCommandChain<DbCommandContext> cmd2) {
			cmd1.Add(
				new NonQueryCommand {
					Statement = "UPDATE T_TX_TEST SET NAME = @name WHERE ID = @id",
					Id = 1,
					Name = "1to2",
				},
				new BarrierCommand(barrier),
				new NonQueryCommand {
					Statement = "UPDATE T_TX_TEST2 SET NAME = @name WHERE ID = @id",
					Id = 1,
					Name = "1to2",
				}
			);
			cmd2.Add(
				new NonQueryCommand {
					Statement = "UPDATE T_TX_TEST2 SET NAME = @name WHERE ID = @id",
					Id = 1,
					Name = "2to1",
				},
				new BarrierCommand(barrier),
				new NonQueryCommand {
					Statement = "UPDATE T_TX_TEST SET NAME = @name WHERE ID = @id",
					Id = 1,
					Name = "2to1",
				}
			);
		}

		[Test]
		[Category("Database")]
		public void SelectDeadLockTest() {
			var barrier = new Barrier(2);
			var cmd1 = new CommandChain();
			var cmd2 = new CommandChain();
			var executor = new ParallelExecution<DbCommandContext> {
				Builder = new DbCommandServiceBuilder {
					MaxRetry = 0,
					IsolationLevel = TxIsolation.RepeatableRead,
				}
			};
			SetupUpdateAndSelect(barrier, cmd1, cmd2);
			executor.Add(cmd1, cmd2);
			executor.Execute();

			Assert.IsFalse(executor.ErrorFor(cmd1) != null
							&& executor.ErrorFor(cmd2) != null,
				"One of tx should succeed.");
			Assert.IsTrue(executor.ErrorFor(cmd1) != null
						|| executor.ErrorFor(cmd2) != null,
				"Should cause deadlock.");
		}

		[Test]
		[Category("Database")]
		public void SelectDeadLockRecoveryTest() {
			var barrier = new Barrier(2);
			var cmd1 = new CommandChain();
			var cmd2 = new CommandChain();
			var executor = new ParallelExecution<DbCommandContext> {
				Builder = new DbCommandServiceBuilder {
					MaxRetry = 1,
					IsolationLevel = TxIsolation.RepeatableRead,
					RetryPolicy = new UnconditionalRetryPolicy(),
				}
			};
			SetupUpdateAndSelect(barrier, cmd1, cmd2);
			executor.Add(cmd1, cmd2);
			executor.Execute();

			Assert.IsNull(executor.ErrorFor(cmd1), "Should not cause exception.");
			Assert.IsNull(executor.ErrorFor(cmd2), "Should not cause exception.");
		}

		private void SetupUpdateAndSelect(Barrier barrier,
					AbstractCommandChain<DbCommandContext> cmd1,
					AbstractCommandChain<DbCommandContext> cmd2) {
			cmd1.Add(
				new NonQueryCommand {
					Statement = "UPDATE T_TX_TEST SET NAME = @name WHERE ID = @id",
					Id = 1,
					Name = "1to2UpdateAndSel",
				},
				new BarrierCommand(barrier),
				new SimpleSelectCommand {
					Statement = "SELECT * FROM T_TX_TEST2 where ID = 1",
				}
			);
			cmd2.Add(
				new NonQueryCommand {
					Statement = "UPDATE T_TX_TEST2 SET NAME = @name WHERE ID = @id",
					Id = 1,
					Name = "2to1UpdateAndSel",
				},
				new BarrierCommand(barrier),
				new SimpleSelectCommand {
					Statement = "SELECT * FROM T_TX_TEST where ID = 1",
				}
			);
		}

		[Test]
		[Category("Database")]
		public void SelectUpdateDeadLockTest() {
			var barrier = new Barrier(2);
			var cmd1 = new CommandChain();
			var cmd2 = new CommandChain();
			var executor = new ParallelExecution<DbCommandContext> {
				Builder = new DbCommandServiceBuilder {
					MaxRetry = 0,
					IsolationLevel = TxIsolation.RepeatableRead,
				}
			};
			SetupSelectAndUpdate(barrier, cmd1, cmd2);
			executor.Add(cmd1, cmd2);
			executor.Execute();
			Assert.IsFalse(executor.ErrorFor(cmd1) != null
						&& executor.ErrorFor(cmd2) != null,
				"One of tx should succeed.");
			Assert.IsTrue(executor.ErrorFor(cmd1) != null
						|| executor.ErrorFor(cmd2) != null,
				"Should cause deadlock.");
		}


		[Test]
		[Category("Database")]
		public void SelectUpdateDeadLockRecoveryTest() {
			var barrier = new Barrier(2);
			var cmd1 = new CommandChain();
			var cmd2 = new CommandChain();
			var executor = new ParallelExecution<DbCommandContext> {
				Builder = new DbCommandServiceBuilder {
					MaxRetry = 1,
					IsolationLevel = TxIsolation.RepeatableRead,
					RetryPolicy = new UnconditionalRetryPolicy(),
				}
			};
			SetupSelectAndUpdate(barrier, cmd1, cmd2);
			executor.Add(cmd1, cmd2);
			executor.Execute();

			Assert.IsNull(executor.ErrorFor(cmd1), "Should not cause exception.");
			Assert.IsNull(executor.ErrorFor(cmd2), "Should not cause exception.");
		}

		private void SetupSelectAndUpdate(Barrier barrier,
						AbstractCommandChain<DbCommandContext> cmd1,
						AbstractCommandChain<DbCommandContext> cmd2) {
			cmd1.Add(
				new SimpleSelectCommand {
					Statement = "SELECT * FROM T_TX_TEST where ID = 1",
				},
				new BarrierCommand(barrier),
				new NonQueryCommand {
					Statement = "UPDATE T_TX_TEST2 SET NAME = @name WHERE ID = @id",
					Id = 1,
					Name = "1to2SelUp",
				}
			);
			cmd2.Add(
				new SimpleSelectCommand {
					Statement = "SELECT * FROM T_TX_TEST2 where ID = 1",
				},
				new BarrierCommand(barrier),
				new NonQueryCommand {
					Statement = "UPDATE T_TX_TEST SET NAME = @name WHERE ID = @id",
					Id = 1,
					Name = "2to1SelUp",
				}
			);
		}
	}
}
