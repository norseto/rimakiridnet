// ==============================================================================
//     Test.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using Net.Rimakiri.Command;
using Net.Rimakiri.Data.SqlServer;
using NUnit.Framework;

namespace Test.Rimakiri.Command {
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class TestDbXactionTimeout {
		[Test]
		[Category("Database")]
		public void TimeoutTest() {
			var builder = new DbCommandServiceBuilder {
				Timeout = TimeSpan.FromSeconds(5),
			};
			var service = builder.Build();
			ICommand<DbCommandContext> cmd = new CoordSelectCommand {
				Statement = "SELECT * FROM T_TX_TEST WITH (UPDLOCK)",
			};

			var chain = new SimpleCommandChain<DbCommandContext>(TxScope.Required);
			chain.Add(cmd, new SleepCommand(TimeSpan.FromSeconds(7)));

			try {
				service.ExecuteCommand(chain);
				Assert.IsTrue(false, "Should cause exception.");
			}
			catch (HeuristicTimeoutException) {
				Assert.IsTrue(true);
			}
		}

		[Test]
		[Category("Database")]
		public void TimeoutTestParallel() {
			var builderShortTo = new DbCommandServiceBuilder {
				Timeout = TimeSpan.FromSeconds(5),
				RetryPolicy = new SqlServerRetryPolicy(),
			};
			var builderLongTo = new DbCommandServiceBuilder {
				Timeout = TimeSpan.FromSeconds(10),
				RetryPolicy = new SqlServerRetryPolicy(),
			};

			var barrier = new Barrier(2);
			var chain01 = new CommandChain();
			var chain02 = new CommandChain();
			var cmd1 = new CoordSelectCommand {
				Statement = "SELECT * FROM T_TX_TEST WITH (UPDLOCK)",
			};
			var cmd2 = new CoordSelectCommand {
				Statement = "SELECT * FROM T_TX_TEST WITH (UPDLOCK)",
			};

			chain01.Add(cmd1, new BarrierCommand(barrier),
					new SleepCommand(TimeSpan.FromSeconds(7)));
			chain02.Add(new BarrierCommand(barrier), cmd2);

			var executor = new ParallelExecution<DbCommandContext> {
				Builder = builderShortTo,
			};
			executor.Add(chain01, chain02);
			executor.Execute(command => command == chain01 ? builderLongTo
							: command == chain02 ? builderShortTo
							: null);
			Assert.IsNull(executor.ErrorFor(chain01));
			Assert.IsNotNull(executor.ErrorFor(chain02));
			Assert.IsTrue(executor.ErrorFor(chain02) is SqlException);
			var sqle = executor.ErrorFor(chain02) as SqlException;
			Assert.IsNotNull(sqle);
			Assert.AreEqual(-2, sqle.Number);
		}
	}
}
