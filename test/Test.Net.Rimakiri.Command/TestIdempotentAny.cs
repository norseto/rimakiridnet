// ==============================================================================
//     Test.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using Net.Rimakiri.Command;
using Net.Rimakiri.Command.Spi;
using Net.Rimakiri.Command.Transactions;
using NUnit.Framework;

namespace Test.Rimakiri.Command {
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestIdempotentAny {
		[Test]
		public void RetryExceptionTest() {
			var s1 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = 3
				}
			}.Build();
			var s2 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
				TransactionFactory = new ScopeCommandTxFactory(),
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = 3
				}
			}.Build();

			Action<ICommandService> action = (service) => {
				var grandChild = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Anytime,
				};
				var child = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Transactional,
					Child = grandChild,
				};
				var parent = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Transactional,
					Child = child,
					TxScope = TxScope.Required,
				};

				service.ExecuteCommand(parent);

				Assert.AreEqual(3, parent.Executed);
				Assert.AreEqual(2, child.Executed);
				Assert.AreEqual(2, grandChild.Executed);
			};
			action(s1);
			action(s2);
		}

		[Test]
		public void RetryExhaustedExceptionTest() {
			var s1 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = 2
				}
			}.Build();
			var s2 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
				TransactionFactory = new ScopeCommandTxFactory(),
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = 2
				}
			}.Build();

			Action<ICommandService> action = (service) => {
				var grandChild = new IdempotentExceptionCommand {
					Threashold = 7,
					Idempotent = Idempotent.Anytime
				};
				var child = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Transactional,
					Child = grandChild,
				};
				var parent = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Transactional,
					Child = child,
					TxScope = TxScope.Required,
				};


				try {
					service.ExecuteCommand(parent);
					Assert.IsTrue(false, "Should throw an exception.");
				}
				catch (AbandonedMutexException) {
					Assert.IsTrue(true);
				}

				Assert.AreEqual(3, parent.Executed);
				Assert.AreEqual(3, child.Executed);
				Assert.AreEqual(6, grandChild.Executed);
			};
			action(s1);
			action(s2);
		}

		[Test]
		public void NotIdempotentExceptionTest() {
			var s1 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = 3
				}
			}.Build();
			var s2 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
				TransactionFactory = new ScopeCommandTxFactory(),
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = 3
				}
			}.Build();

			Action<ICommandService> action = (service) => {
				var grandChild = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.None
				};
				var child = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Anytime,
					Child = grandChild,
				};
				var parent = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Transactional,
					Child = child,
				};

				try {
					service.ExecuteCommand(parent);
					Assert.IsTrue(false, "Should throw an exception.");
				}
				catch (AbandonedMutexException) {
					Assert.IsTrue(true);
				}

				Assert.AreEqual(2, parent.Executed);
				Assert.AreEqual(2, child.Executed);
				Assert.AreEqual(1, grandChild.Executed);
			};
			action(s1);
			action(s2);
		}

		[Test]
		public void NestedTxRetryExceptionTest() {
			var s1 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = 3
				}
			}.Build();
			var s2 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
				TransactionFactory = new ScopeCommandTxFactory(),
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = 3
				}
			}.Build();

			Action<ICommandService> action = (service) => {
				var grandChild = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Anytime
				};
				var child = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Transactional,
					Child = grandChild,
					TxScope = TxScope.RequiresNew,
				};
				var parent = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Transactional,
					Child = child,
				};

				service.ExecuteCommand(parent);

				Assert.AreEqual(2, parent.Executed);
				Assert.AreEqual(2, child.Executed);
				Assert.AreEqual(2, grandChild.Executed);
			};
			action(s1);
			action(s2);
		}

		[Test]
		public void RetryPolicyTest() {
			var s1 = new CommandServiceBuilder {
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = 10,
					RetryPolicy = new DumbPolicy(),
				}
			}.Build();
			var s2 = new CommandServiceBuilder {
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = 10,
					RetryPolicy = new DumbPolicy(),
					CommandTxFactory = new ScopeCommandTxFactory(),
				}
			}.Build();

			Action<ICommandService> action = (service) => {
				var grandChild = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Anytime
				};
				var child = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Transactional,
					Child = grandChild,
					TxScope = TxScope.RequiresNew,
				};
				var parent = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Anytime,
					Child = child,
				};

				try {
					service.ExecuteCommand(parent);
					Assert.IsTrue(false, "Should throw an exception.");
				}
				catch (AbandonedMutexException) {
					Assert.IsTrue(true);
				}

				Assert.AreEqual(1, parent.Executed);
				Assert.AreEqual(1, child.Executed);
				Assert.AreEqual(1, grandChild.Executed);
			};
			action(s1);
			action(s2);
		}

		[Test]
		public void NegativeRetryCountTest() {
			var s1 = new CommandServiceBuilder {
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = -1
				}
			}.Build();
			var s2 = new CommandServiceBuilder {
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = -1,
					CommandTxFactory = new ScopeCommandTxFactory(),
				}
			}.Build();

			Action<ICommandService> action = (service) => {
				var grandChild = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Anytime
				};
				var child = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Anytime,
					Child = grandChild,
				};
				var parent = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Anytime,
					Child = child,
				};

				service.ExecuteCommand(parent);

				Assert.AreEqual(1, parent.Executed);
				Assert.AreEqual(1, child.Executed);
				Assert.AreEqual(1, grandChild.Executed);
			};
			action(s1);
			action(s2);
		}

		[Test]
		public void NegativeRetryCountWithErrorTest() {
			var s1 = new CommandServiceBuilder {
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = -1
				}
			}.Build();
			var s2 = new CommandServiceBuilder {
				TransactionFactory = new ScopeCommandTxFactory(),
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = -1
				}
			}.Build();

			Action<ICommandService> action = (service) => {
				var grandChild = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Anytime
				};
				var child = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Anytime,
					Child = grandChild,
				};
				var parent = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Transactional,
					Child = child,
				};

				try {
					service.ExecuteCommand(parent);
					Assert.IsTrue(false, "Should throw an exception.");
				}
				catch (AbandonedMutexException) {
					Assert.IsTrue(true);
				}

				Assert.AreEqual(1, parent.Executed);
				Assert.AreEqual(1, child.Executed);
				Assert.AreEqual(1, grandChild.Executed);
			};
			action(s1);
			action(s2);
		}

		/// <summary>
		/// トランザクションがない場合には、例外発生コマンド
		/// がリトライされる
		/// </summary>
		[Test]
		public void RetrySupportsTest() {
			var s1 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
			}.Build();
			var s2 = new CommandServiceBuilder {
				TransactionFactory = new ScopeCommandTxFactory(),
				RetryPolicy = new UnconditionalRetryPolicy(),
			}.Build();

			Action<ICommandService> action = (service) => {
				var grandChild = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Anytime,
					TxScope = TxScope.Supports,
				};
				var child = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Anytime,
					Child = grandChild,
					TxScope = TxScope.Supports,
				};
				var parent = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Anytime,
					Child = child,
					TxScope = TxScope.Supports,
				};

				service.ExecuteCommand(parent);

				Assert.AreEqual(1, parent.Executed);
				Assert.AreEqual(1, child.Executed);
				Assert.AreEqual(2, grandChild.Executed);
			};
			action(s1);
			action(s2);
		}

		/// <summary>
		/// トランザクションがない場合には、例外発生コマンド
		/// がリトライされる
		/// </summary>
		[Test]
		public void RetrySuppressTest() {
			var s1 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
			}.Build();
			var s2 = new CommandServiceBuilder {
				TransactionFactory = new ScopeCommandTxFactory(),
				RetryPolicy = new UnconditionalRetryPolicy(),
			}.Build();

			Action<ICommandService> action = (service) => {
				var grandChild = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Anytime,
					TxScope = TxScope.Suppress,
				};
				var child = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Transactional,
					Child = grandChild,
					TxScope = TxScope.Required,
				};
				var parent = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Transactional,
					Child = child,
					TxScope = TxScope.Required,
				};

				service.ExecuteCommand(parent);

				Assert.AreEqual(1, parent.Executed);
				Assert.AreEqual(1, child.Executed);
				Assert.AreEqual(2, grandChild.Executed);
			};
			action(s1);
			action(s2);
		}

		/// <summary>
		/// 途中でトランザクションが開始された場合には、
		/// トランザクション開始コマンドからリトライされる
		/// </summary>
		[Test]
		public void RetryRequiredTest() {
			var s1 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
			}.Build();
			var s2 = new CommandServiceBuilder {
				TransactionFactory = new ScopeCommandTxFactory(),
				RetryPolicy = new UnconditionalRetryPolicy(),
			}.Build();

			Action<ICommandService> action = (service) => {
				var grandChild = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Anytime,
					TxScope = TxScope.Supports,
				};
				var child = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Transactional,
					Child = grandChild,
					TxScope = TxScope.Required,
				};
				var parent = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Transactional,
					Child = child,
					TxScope = TxScope.Supports,
				};

				service.ExecuteCommand(parent);

				Assert.AreEqual(1, parent.Executed);
				Assert.AreEqual(1, child.Executed);
				Assert.AreEqual(2, grandChild.Executed);
			};
			action(s1);
			action(s2);
		}

		/// <summary>
		/// 冪等でないコマンドから呼び出された冪等なコマンド
		/// はエラー時リトライされる
		/// </summary>
		[Test]
		public void AnyInNoneTest() {
			var s1 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
			}.Build();
			var s2 = new CommandServiceBuilder {
				TransactionFactory = new ScopeCommandTxFactory(),
				RetryPolicy = new UnconditionalRetryPolicy(),
			}.Build();

			Action<ICommandService> action = (service) => {
				var child = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Anytime,
					TxScope = TxScope.Supports,
				};
				var parent = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.None,
					Child = child,
					TxScope = TxScope.Supports,
				};

				service.ExecuteCommand(parent);

				Assert.AreEqual(1, parent.Executed);
				Assert.AreEqual(2, child.Executed);
			};
			action(s1);
			action(s2);
		}

		/// <summary>
		/// 冪等でないコマンドから呼び出された冪等なコマンド
		/// はエラー時リトライされる
		/// </summary>
		[Test]
		public void AnyInNoneTest2() {
			var s1 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
			}.Build();
			var s2 = new CommandServiceBuilder {
				TransactionFactory = new ScopeCommandTxFactory(),
				RetryPolicy = new UnconditionalRetryPolicy(),
			}.Build();

			Action<ICommandService> action = (service) => {
				var grandChild = new IdempotentExceptionCommand {
					Threashold = 5,
					Idempotent = Idempotent.Anytime,
					TxScope = TxScope.Supports,
				};
				var child = new IdempotentExceptionCommand {
					Threashold = 1,
					Child = grandChild,
					Idempotent = Idempotent.None,
					TxScope = TxScope.Required,
				};
				var parent = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Transactional,
					Child = child,
					TxScope = TxScope.Required,
				};

				try {
					service.ExecuteCommand(parent);
					Assert.IsTrue(false, "Should throw an exception.");
				}
				catch (AbandonedMutexException) {
					Assert.IsTrue(true);
				}

				Assert.AreEqual(1, parent.Executed);
				Assert.AreEqual(1, child.Executed);
				Assert.AreEqual(3, grandChild.Executed);
			};
			action(s1);
			action(s2);
		}
	}
}
