// ==============================================================================
//     Test.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Diagnostics.CodeAnalysis;
using System.Transactions;
using Net.Rimakiri.Command;
using Net.Rimakiri.Command.Transactions;
using NUnit.Framework;

namespace Test.Rimakiri.Command {
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestIsolationLevelExtension {
		[Test]
		public void ToIsolationLevelTest() {
			Assert.AreEqual(IsolationLevel.Serializable,
				TxIsolation.Serializable.ToIsolationLevel());
			Assert.AreEqual(IsolationLevel.RepeatableRead,
				TxIsolation.RepeatableRead.ToIsolationLevel());
			Assert.AreEqual(IsolationLevel.ReadCommitted,
				TxIsolation.ReadCommitted.ToIsolationLevel());
			Assert.AreEqual(IsolationLevel.ReadUncommitted,
				TxIsolation.ReadUncommitted.ToIsolationLevel());
			Assert.AreEqual(IsolationLevel.Snapshot,
				TxIsolation.Snapshot.ToIsolationLevel());
			Assert.AreEqual(IsolationLevel.ReadCommitted,
				TxIsolation.Unspecified.ToIsolationLevel());
		}

		[Test]
		public void MergedToIsolationLevelTest() {
			Assert.AreEqual(IsolationLevel.Serializable,
				(TxIsolation.Serializable | TxIsolation.RepeatableRead).ToIsolationLevel());
			Assert.AreEqual(IsolationLevel.RepeatableRead,
				(TxIsolation.RepeatableRead | TxIsolation.ReadCommitted).ToIsolationLevel());
			Assert.AreEqual(IsolationLevel.ReadUncommitted,
				(TxIsolation.ReadCommitted | TxIsolation.ReadUncommitted).ToIsolationLevel());
			Assert.AreEqual(IsolationLevel.ReadCommitted,
				(TxIsolation.ReadCommitted | TxIsolation.Unspecified).ToIsolationLevel());
			Assert.AreEqual(IsolationLevel.Snapshot,
				(TxIsolation.Snapshot | TxIsolation.Serializable).ToIsolationLevel());
		}
	}
}
