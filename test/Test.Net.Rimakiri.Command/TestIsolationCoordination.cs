// ==============================================================================
//     Test.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Transactions;
using Net.Rimakiri.Command;
using Net.Rimakiri.Data.SqlServer;
using NUnit.Framework;

namespace Test.Rimakiri.Command {
	/// <summary>
	/// トランザクション分離レベル調整のテスト
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestIsolationCoordination {
		[Test]
		[Category("Database")]
		public void SelectUpdateDeadLockTest() {
			var barrier = new Barrier(2);
			var cmd1 = new CommandChain();
			var cmd2 = new CommandChain();
			var executor = new ParallelExecution<DbCommandContext> {
				Builder = new DbCommandServiceBuilder {
					MaxRetry = 0,
				}
			};
			SetupSelectAndUpdate(barrier, cmd1, cmd2);
			executor.Add(cmd1, cmd2);
			executor.Execute();

			Assert.IsFalse(executor.ErrorFor(cmd1) != null
							&& executor.ErrorFor(cmd2) != null,
				"One of tx should succeed.");
			Assert.IsTrue(executor.ErrorFor(cmd1) != null
						|| executor.ErrorFor(cmd2) != null,
				"Should cause deadlock.");

			var child1 = cmd1[0] as IDbExecution;
			var child2 = cmd2[0] as IDbExecution;
			Assert.IsNotNull(child1);
			Assert.IsNotNull(child2);
			Assert.AreEqual(IsolationLevel.RepeatableRead, child1.IsolationLevelExecuted);
			Assert.AreEqual(IsolationLevel.RepeatableRead, child2.IsolationLevelExecuted);
		}

		[Test]
		[Category("Database")]
		public void SelectUpdateDeadLockRecoveryTest() {
			var barrier = new Barrier(2);
			var cmd1 = new CommandChain();
			var cmd2 = new CommandChain();
			var executor = new ParallelExecution<DbCommandContext> {
				Builder = new DbCommandServiceBuilder {
					MaxRetry = 1,
					RetryPolicy = new SqlServerRetryPolicy(),
				}
			};
			SetupSelectAndUpdate(barrier, cmd1, cmd2);
			Assert.AreEqual(TxCoordination.Normal, executor.Builder.TxCoordination);
			executor.Add(cmd1, cmd2);
			executor.Execute();

			Assert.IsNull(executor.ErrorFor(cmd1), "Should not cause exception.");
			Assert.IsNull(executor.ErrorFor(cmd2), "Should not cause exception.");

			var child1 = cmd1[0] as IDbExecution;
			var child2 = cmd2[0] as IDbExecution;
			Assert.IsNotNull(child1);
			Assert.IsNotNull(child2);
			Assert.AreEqual(IsolationLevel.RepeatableRead, child1.IsolationLevelExecuted);
			Assert.AreEqual(IsolationLevel.RepeatableRead, child2.IsolationLevelExecuted);
		}

		private void SetupSelectAndUpdate(Barrier barrier,
			AbstractCommandChain<DbCommandContext> cmd1,
			AbstractCommandChain<DbCommandContext> cmd2) {
			cmd1.Add(
				DbCommandUtils.CreateSelectSingleCommand("T_TX_TEST", 1),
				new BarrierCommand(barrier),
				DbCommandUtils.CreateUpdateCommand("T_TX_TEST2", 1, "1to2Setup")
			);
			cmd2.Add(
				DbCommandUtils.CreateSelectSingleCommand("T_TX_TEST2", 1),
				new BarrierCommand(barrier),
				DbCommandUtils.CreateUpdateCommand("T_TX_TEST", 1, "2to1Setup")
			);
		}

		[Test]
		[Category("Database")]
		public void SelectForUpdateTest() {
			var barrier = new Barrier(2);
			var cmd1 = new CommandChain();
			var cmd2 = new CommandChain();
			var executor = new ParallelExecution<DbCommandContext> {
				Builder = new DbCommandServiceBuilder {
					MaxRetry = 1,
					RetryPolicy = new SqlServerRetryPolicy(),
				}
			};
			SetupSelectForUpdateAndUpdate(barrier, cmd1, cmd2);
			Assert.AreEqual(TxCoordination.Normal, executor.Builder.TxCoordination);
			executor.Add(cmd1, cmd2);
			executor.Execute();

			Assert.IsNull(executor.ErrorFor(cmd1), "Should not cause exception: " + executor.ErrorFor(cmd1));
			Assert.IsNull(executor.ErrorFor(cmd2), "Should not cause exception: " + executor.ErrorFor(cmd2));

			var child1 = cmd1[0] as IDbExecution;
			var child2 = cmd2[0] as IDbExecution;
			Assert.IsNotNull(child1);
			Assert.IsNotNull(child2);
			Assert.AreEqual(IsolationLevel.ReadCommitted, child1.IsolationLevelExecuted);
			Assert.AreEqual(IsolationLevel.ReadCommitted, child2.IsolationLevelExecuted);
		}

		[Test]
		[Category("Database")]
		[Category("Snapshot")]
		public void SelectForUpdateWithSnapshotTest() {
			var barrier = new Barrier(2);
			var cmd1 = new CommandChain();
			var cmd2 = new CommandChain();
			var executor = new ParallelExecution<DbCommandContext> {
				Builder = new DbCommandServiceBuilder {
					IsolationLevel = TxIsolation.Snapshot,
					RetryPolicy = new SqlServerRetryPolicy(),
				}
			};
			SetupSelectForUpdateAndUpdate(barrier, cmd1, cmd2);
			Assert.AreEqual(TxCoordination.Normal, executor.Builder.TxCoordination);
			executor.Add(cmd1, cmd2);
			executor.Execute();

			if (SnapshotUnsupported(executor.ErrorFor(cmd1))
				|| SnapshotUnsupported(executor.ErrorFor(cmd2))) {
				return;
			}

			Assert.IsNull(executor.ErrorFor(cmd1), "Should not cause exception: " + executor.ErrorFor(cmd1));
			Assert.IsNull(executor.ErrorFor(cmd2), "Should not cause exception: " + executor.ErrorFor(cmd2));

			var child1 = cmd1[0] as IDbExecution;
			var child2 = cmd2[0] as IDbExecution;
			Assert.IsNotNull(child1);
			Assert.IsNotNull(child2);
			Assert.AreEqual(IsolationLevel.Snapshot, child1.IsolationLevelExecuted);
			Assert.AreEqual(IsolationLevel.Snapshot, child2.IsolationLevelExecuted);
		}

		private bool SnapshotUnsupported(Exception ex) {
			var sqlex = ex as SqlException;
			return sqlex != null && sqlex.Number == 3952;
		}

		private void SetupSelectForUpdateAndUpdate(Barrier barrier,
			AbstractCommandChain<DbCommandContext> cmd1,
			AbstractCommandChain<DbCommandContext> cmd2) {
			cmd1.Add(
				DbCommandUtils.CreateSelectForUpdateSingleCommand("T_TX_TEST", 1),
				new BarrierCommand(barrier),
				DbCommandUtils.CreateUpdateCommand("T_TX_TEST2", 1, "1to2Setup")
			);
			cmd2.Add(
				DbCommandUtils.CreateSelectForUpdateSingleCommand("T_TX_TEST2", 1),
				new BarrierCommand(barrier),
				DbCommandUtils.CreateUpdateCommand("T_TX_TEST", 1, "2to1Setup")
			);
		}

		[Test]
		[Category("Database")]
		public void SelectUpdateNoLockTest() {
			var barrier = new Barrier(2);
			var cmd1 = new CommandChain();
			var cmd2 = new CommandChain();
			var executor = new ParallelExecution<DbCommandContext> {
				Builder = new DbCommandServiceBuilder {
					MaxRetry = 0,
					TxCoordination = TxCoordination.None,
				}
			};
			SetupSelectAndUpdate(barrier, cmd1, cmd2);
			executor.Add(cmd1, cmd2);
			executor.Execute();

			// Should not cause exception because isolation level is not coordinated.
			// (This would cause data inconsistency).
			Assert.IsNull(executor.ErrorFor(cmd1), "Should not cause exception.");
			Assert.IsNull(executor.ErrorFor(cmd2), "Should not cause exception.");
		}

		[Test]
		public void IsolationCoordinateTest() {
			var builder = new CommandServiceBuilder();
			var service = builder.Build();

			var chain = new SimpleCommandChain<CommandContext>();
			var rootCmd = new DumbCommand {
				TxScope = TxScope.Required,
				Child = chain,
			};

			var child01 = new DumbCommand {
				TxIntention = TxIntention.Select,
			};

			var child02 = new DumbCommand {
				TxScope = TxScope.RequiresNew,
				TxIntention = TxIntention.Select,
				Child = new DumbCommand {
					TxScope = TxScope.Required,
					TxIntention = TxIntention.Delete,
				}
			};

			var child03 = new DumbCommand {
				TxScope = TxScope.RequiresNew,
				TxIntention = TxIntention.Select,
				Child = new DumbCommand {
					TxScope = TxScope.Required,
					TxIntention = TxIntention.Update,
				}
			};

			var child04 = new DumbCommand {
				TxScope = TxScope.Suppress,
				TxIntention = TxIntention.Select,
				Child = new DumbCommand {
					TxScope = TxScope.Required,
					TxIntention = TxIntention.Update,
				}
			};

			chain.Add(child01, child02, child03, child04);
			service.ExecuteCommand(rootCmd);

			Assert.AreEqual(TxIsolation.ReadCommitted, child01.IsolationLevel);
			Assert.AreEqual(TxIsolation.Serializable, child02.IsolationLevel);
			Assert.AreEqual(TxIsolation.RepeatableRead, child03.IsolationLevel);
			Assert.AreEqual(TxIsolation.ReadCommitted, child04.IsolationLevel);
		}

		[Test]
		public void IsolationCoordinateSnapshotTest() {
			var builder = new CommandServiceBuilder {
				IsolationLevel = TxIsolation.Snapshot,
			};
			var service = builder.Build();

			var chain = new SimpleCommandChain<CommandContext>();
			var rootCmd = new DumbCommand {
				TxScope = TxScope.Required,
				Child = chain,
			};

			var child01 = new DumbCommand {
				TxIntention = TxIntention.Select,
			};

			var child02 = new DumbCommand {
				TxScope = TxScope.RequiresNew,
				TxIntention = TxIntention.Select,
				Child = new DumbCommand {
					TxScope = TxScope.Required,
					TxIntention = TxIntention.Delete,
				}
			};

			var child03 = new DumbCommand {
				TxScope = TxScope.RequiresNew,
				TxIntention = TxIntention.Select,
				Child = new DumbCommand {
					TxScope = TxScope.Required,
					TxIntention = TxIntention.Update,
				}
			};

			var child04 = new DumbCommand {
				TxScope = TxScope.Suppress,
				TxIntention = TxIntention.Select,
				Child = new DumbCommand {
					TxScope = TxScope.Required,
					TxIntention = TxIntention.Update,
				}
			};

			chain.Add(child01, child02, child03, child04);
			service.ExecuteCommand(rootCmd);

			Assert.AreEqual(TxIsolation.Snapshot, child01.IsolationLevel);
			Assert.AreEqual(TxIsolation.Snapshot, child02.IsolationLevel);
			Assert.AreEqual(TxIsolation.Snapshot, child03.IsolationLevel);
			Assert.AreEqual(TxIsolation.Snapshot, child04.IsolationLevel);
		}

		[Test]
		public void IsolationCoordinateReadUncommittedTest() {
			var builder = new CommandServiceBuilder {
				IsolationLevel = TxIsolation.ReadUncommitted,
			};
			var service = builder.Build();

			var chain = new SimpleCommandChain<CommandContext>();
			var rootCmd = new DumbCommand {
				TxScope = TxScope.Required,
				Child = chain,
			};

			var child01 = new DumbCommand {
				TxIntention = TxIntention.Select,
			};

			var child02 = new DumbCommand {
				TxScope = TxScope.RequiresNew,
				TxIntention = TxIntention.Select,
				Child = new DumbCommand {
					TxScope = TxScope.Required,
					TxIntention = TxIntention.Delete,
				}
			};

			var child03 = new DumbCommand {
				TxScope = TxScope.RequiresNew,
				TxIntention = TxIntention.Select,
				Child = new DumbCommand {
					TxScope = TxScope.Required,
					TxIntention = TxIntention.SelectForUpdate,
				}
			};

			var child04 = new DumbCommand {
				TxScope = TxScope.RequiresNew,
				TxIntention = TxIntention.Select,
				Child = new DumbCommand {
					TxScope = TxScope.Required,
					TxIntention = TxIntention.Update,
				}
			};

			var child05 = new DumbCommand {
				TxScope = TxScope.RequiresNew,
				TxIntention = TxIntention.Select,
				Child = new DumbCommand {
					TxScope = TxScope.Required,
					TxIntention = TxIntention.Insert,
				}
			};

			chain.Add(child01, child02, child03, child04, child05);
			service.ExecuteCommand(rootCmd);

			Assert.AreEqual(TxIsolation.ReadUncommitted, child01.IsolationLevel);
			Assert.AreEqual(TxIsolation.Serializable, child02.IsolationLevel);
			Assert.AreEqual(TxIsolation.ReadUncommitted, child03.IsolationLevel);
			Assert.AreEqual(TxIsolation.RepeatableRead, child04.IsolationLevel);
			Assert.AreEqual(TxIsolation.Serializable, child05.IsolationLevel);
		}

		[Test]
		public void IsolationCoordinateStrictTest() {
			var builder = new CommandServiceBuilder {
				TxCoordination = TxCoordination.Strict,
			};
			var service = builder.Build();

			var chain = new SimpleCommandChain<CommandContext>();
			var rootCmd = new DumbCommand {
				TxScope = TxScope.Required,
				Child = chain,
			};

			var child01 = new DumbCommand {
				TxIntention = TxIntention.Select,
			};

			var child02 = new DumbCommand {
				TxScope = TxScope.RequiresNew,
				TxIntention = TxIntention.Select,
				Child = new DumbCommand {
					TxScope = TxScope.Required,
					TxIntention = TxIntention.Delete,
				}
			};

			var child03 = new DumbCommand {
				TxScope = TxScope.RequiresNew,
				TxIntention = TxIntention.Select,
				Child = new DumbCommand {
					TxScope = TxScope.Required,
					TxIntention = TxIntention.SelectForUpdate,
				}
			};

			var child04 = new DumbCommand {
				TxScope = TxScope.RequiresNew,
				TxIntention = TxIntention.SelectForUpdate,
				Child = new DumbCommand {
					TxScope = TxScope.Required,
					TxIntention = TxIntention.SelectForUpdate,
				}
			};

			var child05 = new DumbCommand {
				TxScope = TxScope.RequiresNew,
				TxIntention = TxIntention.Select,
				Child = new DumbCommand {
					TxScope = TxScope.Required,
					TxIntention = TxIntention.Select,
				}
			};

			chain.Add(child01, child02, child03, child04, child05);
			service.ExecuteCommand(rootCmd);

			Assert.AreEqual(TxIsolation.ReadCommitted, child01.IsolationLevel);
			Assert.AreEqual(TxIsolation.Serializable, child02.IsolationLevel);
			Assert.AreEqual(TxIsolation.RepeatableRead, child03.IsolationLevel);
			Assert.AreEqual(TxIsolation.ReadCommitted, child04.IsolationLevel);
			Assert.AreEqual(TxIsolation.RepeatableRead, child05.IsolationLevel);
		}
	}
}
