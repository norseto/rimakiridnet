// ==============================================================================
//     Test.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Diagnostics.CodeAnalysis;
using Net.Rimakiri.Command;
using NUnit.Framework;

namespace Test.Rimakiri.Command {
	[ExcludeFromCodeCoverage]
	public class MarkTest : ICommand {

		public bool Mark { get; set; }
		public bool BeforeMarked { get; set; }
		public bool AfterMarked { get; set; }
		public ICommand Child { get; set; }

		#region Implementation of IGenericCommand<in CommandContext>

		public TxScope TxScope { get; set; }
		public Idempotent Idempotent { get; set; }

		public void Execute(CommandContext context) {
			BeforeMarked = context.IsMarkedRollback;
			if (Mark) {
				context.MarkRollbackOnly();
			}
			if (Child != null) {
				context.Service.ExecuteCommand(Child);
			}
			AfterMarked = context.IsMarkedRollback;
		}

		public void PostExecute(CommandContext context) {
			// Empty
		}
		public void PreExecute(CommandContext context) {
			// Empty
		}

		#endregion
	}

	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestRollbackOnly {
		[Test]
		public void NoMarkTest() {
			var service = new CommandServiceBuilder().Build();
			var cmd = new MarkTest();

			Assert.IsFalse(cmd.BeforeMarked);
			Assert.IsFalse(cmd.AfterMarked);
			service.ExecuteCommand(cmd);
			Assert.IsFalse(cmd.BeforeMarked);
			Assert.IsFalse(cmd.AfterMarked);
		}

		[Test]
		public void MarkTest() {
			var service = new CommandServiceBuilder().Build();
			var cmd = new MarkTest {Mark = true};

			Assert.IsFalse(cmd.BeforeMarked);
			Assert.IsFalse(cmd.AfterMarked);
			service.ExecuteCommand(cmd);
			Assert.IsFalse(cmd.BeforeMarked);
			Assert.IsTrue(cmd.AfterMarked);
		}

		/// <summary>
		/// Requiredの場合には親子関係で同一スコープを使用する
		/// したがって、ステータスも同じになる
		/// </summary>
		[Test]
		public void MarkFlatTest() {
			var service = new CommandServiceBuilder().Build();
			var child = new MarkTest { Mark = true };
			var parent = new MarkTest {Child = child};

			Assert.IsFalse(child.BeforeMarked);
			Assert.IsFalse(child.AfterMarked);
			Assert.IsFalse(parent.BeforeMarked);
			Assert.IsFalse(parent.AfterMarked);

			service.ExecuteCommand(parent);

			Assert.IsFalse(child.BeforeMarked);
			Assert.IsTrue(child.AfterMarked);
			Assert.IsFalse(parent.BeforeMarked);
			Assert.IsTrue(parent.AfterMarked);
		}

		[Test]
		public void MarkFlatParentTest() {
			var service = new CommandServiceBuilder().Build();
			var child = new MarkTest();
			var parent = new MarkTest { Child = child, Mark = true };

			Assert.IsFalse(child.BeforeMarked);
			Assert.IsFalse(child.AfterMarked);
			Assert.IsFalse(parent.BeforeMarked);
			Assert.IsFalse(parent.AfterMarked);

			service.ExecuteCommand(parent);

			Assert.IsTrue(child.BeforeMarked);
			Assert.IsTrue(child.AfterMarked);
			Assert.IsFalse(parent.BeforeMarked);
			Assert.IsTrue(parent.AfterMarked);
		}

		/// <summary>
		/// RequiresNewの場合には親子関係で別スコープを使用する
		/// したがって、ステータスは異なる
		/// </summary>
		[Test]
		public void MarkNestedTest() {
			var service = new CommandServiceBuilder().Build();
			var child = new MarkTest { Mark = true, TxScope = TxScope.RequiresNew };
			var parent = new MarkTest { Child = child };

			Assert.IsFalse(child.BeforeMarked);
			Assert.IsFalse(child.AfterMarked);
			Assert.IsFalse(parent.BeforeMarked);
			Assert.IsFalse(parent.AfterMarked);

			service.ExecuteCommand(parent);

			Assert.IsFalse(child.BeforeMarked);
			Assert.IsTrue(child.AfterMarked);
			Assert.IsFalse(parent.BeforeMarked);
			Assert.IsFalse(parent.AfterMarked);
		}

		[Test]
		public void MarkNestedParentTest() {
			var service = new CommandServiceBuilder().Build();
			var child = new MarkTest { TxScope = TxScope.RequiresNew };
			var parent = new MarkTest { Mark = true, Child = child };

			Assert.IsFalse(child.BeforeMarked);
			Assert.IsFalse(child.AfterMarked);
			Assert.IsFalse(parent.BeforeMarked);
			Assert.IsFalse(parent.AfterMarked);

			service.ExecuteCommand(parent);

			Assert.IsFalse(child.BeforeMarked);
			Assert.IsFalse(child.AfterMarked);
			Assert.IsFalse(parent.BeforeMarked);
			Assert.IsTrue(parent.AfterMarked);
		}


		/// <summary>
		/// Suppressの場合には親子関係で別スコープを使用する
		/// したがって、ステータスは異なる
		/// </summary>
		[Test]
		public void MarkNestedSuppressTest() {
			var service = new CommandServiceBuilder().Build();
			var child = new MarkTest { Mark = true, TxScope = TxScope.Suppress };
			var parent = new MarkTest { Child = child };

			Assert.IsFalse(child.BeforeMarked);
			Assert.IsFalse(child.AfterMarked);
			Assert.IsFalse(parent.BeforeMarked);
			Assert.IsFalse(parent.AfterMarked);

			service.ExecuteCommand(parent);

			Assert.IsFalse(child.BeforeMarked);
			Assert.IsTrue(child.AfterMarked);
			Assert.IsFalse(parent.BeforeMarked);
			Assert.IsFalse(parent.AfterMarked);
		}

		[Test]
		public void MarkNestedSuppressParentTest() {
			var service = new CommandServiceBuilder().Build();
			var child = new MarkTest { TxScope = TxScope.Suppress };
			var parent = new MarkTest { Mark = true, Child = child };

			Assert.IsFalse(child.BeforeMarked);
			Assert.IsFalse(child.AfterMarked);
			Assert.IsFalse(parent.BeforeMarked);
			Assert.IsFalse(parent.AfterMarked);

			service.ExecuteCommand(parent);

			Assert.IsFalse(child.BeforeMarked);
			Assert.IsFalse(child.AfterMarked);
			Assert.IsFalse(parent.BeforeMarked);
			Assert.IsTrue(parent.AfterMarked);
		}

	}
}
