// ==============================================================================
//     Test.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

#if (NET40 || NET45 || NET451 || NET452 || NET46 || NET461 || NET462 || NET47 || NET471 || NET472)
#define NETFRAMEWORK
#endif

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Transactions;
using Net.Rimakiri.Command;
using IsolationLevel = System.Transactions.IsolationLevel;

#if (NETFRAMEWORK)
using Oracle.ManagedDataAccess.Client;
using System.Data.OleDb;
#endif

namespace Test.Rimakiri.Command {
	/// <summary>
	/// DBテスト用コンテキスト
	/// </summary>
	[ExcludeFromCodeCoverage]
	// ReSharper disable once ClassNeverInstantiated.Global
	public class DbCommandContext : CommandContext {
		private const string ConnectionKey = "__ConnectionKey";
		public string ConnectionString { get; set; }

		protected DbConnection GetConnection() {
#if (NETFRAMEWORK)
			var con =  ConnectionString.Contains("Provider=") ? GetOleDbConnection()
					: ConnectionString.Contains("ORCL") ? GetOracleDbConnection()
					: GetSqlServerConnection();
#else
//			var con =  ConnectionString.Contains("ORCL") ? GetOracleDbConnection()
//						: GetSqlServerConnection();
			var con =  GetSqlServerConnection();
#endif
			EnlistTx(con);
			return con;
		}

#if (NETFRAMEWORK)
		protected DbConnection GetOleDbConnection() {
			var con = GetTxAttribute<DbConnection>(ConnectionKey);
			if (con == null) {
				con = new OleDbConnection(ConnectionString);
				SetTxAttribute(ConnectionKey, con);
			}
			if (con.State == ConnectionState.Closed) {
				con.Open();
			}
			return con;
		}
		
		protected DbConnection GetOracleDbConnection() {
			var con = GetTxAttribute<DbConnection>(ConnectionKey);
			if (con == null) {
				con = new OracleConnection(ConnectionString);
				SetTxAttribute(ConnectionKey, con);
			}
			if (con.State == ConnectionState.Closed) {
				con.Open();
			}
			return con;
		}
#endif

		protected DbConnection GetSqlServerConnection() {
			var con = GetTxAttribute<DbConnection>(ConnectionKey);
			if (con == null) {
				con = new SqlConnection(ConnectionString);
				SetTxAttribute(ConnectionKey, con);
			}
			if (con.State == ConnectionState.Closed) {
				con.Open();
				SetupConnection(con);
			}
			return con;
		}

		private void SetupConnection(DbConnection con) {
			if (!(con is SqlConnection)) {
				return;
			}
			using (var cmd = con.CreateCommand()) {
				cmd.Connection = con;
				cmd.CommandText = "SET XACT_ABORT ON";
				cmd.ExecuteNonQuery();
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:")]
		public IDbCommand CreateSqlCommand(string commandText) {
			var con = GetConnection();
			var cmd = con.CreateCommand();
			cmd.Connection = GetConnection();
			cmd.CommandText = commandText;
			cmd.Transaction = GetDbTransaction();
			var remain = GetTimeoutRemainTicks();
			if (remain >= 0) {
				cmd.CommandTimeout = (int)(remain / TimeSpan.TicksPerSecond) + 1;
			}
			return cmd;
		}
	}

	/// <summary>
	/// DBテスト用コンテキストライフサイクルリスナ
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class DbContextLifecycleListener : IContextLifecycleListener<DbCommandContext> {
		private const string ConStr =
				"Persist Security Info=False;" +
				"User ID=foo;" +
				"Password=bar;" +
				"Initial Catalog=Work;" +
				"Server=DbServ";
//		private const string ConStr =
//			"Data Source=localhost;Initial Catalog=Work;Integrated Security=True;";

		#region Implementation of IContextLifecycleListener<in DbCommandContext>
		/// <inheritdoc />
		public void OnPostContextCreate(DbCommandContext context) {
			context.ConnectionString = ConStr;
		}
		/// <inheritdoc />
		public void OnPreContextDestroy(DbCommandContext context) {
			// Nothing to do.
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	public class DbCommandServiceBuilder : CommandServiceBuilder<DbCommandContext> {
		public DbCommandServiceBuilder() {
			ContextLifecycleListener = new DbContextLifecycleListener();
		}
	}

	public interface IDbExecution : ICommand<DbCommandContext> {
		IsolationLevel? IsolationLevelExecuted { get; }
	}

	[ExcludeFromCodeCoverage]
	public abstract class AbstractSqlCommand : IDbExecution {
		public const string KeyLastAffected = "_LastAffected";
		public const string KeyLastSelectionCont = "_LastSelectionCount";

		public IsolationLevel? IsolationLevelExecuted { get; private set; }
		public string Statement { get; set; }
		private readonly List<IDbDataParameter> parameters = new List<IDbDataParameter>();
		private TxScope txScope = TxScope.Required;

		public void AddParameter(params IDbDataParameter[] targets) {
			if (targets == null) {
				return;
			}
			foreach (var prm in targets.Where(it => it != null)) {
				parameters.Add(prm);
			}
		}

		#region Implementation of ICommand<in CommandContext>
		/// <inheritdoc />
		public TxScope TxScope {
			get { return txScope; }
			set { txScope = value; }
		}

		/// <inheritdoc />
		public Idempotent Idempotent {
			get { return Idempotent.Transactional; }
		}

		/// <inheritdoc />
		public void PreExecute(DbCommandContext context) {
			// Empty
		}

		/// <inheritdoc />
		public void PostExecute(DbCommandContext context) {
			// Empty
		}

		/// <inheritdoc />
		public abstract void Execute(DbCommandContext context);

		#endregion


		/// <summary>
		/// Setup SQL parameters.
		/// </summary>
		/// <param name="command">Database command.</param>
		protected virtual void SetParameters(IDbCommand command) {
			var targets = command.Parameters;
			parameters.ForEach(it => targets.Add(CloneParameter(command, it)));
		}

		protected virtual IDbDataParameter CloneParameter(IDbCommand command, IDbDataParameter origin) {
			var param = command.CreateParameter();
			param.ParameterName = origin.ParameterName;
			param.DbType = origin.DbType;
			param.Size = origin.Size;
			param.Scale = origin.Scale;
			param.Precision = origin.Precision;
			param.Value = origin.Value;
			param.Direction = origin.Direction;

			var parameter = param as SqlParameter;
			if (parameter == null || !(origin is SqlParameter)) {
				return param;
			}
			parameter.ResetSqlDbType();
			parameter.SqlDbType = ((SqlParameter)origin).SqlDbType;
			return param;
		}

		protected void KeepIsolationLevel(DbCommandContext context) {
			var current = Transaction.Current;
			if (current != null) {
				IsolationLevelExecuted = current.IsolationLevel;
				return;
			}
			if (context == null) {
				return;
			}
			System.Data.IsolationLevel? isolation = null;
			using (var cmd = context.CreateSqlCommand(null)) {
				var tx = cmd.Transaction;
				if (tx != null) {
					isolation = tx.IsolationLevel;
				}
			}
			if (isolation == null) {
				return;
			}
			IsolationLevel? levelOut = null;
			switch (isolation) {
			case System.Data.IsolationLevel.ReadCommitted:
				levelOut = IsolationLevel.ReadCommitted;
				break;
			case System.Data.IsolationLevel.RepeatableRead:
				levelOut = IsolationLevel.RepeatableRead;
				break;
			case System.Data.IsolationLevel.Serializable:
				levelOut = IsolationLevel.Serializable;
				break;
			case System.Data.IsolationLevel.Snapshot:
				levelOut = IsolationLevel.Snapshot;
				break;
			case System.Data.IsolationLevel.Unspecified:
				levelOut = IsolationLevel.Unspecified;
				break;
			}
			IsolationLevelExecuted = levelOut;
		}
	}

	[Serializable]
	[ExcludeFromCodeCoverage]
	public class SampleException : Exception {
		// Empty
	}

	[ExcludeFromCodeCoverage]
	// ReSharper disable once UnusedMember.Global
	public class ThrowCommand : AbstractSqlCommand {
		#region Overrides of AbstractSqlCommand
		public override void Execute(DbCommandContext context) {
			throw new SampleException();
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	// ReSharper disable once UnusedMember.Global
	public class SleepCommand : AbstractSqlCommand {
		private readonly TimeSpan sleep;

		public SleepCommand() : this(TimeSpan.FromSeconds(15)) {
			// Empty
		}

		public SleepCommand(TimeSpan sleep) {
			KeepIsolationLevel(null);
			this.sleep = sleep;
		}
		#region Overrides of AbstractSqlCommand
		public override void Execute(DbCommandContext context) {
			Thread.Sleep(sleep);
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	public class BarrierCommand : AbstractSqlCommand, ICommandVisitAcceptor {
		private readonly Barrier barrier;
		private readonly long phaseNumber;

		public BarrierCommand(Barrier barrier) {
			this.barrier = barrier;
			phaseNumber = barrier.CurrentPhaseNumber;
		}

		#region Overrides of AbstractSqlCommand
		public override void Execute(DbCommandContext context) {
			KeepIsolationLevel(context);
			long timeout = context.GetTimeoutRemainTicks();
			if (timeout != 0 && barrier.ParticipantsRemaining > 0
				&& barrier.CurrentPhaseNumber == phaseNumber) {
				if (timeout < 0) {
					barrier.SignalAndWait();
				}
				double mills = Math.Min((double)10 * 1000,
					(double)timeout / TimeSpan.TicksPerMillisecond);
				barrier.SignalAndWait(TimeSpan.FromMilliseconds(mills));
			}
		}
		#endregion

		#region Implementation of ICommandVisitAcceptor
		public void AcceptVisitor(ICommandVisitor visitor) {
			visitor.Visit(this);
			visitor.Visit((BarrierCommand)null);
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	public class CommandChain : AbstractCommandChain<DbCommandContext> {
		private Idempotent idempotent = Idempotent.Transactional;
		private TxScope txScope = TxScope.Required;

		#region Overrides of AbstractCommandChain<DbCommandContext>
		public override Idempotent Idempotent {
			get { return idempotent; }
			set { idempotent = value; }
		}

		public override TxScope TxScope {
			get { return txScope; }
			set { txScope = value; }
		}
		#endregion

		public CommandChain(params ICommand<DbCommandContext>[] commands)
			: base(commands) {
		}

		public CommandChain() {
			// Empty
		}
	}

	[ExcludeFromCodeCoverage]
	public abstract class AbstractSqlSelectCommand : AbstractSqlCommand {
		#region Implementation of ICommand<in CommandContext>
		/// <inheritdoc />
		public override void Execute(DbCommandContext context) {
			KeepIsolationLevel(context);
			using (var sqlcmd = context.CreateSqlCommand(Statement)) {
				SetParameters(sqlcmd);
				using (var reader = sqlcmd.ExecuteReader()) {
					DoInReader(context, reader);
				}
			}
		}
		#endregion

		protected abstract void DoInReader(DbCommandContext context, IDataReader reader);
	}

	[ExcludeFromCodeCoverage]
	public class AbstractNonQueryCommand : AbstractSqlCommand {
		public int Affected { get; private set; }

		#region Overrides of AbstractSqlCommand
		public override void Execute(DbCommandContext context) {
			KeepIsolationLevel(context);
			using (var sqlcmd = context.CreateSqlCommand(Statement)) {
				SetParameters(sqlcmd);
				Affected = sqlcmd.ExecuteNonQuery();
				if (Affected > 1) {
					throw new InvalidOperationException("Affected multiple records.");
				}
				context.SetAttribute(KeyLastAffected, Affected);
			}
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	public class SimpleSelectCommand : AbstractSqlSelectCommand {
		public int ResultCount { get; private set; }
		public IList<IDictionary<string, object>> Results { get; private set; }

		#region Overrides of AbstractSqlSelectCommand
		/// <inheritdoc />
		protected override void DoInReader(DbCommandContext context, IDataReader reader) {
			ResultCount = 0;
			var rslts = new List<IDictionary<string, object>>();
			while (reader.Read()) {
				ResultCount++;
				var dic = new Dictionary<string, object>();
				for (int i = 0, l = reader.FieldCount; i < l; i++) {
					var value = reader.IsDBNull(i) ? null : reader.GetValue(i);
					dic[reader.GetName(i)] = value;
				}
				rslts.Add(dic);
			}
			context.SetAttribute(KeyLastSelectionCont, ResultCount);
			Results = rslts.AsReadOnly();
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	public class NonQueryCommand : AbstractNonQueryCommand {
		private string paramPrefix = "@";
		public int Id { get; set; }
		public string Name { get; set; }

		public string ParamPrefix {
			get { return paramPrefix; }
			set { paramPrefix = value; }
		}

		#region Overrides of AbstractSqlCommand
		protected override void SetParameters(IDbCommand command) {
			var parameters = command.Parameters;
			parameters.Add(CloneParameter(command,
				new SqlParameter(ParamPrefix + "id", SqlDbType.Int) { Value = Id }));
			parameters.Add(CloneParameter(command,
				new SqlParameter(ParamPrefix + "name", SqlDbType.NVarChar) { Value = Name }));
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	public class DumbCommand : ICommand, IIsolationCoodinatee {
		private TxIsolation isolationLevel = TxIsolation.Unspecified;
		private IsolationLevel isolationLevelExecuted = System.Transactions.IsolationLevel.Unspecified;

		public TxIsolation IsolationLevel {
			get { return isolationLevel; }
			private set { isolationLevel = value; }
		}

		public IsolationLevel IsolationLevelExecuted {
			get { return isolationLevelExecuted; }
			private set { isolationLevelExecuted = value; }
		}

		public ICommand<CommandContext> Child { get; set; }

		#region Implementation of ICommand<in CommandContext>
		public TxScope TxScope { get; set; }

		public Idempotent Idempotent {
			get { return Idempotent.None; }
		}

		public void Execute(CommandContext context) {
			var currentTx = Transaction.Current;
			if (currentTx != null) {
				isolationLevelExecuted = currentTx.IsolationLevel;
			}
			IsolationLevel = context.TxIsolation;
			if (Child != null) {
				context.Service.ExecuteCommand(Child);
			}
		}

		public void PostExecute(CommandContext context) {
			// Empty
		}

		public void PreExecute(CommandContext context) {
			// Empty
		}
		#endregion

		#region Implementation of ICommandVisitAcceptor
		public void AcceptVisitor(ICommandVisitor visitor) {
			visitor.Visit(this);
			visitor.PropagateChild(Child);
		}
		#endregion

		#region Implementation of IIsolationCoodinatee
		public TxIntention TxIntention { get; set; }
		#endregion
	}
	[ExcludeFromCodeCoverage]
	public class CoordSelectCommand : SimpleSelectCommand, IIsolationCoodinatee {
		private readonly TxIntention intention;

		public CoordSelectCommand() : this(TxIntention.Select) {
			// Empty.
		}

		public CoordSelectCommand(TxIntention intention) {
			this.intention = intention;
		}
		#region Implementation of IIsolationCoodinatee
		public void AcceptVisitor(ICommandVisitor visitor) {
			visitor.Visit(this);
		}

		public TxIntention TxIntention {
			get { return intention; }
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	public class CoordNonQueryCommand : AbstractNonQueryCommand, IIsolationCoodinatee {
		private readonly TxIntention intention;

		public CoordNonQueryCommand() : this(TxIntention.Update) {
			// Empty
		}

		public CoordNonQueryCommand(TxIntention intention) {
			this.intention = intention;
		}

		#region Implementation of IIsolationCoodinatee
		public void AcceptVisitor(ICommandVisitor visitor) {
			visitor.Visit(this);
		}

		public TxIntention TxIntention {
			get { return intention; }
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	public static class DbCommandUtils {
		private static string CheckTableName(string tableName) {
			var table = tableName.ToUpper(CultureInfo.InvariantCulture);
			switch (table) {
			case "T_TX_TEST":
			case "T_TX_TEST2":
				break;
			default:
				throw new InvalidOperationException("Invalid table: " + tableName);
			}
			return table;
		}

		public static AbstractNonQueryCommand CreateDeleteCommand(string tableName, int id) {
			var table = CheckTableName(tableName);
			var delcmd = new CoordNonQueryCommand(TxIntention.Delete) {
				Statement = string.Format("DELETE FROM {0} WHERE ID = @id", table)
			};
			delcmd.AddParameter(new SqlParameter("@id", SqlDbType.Int) { Value = id });
			return delcmd;
		}

		public static AbstractNonQueryCommand CreateSafeInsertCommand(string tableName, int id, string name) {
			var table = CheckTableName(tableName);
			var cmd = new CoordNonQueryCommand {
				Statement = string.Format("INSERT INTO {0} (ID, NAME) "
										+ "SELECT @id, @name WHERE NOT EXISTS ("
										+ "SELECT 'X' FROM {0} WHERE ID = @id)", table),
			};
			cmd.AddParameter(new SqlParameter("@id", SqlDbType.Int) { Value = id });
			cmd.AddParameter(new SqlParameter("@name", SqlDbType.NVarChar) { Value = name });
			return cmd;
		}

		public static AbstractNonQueryCommand CreateDumbInsertCommand(string tableName, int id, string name) {
			var table = CheckTableName(tableName);
			var cmd = new CoordNonQueryCommand(TxIntention.Insert) {
				Statement = string.Format("INSERT INTO {0} (ID, NAME) VALUES (@id, @name) ", table),
			};
			cmd.AddParameter(new SqlParameter("@id", SqlDbType.Int) { Value = id });
			cmd.AddParameter(new SqlParameter("@name", SqlDbType.NVarChar) { Value = name });
			return cmd;
		}

		public static AbstractNonQueryCommand CreateUpdateCommand(string tableName, int id, string name) {
			var table = CheckTableName(tableName);
			var cmd = new CoordNonQueryCommand {
				Statement = string.Format("UPDATE {0} SET NAME = @name WHERE ID = @id", table),
			};
			cmd.AddParameter(new SqlParameter("@id", SqlDbType.Int) { Value = id });
			cmd.AddParameter(new SqlParameter("@name", SqlDbType.NVarChar) { Value = name });
			return cmd;
		}

		public static SimpleSelectCommand CreateSelectSingleCommand(string tableName, int id) {
			var table = CheckTableName(tableName);
			var cmd = new CoordSelectCommand {
				Statement = string.Format("SELECT * FROM {0} WHERE ID = @id", table),
			};
			cmd.AddParameter(new SqlParameter("@id", SqlDbType.Int) { Value = id });
			return cmd;
		}

		public static SimpleSelectCommand CreateSelectForUpdateSingleCommand(string tableName, int id) {
			var table = CheckTableName(tableName);
			var cmd = new CoordSelectCommand(TxIntention.SelectForUpdate) {
				Statement = string.Format("SELECT * FROM {0} WITH(UPDLOCK) WHERE ID = @id", table),
			};
			cmd.AddParameter(new SqlParameter("@id", SqlDbType.Int) { Value = id });
			return cmd;
		}
	}

	[ExcludeFromCodeCoverage]
	public class SelectionCountFilter : AbstractSqlCommand {
		private readonly int checkValue;

		public SelectionCountFilter(int checkValue) {
			this.checkValue = checkValue;
		}

		#region Overrides of AbstractSqlCommand
		public override void Execute(DbCommandContext context) {
			if (context.GetAttribute<int>(KeyLastSelectionCont) == checkValue) {
				throw new InvalidOperationException("Last selection: " + checkValue);
			}
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	public class ParallelExecution<T> where T: CommandContext {
		private readonly List<ICommand<T>> commands = new List<ICommand<T>>();
		public CommandServiceBuilder<T> Builder { get; set; }
		private readonly IDictionary<ICommand<T>, Exception> errors
			= new Dictionary<ICommand<T>,Exception>();

		public Exception ErrorFor(ICommand<T> command) {
			Exception ex;
			return errors.TryGetValue(command, out ex) ? ex : null;
		}

		public void Add(params ICommand<T>[] command) {
			if (command == null) {
				return;
			}
			foreach (var cmd in command.Where(it => it != null)) {
				commands.Add(cmd);
			}
		}

		public ICommand<T> this[int index] {
			get { return commands[index]; }
		}

		public void Execute(Func<ICommand<T>, CommandServiceBuilder<T>> builderGetter = null) {
			var targets = new List<ICommand<T>>(commands);
			if (targets.Count < 1) {
				return;
			}
			var threads = new List<Thread>();
			foreach (var cmd in targets) {
				var target = cmd;
				var builder = (CommandServiceBuilder<T>)null;
				if (builderGetter != null) {
					builder = builderGetter(cmd);
				}
				if (builder == null) {
					builder = Builder;
				}
				threads.Add(new Thread(() => { ExecuteInThread(builder, target); }));
			}
			foreach (var thread in threads) {
				thread.Start();
			}
			foreach (var thread in threads) {
				thread.Join();
			}
		}

		private void ExecuteInThread(ICommandServiceBuilder<T> builder, ICommand<T> target) {
			try {
				var service = builder.Build();
				service.ExecuteCommand(target);
			}
			catch (Exception ex) {
				errors[target] = ex;
			}
		}
	}

	[ExcludeFromCodeCoverage]
	internal class UnconditionalRetryPolicy : IRetryPolicy {
		#region Implementation of IRetryPolicy
		public bool IsRetryable(Exception error, int tryCount) {
			return true;
		}
		#endregion
	}
}

