// ==============================================================================
//     Test.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using NUnit.Framework;
using Net.Rimakiri.Command;
using Net.Rimakiri.Command.Spi;

namespace Test.Rimakiri.Command {
	[ExcludeFromCodeCoverage]
	public class HeuristicTimeoutCheckingCommand : ICommand {
		public int Sleep { get; set; }
		public ICommand Child { get; set; }

		#region Implementation of IGenericCommand<in CommandContext>

		public void Execute(CommandContext context) {
			var remain = TimeSpan.FromSeconds(Sleep).Ticks;
			while (remain > 0) {
				var before = DateTime.Now.Ticks;
				var ticks = Math.Min(TimeSpan.TicksPerSecond / 10, remain);
				var millis = ticks / TimeSpan.TicksPerMillisecond;
				Thread.Sleep((int)millis);
				context.CheckTimeout(GetType().FullName);

				var after = DateTime.Now.Ticks;
				remain -= (after - before);
			}
			if (Child != null) {
				context.Service.ExecuteCommand(Child);
			}
		}

		public Idempotent Idempotent { get; set; }
		public TxScope TxScope { get; set; }

		public void PostExecute(CommandContext context) {
			// Empty
		}
		public void PreExecute(CommandContext context) {
			// Empty
		}

		#endregion
	}

	/// <summary>
	/// タイムアウトのテスト
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestTimeout {
		[Test]
		[Category("Timeout")]
		public void MinimumTimeoutTest() {
			var service = new CommandServiceBuilder {
				Timeout = TimeSpan.FromSeconds(3),
			}.Build();
			var child = new CommandTestImpl {
				Sleep = 4
			};
			var parent = new CommandTestImpl {
				Sleep = 4,
				ExecCmd = child,
				TxScope = TxScope.Required,
			};

			var sw = new Stopwatch();
			sw.Start();
			try {
				service.ExecuteCommand(parent);
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (TimeoutException) {
				Assert.IsTrue(true);
			}
			sw.Stop();
			Assert.IsTrue(sw.ElapsedMilliseconds < 5000,
				"Invalid actual Timeout. Actual elapsed: " + sw.ElapsedMilliseconds);
		}

		[Test]
		[Category("Timeout")]
		public void HeuristicMinimumTimeoutTest() {
			var service = new CommandServiceBuilder {
				Timeout = TimeSpan.FromSeconds(3),
			}.Build();
			var child = new HeuristicTimeoutCheckingCommand {
				Sleep = 2
			};
			var parent = new HeuristicTimeoutCheckingCommand {
				Sleep = 4,
				Child = child,
				TxScope = TxScope.Required,
			};

			var sw = new Stopwatch();
			sw.Start();
			try {
				service.ExecuteCommand(parent);
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (HeuristicTimeoutException) {
				Assert.IsTrue(true);
			}
			sw.Stop();
			Assert.IsTrue(sw.ElapsedMilliseconds < 3500,
				"Invalid actual Timeout. Actual elapsed: " + sw.ElapsedMilliseconds);
		}

		[Test]
		[Category("Timeout")]
		public void DefaultTimeoutTest() {
			var service = new CommandServiceBuilder().Build();

			var sw = new Stopwatch();
			sw.Start();
			try {
				service.ExecuteCommand(new CommandTestImpl {
					Sleep = 61,
					TxScope = TxScope.Required,
				});
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (TimeoutException) {
				Assert.IsTrue(true);
			}
			sw.Stop();
			Assert.IsTrue(sw.ElapsedMilliseconds >= 60000 && sw.ElapsedMilliseconds < 62000,
				"Invalid actual Timeout. Actual elapsed: " + sw.ElapsedMilliseconds);
		}

		[Test]
		[Category("Timeout")]
		public void NoTimeoutTest() {
			var service = new CommandServiceBuilder {
				Timeout = TimeSpan.FromSeconds(1),
			}.Build();

			// トランザクションなしで動作するため、タイムアウトは発生しない
			var cmd = new CommandTestImpl {
				Sleep = 3,
				TxScope = TxScope.Supports,
			};
			service.ExecuteCommand(cmd);
			Assert.IsNotNull(cmd.Post.Context);
		}

		[Test]
		[Category("Timeout")]
		public void NotIdempotentTimeoutTest() {
			var service = new CommandServiceBuilder {
				Timeout = TimeSpan.FromSeconds(5),
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = 5
				}
			}.Build();

			var grandChild = new IdempotentSleepCommand {
				Idempotent = Idempotent.Transactional
			};
			var child = new IdempotentSleepCommand {
				Idempotent = Idempotent.None,
				Child = grandChild,
			};
			var parent = new IdempotentSleepCommand {
				Idempotent = Idempotent.Transactional,
				Child = child,
				TxScope = TxScope.Required,
			};

			try {
				service.ExecuteCommand(parent);
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (TimeoutException) {
				Assert.IsTrue(true);
			}
			Assert.AreEqual(1, parent.Executed);
			Assert.AreEqual(1, child.Executed);
			Assert.AreEqual(1, grandChild.Executed);
		}

		[Test]
		[Category("Timeout")]
		public void IdempotentRetryTimeoutTest() {
			var service = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
				Timeout = TimeSpan.FromSeconds(4),
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = 5
				}
			}.Build();

			var grandChild = new IdempotentSleepCommand {
				Idempotent = Idempotent.Transactional
			};
			var child = new IdempotentSleepCommand {
				Idempotent = Idempotent.Transactional,
				Child = grandChild,
			};
			var parent = new IdempotentSleepCommand {
				Idempotent = Idempotent.Transactional,
				Child = child,
				TxScope = TxScope.Required,
			};

			service.ExecuteCommand(parent);

			Assert.AreEqual(3, parent.Executed);
			Assert.AreEqual(3, child.Executed);
			Assert.AreEqual(3, grandChild.Executed);
		}

		/// <summary>
		/// タイムアウトを指定したビルド
		/// </summary>
		[Test]
		[Category("Timeout")]
		public void BuildWithTimeoutTest() {
			var target = new CommandServiceBuilder {
				Timeout = TimeSpan.FromSeconds(3),
			};
			var service = target.Build();
			Assert.IsNotNull(service);

			var cmd = new CommandTestImpl {TxScope = TxScope.Required};
			service.ExecuteCommand(cmd);

			try {
				service.ExecuteCommand(new CommandTestImpl {
					Sleep = 4,
					TxScope = TxScope.Required
				});
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (TimeoutException) {
				Assert.IsTrue(true);
			}
		}
	}
}
