// ==============================================================================
//     Test.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;
using Net.Rimakiri.Command;

namespace Test.Rimakiri.Command {
	[ExcludeFromCodeCoverage]
	public class ScopeAttrCommand : ICommand {
		public Action<CommandContext> Action { get; set; }

		#region Implementation of ICommand<in CommandContext>
		public TxScope TxScope { get; set; }

		public Idempotent Idempotent {
			get {return Idempotent.None;}
		}

		public void Execute(CommandContext context) {
			if (Action != null) {
				Action(context);
			}
		}

		public void PostExecute(CommandContext context) {
			// Empty
		}
		public void PreExecute(CommandContext context) {
			// Empty
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	public sealed class DisposeChecker : IDisposable {
		public bool Disposed { get; private set; }

		#region Implementation of IDisposable
		public void Dispose() {
			Disposed = true;
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class TestScopeAttribute {
		[Test]
		public void TestSimple() {
			const string key = "KEY";
			var value = new DisposeChecker();
			var builder = new CommandServiceBuilder();
			var service = builder.Build();

			var cmd = new ScopeAttrCommand {
				Action = context => {
					Assert.IsNull(context.GetTxAttribute<object>(key));
					context.SetTxAttribute(key, value);
					Assert.AreEqual(value, context.GetTxAttribute<IDisposable>(key));
				}
			};

			Assert.IsFalse(value.Disposed);
			service.ExecuteCommand(cmd);
			Assert.IsTrue(value.Disposed);
		}

		[Test]
		public void TestSimpleWithSupports() {
			const string key = "KEY";
			var value = new DisposeChecker();
			var builder = new CommandServiceBuilder();
			var service = builder.Build();

			var cmd = new ScopeAttrCommand {
				TxScope = TxScope.Supports,
				Action = context => {
					Assert.IsNull(context.GetTxAttribute<object>(key));
					context.SetTxAttribute(key, value);
					Assert.AreEqual(value, context.GetTxAttribute<IDisposable>(key));
				}
			};

			Assert.IsFalse(value.Disposed);
			service.ExecuteCommand(cmd);
			Assert.IsTrue(value.Disposed);
		}

		[Test]
		public void TestSharedScope() {
			const string key = "KEY";
			var value = new DisposeChecker();
			var builder = new CommandServiceBuilder();
			var service = builder.Build();

			var chain = new SimpleCommandChain<CommandContext>(TxScope.Required);
			var cmd1 = new ScopeAttrCommand {
				TxScope = TxScope.Supports,
				Action = context => {
					Assert.IsNull(context.GetTxAttribute<object>(key));
					context.SetTxAttribute(key, value);
					Assert.AreEqual(value, context.GetTxAttribute<IDisposable>(key));
				}
			};
			var cmd2 = new ScopeAttrCommand {
				TxScope = TxScope.Supports,
				Action = context => {
					Assert.AreEqual(value, context.GetTxAttribute<IDisposable>(key));
					context.SetTxAttribute(key, value);
				}
			};

			chain.Add(cmd1, cmd2);

			Assert.IsFalse(value.Disposed);
			service.ExecuteCommand(chain);
			Assert.IsTrue(value.Disposed);
		}

		[Test]
		public void TestSeparatedScope() {
			const string key = "KEY";
			var value = new DisposeChecker();
			var value2 = new DisposeChecker();
			var builder = new CommandServiceBuilder();
			var service = builder.Build();

			var chain = new SimpleCommandChain<CommandContext>(TxScope.Required);
			var cmd1 = new ScopeAttrCommand {
				TxScope = TxScope.Supports,
				Action = context => {
					Assert.IsNull(context.GetTxAttribute<object>(key));
					context.SetTxAttribute(key, value);
					Assert.AreEqual(value, context.GetTxAttribute<IDisposable>(key));
					Assert.AreEqual(value, context.RemoveTxAttribute(key));
					Assert.IsNull(context.RemoveTxAttribute(key));
					context.SetTxAttribute(key, value);
					Assert.AreEqual(value, context.GetTxAttribute<IDisposable>(key));
				}
			};
			var cmd2 = new ScopeAttrCommand {
				TxScope = TxScope.Suppress,
				Action = context => {
					Assert.IsNull(context.GetTxAttribute<object>(key));
					context.SetTxAttribute(key, value2);
					Assert.AreEqual(value2, context.GetTxAttribute<IDisposable>(key));
				}
			};
			var cmd3 = new ScopeAttrCommand {
				TxScope = TxScope.Supports,
				Action = context => {
					Assert.AreEqual(value, context.GetTxAttribute<IDisposable>(key));
				}
			};

			chain.Add(cmd1, cmd2, cmd3);

			Assert.IsFalse(value.Disposed);
			Assert.IsFalse(value2.Disposed);
			service.ExecuteCommand(chain);
			Assert.IsTrue(value.Disposed);
			Assert.IsTrue(value2.Disposed);
		}
	}
}
