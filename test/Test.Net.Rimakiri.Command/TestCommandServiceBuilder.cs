// ==============================================================================
//     Test.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Diagnostics.CodeAnalysis;
using System.Transactions;
using Net.Rimakiri.Command;
using Net.Rimakiri.Command.Spi;
using Net.Rimakiri.Command.Transactions;
using NUnit.Framework;

namespace Test.Rimakiri.Command {
	/// <summary>
	/// <see cref="TestCommandServiceBuilder"/>のテスト
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestCommandServiceBuilder {
		/// <summary>
		/// デフォルトのビルド
		/// </summary>
		[Test]
		public void BuildTest() {
			var target = new CommandServiceBuilder {TransactionFactory = new ScopeCommandTxFactory()};
			var service = target.Build();
			Assert.IsNotNull(service);

			var cmd = new CommandTestImpl {
				TxScope = TxScope.Required,
			};
			service.ExecuteCommand(cmd);

			Assert.IsNotNull(cmd.Exec);
			Assert.IsNotNull(cmd.Exec.Transaction);
			Assert.AreEqual(IsolationLevel.ReadCommitted, cmd.Exec.IsolationLevel);
		}

		/// <summary>
		/// デフォルトのビルド
		/// </summary>
		[Test]
		public void BuildInitializeTest() {
			var target = new CommandServiceBuilder {TransactionFactory = new ScopeCommandTxFactory()};
			target.InitializeForSingleton();
			var service = target.Build();
			Assert.IsNotNull(service);

			var cmd = new CommandTestImpl {
				TxScope = TxScope.Required,
			};
			service.ExecuteCommand(cmd);

			Assert.IsNotNull(cmd.Exec);
			Assert.IsNotNull(cmd.Exec.Transaction);
			Assert.AreEqual(IsolationLevel.ReadCommitted, cmd.Exec.IsolationLevel);
		}

		/// <summary>
		/// ライフサイクルリスナを指定したビルド
		/// </summary>
		[Test]
		public void BuildWithLifecycleTest() {
			var listener = new DummyContextListener<CommandContext>();
			var target = new CommandServiceBuilder {
				ContextLifecycleListener = listener,
			};
			var service = target.Build();
			Assert.IsNotNull(service);

			var cmd = new CommandTestImpl {
				TxScope = TxScope.Required,
			};
			service.ExecuteCommand(cmd);

			Assert.AreEqual(1, listener.PostCalled);
			Assert.AreEqual(1, listener.PreCalled);

			service = target.Build(listener.OuterPost);
			service.ExecuteCommand(cmd);
			Assert.AreEqual(3, listener.PostCalled);
			Assert.AreEqual(2, listener.PreCalled);

			service = target.Build(listener.OuterPost, listener.OuterPre);
			service.ExecuteCommand(cmd);
			Assert.AreEqual(5, listener.PostCalled);
			Assert.AreEqual(4, listener.PreCalled);
		}

		/// <summary>
		/// トランザクション分離レベルを指定したビルド
		/// </summary>
		[Test]
		public void BuildWithIsolationTest() {
			var target = new CommandServiceBuilder {
				IsolationLevel = TxIsolation.Serializable,
				TransactionFactory = new ScopeCommandTxFactory(),
			};
			var service = target.Build();
			Assert.IsNotNull(service);

			var cmd = new CommandTestImpl {
				TxScope = TxScope.Required,
			};
			service.ExecuteCommand(cmd);

			Assert.IsNotNull(cmd.Exec);
			Assert.IsNotNull(cmd.Exec.Transaction);
			Assert.AreEqual(IsolationLevel.Serializable, cmd.Exec.IsolationLevel);
		}

		/// <summary>
		/// コンテキストの型を指定したビルド
		/// </summary>
		[Test]
		public void BuildWithIllegalCtxFactory() {
			var target = new CommandServiceBuilder<OtherCustContext>();
			var service = target.Build();
			Assert.IsNotNull(service);

			var cmd = new CommandTestImpl();
			service.ExecuteCommand(cmd);

			try {
				service.ExecuteCommand(new CustCmd());
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (InvalidOperationException) {
				Assert.IsTrue(true);
			}
		}

		/// <summary>
		/// コンテキストの型を指定したビルド(継承)
		/// </summary>
		[Test]
		public void BuildWithContextInheritance()
		{
			var target = new CommandServiceBuilder<ChildContext>();
			var service = target.Build();
			Assert.IsNotNull(service);

			service.ExecuteCommand(new ChildCmd());
			// Parent can run because ChildContext is also ParentContext
			service.ExecuteCommand(new ParentCmd());
		}

		/// <summary>
		/// コンテキストの型を指定したビルド(継承)
		/// </summary>
		[Test]
		public void BuildWithContextCovariance()
		{
			ICommand<ParentContext> cmdParent = new ParentCmd();
			ICommand<ChildContext> cmdChild = new ChildCmd();
			ICommandServiceBuilder<ParentContext> bldParent = new CommandServiceBuilder<ParentContext>();
			ICommandServiceBuilder<ChildContext> bldChild = new CommandServiceBuilder<ChildContext>();

			// 次の行はコンパイルエラーとなる（ChildCmdはChildContextが必要）
			// ICommand<ParentContext> cmd1 = cmdChild;

			// 次の行はOK（ParentCmdにChildContextが渡されても問題ない）
			ICommand<ChildContext> cmd2 = cmdParent;

			// 次の行はOK（Parentを生成するビルダがChildを生成してもよい）
			ICommandServiceBuilder<ParentContext> bld1 = bldChild;

			// 次の行はコンパイルエラーとなる（Childを生成するビルダが
			// Parentを生成することは許されない）
			// ICommandServiceBuilder<ChildContext> bld2 = bldParent;

			var service = bld1.Build();

			service.ExecuteCommand(new ChildCmd());
			// Parent can run because ChildContext is also ParentContext
			service.ExecuteCommand(new ParentCmd());
		}

		/// <summary>
		/// コンテキストの型を指定したビルド(継承)
		/// </summary>
		[Test]
		public void BuildWithContextReverseInheritance() {
			var target = new CommandServiceBuilder<ParentContext>();
			var service = target.Build();
			Assert.IsNotNull(service);

			service.ExecuteCommand(new ParentCmd());
			try {
				// Child cannot run with parent context.
				service.ExecuteCommand(new ChildCmd());
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (InvalidOperationException) {
				Assert.IsTrue(true);
			}
		}

		/// <summary>
		/// コールバックのテスト
		/// </summary>
		[Test]
		public void TesContextCallback() {
			var target = new CommandServiceBuilder<ChildContext>();
			var value = 0;
			CommandContext ctx = null;
			var service = target.Build(postCreate: context => {
				value++;
				ctx = context;
			}, preDestroy: context => {
				value += 2;
				Assert.AreSame(ctx, context);
			});
			Assert.IsNotNull(service);

			var cmd = new CommandTestImpl();
			service.ExecuteCommand(cmd);

			Assert.AreEqual(3, value);
			Assert.IsNotNull(ctx);
			Assert.AreSame(cmd.Exec.Context, ctx);
		}

		/// <summary>
		/// コールバックのテスト
		/// </summary>
		[Test]
		public void TesContextListenerCallback()
		{
			var listener = new DummyListener();
			var target = new CommandServiceBuilder<ChildContext> {
				ContextLifecycleListener = listener,
			};
			var service = target.Build();
			Assert.IsNotNull(service);

			var cmd = new CommandTestImpl();
			service.ExecuteCommand(cmd);

			Assert.AreEqual(1, listener.PostCalled);
			Assert.AreEqual(1, listener.PreCalled);
		}

		/// <summary>
		/// SPIのテスト
		/// </summary>
		[Test]
		public void TesSpiCalled()
		{
			var serviceSpi = new DummySpi();
			var spiFactory = new DummySpiFactory();
			var target = new CommandServiceBuilder<ChildContext> {
				ServiceSpi = serviceSpi,
				ContextSpiFactory = spiFactory,
			};
			var service = target.Build();
			Assert.IsNotNull(service);

			Assert.AreEqual(0, serviceSpi.Called);
			Assert.AreEqual(0, spiFactory.Called);

			var cmd = new CommandTestImpl();
			service.ExecuteCommand(cmd);

			Assert.AreEqual(1, serviceSpi.Called);
			Assert.AreEqual(1, spiFactory.Called);
		}

		[ExcludeFromCodeCoverage]
		public class DummyListener : IContextLifecycleListener {
			private int postCalled;
			private int preCalled;

			public int PostCalled {
				get { return postCalled; }
			}
			public int PreCalled {
				get { return preCalled; }
			}

			#region Implementation of IContextLifecycleListener
			/// <inheritdoc />
			public void OnPostContextCreate(CommandContext context) {
				Assert.IsNotNull(context);
				postCalled++;
			}

			/// <inheritdoc />
			public void OnPreContextDestroy(CommandContext context) {
				Assert.IsNotNull(context);
				Assert.AreEqual(preCalled + 1, postCalled);
				preCalled++;
			}
			#endregion
		}

		[ExcludeFromCodeCoverage]
		public class DummySpi : DefaultServiceSpiImpl {
			private int called = 0;
			public int Called {
				get { return called; }
			}
			#region Overrides of DefaultServiceSpiImpl
			/// <inheritdoc />
			public override void ExecuteCommand<T>(T context, ICommand<T> command) {
				called++;
				base.ExecuteCommand(context, command);
			}
			#endregion
		}

		[ExcludeFromCodeCoverage]
		public class DummySpiFactory : ICommandContextSpiFactory {
			private int called = 0;
			public int Called {
				get { return called; }
			}
			#region Implementation of ICommandContextSpiFactory
			/// <inheritdoc />
			public ICommandContextSpi Create(ICommandService service) {
				called++;
				return new DefaultContextSpiImpl(service);
			}
			#endregion
		}
		public class ParentContext : CommandContext {
			// Empty			
		}

		public class ChildContext : ParentContext {
			// Empty
		}

		public class ParentCmd : ICommand<ParentContext> {
			#region Implementation of ICommand<in ParentContext>

			public Idempotent Idempotent { get; set; }
			public TxScope TxScope { get; set; }

			public void Execute(ParentContext context) {
				// Empty
			}

			public void PostExecute(ParentContext context) {
				// Empty
			}

			public void PreExecute(ParentContext context) {
				// Empty
			}

			#endregion
		}

		public class ChildCmd : ICommand<ChildContext> {
			#region Implementation of ICommand<in ChildContext>

			public Idempotent Idempotent { get; set; }
			public TxScope TxScope { get; set; }

			public void Execute(ChildContext context) {
				// Empty
			}

			public void PostExecute(ChildContext context) {
				// Empty
			}

			public void PreExecute(ChildContext context) {
				// Empty
			}

			#endregion
		}
	}

	[ExcludeFromCodeCoverage]
	public class DummyContextListener<T> : IContextLifecycleListener<T>
		where T : CommandContext {
		public int PostCalled { get; private set; }
		public int PreCalled { get; private set; }

		public void OuterPost(T context) {
			PostCalled++;
		}

		public void OuterPre(T context) {
			PreCalled++;
		}

		#region Implementation of IContextLifecycleListener<in T>
		public void OnPostContextCreate(T context) {
			PostCalled++;
		}

		public void OnPreContextDestroy(T context) {
			PreCalled++;
		}
		#endregion
	}
}
