// ==============================================================================
//     Test.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Diagnostics.CodeAnalysis;
using Net.Rimakiri.Command;
using NUnit.Framework;

namespace Test.Rimakiri.Command {
	[ExcludeFromCodeCoverage]
	public class SimpleCommandChain<T> : AbstractCommandChain<T> where T: CommandContext {
		public SimpleCommandChain() : this(TxScope.Supports) {
			// Empty
		}
		public SimpleCommandChain(TxScope scope) {
			this.scope = scope;
		}
		
		#region Overrides of AbstractCommandChain<CommandContext>
		public override Idempotent Idempotent {
			get { return Idempotent.Transactional; }
			set { }
		}

		private readonly TxScope scope;
		public override TxScope TxScope {
			get { return scope; }
			set { }
		}

		/// <inheritdoc />
		public override void Execute(T context) {
			var service = context.Service;
			foreach (var command in Commands) {
				if (command == null) {
					continue;
				}
				try {
					service.ExecuteCommand(command);
				}
				catch (AssertionException) {
					throw;
				}
				catch (SampleException) {
					throw;
				}
				catch (Exception) {
					// Ignore
				}
			}
		}
		#endregion
	}
	[ExcludeFromCodeCoverage]
	public abstract class AbstractAttrCmd<T> : ICommand {
		protected readonly T Value;
		protected readonly string Key;
		public ICommand<CommandContext> Child { get; set; }

		protected AbstractAttrCmd(string key, T value) {
			Key = key;
			Value = value;
		}

		#region Implementation of ICommand<in CommandContext>
		public Idempotent Idempotent {
			get { return Idempotent.None; }
		}
		public TxScope TxScope {
			get { return TxScope.Supports; }
		}
		public virtual void PostExecute(CommandContext context) {
			// Empty
		}
		public virtual void PreExecute(CommandContext context) {
			// Empty
		}

		public void Execute(CommandContext context) {
			DoExecute(context);
			var child = Child;
			if (child == null) {
				return;
			}
			try {
				context.Service.ExecuteCommand(child);
			}
			catch (Exception) {
				// Ignore.
			}
		}
		#endregion

		public abstract void DoExecute(CommandContext context);
	}

	[ExcludeFromCodeCoverage]
	public class AttrSetCommand<T> : AbstractAttrCmd<T> {
		public AttrSetCommand(string key, T value)
			: base(key, value) {
			// Empty
		}

		#region Overrides of AbstractAttrCmd<T>
		public override void DoExecute(CommandContext context) {
			context.SetAttribute(Key, Value);
			Assert.AreEqual(Value, context.GetAttribute<T>(Key));
			context.RemoveAttribute(Key);
			Assert.AreEqual(default(T), context.GetAttribute<T>(Key));
			context.SetAttribute(Key, Value);
			Assert.AreEqual(Value, context.GetAttribute<T>(Key));
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	public class AttrSetWithExceptionCommand<T> : AttrSetCommand<T> {
		public AttrSetWithExceptionCommand(string key, T value) : base(key, value) {
			// Empty
		}

		#region Overrides of AbstractAttrCmd<T>
		public override void DoExecute(CommandContext context) {
			base.DoExecute(context);
			throw new InvalidOperationException();
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	public class AttrRemoveCommand<T> : AbstractAttrCmd<T> {
		public AttrRemoveCommand(string key) : base(key, default(T)) {
			// Empty
		}

		#region Overrides of AbstractAttrCmd<T>
		public override void DoExecute(CommandContext context) {
			context.RemoveAttribute(Key);
			Assert.AreEqual(context.GetAttribute<T>(Key), null);
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	public class AttrAssertCommand<T> : AbstractAttrCmd<T> {
		public AttrAssertCommand(string key, T value)
			: base(key, value) {
			// Empty
		}

		#region Overrides of AbstractAttrCmd<T>
		public override void DoExecute(CommandContext context) {
			Assert.AreEqual(Value, context.GetAttribute<T>(Key));
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class TestContextAttributes {
		[Test]
		public void AttributeValueSetTest() {
			const string key = "TESTKEY";
			const string value = "TESTVALUE";
			var builder = new CommandServiceBuilder();
			var service = builder.Build();

			var chain = new SimpleCommandChain<CommandContext>();
			var cmd1 = new AttrAssertCommand<string>(key, null);
			var cmd2 = new AttrSetCommand<string>(key, value);
			var cmd3 = new AttrSetCommand<string>(key, value);
			var cmd4 = new AttrAssertCommand<int>(key, 0);
			var cmd5 = new AttrSetCommand<string>(key, null);
			var cmd6 = new AttrAssertCommand<string>(key, null);
			chain.Add(cmd1, cmd2, cmd3, cmd4, cmd5, cmd6);

			service.ExecuteCommand(chain);
		}

		[Test]
		public void AttributeValueTypeTest() {
			const string key = "TESTKEY";
			const string value = "TESTVALUE";
			var builder = new CommandServiceBuilder();
			var service = builder.Build();

			var chain = new SimpleCommandChain<CommandContext>();
			var cmd1 = new AttrAssertCommand<string>(key, null);
			var cmd2 = new AttrSetCommand<string>(key, value);
			var cmd3 = new AttrAssertCommand<int>(key, 0);
			var cmd4 = new AttrSetCommand<int>(key, 112);
			var cmd5 = new AttrAssertCommand<int>(key, 112);
			var cmd6 = new AttrSetCommand<string>(key, null);
			chain.Add(cmd1, cmd2, cmd3, cmd4, cmd5, cmd6);

			service.ExecuteCommand(chain);
		}

		[Test]
		public void AttributeRollbackTest() {
			const string key = "TESTKEY";
			const string value = "TESTVALUE";
			var builder = new CommandServiceBuilder();
			var service = builder.Build();

			var chain = new SimpleCommandChain<CommandContext>();
			var cmd1 = new AttrAssertCommand<string>(key, null);
			var cmd2 = new AttrSetCommand<string>(key, value);
			var cmd3 = new AttrAssertCommand<string>(key, value);
			var cmd4 = new AttrSetWithExceptionCommand<int>(key, 112);
			var cmd5 = new AttrAssertCommand<int>(key, 0);
			var cmd6 = new AttrAssertCommand<string>(key, value);
			chain.Add(cmd1, cmd2, cmd3, cmd4, cmd5, cmd6);

			service.ExecuteCommand(chain);
		}

		[Test]
		public void AttributeInitializeTest() {
			const string key = "TESTKEY";
			const string value = "TESTVALUE";
			var builder = new CommandServiceBuilder();
			var service = builder.Build(
				ctx => {
					ctx.SetAttribute(key, value);
				}
			);

			var chain = new SimpleCommandChain<CommandContext>();
			var cmd1 = new AttrAssertCommand<string>(key, value);
			chain.Add(cmd1);

			service.ExecuteCommand(chain);
		}

		[Test]
		public void NestedAttributeTest() {
			const string key = "TESTKEY";
			const string value = "TESTVALUE";
			var builder = new CommandServiceBuilder();
			var service = builder.Build(
				ctx => {
					ctx.SetAttribute(key, value);
				}
			);

			var chain = new SimpleCommandChain<CommandContext>();
			var cmd1 = new AttrAssertCommand<string>(key, value);
			var cmd2 = new AttrAssertCommand<string>(key, value);
			var cmd11 = new AttrRemoveCommand<string>(key);
			var cmd12 = new AttrAssertCommand<string>(key, null);
			var cmd13 = new AttrSetWithExceptionCommand<string>(key, "HogeHoge");
			var child = new SimpleCommandChain<CommandContext>();
			child.Add(cmd11, cmd12, cmd13);
			cmd1.Child = child;
			chain.Add(cmd1, cmd2);

			service.ExecuteCommand(chain);
		}
	}
}
