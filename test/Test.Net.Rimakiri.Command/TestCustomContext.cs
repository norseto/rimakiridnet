// ==============================================================================
//     Test.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using Net.Rimakiri.Command;
using Net.Rimakiri.Command.Spi;
using NUnit.Framework;

namespace Test.Rimakiri.Command {
	[ExcludeFromCodeCoverage]
	internal class CustContext : CommandContext {
		public string Name { get; set; }
		public int DisposedCount { get; private set; }

		#region Overrides of CommandContext
		/// <inheritdoc />
		protected override void Dispose(bool disposing) {
			DisposedCount++;
			base.Dispose(disposing);
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	internal class OtherCustContext : CommandContext {
		public string User { get; set; }
	}

	[ExcludeFromCodeCoverage]
	internal class CallbackCounter : IContextLifecycleListener {
		private int calledback;
		public DateTime Created { get; private set; }
		public DateTime Destryoed { get; private set; }

		public int CalledBack {
			get { return calledback; }
		}

		#region Implementation of IContextLifecycleListener
		/// <inheritdoc />
		public void OnPostContextCreate(CommandContext context) {
			Created = DateTime.Now;
			Interlocked.Increment(ref calledback);
			Thread.Sleep(5);
		}

		/// <inheritdoc />
		public void OnPreContextDestroy(CommandContext context) {
			Destryoed = DateTime.Now;
			Interlocked.Increment(ref calledback);
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	internal class CustCmd : ICommand<CustContext> {
		public CustContext Context { get; private set; }
		#region Implementation of IGenericCommand<in CustContext>
		public void Execute(CustContext context) {
			Context = context;
		}
		public void PostExecute(CustContext context) {
			// Empty
		}
		public void PreExecute(CustContext context) {
			// Empty
		}
		public TxScope TxScope { get; set; }
		public Idempotent Idempotent { get; set; }
		#endregion
	}

	[ExcludeFromCodeCoverage]
	internal class OtherCustCmd : ICommand<OtherCustContext> {
		#region Implementation of IGenericCommand<in OtherCustContext>
		public void Execute(OtherCustContext context) {
			// Empty
		}
		public void PostExecute(OtherCustContext context) {
			// Empty
		}
		public void PreExecute(OtherCustContext context) {
			// Empty
		}
		public TxScope TxScope { get; set; }
		public Idempotent Idempotent { get; set; }
		#endregion
	}

	/// <summary>
	/// カスタムコンテキストのテスト
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestCustomContext {
		[Test]
		public void CustomCtxTest() {
			var facade = new CommandServiceFacade {
				ContextType = typeof(CustContext),
				ServiceSpi = new DefaultServiceSpiImpl(),
				ContextSpiFactory = new DefaultContextSpiFactoryImpl(),
			};
			var cmd = new CommandTestImpl();

			facade.ExecuteCommand(cmd);
			Assert.IsNotNull(cmd.Exec.Context);
			Assert.IsTrue(cmd.Exec.Context is CustContext);

			facade.ExecuteCommand(new CustCmd());
		}

		[Test]
		public void DisposeOnceTest()
		{
			var facade = new CommandServiceFacade {
				ContextType = typeof(CustContext),
				ServiceSpi = new DefaultServiceSpiImpl(),
				ContextSpiFactory = new DefaultContextSpiFactoryImpl(),
			};
			var child = new CommandTestImpl();
			var cmd = new CommandTestImpl {
				ExecCmd = child,
			};

			// Check finalizer call dispose
			// ReSharper disable once UnusedVariable
			var orphan = new CustContext();

			facade.ExecuteCommand(cmd);
			Assert.IsNotNull(cmd.Exec.Context);
			Assert.IsTrue(cmd.Exec.Context is CustContext);
			Assert.IsNotNull(child.Exec.Context);
			Assert.IsTrue(child.Exec.Context is CustContext);
			Assert.AreSame(child.Exec.Context, cmd.Exec.Context);
			Assert.AreEqual(1, ((CustContext)cmd.Exec.Context).DisposedCount);
		}

		[Test]
		public void InvalidCustomCtxTest() {
			var facade = new CommandServiceFacade {
				ContextType = typeof(CustContext),
				ServiceSpi = new DefaultServiceSpiImpl(),
				ContextSpiFactory = new DefaultContextSpiFactoryImpl(),
			};
			try {
				facade.ExecuteCommand(new OtherCustCmd());
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (InvalidOperationException) {
				Assert.IsTrue(true);
			}
		}

		[Test]
		public void CallbackTest() {
			var counter = new CallbackCounter();
			var facade = new CommandServiceFacade {
				ServiceSpi = new DefaultServiceSpiImpl(),
				ContextSpiFactory = new DefaultContextSpiFactoryImpl(),
				LifecycleListener = counter,
			};
			var cmd = new CustCmd();
			facade.ExecuteCommand(cmd);
			Assert.AreEqual(2, counter.CalledBack);
			Assert.IsTrue(counter.Created.Ticks < counter.Destryoed.Ticks);
		}
	}
}

