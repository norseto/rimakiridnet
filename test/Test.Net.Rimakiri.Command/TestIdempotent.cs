// ==============================================================================
//     Test.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using Net.Rimakiri.Command;
using Net.Rimakiri.Command.Spi;
using Net.Rimakiri.Command.Transactions;
using NUnit.Framework;

namespace Test.Rimakiri.Command {
	/// <summary>
	/// 冪等コマンドのテストコマンド
	/// </summary>
	[ExcludeFromCodeCoverage]
	public abstract class AbstractIdempotentCommand : ICommand {

		public int Executed { get; set; }
		public ICommand ElderChild { get; set; }
		public ICommand Child { get; set; }
		public string Name { get;set; }

		protected abstract void DoInExec(CommandContext context);

		#region Implementation of IGenericCommand<in CommandContext>

		public void Execute(CommandContext context) {
			Executed += 1;
			DoInExec(context);
			if (ElderChild != null) {
				context.Service.ExecuteCommand(ElderChild);
			}
			if (Child != null) {
				context.Service.ExecuteCommand(Child);
			}
		}

		public void PostExecute(CommandContext context) {
			// Empty
		}
		public void PreExecute(CommandContext context) {
			// Empty
		}

		public TxScope TxScope { get; set; }
		public Idempotent Idempotent { get; set; }

		#endregion

		public void Reset() {
			Executed = 0;
		}
	}

	/// <summary>
	/// スリープするコマンド
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class IdempotentSleepCommand : AbstractIdempotentCommand {
		#region Overrides of AbstractIdempotentCommand

		protected override void DoInExec(CommandContext context) {
			var sleep = 8000 >> Executed;
			Thread.Sleep(sleep);
		}

		#endregion
	}

	/// <summary>
	/// 例外を送出するコマンド
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class IdempotentExceptionCommand : AbstractIdempotentCommand {
		public int Threashold { get; set; }

		#region Overrides of AbstractIdempotentCommand

		protected override void DoInExec(CommandContext context) {
			if (Executed < Threashold) {
				throw new AbandonedMutexException();
			}
		}

		#endregion
	}

	/// <summary>
	/// テスト用ダミーポリシ（全ての例外のリトライを行わない）。
	/// </summary>
	[ExcludeFromCodeCoverage]
	internal class DumbPolicy : IRetryPolicy {
		#region Implementation of IRetryPolicy

		public bool IsRetryable(Exception error, int tryCount) {
			return false;
		}

		#endregion
	}

	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestIdempotent {
		[Test]
		public void RetryExceptionTest() {
			var s1 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = 4
				}
			}.Build();
			var s2 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = 4
				}
			}.Build();

			Action<ICommandService> action = (service) => {
				var grandChild = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Transactional,
					TxScope = TxScope.Required,
				};
				var child = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Transactional,
					Child = grandChild,
					TxScope = TxScope.Required,
				};
				var parent = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Transactional,
					Child = child,
					TxScope = TxScope.Required,
				};

				service.ExecuteCommand(parent);

				Assert.AreEqual(4, parent.Executed);
				Assert.AreEqual(3, child.Executed);
				Assert.AreEqual(2, grandChild.Executed);
			};

			action(s1);
			action(s2);
		}

		[Test]
		public void RetryExhaustedExceptionTest() {
			var s1 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = 2
				}
			}.Build();
			var s2 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = 2
				},
				TransactionFactory = new ScopeCommandTxFactory(),
			}.Build();

			Action<ICommandService> action = (service) => {
				var grandChild = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Transactional
				};
				var child = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Transactional,
					Child = grandChild,
				};
				var parent = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Transactional,
					Child = child,
					TxScope = TxScope.Required,
				};


				try {
					service.ExecuteCommand(parent);
					Assert.IsTrue(false, "Should throw an exception.");
				}
				catch (AbandonedMutexException) {
					Assert.IsTrue(true);
				}

				Assert.AreEqual(3, parent.Executed);
				Assert.AreEqual(2, child.Executed);
				Assert.AreEqual(1, grandChild.Executed);
			};
			action(s1);
			action(s2);
		}

		[Test]
		public void NotIdempotentExceptionTest() {
			var s1 = new CommandServiceBuilder {
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = 3
				},
				RetryPolicy = new UnconditionalRetryPolicy(),
			}.Build();
			var s2 = new CommandServiceBuilder {
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = 3
				},
				RetryPolicy = new UnconditionalRetryPolicy(),
				TransactionFactory = new ScopeCommandTxFactory(),
			}.Build();

			Action<ICommandService> action = (service) => {
				var grandChild = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Transactional
				};
				var child = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.None,
					Child = grandChild,
				};
				var parent = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Transactional,
					Child = child,
				};

				try {
					service.ExecuteCommand(parent);
					Assert.IsTrue(false, "Should throw an exception.");
				}
				catch (AbandonedMutexException) {
					Assert.IsTrue(true);
				}

				Assert.AreEqual(2, parent.Executed);
				Assert.AreEqual(1, child.Executed);
				Assert.AreEqual(0, grandChild.Executed);
			};

			action(s1);
			action(s2);
		}

		[Test]
		public void IdempotentFinishedExceptionTest() {
			var s1 = new CommandServiceBuilder {
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = 10
				},
				RetryPolicy = new UnconditionalRetryPolicy(),
			}.Build();
			var s2 = new CommandServiceBuilder {
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = 10
				},
				RetryPolicy = new UnconditionalRetryPolicy(),
				TransactionFactory = new ScopeCommandTxFactory(),
			}.Build();

			Action<ICommandService> action = (service) => {
				var child1 = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Transactional,
					TxScope = TxScope.Required,
					Name = "child1",
				};
				var child2 = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Anytime,
					Name = "child2",
				};

				var cmd = new CommandChain(child1, child2) {
					Idempotent = Idempotent.Unspecified,
					TxScope = TxScope.Supports,
				};
				try {
					service.ExecuteCommand(cmd);
					Assert.IsTrue(false, "Should throw an exception.");
				}
				catch (AbandonedMutexException) {
					Assert.IsTrue(true);
				}

				Assert.AreEqual(2, child1.Executed);
				Assert.AreEqual(1, child2.Executed);
			};

			action(s1);
			action(s2);
		}

		[Test]
		public void NestedTxRetryExceptionTest() {
			var builder1 = new CommandServiceBuilder {
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = 3,
				},
				RetryPolicy = new UnconditionalRetryPolicy(),
			};

			var builder2 = new CommandServiceBuilder {
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = 3,
				},
				RetryPolicy = new UnconditionalRetryPolicy(),
				TransactionFactory = new ScopeCommandTxFactory(),
			};

			Action<ICommandService> action = (service) => {
				var grandChild = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Transactional
				};
				var child = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Transactional,
					Child = grandChild,
					TxScope = TxScope.RequiresNew,
				};
				var parent = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Transactional,
					Child = child,
				};

				service.ExecuteCommand(parent);

				Assert.AreEqual(2, parent.Executed);
				Assert.AreEqual(3, child.Executed);
				Assert.AreEqual(2, grandChild.Executed);
			};

			action(builder1.Build());
			action(builder2.Build());
		}

		[Test]
		public void RetryPolicyTest() {
			var s1 = new CommandServiceBuilder {
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = 10,
					RetryPolicy = new DumbPolicy(),
				}
			}.Build();
			var s2 = new CommandServiceBuilder {
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = 10,
					RetryPolicy = new DumbPolicy(),
				},
				TransactionFactory = new ScopeCommandTxFactory(),
			}.Build();

			Action<ICommandService> action = (service) => {
				var grandChild = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Transactional
				};
				var child = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Transactional,
					Child = grandChild,
					TxScope = TxScope.RequiresNew,
				};
				var parent = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Transactional,
					Child = child,
				};

				try {
					service.ExecuteCommand(parent);
					Assert.IsTrue(false, "Should throw an exception.");
				}
				catch (AbandonedMutexException) {
					Assert.IsTrue(true);
				}

				Assert.AreEqual(1, parent.Executed);
				Assert.AreEqual(1, child.Executed);
				Assert.AreEqual(1, grandChild.Executed);
			};

			action(s1);
			action(s2);
		}

		[Test]
		public void NegativeRetryCountTest() {
			var s1 = new CommandServiceBuilder {
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = -1
				}
			}.Build();
			var s2 = new CommandServiceBuilder {
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = -1
				},
				TransactionFactory = new ScopeCommandTxFactory(),
			}.Build();

			Action<ICommandService> action = (service) => {
				var grandChild = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Transactional
				};
				var child = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Transactional,
					Child = grandChild,
				};
				var parent = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Transactional,
					Child = child,
				};

				service.ExecuteCommand(parent);

				Assert.AreEqual(1, parent.Executed);
				Assert.AreEqual(1, child.Executed);
				Assert.AreEqual(1, grandChild.Executed);
			};

			action(s1);
			action(s2);
		}

		[Test]
		public void NegativeRetryCountWithErrorTest() {
			var s1 = new CommandServiceBuilder {
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = -1
				}
			}.Build();
			var s2 = new CommandServiceBuilder {
				ServiceSpi = new DefaultServiceSpiImpl {
					MaxRetry = -1
				},
				TransactionFactory = new ScopeCommandTxFactory(),
			}.Build();

			Action<ICommandService> action = (service) => {
				var grandChild = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Transactional
				};
				var child = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Transactional,
					Child = grandChild,
				};
				var parent = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Transactional,
					Child = child,
				};

				try {
					service.ExecuteCommand(parent);
					Assert.IsTrue(false, "Should throw an exception.");
				}
				catch (AbandonedMutexException) {
					Assert.IsTrue(true);
				}

				Assert.AreEqual(1, parent.Executed);
				Assert.AreEqual(1, child.Executed);
				Assert.AreEqual(1, grandChild.Executed);
			};
			action(s1);
			action(s2);
		}

		/// <summary>
		/// トランザクションがない場合には、例外発生コマンド
		/// がリトライされる
		/// </summary>
		[Test]
		public void RetrySupportsTest() {
			var s1 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
			}.Build();
			var s2 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
				TransactionFactory = new ScopeCommandTxFactory(),
			}.Build();

			Action<ICommandService> action = (service) => {
				var grandChild = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Transactional,
					TxScope = TxScope.Supports,
				};
				var child = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Transactional,
					Child = grandChild,
					TxScope = TxScope.Supports,
				};
				var parent = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Transactional,
					Child = child,
					TxScope = TxScope.Supports,
				};

				service.ExecuteCommand(parent);

				Assert.AreEqual(1, parent.Executed);
				Assert.AreEqual(1, child.Executed);
				Assert.AreEqual(2, grandChild.Executed);
			};
			action(s1);
			action(s2);
		}

		/// <summary>
		/// トランザクションがない場合には、例外発生コマンド
		/// がリトライされる
		/// </summary>
		[Test]
		public void RetrySuppressTest() {
			var s1 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
			}.Build();
			var s2 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
				TransactionFactory = new ScopeCommandTxFactory(),
			}.Build();

			Action<ICommandService> action = (service) => {
				var grandChild = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Transactional,
					TxScope = TxScope.Suppress,
				};
				var child = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Transactional,
					Child = grandChild,
					TxScope = TxScope.Required,
				};
				var parent = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Transactional,
					Child = child,
					TxScope = TxScope.Required,
				};

				service.ExecuteCommand(parent);

				Assert.AreEqual(1, parent.Executed);
				Assert.AreEqual(1, child.Executed);
				Assert.AreEqual(2, grandChild.Executed);
			};
			action(s1);
			action(s2);
		}


		/// <summary>
		/// 途中でトランザクションが開始された場合には、
		/// トランザクション開始コマンドからリトライされる
		/// </summary>
		[Test]
		public void RetryRequiredTest() {
			var s1 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
			}.Build();
			var s2 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
				TransactionFactory = new ScopeCommandTxFactory(),
			}.Build();

			Action<ICommandService> action = (service) => {
				var grandChild = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Transactional,
					TxScope = TxScope.Supports,
				};
				var child = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Transactional,
					Child = grandChild,
					TxScope = TxScope.Required,
				};
				var parent = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Transactional,
					Child = child,
					TxScope = TxScope.Supports,
				};

				service.ExecuteCommand(parent);

				Assert.AreEqual(1, parent.Executed);
				Assert.AreEqual(2, child.Executed);
				Assert.AreEqual(2, grandChild.Executed);
			};
			action(s1);
			action(s2);
		}

		/// <summary>
		/// 途中でトランザクションが開始された場合には、
		/// トランザクション開始コマンドからリトライされる
		/// </summary>
		[Test]
		public void RetryRequired2Test() {
			var s1 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
			}.Build();
			var s2 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
				TransactionFactory = new ScopeCommandTxFactory(),
			}.Build();

			Action<ICommandService> action = (service) => {
				var grandChild = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Transactional,
					TxScope = TxScope.Required,
				};
				var child = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Transactional,
					Child = grandChild,
					TxScope = TxScope.Required,
				};
				var parent = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Transactional,
					Child = child,
					TxScope = TxScope.Supports,
				};

				service.ExecuteCommand(parent);

				Assert.AreEqual(1, parent.Executed);
				Assert.AreEqual(2, child.Executed);
				Assert.AreEqual(2, grandChild.Executed);
			};
			action(s1);
			action(s2);
		}

		/// <summary>
		/// 途中でトランザクションが開始された場合には、
		/// トランザクション開始コマンドからリトライされる
		/// </summary>
		[Test]
		public void RetryRequired3Test() {
			var s1 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
			}.Build();
			var s2 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
				TransactionFactory = new ScopeCommandTxFactory(),
			}.Build();

			Action<ICommandService> action = (service) => {
				var grandChild = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Transactional,
					TxScope = TxScope.Mandatory,
				};
				var child = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Transactional,
					Child = grandChild,
					TxScope = TxScope.Required,
				};
				var parent = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Transactional,
					Child = child,
					TxScope = TxScope.Supports,
				};

				service.ExecuteCommand(parent);

				Assert.AreEqual(1, parent.Executed);
				Assert.AreEqual(2, child.Executed);
				Assert.AreEqual(2, grandChild.Executed);
			};
			action(s1);
			action(s2);
		}

		/// <summary>
		/// 途中でトランザクションが開始された場合には、
		/// トランザクション開始コマンドからリトライされる
		/// </summary>
		[Test]
		public void RetryRequiresNewTest() {
			var s1 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
			}.Build();
			var s2 = new CommandServiceBuilder {
				RetryPolicy = new UnconditionalRetryPolicy(),
				TransactionFactory = new ScopeCommandTxFactory(),
			}.Build();

			Action<ICommandService> action = (service) => {
				var grandChild = new IdempotentExceptionCommand {
					Threashold = 2,
					Idempotent = Idempotent.Transactional,
					TxScope = TxScope.Required,
				};
				var child = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Transactional,
					Child = grandChild,
					TxScope = TxScope.RequiresNew,
				};
				var parent = new IdempotentExceptionCommand {
					Threashold = 1,
					Idempotent = Idempotent.Transactional,
					Child = child,
					TxScope = TxScope.Required,
				};

				service.ExecuteCommand(parent);

				Assert.AreEqual(1, parent.Executed);
				Assert.AreEqual(2, child.Executed);
				Assert.AreEqual(2, grandChild.Executed);
			};
			action(s1);
			action(s2);
		}
	}
}
