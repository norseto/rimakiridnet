// ==============================================================================
//     Test.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Transactions;
using Net.Rimakiri.Command;
using Net.Rimakiri.Command.Spi;
using Net.Rimakiri.Command.Transactions;
using NUnit.Framework;

namespace Test.Rimakiri.Command {
	[ExcludeFromCodeCoverage]
	internal class CtxTxPair {
		public CommandContext Context { get; private set; }
		public Transaction Transaction { get; private set; }
		public IsolationLevel IsolationLevel { get; private set; }

		public CtxTxPair(CommandContext ctx) {
			Context = ctx;
			Transaction = Transaction.Current;
			if (Transaction != null) {
				IsolationLevel = Transaction.IsolationLevel;
			}
		}
	}

	[ExcludeFromCodeCoverage]
	internal class CommandTestImpl : ICommand {
		public CtxTxPair Pre { get; set; }
		public CtxTxPair Post { get; set; }
		public CtxTxPair Exec { get; set; }
		public ICommand PreCmd { get; set; }
		public ICommand PostCmd { get; set; }
		public ICommand ExecCmd { get; set; }
		public int Sleep { get; set; }

		#region Implementation of ICommand
		/// <inheritdoc />
		public TxScope TxScope { get; set; }

		/// <inheritdoc />
		public Idempotent Idempotent { get; set; }
		#endregion

		#region Implementation of ICommand
		/// <inheritdoc />
		public void PreExecute(CommandContext context) {
			Pre = new CtxTxPair(context);
			if (PreCmd != null) {
				context.Service.ExecuteCommand(PreCmd);
			}
		}

		/// <inheritdoc />
		public void Execute(CommandContext context) {
			Exec = new CtxTxPair(context);
			if (ExecCmd != null) {
				context.Service.ExecuteCommand(ExecCmd);
			}
			if (Sleep > 0) {
				Thread.Sleep(TimeSpan.FromSeconds(Sleep));
			}
		}

		/// <inheritdoc />
		public void PostExecute(CommandContext context) {
			Post = new CtxTxPair(context);
			if (PostCmd != null) {
				context.Service.ExecuteCommand(PostCmd);
			}
		}
		#endregion
	}

	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestCommandService {
		private static void CheckNotInTx() {
			Assert.IsNull(Transaction.Current, "Transaction still remain.");
		}

		#region Basic Test
		[Test]
		public void TestDefault() {
			var srv = new CommandServiceFacade {
				ServiceSpi = new DefaultServiceSpiImpl(),
				ContextSpiFactory = new DefaultContextSpiFactoryImpl(),
			};
			var cmd = new CommandTestImpl();
			srv.ExecuteCommand(cmd);
			CheckNotInTx();

			Assert.IsNull(cmd.Pre.Transaction);
			Assert.IsNull(cmd.Exec.Transaction);
			Assert.IsNull(cmd.Post.Transaction);
		}

		[Test]
		public void TestSameContext() {
			var srv = new CommandServiceFacade {
				ServiceSpi = new DefaultServiceSpiImpl(),
				ContextSpiFactory = new DefaultContextSpiFactoryImpl(),
			};
			var child = new CommandTestImpl();
			var cmd = new CommandTestImpl {
				ExecCmd = child,
			};

			srv.ExecuteCommand(cmd);
			CheckNotInTx();

			Assert.AreSame(cmd.Pre.Context, child.Pre.Context);

			Assert.AreSame(cmd.Pre.Context, cmd.Exec.Context);
			Assert.AreSame(cmd.Exec.Context, cmd.Post.Context);
			Assert.AreSame(child.Pre.Context, child.Exec.Context);
			Assert.AreSame(child.Exec.Context, child.Post.Context);
		}

		[Test]
		public void TestNullSpi()
		{
			var srv = new CommandServiceFacade {
				ContextSpiFactory = new DefaultContextSpiFactoryImpl(),
			};
			var cmd = new CommandTestImpl();

			try {
				srv.ExecuteCommand(cmd);
				Assert.IsTrue(false, "Should throw an exception");
			}
			catch (InvalidOperationException) {
				Assert.IsTrue(true);
			}
			srv = new CommandServiceFacade {
				ServiceSpi = new DefaultServiceSpiImpl(),
			};

			try {
				srv.ExecuteCommand(cmd);
				Assert.IsTrue(false, "Should throw an exception");
			}
			catch (InvalidOperationException) {
				Assert.IsTrue(true);
			}
		}

		[Test]
		public void TestInvalidType()
		{
			var srv = new CommandServiceFacade {
				ServiceSpi = new DefaultServiceSpiImpl(),
				ContextSpiFactory = new DefaultContextSpiFactoryImpl(),
				ContextType = typeof(string)
			};
			var cmd = new CommandTestImpl();

			try {
				srv.ExecuteCommand(cmd);
				Assert.IsTrue(false, "Should throw an exception");
			}
			catch (InvalidOperationException) {
				Assert.IsTrue(true);
			}
		}
		#endregion

		#region Supports Tests
		[Test]
		public void TestParentSupports() {
			var srv = new CommandServiceFacade {
				ServiceSpi = new DefaultServiceSpiImpl {
					CommandTxFactory = new ScopeCommandTxFactory()
				},
				ContextSpiFactory = new DefaultContextSpiFactoryImpl(),
			};
			var cmdPre = new CommandTestImpl { TxScope = TxScope.Required };
			var cmdExec = new CommandTestImpl { TxScope = TxScope.Required };
			var cmdPost = new CommandTestImpl { TxScope = TxScope.Required };
			var parent = new CommandTestImpl {
				PreCmd = cmdPre,
				ExecCmd = cmdExec,
				PostCmd = cmdPost,
				TxScope = TxScope.Supports
			};
			srv.ExecuteCommand(parent);
			CheckNotInTx();

			Assert.IsNull(parent.Pre.Transaction);
			Assert.IsNull(parent.Exec.Transaction);
			Assert.IsNull(parent.Post.Transaction);

			Assert.IsNull(cmdPre.Pre.Transaction);
			Assert.IsNotNull(cmdPre.Exec.Transaction);
			Assert.IsNull(cmdPre.Post.Transaction);

			Assert.IsNull(cmdExec.Pre.Transaction);
			Assert.IsNotNull(cmdExec.Exec.Transaction);
			Assert.IsNull(cmdExec.Post.Transaction);

			Assert.IsNull(cmdPost.Pre.Transaction);
			Assert.IsNotNull(cmdPost.Exec.Transaction);
			Assert.IsNull(cmdPost.Post.Transaction);
		}

		[Test]
		public void TestChildSupports() {
			var srv = new CommandServiceFacade {
				ServiceSpi = new DefaultServiceSpiImpl {
					CommandTxFactory = new ScopeCommandTxFactory(),
				},
				ContextSpiFactory = new DefaultContextSpiFactoryImpl(),
			};
			var cmdPre = new CommandTestImpl {
				TxScope = TxScope.Supports
			};
			var cmdExec = new CommandTestImpl {
				TxScope = TxScope.Supports
			};
			var cmdPost = new CommandTestImpl {
				TxScope = TxScope.Supports
			};
			var parent = new CommandTestImpl {
				PreCmd = cmdPre,
				ExecCmd = cmdExec,
				PostCmd = cmdPost,
				TxScope = TxScope.Required,
			};
			srv.ExecuteCommand(parent);
			CheckNotInTx();

			Assert.IsNull(parent.Pre.Transaction);
			Assert.IsNotNull(parent.Exec.Transaction);
			Assert.IsNull(parent.Post.Transaction);

			Assert.IsNull(cmdPre.Pre.Transaction);
			Assert.IsNull(cmdPre.Exec.Transaction);
			Assert.IsNull(cmdPre.Post.Transaction);

			Assert.IsNotNull(cmdExec.Pre.Transaction);
			Assert.IsNotNull(cmdExec.Exec.Transaction);
			Assert.IsNotNull(cmdExec.Post.Transaction);

			Assert.IsNull(cmdPost.Pre.Transaction);
			Assert.IsNull(cmdPost.Exec.Transaction);
			Assert.IsNull(cmdPost.Post.Transaction);
		}
		#endregion

		#region Mandatory Tests
		[Test]
		public void TestMandatory() {
			var srv = new CommandServiceFacade {
				ServiceSpi = new DefaultServiceSpiImpl {
					CommandTxFactory = new ScopeCommandTxFactory(),
				},
				ContextSpiFactory = new DefaultContextSpiFactoryImpl(),
			};
			var cmd = new CommandTestImpl {
				TxScope = TxScope.Mandatory
			};
			try {
				srv.ExecuteCommand(cmd);
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (InvalidOperationException) {
				Assert.IsTrue(true);
			}
			CheckNotInTx();

			Assert.IsNotNull(cmd.Pre);
			Assert.IsNull(cmd.Exec);
			Assert.IsNull(cmd.Post);
		}

		[Test]
		public void TestMandatoryWithLocal() {
			var srv = new CommandServiceFacade {
				ServiceSpi = new DefaultServiceSpiImpl {
					CommandTxFactory = new LocalCommandTxFactory(),
				},
				ContextSpiFactory = new DefaultContextSpiFactoryImpl(),
			};
			var cmd = new CommandTestImpl {
				TxScope = TxScope.Mandatory
			};
			try {
				srv.ExecuteCommand(cmd);
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (InvalidOperationException) {
				Assert.IsTrue(true);
			}
			CheckNotInTx();

			Assert.IsNotNull(cmd.Pre);
			Assert.IsNull(cmd.Exec);
			Assert.IsNull(cmd.Post);
		}

		[Test]
		public void TestMandatoryOk() {
			var s1 = new CommandServiceFacade {
				ServiceSpi = new DefaultServiceSpiImpl {
					CommandTxFactory = new LocalCommandTxFactory(),
				},
				ContextSpiFactory = new DefaultContextSpiFactoryImpl(),
			};
			var s2 = new CommandServiceFacade {
				ServiceSpi = new DefaultServiceSpiImpl {
					CommandTxFactory = new ScopeCommandTxFactory(),
				},
				ContextSpiFactory = new DefaultContextSpiFactoryImpl(),
			};

			Action<ICommandService,bool> action = (srv, global) => {
				var child = new CommandTestImpl {
					TxScope = TxScope.Mandatory
				};
				var cmd = new CommandTestImpl {
					ExecCmd = child,
					TxScope = TxScope.Required,
				};

				srv.ExecuteCommand(cmd);
				CheckNotInTx();

				if (global) {
					Assert.IsNotNull(child.Exec.Transaction);
				}
			};
			action(s1, false);
			action(s2, true);
		}
		#endregion

		#region Required Tests
		[Test]
		public void TestSameTran() {
			var srv = new CommandServiceFacade {
				ServiceSpi = new DefaultServiceSpiImpl {
					CommandTxFactory = new ScopeCommandTxFactory()
				},
				ContextSpiFactory = new DefaultContextSpiFactoryImpl(),
			};
			var child = new CommandTestImpl();
			var cmd = new CommandTestImpl {
				ExecCmd = child,
				TxScope = TxScope.Required,
			};

			srv.ExecuteCommand(cmd);
			CheckNotInTx();

			Assert.IsNotNull(child.Exec.Transaction);
			Assert.AreSame(cmd.Exec.Transaction, child.Exec.Transaction);
		}
		#endregion

		#region RequiresNew Tests
		[Test]
		public void TestNewTran() {
			var srv = new CommandServiceFacade {
				ServiceSpi = new DefaultServiceSpiImpl {
					CommandTxFactory	= new ScopeCommandTxFactory()
				},
				ContextSpiFactory = new DefaultContextSpiFactoryImpl(),
			};
			var child = new CommandTestImpl {
				TxScope = TxScope.RequiresNew
			};
			var cmd = new CommandTestImpl {
				ExecCmd = child,
				TxScope = TxScope.Required,
			};

			srv.ExecuteCommand(cmd);
			CheckNotInTx();

			Assert.IsNotNull(cmd.Exec.Transaction);
			Assert.IsNotNull(child.Exec.Transaction);
			Assert.AreNotSame(cmd.Exec.Transaction, child.Exec.Transaction);
		}
		#endregion

		#region Suppress Tests
		[Test]
		public void TestSuppress() {
			var srv = new CommandServiceFacade {
				ServiceSpi = new DefaultServiceSpiImpl {
					CommandTxFactory = new ScopeCommandTxFactory()
				},
				ContextSpiFactory = new DefaultContextSpiFactoryImpl(),
			};
			var child = new CommandTestImpl {
				TxScope = TxScope.Suppress,
			};
			var cmd = new CommandTestImpl {
				ExecCmd = child,
				TxScope = TxScope.Required,
			};

			srv.ExecuteCommand(cmd);
			CheckNotInTx();

			Assert.IsNotNull(cmd.Exec.Transaction);
			Assert.IsNull(child.Exec.Transaction);
		}
		[Test]
		public void TestParentSuppress() {
			var srv = new CommandServiceFacade {
				ServiceSpi = new DefaultServiceSpiImpl {
					CommandTxFactory = new ScopeCommandTxFactory()
				},
				ContextSpiFactory = new DefaultContextSpiFactoryImpl(),
			};
			var child = new CommandTestImpl {
				TxScope = TxScope.Required,
			};
			var cmd = new CommandTestImpl {
				ExecCmd = child,
				TxScope = TxScope.Suppress,
			};

			srv.ExecuteCommand(cmd);
			CheckNotInTx();

			Assert.IsNotNull(child.Exec.Transaction);
			Assert.IsNull(cmd.Exec.Transaction);
		}
		#endregion
	}
}
