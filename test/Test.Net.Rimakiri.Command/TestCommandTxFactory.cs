// ==============================================================================
//     Test.Rimakiri.Command
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Diagnostics.CodeAnalysis;
using Net.Rimakiri.Command.Spi;
using Net.Rimakiri.Command.Transactions;
using NUnit.Framework;

namespace Test.Rimakiri.Command {
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class TestCommandTxFactory {
		[Test]
		public void TestCoordinatorFactory() {
			var target = new ScopeCommandTxFactory();
			Assert.IsNotNull(target.CoordinatorFactory);

			target.CoordinatorFactory = null;
			Assert.IsNotNull(target.CoordinatorFactory);

			var factory = new DefaultCoordinatorFactoryImpl();
			Assert.AreNotEqual(target.CoordinatorFactory, factory);

			target.CoordinatorFactory = factory;
			Assert.AreEqual(target.CoordinatorFactory, factory);
		}
	}
}
