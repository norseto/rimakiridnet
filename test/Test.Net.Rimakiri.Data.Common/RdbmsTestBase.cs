// ==============================================================================
//     Test.Rimakiri.Data.Rdbms
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using Net.Rimakiri.Command;
using Net.Rimakiri.Command.Database;
using Net.Rimakiri.Command.Transactions;
using Net.Rimakiri.Data;
using Net.Rimakiri.Data.Interfaces;
using Net.Rimakiri.Data.TypeWrappers;
using Net.Rimakiri.Data.Util;
using NUnit.Framework;
using Test.Net.Rimakiri.Data.Common.Templates;

namespace Test.Net.Rimakiri.Data.Common {
	[ExcludeFromCodeCoverage]
	internal class DummyNumeric : DecimalType {
		#region Overrides of DecimalType
		public override byte Precision {
			get { return 10; }
		}

		public override byte Scale {
			get { return 3; }
		}
		#endregion

		public DummyNumeric(decimal? value)
			: base(value) {
		}
	}
	[ExcludeFromCodeCoverage]
	public abstract class RdbmsTestBase {
		/// <summary>
		/// Get connection factory for test.
		/// </summary>
		/// <returns>Connection factory</returns>
		protected abstract IConnectionFactory GetConnectionFactory();

		protected virtual DacCommandServiceBuilder<TestDacContext> MakeServiceBuilder(
						bool useGlobalTx = true) {
			var listener = new DummyLifecycleListener<TestDacContext>();
			var factory = GetConnectionFactory();
			var builder = new DacCommandServiceBuilder<TestDacContext> {
				ConnectionFactory = factory,
				ContextLifecycleListener = listener,
			};
			if (useGlobalTx) {
				builder.TransactionFactory = new ScopeCommandTxFactory();
			}
			Assert.AreSame(factory, builder.ConnectionFactory);
			return builder;
		}

		protected IList<IDictionary<string, object>> DoSelect(ICommandService service,
			Action<DacCommandContext, IDbCommand> action) {
			var cmd = new DelegateSelectCommand(action);
			// Invalid QUERY
			cmd.Query = "SELECT NOW()";

			Assert.AreEqual(Idempotent.Transactional, cmd.Idempotent);
			Assert.AreEqual(TxScope.Required, cmd.TxScope);
			Assert.AreEqual(TxIntention.Select, cmd.TxIntention);

			cmd.Idempotent = Idempotent.Anytime;
			cmd.TxScope = TxScope.Supports;
			cmd.TxIntention = TxIntention.Delete;

			Assert.AreEqual(Idempotent.Anytime, cmd.Idempotent);
			Assert.AreEqual(TxScope.Supports, cmd.TxScope);
			Assert.AreEqual(TxIntention.Delete, cmd.TxIntention);

			cmd.Idempotent = Idempotent.Transactional;
			cmd.TxScope = TxScope.Required;
			cmd.TxIntention = TxIntention.Select;

			service.ExecuteCommand(cmd);
			return cmd.Results;
		}

		protected int DoNonQuery(ICommandService service,
			Action<DacCommandContext, IDbCommand> action) {
			var cmd = new DelegateNonQueryCommand(action);
			service.ExecuteCommand(cmd);
			return cmd.Result;
		}

		protected void DoTestConnect() {
			var factory = GetConnectionFactory();
			using (var con = factory.NewConnection(false, IsolationLevel.ReadCommitted)) {
				if (con.State != ConnectionState.Open) {
					con.Open();
				}
				Assert.AreEqual(ConnectionState.Open, con.State);
			}
		}

		protected void DoClean(ICommandService service) {
//			Action<DacCommandContext> action = (ctx) => {
//				var dialect = ctx.ConnectionFactory.DatabaseDialect;
//				var binder = new ValueBinderBuilder("DELETE FROM T_TX_TEST").Build(dialect);
//				using (var dbCmd = ctx.CreateDbCommand()) {
//					binder.Bind(dbCmd).ExecuteNonQuery();
//				}
//			};
			var cmd = new SimpleNonQueryDacCommand() {
				Statement = "DELETE FROM T_TX_TEST",
			};
			Assert.AreEqual("DELETE FROM T_TX_TEST", cmd.Statement);
			Assert.AreEqual(Idempotent.Transactional, cmd.Idempotent);
			Assert.AreEqual(TxScope.Required, cmd.TxScope);
			Assert.AreEqual(TxIntention.Update, cmd.TxIntention);

			cmd.Idempotent = Idempotent.Anytime;
			cmd.TxScope = TxScope.Supports;
			cmd.TxIntention = TxIntention.Delete;

			Assert.AreEqual(Idempotent.Anytime, cmd.Idempotent);
			Assert.AreEqual(TxScope.Supports, cmd.TxScope);
			Assert.AreEqual(TxIntention.Delete, cmd.TxIntention);

			cmd.Idempotent = Idempotent.Transactional;
			cmd.TxScope = TxScope.Required;
			cmd.TxIntention = TxIntention.Update;

			service.ExecuteCommand(cmd);
			Assert.IsTrue(cmd.Result >= 3);
		}

		protected void DoThrowError(ICommandService service) {
			Action<DacCommandContext> action = (ctx) => {
				throw new NullReferenceException();
			};
			var dacCmd = new DelegateDacCommand(action);
			service.ExecuteCommand(dacCmd);
		}

		protected void DoInitialize(ICommandService service) {
			Action<DacCommandContext> action = (ctx) => {
				var dialect = ctx.ConnectionFactory.DatabaseDialect;
				var binder = new ValueBinderBuilder("DELETE FROM T_TX_TEST").Build(dialect);
				using (var cmd = ctx.CreateDbCommand()) {
					binder.Bind(cmd).ExecuteNonQuery();
				}
				binder = new ValueBinderBuilder("INSERT INTO T_TX_TEST(ID, NAME) " +
												"VALUES ({0}, {1})",
					BindValue.Create("id", 1),
					BindValue.Create("name", "XXX", 10)).Build(dialect);
				using (var cmd = ctx.CreateDbCommand(null)) {
					binder.Bind(cmd).ExecuteNonQuery();
					binder.Replace("id", 2).Replace("name", "YYY").Bind(cmd).ExecuteNonQuery();
					binder.Replace("id", 3).Replace("name", "ZZZ").Bind(cmd).ExecuteNonQuery();
				}
			};
			var dacCmd = new DelegateDacCommand(action);
			service.ExecuteCommand(dacCmd);
			DoPickFirst(service);
		}

		protected void DoPickFirst(ICommandService service) {
			var dacCmd = new FpickerCommand();
			service.ExecuteCommand(dacCmd);
			Assert.AreEqual(2, dacCmd.ResultCount);
			Assert.AreEqual(2, int.Parse(dacCmd.First[0].ToString()));
		}

		protected void DoSelectLike(ICommandService service) {
			DoInitialize(service);
			var builder = new ValueBinderBuilder("SELECT * FROM T_TX_TEST WHERE NAME LIKE {0}",
				BindValue.WithFormat("name", BindValue.ForwardMatch, "Y", 20));
			var cmd = new BinderQueryDacCommand(builder);
			service.ExecuteCommand(cmd);
			var result = cmd.Results;
			Assert.AreEqual(1, result.Count);
			Assert.AreEqual("2", result[0]["ID"].ToString());
			Assert.AreEqual("YYY", result[0]["NAME"]);
		}

		protected void DoSelectIn(ICommandService service) {
			DoInitialize(service);
			var expander = new InExpander<DummyNVarCharType>("nam", "SELECT * FROM T_TX_TEST WHERE NAME IN ({0}) ORDER BY ID", 3, 20);
			expander.AddValue(new DummyNVarCharType("ZZZ")).AddValue(new DummyNVarCharType("YYY"));
			var builder = new ValueBinderBuilder(expander.Text, expander.BindValues.ToArray());
			var cmd = new BinderQueryDacCommand(builder) {
				OnBound = (dbcmd) => {
					Assert.AreEqual(3, dbcmd.Parameters.Count);
					Assert.AreEqual(20, dbcmd.Parameters[0].Size);
					Assert.AreEqual(20, dbcmd.Parameters[1].Size);
					Assert.AreEqual(20, dbcmd.Parameters[2].Size);
				}
			};
			service.ExecuteCommand(cmd);
			var result = cmd.Results;
			Assert.AreEqual(2, result.Count);
			Assert.AreEqual("2", result[0]["ID"].ToString());
			Assert.AreEqual("YYY", result[0]["NAME"]);
			Assert.AreEqual("3", result[1]["ID"].ToString());
			Assert.AreEqual("ZZZ", result[1]["NAME"]);
			
			expander = new InExpander<DummyNVarCharType>("nam", "SELECT * FROM T_TX_TEST WHERE NAME IN ({0}) ORDER BY ID");
			expander.AddValue(new DummyNVarCharType("ZZZ")).AddValue(new DummyNVarCharType("YYY"));
			builder = new ValueBinderBuilder(expander.Text, expander.BindValues.ToArray());
			cmd = new BinderQueryDacCommand(builder) {
				OnBound = (dbcmd) => {
					Assert.AreEqual(2, dbcmd.Parameters.Count);
					Assert.AreEqual(64, dbcmd.Parameters[0].Size);
					Assert.AreEqual(64, dbcmd.Parameters[1].Size);
				}
			};
			service.ExecuteCommand(cmd);
			result = cmd.Results;
			Assert.AreEqual(2, result.Count);
			Assert.AreEqual("2", result[0]["ID"].ToString());
			Assert.AreEqual("YYY", result[0]["NAME"]);
			Assert.AreEqual("3", result[1]["ID"].ToString());
			Assert.AreEqual("ZZZ", result[1]["NAME"]);
		}

		protected void DoSelectNotIn(ICommandService service) {
			DoInitialize(service);
			var expander = new InExpander<DummyNVarCharType>("nam", "SELECT * FROM T_TX_TEST WHERE NAME NOT IN ({0}) ORDER BY ID", 3, 20);
			expander.AddValue(new DummyNVarCharType("ZZZ")).AddValue(new DummyNVarCharType("YYY"));
			var builder = new ValueBinderBuilder(expander.Text, expander.BindValues.ToArray());
			var cmd = new BinderQueryDacCommand(builder) {
				OnBound = (dbcmd) => {
					Assert.AreEqual(3, dbcmd.Parameters.Count);
					Assert.AreEqual(20, dbcmd.Parameters[0].Size);
					Assert.AreEqual(20, dbcmd.Parameters[1].Size);
					Assert.AreEqual(20, dbcmd.Parameters[2].Size);
				}
			};
			service.ExecuteCommand(cmd);
			var result = cmd.Results;
			Assert.AreEqual(1, result.Count);
			Assert.AreEqual("1", result[0]["ID"].ToString());
			Assert.AreEqual("XXX", result[0]["NAME"]);
		}

		protected int DoTestSimple(ICommandService service) {
			Action<DacCommandContext, IDbCommand> action = (ctx, cmd) => {
				var binder = new ValueBinderBuilder("Select * from T_TX_TEST")
								.Build(ctx.ConnectionFactory.DatabaseDialect);
				binder.Bind(cmd);
			};
			var result = DoSelect(service, action);
			Assert.IsNotNull(result);
			return result.Count;
		}

		protected void DoDeadLockRecovery(ICommandService service, IDatabaseDialect dialect) {
			var barrier = new Barrier(2);
			var cmd1 = new SimpleCommandChain();
			var cmd2 = new SimpleCommandChain();
			var executor = new ParallelExecution<DacCommandContext> {
				Service = service
			};
			SetupUpdates(dialect, barrier, cmd1, cmd2);
			executor.Add(cmd1, cmd2);
			executor.Execute();

			Assert.IsNull(executor.ErrorFor(cmd1), "Should not cause exception.");
			Assert.IsNull(executor.ErrorFor(cmd2), "Should not cause exception.");
		}

		private void SetupUpdates(IDatabaseDialect dialect, Barrier barrier,
			AbstractCommandChain<DacCommandContext> cmd1,
			AbstractCommandChain<DacCommandContext> cmd2) {
			var fragment1 = new BasicFragment("UPDATE T_TX_TEST SET NAME = {0} WHERE ID = {1}")
					.AddValues(BindValue.Create("name", "1to2", 10))
					.AddValues(BindValue.Create("id", 1));
			var fragment2 = new BasicFragment("UPDATE T_TX_TEST2 SET NAME = {0} WHERE ID = {1}")
					.AddValues(BindValue.Create("name", "1to2", 10))
					.AddValues(BindValue.Create("id", 1));

			var binder1 = new ValueBinderBuilder(fragment1).Build(dialect);
			var binder2 = new ValueBinderBuilder(fragment2).Build(dialect);
			cmd1.Add(
				new BinderNonQueryDacCommand(binder1),
				new BarrierCommand(barrier),
				new BinderNonQueryDacCommand(binder2)
			);

			binder1 = new ValueBinderBuilder(fragment1).Build(dialect);
			binder2 = new ValueBinderBuilder(fragment2).Build(dialect);
			binder1.Replace("name", "2to1");
			binder2.Replace("name", "2to1");
			cmd2.Add(
				new BinderNonQueryDacCommand(binder2),
				new BarrierCommand(barrier),
				new BinderNonQueryDacCommand(binder1)
			);
		}

		protected void DoSerializationRecovery(ICommandService service, IDatabaseDialect dialect) {
			DoInitialize(service);
			var cmd1 = new SimpleCommandChain();
			var cmd2 = new SimpleCommandChain();
			var executor = new ParallelExecution<DacCommandContext> {
				Service = service
			};
			SetupSerialization(dialect, cmd1, cmd2);
			executor.Add(cmd1, cmd2);
			executor.Execute();

			Assert.IsNull(executor.ErrorFor(cmd1), "Should not cause exception.");
			Assert.IsNull(executor.ErrorFor(cmd2), "Should not cause exception.");
		}

		private void SetupSerialization(IDatabaseDialect dialect,
			AbstractCommandChain<DacCommandContext> cmd1,
			AbstractCommandChain<DacCommandContext> cmd2) {
			var fragment1 = new BasicFragment("SELECT * FROM T_TX_TEST WHERE ID = {0}")
					.AddValues(BindValue.Create("id", 1));
			var fragment2 = new BasicFragment("UPDATE T_TX_TEST SET NAME = {0} WHERE ID = {1}")
					.AddValues(BindValue.Create("name", "1to2", 10))
					.AddValues(BindValue.Create("id", 1));

			var barrier1 = new Barrier(2);
			var barrier2 = new Barrier(2);

			var binder1 = new ValueBinderBuilder(fragment1).Build(dialect);
			var binder2 = new ValueBinderBuilder(fragment2).Build(dialect);
			cmd1.Add(
				new BinderQueryDacCommand(binder1),
				new BarrierCommand(barrier1),
				new BinderQueryDacCommand(binder1),
				new BarrierCommand(barrier2),
				new BinderNonQueryDacCommand(binder2)
			);

			fragment2.ReplaceValue(BindValue.Create("name", "2to1", 10));
			binder1 = new ValueBinderBuilder(fragment1).Build(dialect);
			binder2 = new ValueBinderBuilder(fragment2).Build(dialect);
			cmd2.Add(
				new BinderQueryDacCommand(binder1),
				new BarrierCommand(barrier1),
				new BinderNonQueryDacCommand(binder2),
				new BarrierCommand(barrier2),
				new BinderQueryDacCommand(binder1)
			);
		}

		protected void DoBindNumericValues<T>(Action<T> verifier) where T: DbParameterCollection {
			var factory = GetConnectionFactory();
			int idx = 0;
			var pre = "para";

			var frg = new FragmentCollection();
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (sbyte)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (byte)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (short)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (ushort)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (int)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (uint)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (long)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (ulong)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (float)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (double)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (decimal)idx++)));

			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (sbyte?)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (byte?)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (short?)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (ushort?)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (int?)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (uint?)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (long?)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (ulong?)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (float?)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (double?)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (decimal?)idx++)));
			var builder = new ValueBinderBuilder(frg);
			var dialect = factory.DatabaseDialect;
			var binder = builder.Build(dialect);

			using (var con = factory.NewConnection(false, IsolationLevel.ReadCommitted)) {
				using (var cmd = con.CreateCommand()) {
					binder.Bind(cmd);
					verifier((T)cmd.Parameters);
				}
			}
		}

		protected void DoBindNumericValuesOra<T>(Action<T> verifier) where T: DbParameterCollection {
			var factory = GetConnectionFactory();
			int idx = 0;
			var pre = "para";

			var frg = new FragmentCollection();
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (byte)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (short)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (int)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (long)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (float)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (double)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (decimal)idx++)));

			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (byte?)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (short?)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (int?)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (long?)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (float?)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (double?)idx++)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx, (decimal?)idx++)));
			var builder = new ValueBinderBuilder(frg);
			var dialect = factory.DatabaseDialect;
			var binder = builder.Build(dialect);

			using (var con = factory.NewConnection(false, IsolationLevel.ReadCommitted)) {
				using (var cmd = con.CreateCommand()) {
					binder.Bind(cmd);
					verifier((T)cmd.Parameters);
				}
			}
		}

		protected void DoBindWrapper<T>(Action<T> verifier) where T: DbParameterCollection {
			var factory = GetConnectionFactory();
			int idx = 0;
			var pre = "para";

			var frg = new FragmentCollection();
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx++, new CharType(null))));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx++, new NCharType(null))));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx++, new VarCharType(null))));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx++, new NVarCharType(null))));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx++, new DateType(null))));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx++, new TimestampType(null))));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Numeric(pre + idx, new DummyNumeric(null))));
			var builder = new ValueBinderBuilder(frg);
			var dialect = factory.DatabaseDialect;
			var binder = builder.Build(dialect);

			using (var con = factory.NewConnection(false, IsolationLevel.ReadCommitted)) {
				using (var cmd = con.CreateCommand()) {
					binder.Bind(cmd);
					verifier((T)cmd.Parameters);
				}
			}
		}

		protected void DoBindString<T>(Action<T> verifier) where T: DbParameterCollection {
			var factory = GetConnectionFactory();
			int idx = 0;
			var pre = "para";

			var frg = new FragmentCollection();
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx++, "Test", 10)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx++, "Test".ToCharArray(), 10)));
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx++, DateTime.Now)));
			var builder = new ValueBinderBuilder(frg);
			var dialect = factory.DatabaseDialect;
			var binder = builder.Build(dialect);

			using (var con = factory.NewConnection(false, IsolationLevel.ReadCommitted)) {
				using (var cmd = con.CreateCommand()) {
					binder.Bind(cmd);
					verifier((T)cmd.Parameters);
				}
			}
		}

		protected void DoBindUnknown<T>(Action<T> verifier) where T: DbParameterCollection {
			var factory = GetConnectionFactory();
			int idx = 0;
			var pre = "para";

			var frg = new FragmentCollection();
			frg.Append(new BasicCondition(pre + idx + "={0}", BindValue.Create(pre + idx++, true, 10)));
			var builder = new ValueBinderBuilder(frg);
			var dialect = factory.DatabaseDialect;
			var binder = builder.Build(dialect);

			using (var con = factory.NewConnection(false, IsolationLevel.ReadCommitted)) {
				using (var cmd = con.CreateCommand()) {
					binder.Bind(cmd);
					verifier((T)cmd.Parameters);
				}
			}
		}

		protected void DoGetByType(ICommandService service) {
			var cmd = new ReaderTestCommand();
			service.ExecuteCommand(cmd);

			var cmd2 = new ReaderMapTestCommand();
			service.ExecuteCommand(cmd2);
		}

		protected void DoGetByTypeAll(ICommandService service) {
			var cmd2 = new ReaderMapAllTestCommand();
			service.ExecuteCommand(cmd2);
		}

		protected void DoGetCurrentTime(ICommandService service, string dummy, TimeSpan offset) {
			var cmd = new DelegateSelectCommand((ctx, dbcmd) => {
				var dialect = ctx.ConnectionFactory.DatabaseDialect;
				Assert.AreEqual(dummy.ToUpper(), dialect.DummyTable.ToUpper());
				new ValueBinderBuilder(new SimpleFragment(string.Format("SELECT {0} AS CUR {1}",
							dialect.CurrentTimestampLiteral, dialect.DummyTableWithFrom))).Build(dialect)
						.Bind(dbcmd);
			});
			var error = 15;
			var before = DateTime.UtcNow - TimeSpan.FromSeconds(error) + offset; 
			service.ExecuteCommand(cmd);
			var after = DateTime.UtcNow + TimeSpan.FromSeconds(error) + offset;
			Assert.AreEqual(1, cmd.ResultCount);
			var dt = cmd.Results[0]["CUR"] as DateTime?;
			Assert.IsNotNull(dt);
			var msg = "Before: " + before + " After: " + after + " DbDate: " + dt;
			Assert.IsTrue(dt <= after, msg);
			Assert.IsTrue(before <= dt, msg);
		}

		protected void DoGetCurrentUtcTime(ICommandService service, string dummy, TimeSpan offset) {
			var cmd = new DelegateSelectCommand((ctx, dbcmd) => {
				var dialect = ctx.ConnectionFactory.DatabaseDialect;
				Assert.AreEqual(dummy.ToUpper(), dialect.DummyTable.ToUpper());
				new ValueBinderBuilder(new SimpleFragment(string.Format("SELECT {0} AS CUR {1}",
							dialect.CurrentUtcTimestampLiteral, dialect.DummyTableWithFrom))).Build(dialect)
						.Bind(dbcmd);
			});
			var error = 15;
			var before = DateTime.UtcNow - TimeSpan.FromSeconds(error) + offset;
			service.ExecuteCommand(cmd);
			var after = DateTime.UtcNow + TimeSpan.FromSeconds(error) + offset;
			Assert.AreEqual(1, cmd.ResultCount);
			var dt = cmd.Results[0]["CUR"] as DateTime?;
			Assert.IsNotNull(dt);
			var msg = "Before: " + before + " After: " + after + " DbDate: " + dt;
			Assert.IsTrue(dt <= after, msg);
			Assert.IsTrue(before <= dt, msg);
		}
	}

	[ExcludeFromCodeCoverage]
	internal class ReaderTestCommand : BulkSelectCommandTemplate<DacCommandContext> {
		public ReaderTestCommand() {
			TxIntention = TxIntention.Select;
		}

		#region Overrides of BulkSelectCommandTemplate<DacCommandContext>
		public override void Execute(DacCommandContext context) {
			DoExecute(context);
		}

		protected override void SetParameters(DacCommandContext context, IDatabaseDialect dialect, DbCommand dbCommand) {
			new ValueBinderBuilder(
						new BasicFragment("SELECT * FROM T_TYPE_TEST WHERE ID IN ({0}, {1})",
								BindValue.Create("id1", 1), BindValue.Create("id2", 2)))
					.Build(context.ConnectionFactory.DatabaseDialect).Bind(dbCommand);
		}

		protected override void DoInReader(DacCommandContext context, DbDataReader reader) {
			while (reader.Read()) {
				var id = reader.GetInteger("ID");
				if (id == 1) {
					CheckValue(reader);
				}
				else {
					CheckNull(reader);
				}
			}
		}
		#endregion

		private void CheckValue(DbDataReader reader) {
			Assert.AreEqual(1234567890, reader.GetInteger("INT_TYPE"));
			Assert.AreEqual((short)20, reader.GetShort("SHORT_TYPE"));
			Assert.AreEqual(12345678901234, reader.GetLong("LONG_TYPE"));
			Assert.AreEqual("TEST", reader.GetString("STRING_TYPE"));
			Assert.AreEqual((float)1.23456, reader.GetFloat("FLOAT_TYPE"));
			Assert.AreEqual(12345.67890, reader.GetDouble("DOUBLE_TYPE"));
			Assert.AreEqual(DateTime.Parse("2009-07-16 08:28:01"), reader.GetDateTime("DATETIME_TYPE"));
			Assert.AreEqual(123456789012345.678m, reader.GetDecimal("DECIMAL_TYPE"));
		}
		private void CheckNull(DbDataReader reader) {
			Assert.IsNull(reader.GetInteger("INT_TYPE"));
			Assert.IsNull(reader.GetShort("SHORT_TYPE"));
			Assert.IsNull(reader.GetLong("LONG_TYPE"));
			Assert.IsNull(reader.GetString("STRING_TYPE"));
			Assert.IsNull(reader.GetFloat("FLOAT_TYPE"));
			Assert.IsNull(reader.GetDouble("DOUBLE_TYPE"));
			Assert.IsNull(reader.GetDateTime("DATETIME_TYPE"));
			Assert.IsNull(reader.GetDecimal("DECIMAL_TYPE"));
		}
	}

	[ExcludeFromCodeCoverage]
	public class TypeTestModel {
		[EntityMapping(CName = "ID")]
		public int Id { get; set; }
		[EntityMapping(CName = "INT_TYPE")]
		public int? intType { get; set; }
		[EntityMapping(CName = "SHORT_TYPE")]
		public short? shortType { get; set; }
		[EntityMapping(CName = "LONG_TYPE")]
		public long? longType {get; set; }
		public string stringType { get;set; }
		[EntityMapping(CName = "FLOAT_TYPE")]
		public float? floatType { get; set; }
		[EntityMapping(CName = "DOUBLE_TYPE")]
		public double? doubleType { get; set; }
		[EntityMapping(CName = "DATETIME_TYPE")]
		public DateTime? dateTmeType { get; set; }
		[EntityMapping(CName = "DECIMAL_TYPE")]
		public decimal? decimalType { get;set; }
	}

	[ExcludeFromCodeCoverage]
	internal class ReaderMapAllTestCommand : ReaderMapTestCommand {
		protected override void DoInReader(DacCommandContext context, DbDataReader reader) {
			var mapper = new DbDataReaderMapper<TypeTestModel>();
			try {
				mapper.MapTo(m => m.stringType);
			}
			catch (ArgumentNullException ane) {
				Assert.AreEqual("name", ane.ParamName);
			}

			mapper.ApplyMapTo()
					.MapTo("STRING_TYPE", m => m.stringType);
			mapper.SetUnmodifiable();
			try {
				mapper.MapTo("ERROR", m => m.stringType);
				Assert.Fail("Should throw an exception.");
			}
			catch (InvalidOperationException) {
				// Success.
			}

			while (reader.Read()) {
				var id = reader.GetInteger("ID");
				TypeTestModel model = mapper.Map(reader);
				if (id == 1) {
					CheckValue(model);
				}
				else {
					CheckNull(model);
				}
			}
		}
	}

	[ExcludeFromCodeCoverage]
	internal class ReaderMapTestCommand : BulkSelectCommandTemplate<DacCommandContext> {
		public ReaderMapTestCommand() {
			TxIntention = TxIntention.Select;
		}

		#region Overrides of BulkSelectCommandTemplate<DacCommandContext>
		public override void Execute(DacCommandContext context) {
			DoExecute(context);
		}

		protected override void SetParameters(DacCommandContext context, IDatabaseDialect dialect, DbCommand dbCommand) {
			new ValueBinderBuilder(
						new BasicFragment("SELECT * FROM T_TYPE_TEST WHERE ID IN ({0}, {1})",
								BindValue.Create("id1", 1), BindValue.Create("id2", 2)))
					.Build(context.ConnectionFactory.DatabaseDialect).Bind(dbCommand);
		}

		protected override void DoInReader(DacCommandContext context, DbDataReader reader) {
			var mapper = new DbDataReaderMapper<TypeTestModel>();
			try {
				mapper.MapTo(m => m.stringType);
			}
			catch (ArgumentNullException ane) {
				Assert.AreEqual("name", ane.ParamName);
			}
			mapper.MapTo(m => m.Id)
				.MapTo(m => m.intType)
				.MapTo(m => m.shortType)
				.MapTo(m => m.longType)
				.MapTo("STRING_TYPE", m => m.stringType)
				.MapTo(m => m.floatType)
				.MapTo(m => m.doubleType)
				.MapTo(m => m.dateTmeType)
				.MapTo(m => m.decimalType);

			while (reader.Read()) {
				var id = reader.GetInteger("ID");
				TypeTestModel model = mapper.Map(reader);
				if (id == 1) {
					CheckValue(model);
				}
				else {
					CheckNull(model);
				}
			}
		}
		#endregion

		protected void CheckValue(TypeTestModel model) {
			Assert.AreEqual(1, model.Id);
			Assert.AreEqual(1234567890, model.intType);
			Assert.AreEqual((short)20, model.shortType);
			Assert.AreEqual(12345678901234, model.longType);
			Assert.AreEqual("TEST", model.stringType);
			Assert.AreEqual((float)1.23456, model.floatType);
			Assert.AreEqual(12345.67890, model.doubleType);
			Assert.AreEqual(DateTime.Parse("2009-07-16 08:28:01"), model.dateTmeType);
			Assert.AreEqual(123456789012345.678m, model.decimalType);
		}
		protected void CheckNull(TypeTestModel model) {
			Assert.AreEqual(2, model.Id);
			Assert.IsNull(model.intType);
			Assert.IsNull(model.shortType);
			Assert.IsNull(model.longType);
			Assert.IsNull(model.stringType);
			Assert.IsNull(model.floatType);
			Assert.IsNull(model.doubleType);
			Assert.IsNull(model.dateTmeType);
			Assert.IsNull(model.decimalType);
		}
	}
}
