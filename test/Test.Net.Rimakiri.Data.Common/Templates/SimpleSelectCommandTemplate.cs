// ==============================================================================
//     Net.Rimakiri.Command.Database
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Collections.Generic;
using System.Data.Common;
using System.Diagnostics.CodeAnalysis;
using Net.Rimakiri.Command.Database;

namespace Test.Net.Rimakiri.Data.Common.Templates {
	/// <summary>
	/// SELECT文を発行し、結果を簡易に取得するためのテンプレートクラスです。
	/// </summary>
	/// <inheritdoc />
	[ExcludeFromCodeCoverage]
	public abstract class SimpleSelectCommandTemplate<T> : BulkSelectCommandTemplate<T>
				where T:DacCommandContext {
		/// <summary>
		/// SELECTの結果件数を取得します。
		/// </summary>
		public int ResultCount { get; private set; }

		/// <summary>
		/// SELECTの結果を取得します。
		/// </summary>
		public IList<IDictionary<string, object>> Results { get; private set; }

		#region Overrides of BulkSelectCommandTemplate
		/// <inheritdoc />
		protected override void DoInReader(T context, DbDataReader reader) {
			ResultCount = 0;
			var rslts = new List<IDictionary<string, object>>();
			while (reader.Read()) {
				ResultCount++;
				var dic = new Dictionary<string, object>();
				for (int i = 0, l = reader.FieldCount; i < l; i++) {
					var value = reader.IsDBNull(i) ? null : reader.GetValue(i);
					dic[reader.GetName(i)] = value;
				}
				rslts.Add(dic);
			}
			Results = rslts.AsReadOnly();
		}
		#endregion
	}
}
