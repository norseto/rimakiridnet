// ==============================================================================
//     Net.Rimakiri.Command.Database
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Data;
using System.Data.Common;
using System.Diagnostics.CodeAnalysis;
using Net.Rimakiri.Command;
using Net.Rimakiri.Command.Database;
using Net.Rimakiri.Data.Interfaces;

namespace Test.Net.Rimakiri.Data.Common.Templates {
	/// <summary>
	/// SELECT文を発行し、全データの<see cref="IDataReader" />に対する処理を行う
	/// ための抽象クラスです。
	/// </summary>
	/// <typeparam name="T">
	/// 使用する<see cref="CommandContext"/>のタイプ。
	/// <see cref="DacCommandContext"/>またはその派生クラスである必要があります。
	/// </typeparam>
	/// <inheritdoc />
	[ExcludeFromCodeCoverage]
	public abstract class BulkSelectCommandTemplate<T> : DacCommandTemplateBase<T>
			 where T: DacCommandContext {
		/// <summary>
		/// SELECTのクエリを設定または取得します。ここで指定したクエリに対して、
		/// <see cref="SetParameters"/>を実行します。
		/// </summary>
		public string Query { get; set; }

		/// <summary>
		/// コマンドを実行します。<see cref="Query" />で指定されたSELECT文の
		/// コマンドを生成し、<see cref="SetParameters" />を呼出した後にクエリ
		/// を実行し、結果を<see cref="DoInReader" />にコールバックします。
		/// </summary>
		/// <param name="context">実行中のコマンドコンテキスト</param>
		protected virtual void DoExecute(T context) {
			using (var dbcmd = context.CreateDbCommand(ConnectionFactoryName)) {
				if (Query != null) {
					dbcmd.CommandText = Query;
				}
				var dialect = context.GetConnectionFactory(ConnectionFactoryName).DatabaseDialect;
				SetParameters(context, dialect, dbcmd);
				using (var reader = dbcmd.ExecuteReader()) {
					DoInReader(context, reader);
				}
			}
		}

		/// <summary>
		/// パラメタを設定します。
		/// </summary>
		/// <param name="context">コマンド実行コンテキスト</param>
		/// <param name="dialect">対象コマンドのデータベース接続に対応した<see cref="IDatabaseDialect"/></param>
		/// <param name="dbCommand">対象コマンド</param>
		protected abstract void SetParameters(T context, IDatabaseDialect dialect, DbCommand dbCommand);

		/// <summary>
		/// クエリの結果に対する<see cref="IDataReader"/>を処理します。
		/// </summary>
		/// <param name="context">コマンド実行コンテキスト</param>
		/// <param name="reader">クエリ結果の<see cref="DbDataReader"/></param>
		protected abstract void DoInReader(T context, DbDataReader reader);
	}
}
