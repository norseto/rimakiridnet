// ==============================================================================
//     Net.Rimakiri.Command.Database
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Data.Common;
using System.Diagnostics.CodeAnalysis;
using Net.Rimakiri.Command;
using Net.Rimakiri.Command.Database;
using Net.Rimakiri.Data.Interfaces;

namespace Test.Net.Rimakiri.Data.Common.Templates {
	/// <summary>
	/// 非検索実行を行うテンプレートクラスです。
	/// </summary>
	/// <typeparam name="T">
	/// 使用する<see cref="CommandContext"/>のタイプ。
	/// <see cref="DacCommandContext"/>またはその派生クラスである必要があります。
	/// </typeparam>
	/// <inheritdoc />
	[ExcludeFromCodeCoverage]
	public abstract class NonQueryCommandTemplate<T> : DacCommandTemplateBase<T>
				where T: DacCommandContext {
		/// <summary>
		/// インスタンスを初期化します。<see cref="TxIntention"/>は、
		/// <see cref="TxIntention.Update"/>で初期化されます。
		/// </summary>
		protected NonQueryCommandTemplate() {
			TxIntention = TxIntention.Update;
		}

		/// <summary>
		/// 実行するステートメントを設定または取得します。ここで指定したステートメントに対して、
		/// <see cref="SetParameters"/>を実行します。
		/// </summary>
		public string Statement { get; set; }

		/// <summary>
		/// 実行結果を取得します。
		/// </summary>
		public int Result { get; private set; }

		/// <summary>
		/// コマンドを実行します。<see cref="Statement" />で指定されたSELECT文の
		/// コマンドを生成し、<see cref="SetParameters" />を呼出した後にクエリ
		/// を実行し、結果を<see cref="Result" />に設定します。
		/// </summary>
		/// <param name="context">実行中のコマンドコンテキスト</param>
		protected virtual void DoExecute(T context) {
			var dialect = context.GetConnectionFactory(ConnectionFactoryName).DatabaseDialect;
			using (var dbcmd = context.CreateDbCommand(ConnectionFactoryName)) {
				if (Statement != null) {
					dbcmd.CommandText = Statement;
				}
				SetParameters(context, dialect, dbcmd);
				Result = dbcmd.ExecuteNonQuery();
			}
		}

		/// <summary>
		/// パラメタを設定します。
		/// </summary>
		/// <param name="context">コマンド実行コンテキスト</param>
		/// <param name="dialect">対象コマンドのデータベース接続に対応した<see cref="IDatabaseDialect"/></param>
		/// <param name="dbCommand">対象コマンド</param>
		protected abstract void SetParameters(T context, IDatabaseDialect dialect, DbCommand dbCommand);
	}
}
