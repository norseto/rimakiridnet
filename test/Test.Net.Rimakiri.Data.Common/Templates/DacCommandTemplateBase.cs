// ==============================================================================
//     Net.Rimakiri.Command.Database
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Diagnostics.CodeAnalysis;
using Net.Rimakiri.Command;
using Net.Rimakiri.Command.Database;
using Net.Rimakiri.Data.Interfaces;

namespace Test.Net.Rimakiri.Data.Common.Templates {
	/// <summary>
	/// <see cref="DacCommandContext"/>を使用するテンプレートクラスの抽象基底クラスです。
	/// </summary>
	/// <typeparam name="T">コンテキストのタイプ</typeparam>
	[ExcludeFromCodeCoverage]
	public abstract class DacCommandTemplateBase<T> : ICommand<T>, IIsolationCoodinatee
		where T: DacCommandContext {
		/// <summary>
		/// 使用する<see cref="IConnectionFactory"/>の名称を設定または取得します。
		/// </summary>
		public string ConnectionFactoryName { get; set; }

		#region Implementation of IIsolationCoodinatee
		/// <inheritdoc />
		public TxIntention TxIntention { get; set; } = TxIntention.Select;
		#endregion

		#region Implementation of ICommand<in DacCommandContext>
		/// <summary>
		/// 冪等性を取得または設定します
		/// </summary>
		public Idempotent Idempotent { get; set; } = Idempotent.Transactional;

		/// <summary>
		/// トランザクション分離レベルを取得または設定します。
		/// </summary>
		public TxScope TxScope { get; set; } = TxScope.Required;

		/// <summary>
		/// コマンド実行後コールバックです。このクラスでは何もしません。
		/// </summary>
		/// <param name="context">コマンドのコンテキスト</param>
		public virtual void PostExecute(T context) {
			// Nothing to do
		}

		/// <summary>
		/// コマンド実行前コールバックです。このクラスでは何もしません。
		/// </summary>
		/// <param name="context">コマンドのコンテキスト</param>
		public virtual void PreExecute(T context) {
			// Nothing to do
		}

		/// <inheritdoc />
		/// <summary>
		/// コマンドを実行します。
		/// </summary>
		/// <param name="context">コマンドコンテキスト</param>
		public abstract void Execute(T context);
		#endregion

		#region Implementation of ICommandVisitAcceptor
		/// <inheritdoc />
		public virtual void AcceptVisitor(ICommandVisitor visitor) {
			visitor.Visit(this);
		}
		#endregion

	}
}
