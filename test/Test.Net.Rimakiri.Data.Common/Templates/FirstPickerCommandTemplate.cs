// ==============================================================================
//     Net.Rimakiri.Command.Database
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Collections.Generic;
using System.Data.Common;
using System.Diagnostics.CodeAnalysis;
using Net.Rimakiri.Command.Database;

namespace Test.Net.Rimakiri.Data.Common.Templates {
	/// <summary>
	/// 検索を実行し、最初のデータのみを取得するコマンドのテンプレートクラスです。
	/// </summary>
	/// <inheritdoc />
	[ExcludeFromCodeCoverage]
	public abstract class FirstPickerCommandTemplate<T> : BulkSelectCommandTemplate<T>
				where T:DacCommandContext {
		/// <summary>
		/// SELECTの結果件数を取得します。
		/// </summary>
		public int ResultCount { get; private set; }

		/// <summary>
		/// SELECTの最初の結果を取得します。
		/// </summary>
		public IList<object> First { get; private set; }

		#region Overrides of BulkSelectCommandTemplate
		/// <inheritdoc />
		/// <remarks>
		/// このクラスでは、単にベースクラスの同名のメソッドを呼び出します。
		/// 例外発生時にスタックトレースに出力されるために存在しています。
		/// </remarks>
		protected override void DoExecute(T context) {
			base.DoExecute(context);
		}

		/// <inheritdoc />
		protected override void DoInReader(T context, DbDataReader reader) {
			ResultCount = 0;
			while (reader.Read()) {
				if (ResultCount++ > 0) {
					continue;
				}
				var list = new List<object>();
				for (int i = 0, l = reader.FieldCount; i < l; i++) {
					var value = reader.IsDBNull(i) ? null : reader.GetValue(i);
					list.Add(value);
				}
				First = list;
			}
		}
		#endregion
	}
}
