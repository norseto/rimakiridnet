// ==============================================================================
//     Test.Rimakiri.Data.Rdbms
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Transactions;
using Net.Rimakiri.Command;
using Net.Rimakiri.Command.Database;
using Net.Rimakiri.Data;
using Net.Rimakiri.Data.Interfaces;
using Net.Rimakiri.Data.TypeWrappers;
using Test.Net.Rimakiri.Data.Common.Templates;
using IsolationLevel = System.Transactions.IsolationLevel;

namespace Test.Net.Rimakiri.Data.Common {
	[ExcludeFromCodeCoverage]
	public class SimpleCommandChain : AbstractCommandChain<DacCommandContext> {
		#region Overrides of AbstractCommandChain<DbCommandContext>
		public override Idempotent Idempotent {
			get { return Idempotent.Transactional; }
			set { }
		}

		public override TxScope TxScope {
			get { return TxScope.Required; }
			set { }
		}
		#endregion

		public SimpleCommandChain(params ICommand<DacCommandContext>[] commands)
			: base(commands) {
		}

		public SimpleCommandChain() {
			// Empty
		}
	}

	[ExcludeFromCodeCoverage]
	public class TestDacContext : DacCommandContext {
		#region Overrides of DacCommandContext
		protected override DbCommand NewDbCommand(DbConnection con) {
			var cmd = base.NewDbCommand(con);
			cmd.Connection = null;
			return cmd;
//			return new DbCommandWrapper(cmd);
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	internal class DelegateSelectCommand : SimpleSelectCommandTemplate<DacCommandContext> {
		private readonly Action<DacCommandContext, DbCommand> action;
		public DelegateSelectCommand(Action<DacCommandContext, DbCommand> action) {
			this.action = action;
		}
		#region Overrides of BulkSelectCommandTemplate<DacCommandContext>
		public override void Execute(DacCommandContext context) {
			DoExecute(context);
		}

		protected override void SetParameters(DacCommandContext context, IDatabaseDialect dialect, DbCommand dbCommand) {
			action(context, dbCommand);
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	internal class FpickerCommand : FirstPickerCommandTemplate<DacCommandContext> {
		private IValueBinder binder;

		#region Overrides of BulkSelectCommandTemplate<DacCommandContext>
		public override void PreExecute(DacCommandContext context) {
			base.PreExecute(context);
			var builder = new ValueBinderBuilder("SELECT * FROM T_TX_TEST WHERE ID > {0}", BindValue.Create("id", 1));
			binder = builder.Build(context.ConnectionFactory.DatabaseDialect);
		}

		public override void Execute(DacCommandContext context) {
			DoExecute(context);
		}

		protected override void SetParameters(DacCommandContext context, IDatabaseDialect dialect, DbCommand dbCommand) {
			binder.Bind(dbCommand);
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	internal class DelegateNonQueryCommand : NonQueryCommandTemplate<DacCommandContext> {
		private readonly Action<DacCommandContext, IDbCommand> action;
		public DelegateNonQueryCommand(Action<DacCommandContext, IDbCommand> action) {
			this.action = action;
		}
		#region Overrides of BulkSelectCommandTemplate<DacCommandContext>
		public override void Execute(DacCommandContext context) {
			DoExecute(context);
		}

		protected override void SetParameters(DacCommandContext context, IDatabaseDialect dialect, DbCommand dbCommand) {
			action(context, dbCommand);
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	public class DelegateDacCommand : NonQueryCommandTemplate<DacCommandContext> {
		private readonly Action<DacCommandContext> action;
		public DelegateDacCommand(Action<DacCommandContext> action) {
			this.action = action;
		}
		public override void Execute(DacCommandContext context) {
			action(context);
		}

		protected override void SetParameters(DacCommandContext context, IDatabaseDialect dialect, DbCommand dbCommand) {
		}
	}

	[ExcludeFromCodeCoverage]
	public class SimpleNonQueryDacCommand : NonQueryCommandTemplate<DacCommandContext> {
		public override void Execute(DacCommandContext context) {
			DoExecute(context);
		}

		protected override void SetParameters(DacCommandContext context, IDatabaseDialect dialect, DbCommand dbCommand) {
		}
	}

	[ExcludeFromCodeCoverage]
	internal class BinderNonQueryDacCommand : NonQueryCommandTemplate<DacCommandContext> {
		private readonly IValueBinder binder;
		public Action<DbCommand> OnBound { get; set; }
		public BinderNonQueryDacCommand(IValueBinder binder) {
			this.binder = binder;
		}
		#region Overrides of NonQueryCommandTemplate<DacCommandContext>
		public override void Execute(DacCommandContext context) {
			DoExecute(context);
		}

		protected override void SetParameters(DacCommandContext context, IDatabaseDialect dialect, DbCommand dbCommand) {
			binder.Bind(dbCommand);
			Statement = binder.Statement;
			if (OnBound != null) {
				OnBound(dbCommand);
			}
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	public class BinderQueryDacCommand : SimpleSelectCommandTemplate<DacCommandContext> {
		private IValueBinder binder;
		private readonly ValueBinderBuilder builder;
		public Action<DbCommand> OnBound { get; set; }
		public BinderQueryDacCommand(IValueBinder binder) {
			this.binder = binder;
		}
		public BinderQueryDacCommand(ValueBinderBuilder builder) {
			this.builder = builder;
		}
		#region Overrides of NonQueryCommandTemplate<DacCommandContext>
		public override void Execute(DacCommandContext context) {
			DoExecute(context);
		}

		protected override void SetParameters(DacCommandContext context, IDatabaseDialect dialect, DbCommand dbCommand) {
			if (builder != null) {
				binder = builder.Build(dialect);
			}
			Query = binder.Statement;
			binder.Bind(dbCommand);
			if (OnBound != null) {
				OnBound(dbCommand);
			}
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	public class BarrierCommand : NonQueryCommandTemplate<DacCommandContext>, ICommandVisitAcceptor {
		private readonly Barrier barrier;
		private readonly long phaseNumber;
		public IsolationLevel? IsolationLevel { get; private set; }

		public BarrierCommand(Barrier barrier) {
			this.barrier = barrier;
			phaseNumber = barrier.CurrentPhaseNumber;
		}

		#region Overrides of NonQueryCommandTemplate
		public override void Execute(DacCommandContext context) {
			IsolationLevel =
					Transaction.Current != null ? Transaction.Current.IsolationLevel : (IsolationLevel?)null;
			long timeout = context.GetTimeoutRemainTicks();
			if (timeout != 0 && barrier.ParticipantsRemaining > 0
				&& barrier.CurrentPhaseNumber == phaseNumber) {
				if (timeout < 0) {
					barrier.SignalAndWait();
				}
				double mills = Math.Min((double)10 * 1000,
					(double)timeout / TimeSpan.TicksPerMillisecond);
				barrier.SignalAndWait(TimeSpan.FromMilliseconds(mills / 3));
			}
		}

		protected override void SetParameters(DacCommandContext context, IDatabaseDialect dialect, DbCommand dbCommand) {
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	internal class DummyLifecycleListener<T> : IContextLifecycleListener<T> where T: DacCommandContext{
		#region Implementation of IContextLifecycleListener<in DacCommandContext>
		public void OnPostContextCreate(T context) {
			// Nothing to do
		}

		public void OnPreContextDestroy(T context) {
			// Nothing to do
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	internal class DummyNVarCharType : NVarCharType {
		public DummyNVarCharType(string value) : base(value, 64) {
		}
	}

	[ExcludeFromCodeCoverage]
	public class ParallelExecution<T> where T: CommandContext {
		private readonly List<ICommand<T>> commands = new List<ICommand<T>>();
		public ICommandService Service { get; set; }
		private readonly IDictionary<ICommand<T>, Exception> errors
				= new Dictionary<ICommand<T>,Exception>();

		public Exception ErrorFor(ICommand<T> command) {
			Exception ex;
			return errors.TryGetValue(command, out ex) ? ex : null;
		}

		public void Add(params ICommand<T>[] command) {
			if (command == null) {
				return;
			}
			foreach (var cmd in command.Where(it => it != null)) {
				commands.Add(cmd);
			}
		}

		public ICommand<T> this[int index] {
			get { return commands[index]; }
		}

		public void Execute(Func<ICommand<T>, ICommandService> serviceGetter = null) {
			var targets = new List<ICommand<T>>(commands);
			if (targets.Count < 1) {
				return;
			}
			var threads = new List<Thread>();
			foreach (var cmd in targets) {
				var target = cmd;
				var service = (ICommandService)null;
				if (serviceGetter != null) {
					service = serviceGetter(cmd);
				}
				if (service == null) {
					service = Service;
				}
				threads.Add(new Thread(() => { ExecuteInThread(service, target); }));
			}
			foreach (var thread in threads) {
				thread.Start();
			}
			foreach (var thread in threads) {
				thread.Join();
			}
		}

		private void ExecuteInThread(ICommandService service, ICommand<T> target) {
			try {
				service.ExecuteCommand(target);
			}
			catch (Exception ex) {
				errors[target] = ex;
			}
		}
	}

}
