// ==============================================================================
//     Test.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;
using Net.Rimakiri.Container;

namespace Test.Rimakiri.Container {
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class DummyContainerTest {
		[Test]
		public void MultipleRootTest() {
			var reg = new DIContainerRegistry();
			var root = reg.RootContainer;

			root.BuildUp(typeof(IQueryCommand), null);
			ShouldError(() => { root.Resolve<IQueryCommand>(); });
			ShouldError(() => { root.TryResolve<IQueryCommand>(); });
			ShouldError(() => { root.Resolve<IQueryCommand>("hoge"); });
			ShouldError(() => { root.TryResolve<IQueryCommand>("hoge"); });
			root.Dispose();
		}

		private void ShouldError(Action action) {
			try {
				action();
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (DIContainerException dice) {
				Assert.IsTrue(dice.InnerException is ConfigurationErrorsException);
			}
		}
	}
}
