// ==============================================================================
//     Test.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Text;
using NUnit.Framework;
using Net.Rimakiri.Command;
using Test.Rimakiri.Container.Models.Command;
using Test.Rimakiri.Container.Models.Dao;
using Test.Rimakiri.Container.Models.File;
using Test.Rimakiri.Container.Models.Locator;

namespace Test.Rimakiri.Container {

	[ExcludeFromCodeCoverage]
	internal class UserIdentityImpl : IUserIdentity {
		public int CompanyId { get; set; }
		public string UserName { get; set; }
		public int UserSeq { get; set; }
	}
	[ExcludeFromCodeCoverage]
	internal static class IdentityUtils {
		public static IUserIdentity GetIdentity() {
			return new UserIdentityImpl {
					CompanyId = 990001,
					UserSeq = 123456,
				};
		}
	}

	/// <summary>
	/// DownloadTest の概要の説明
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class DownloadTest {
		private DirectoryInfo tempDir;
		private string tempDirPath;

		public ICommandService GetContext() {
			var service = new CommandServiceBuilder().Build(it => {
				var ctx = it as SampleContext;
				if (ctx == null) {
					return;
				}
				ctx.ResourceLocator = new DummyResourceLocator() {
					ConnectionString = "hogehoge",
					ResourceBase = tempDirPath,
					TmpResourceBase = tempDirPath,
				};
				ctx.UserIdentity = IdentityUtils.GetIdentity();
			});
			return service;
		}

		private TestContext testContextInstance;

		/// <summary>
		///現在のテストの実行についての情報および機能を
		///提供するテスト コンテキストを取得または設定します。
		///</summary>
		public TestContext TestContext {
			get {
				return testContextInstance;
			}
			set {
				testContextInstance = value;
			}
		}

		[SetUp]
		public void Setup() {
			var current = Directory.GetCurrentDirectory();
			var target = Path.Combine(current, "Temp");
			if (!Directory.Exists(target)) {
				tempDir = Directory.CreateDirectory(target);
			}
			tempDirPath = target;
		}

		[TearDown]
		public void TearDown() {
			if (tempDir != null) {
				foreach (var fileInfo in tempDir.GetFiles()) {
					fileInfo.Delete();
				}
				try {
					tempDir.Delete();
				}
				catch (IOException) {
					// Ignore.
				}
			}
		}

		[Test]
		public void TestDownloadTSV() {
			string outDir = tempDirPath;

			var command = new CsvFileCreationCommand {
				ViewModel = new MockFileView { OutDir = outDir },
				DataAdapterCommand = new SimpleQueryCommand(),
				BaseName = "TestTsv",
				QuoteType = QuoteType.StringOnly,
				Separator = "\t",
			};

			var outFile = new FileInfo(Path.Combine(outDir, "Mock_TestTsv.tsv"));
			outFile.Delete();
			outFile.Refresh();

			Assert.IsFalse(outFile.Exists);

			Assert.AreEqual("\t", command.Separator);
			command.Separator = null;
			Assert.AreEqual("\t", command.Separator);
			command.Separator = "";
			Assert.AreEqual("\t", command.Separator);

			var ctx = GetContext();
			ctx.ExecuteCommand(command);

			outFile.Refresh();
			Assert.IsTrue(outFile.Exists, outFile + " Not Exists.");
			Assert.AreEqual("0966725c1d70af857742f06c93a561b85ff04007d28daad12aba0bf3a1c0af23",
					FileUtils.ComputeHash(outFile));
		}

		[Test]
		public void TestDownloadCSV() {
			string outDir = tempDirPath;

			var command = new CsvFileCreationCommand {
				ViewModel = new MockFileView { OutDir = outDir },
				DataAdapterCommand = new SimpleQueryCommand(),
				BaseName = "TestCsv",
				QuoteType = QuoteType.OnlyNeeded,
			};

			var outFile = new FileInfo(Path.Combine(outDir, "Mock_TestCsv.csv"));
			outFile.Delete();
			outFile.Refresh();

			Assert.IsFalse(outFile.Exists);

			var ctx = GetContext();
			ctx.ExecuteCommand(command);

			outFile.Refresh();
			Assert.IsTrue(outFile.Exists, outFile + " Not Exists.");
			Assert.AreEqual("c8d5708c0c6ec09e4197fbfa00175c67c9282495b2059373c25f9a152b5c20b6",
					FileUtils.ComputeHash(outFile));
		}

		[Test]
		public void TestDownloadCSVUtf8() {
			string outDir = tempDirPath;

			var command = new CsvFileCreationCommand {
				ViewModel = new MockFileView { OutDir = outDir },
				DataAdapterCommand = new SimpleQueryCommand(),
				BaseName = "TestCsvUtf8",
				Encoding = Encoding.UTF8,
				QuoteType = QuoteType.All,
			};

			var outFile = new FileInfo(Path.Combine(outDir, "Mock_TestCsvUtf8.csv"));
			outFile.Delete();
			outFile.Refresh();

			Assert.IsFalse(outFile.Exists);

			var ctx = GetContext();
			ctx.ExecuteCommand(command);

			outFile.Refresh();
			Assert.IsTrue(outFile.Exists, outFile + " Not Exists.");
			Assert.AreEqual("adbfaf3b6b109991653e57bcb680c069b72a0e96467a021f6af9e33a49ed8c65",
					FileUtils.ComputeHash(outFile));
		}
	}
}
