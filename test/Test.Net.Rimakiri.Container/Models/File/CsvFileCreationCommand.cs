﻿// ==============================================================================
//     Test.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Text;
using Test.Rimakiri.Container.Models.Command;

namespace Test.Rimakiri.Container.Models.File {
	/// <summary>
	/// 
	/// </summary>
	public enum QuoteType {
		/// <summary>
		/// 必要な場合のみ
		/// </summary>
		OnlyNeeded,

		/// <summary>
		/// 文字列のみ
		/// </summary>
		StringOnly,

		/// <summary>
		/// 全て
		/// </summary>
		All,
	}

	/// <summary>
	/// CSV定数
	/// </summary>
	[ExcludeFromCodeCoverage]
	internal static class CsvConstants {
		public const string CrLf = "\r\n";
		public const string DQuote = "\"";
		public const string Cr = "\r";
		public const string Lf = "\n";
		public const string EscapedDQuote = "\"\"";
		public const string QuotedEmpty = "\"\"";
	}

	/// <summary>
	/// CSV作成コマンド
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class CsvFileCreationCommand : FileCreationCommand {
		private static readonly byte[] CrlfBytes = Encoding.ASCII.GetBytes(CsvConstants.CrLf);

		/// <summary>
		/// データを取得するアダプタコマンド
		/// </summary>
		public ISampleCommand<DataTable> DataAdapterCommand { get; set; }

		private QuoteType quoteType = QuoteType.OnlyNeeded;
		private string separator = ",";
		private byte[] separatorBytes = { (byte)',' };
		private Encoding encoding = Encoding.GetEncoding("shift_jis");

		/// <summary>
		/// クオート条件
		/// </summary>
		public QuoteType QuoteType {
			get { return quoteType; }
			set { quoteType = value; }
		}

		/// <summary>
		/// 出力ファイルのエンコーディング
		/// </summary>
		public Encoding Encoding {
			get { return encoding; }
			set {
				if (value != null) {
					encoding = value;
				}
			}
		}

		public override FileType FileType {
			get {
				return (Separator == "\t") ? FileType.Tsv
					: FileType.Csv;
			}
		}

		public override void Execute(SampleContext ctx) {
			ctx.Service.ExecuteCommand(DataAdapterCommand);
		}

		public override void PostExecute(SampleContext ctx) {
			try {
				MakeResultFile(ctx);
			}
			finally {
				base.PostExecute(ctx);
			}
		}

		/// <summary>
		/// セパレータ
		/// </summary>
		public string Separator {
			get { return separator; }
			set {
				if (string.IsNullOrEmpty(value)) {
					return;
				}
				separator = value;
				separatorBytes = Encoding.ASCII.GetBytes(value);
			}
		}

		/// <summary>
		/// 出力する一時ファイルパスを取得する。
		/// </summary>
		/// <param name="ctx">コンテキスト</param>
		/// <returns>一時ファイルパス</returns>
		protected virtual string GetOutputFileName(SampleContext ctx) {
			var fileName = Path.Combine(ctx.ResourceLocator.TmpResourceBase,
								+ ctx.UserIdentity.CompanyId + "_"
								+ ctx.UserIdentity.UserSeq);
			return fileName;
		}

		private void MakeResultFile(SampleContext ctx) {
			var fi = new FileInfo(GetOutputFileName(ctx));
			var formatter = GetQuoteFormatter(QuoteType);

			using (var stream = new BufferedStream(fi.Create())) {
				WriteHeadder(stream, GetQuoteFormatter(QuoteType.OnlyNeeded));
				WriteData(stream, formatter);
			}
			fi.Refresh();
			Result = fi;
		}

		private static void AddCrLf(Stream target) {
			target.Write(CrlfBytes, 0, CrlfBytes.Length);
		}

		private void WriteHeadder(Stream target, DataQuoteFomatter formatter) {
			var dt = DataAdapterCommand.Result;
			var columns = dt.Columns;

			for (int i = 0, l = dt.Columns.Count; i < l; i++) {
				WriteFormatted(formatter, target, columns[i].ColumnName, typeof(string), i + 1 < l);
			}
			AddCrLf(target);
		}

		private void WriteData(Stream target, DataQuoteFomatter formatter) {
			var dt = DataAdapterCommand.Result;
			var columns = dt.Columns.Count;

			if (columns < 1) {
				return;
			}

			var types = new Type[columns];
			for (int i = 0; i < columns; i++) {
				types[i] = dt.Columns[i].DataType;
			}

			foreach (DataRow row in dt.Rows) {
				for (var i = 0; i < columns; i++) {
					WriteFormatted(formatter, target, row[i], types[i], i + 1 < columns);
				}
				AddCrLf(target);
			}
		}

		private void WriteFormatted(DataQuoteFomatter formatter, Stream stream, object target, Type type, bool addComma) {
			var strValue = formatter.Format(target, type);

			if (!string.IsNullOrEmpty(strValue)) {
				byte[] data = Encoding.GetBytes(strValue);
				stream.Write(data, 0, data.Length);
			}
			if (addComma) {
				stream.Write(separatorBytes, 0, separatorBytes.Length);
			}
		}

		private DataQuoteFomatter GetQuoteFormatter(QuoteType type) {
			return (type == QuoteType.All) ? new AllQuoteFormatter(separator)
				: (type == QuoteType.StringOnly) ? new StringQuoteFormatter(separator)
				: (DataQuoteFomatter)new NeededQuoteFormatter(separator);
		}
	}

	[ExcludeFromCodeCoverage]
	internal abstract class DataQuoteFomatter {
		private readonly string separator;

		protected DataQuoteFomatter(string separator) {
			this.separator = separator;
		}
		public abstract string Format(object target, Type type);

		protected string FormatString(string target) {
			return CsvConstants.DQuote + target.Replace(CsvConstants.DQuote, CsvConstants.EscapedDQuote) + CsvConstants.DQuote;
		}

		protected bool NeedsEscape(string str) {
			return str.IndexOf(separator, StringComparison.InvariantCulture) >= 0
					|| str.StartsWith(CsvConstants.DQuote, StringComparison.InvariantCulture)
					|| str.IndexOf(CsvConstants.Cr, StringComparison.InvariantCulture) >= 0
					|| str.IndexOf(CsvConstants.Lf, StringComparison.InvariantCulture) >= 0
				;
		}
	}

	[ExcludeFromCodeCoverage]
	internal sealed class AllQuoteFormatter : DataQuoteFomatter {
		public AllQuoteFormatter(string separator) : base(separator) {
			// Empty.
		}
		public override string Format(object target, Type type) {
			if (ReferenceEquals(null, target) || target is DBNull) {
				return CsvConstants.QuotedEmpty;
			}
			var str = target.ToString();
			return string.IsNullOrEmpty(str) ? CsvConstants.QuotedEmpty : FormatString(str);
		}
	}

	[ExcludeFromCodeCoverage]
	internal sealed class NeededQuoteFormatter : DataQuoteFomatter {
		public NeededQuoteFormatter(string separator) : base(separator) {
			// Empty.
		}
		public override string Format(object target, Type type) {
			if (ReferenceEquals(null, target) || target is DBNull) {
				return string.Empty;
			}
			var str = target.ToString();
			return NeedsEscape(str) ? FormatString(str) : str;
		}
	}

	[ExcludeFromCodeCoverage]
	internal sealed class StringQuoteFormatter : DataQuoteFomatter {
		public StringQuoteFormatter(string separator) : base(separator) {
			// Empty.
		}

		private static bool IsNumericType(Type type) {
			switch (Type.GetTypeCode(type)) {
			case TypeCode.Byte:
			case TypeCode.SByte:
			case TypeCode.UInt16:
			case TypeCode.UInt32:
			case TypeCode.UInt64:
			case TypeCode.Int16:
			case TypeCode.Int32:
			case TypeCode.Int64:
			case TypeCode.Decimal:
			case TypeCode.Double:
			case TypeCode.Single:
				return true;
			default:
				return false;
			}
		}

		public override string Format(object target, Type type) {
			if (IsNumericType(type)) {
				return target.ToString();
			}
			return FormatString(ReferenceEquals(null, target) || target is DBNull
						? string.Empty : target.ToString());
		}
	}
}
