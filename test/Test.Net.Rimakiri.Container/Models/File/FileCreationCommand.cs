﻿// ==============================================================================
//     Test.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Diagnostics.CodeAnalysis;
using System.IO;
using Test.Rimakiri.Container.Models.Command;
using Test.Rimakiri.Container.Models.View;

namespace Test.Rimakiri.Container.Models.File {
	/// <summary>
	/// ファイル出力コマンド。結果をファイル出力し、出力したファイルを結果とする。
	/// </summary>
	[ExcludeFromCodeCoverage]
	public abstract class FileCreationCommand : AbstractCommand<FileInfo> {
		/// <summary>
		/// 結果の出力先
		/// </summary>
		public IFileViewModel ViewModel { get; set; }

		/// <summary>
		/// ファイルのベース名
		/// </summary>
		public string BaseName { get; set; }

		/// <summary>
		/// 出力するファイルタイプ
		/// </summary>
		public abstract FileType FileType { get; }

		/// <summary>
		/// 出力ファイル名を設定する
		/// </summary>
		private void SetupFileName() {
			var fileName = BaseName + FileType.Extension();
			ViewModel.ViewFileName = fileName;
			ViewModel.FileType = FileType;
		}

		/// <summary>
		/// 前処理を行う。ファイル名を設定し、ビューモデルを初期化する。
		/// </summary>
		/// <param name="ctx"></param>
		public override void PreExecute(SampleContext ctx) {
			base.PreExecute(ctx);
			SetupFileName();
			ViewModel.Init();
		}

		/// <summary>
		/// 結果ファイルを出力する。結果がnullの場合には何も出力されない。
		/// </summary>
		protected void OutputFile() {
			if (ReferenceEquals(null, Result)) {
				return;
			}
			ViewModel.OutputFile(Result);
		}

		/// <summary>
		/// 文字列を出力する。
		/// </summary>
		/// <param name="target">出力対象文字列</param>
		protected void Write(string target) {
			ViewModel.Write(target);
		}

		/// <summary>
		/// バイナリコンテンツを出力する。
		/// </summary>
		/// <param name="target">出力対象コンテンツ</param>
		protected void BinaryWrite(byte[] target) {
			ViewModel.BinaryWrite(target);
		}

		public override void PostExecute(SampleContext ctx) {
			try {
				OutputFile();
			}
			finally {
				ViewModel.Dispose();
			}
		}
	}
}
