﻿// ==============================================================================
//     Test.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;

namespace Test.Rimakiri.Container.Models.File {
	/// <summary>
	/// ダウンロードファイルタイプ
	/// </summary>
	public enum FileType {
		Unknown,
		Xls,
		Pdf,
		Csv,
		Tsv,
	};

	/// <summary>
	/// FileTypeのヘルパクラス
	/// </summary>
	[ExcludeFromCodeCoverage]
	public static class FileTypeExt {
		private static readonly string[] ContentTypes = {
			"application/octet-stream",
			"application/vnd.ms-excel",
			"application/pdf",
			"application/octet-stream",
			"application/octet-stream",
		};
		private static readonly string[] Extensions = {
			null,
			".xls",
			".pdf",
			".csv",
			".tsv",
		};
		public static string ContentType(this FileType target) {
			return ContentTypes[(int)target];
		}
		public static string Extension(this FileType target) {
			return Extensions[(int)target];
		}

		/// <summary>
		/// ファイル名からファイルタイプを取得する。
		/// </summary>
		/// <param name="fileName"></param>
		/// <returns></returns>
		public static FileType DetectFileType(string fileName) {
			if (string.IsNullOrEmpty(fileName)) {
				return FileType.Unknown;
			}
			string extension = Path.GetExtension(fileName);
			if (string.IsNullOrEmpty(extension)) {
				return FileType.Unknown;
			}
			foreach (FileType value in Enum.GetValues(typeof(FileType))) {
				if (extension.ToLower(CultureInfo.InvariantCulture)
						== value.Extension()) {
					return value;
				}
			}
			return FileType.Unknown;
		}
	}
}
