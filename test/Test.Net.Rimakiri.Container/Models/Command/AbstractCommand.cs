﻿// ==============================================================================
//     Test.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Diagnostics.CodeAnalysis;
using Net.Rimakiri.Command;

namespace Test.Rimakiri.Container.Models.Command {
	[ExcludeFromCodeCoverage]
	public abstract class AbstractCommand<T> : ISampleCommand<T> {
		/// <summary>
		/// コマンド実行結果
		/// </summary>
		public T Result { get; set; }

		#region Implementation of IGenericCommand<in CommandContext>
		/// <inheritdoc />
		public TxScope TxScope { get; set; }

		/// <inheritdoc />
		public Idempotent Idempotent { get; set; }

		/// <inheritdoc />
		public virtual void PreExecute(SampleContext context) {
			// Empty
		}

		/// <inheritdoc />
		public abstract void Execute(SampleContext context);

		/// <inheritdoc />
		public virtual void PostExecute(SampleContext context) {
			// Empty
		}
		#endregion
	}
}
