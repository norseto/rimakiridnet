﻿// ==============================================================================
//     Test.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Diagnostics.CodeAnalysis;
using Net.Rimakiri.Command;
using Test.Rimakiri.Container.Models.Locator;

namespace Test.Rimakiri.Container.Models.Command {
	/// <summary>
	/// サンプルコンテキスト
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class SampleContext : CommandContext {
		/// <summary>
		/// リソースロケータ
		/// </summary>
		public IResourceLocator ResourceLocator { get; set; }

		/// <summary>
		/// ユーザ情報
		/// </summary>
		public IUserIdentity UserIdentity { get; set; }
	}
}
