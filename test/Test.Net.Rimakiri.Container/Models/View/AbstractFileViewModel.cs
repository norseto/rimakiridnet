﻿// ==============================================================================
//     Test.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using Test.Rimakiri.Container.Models.File;

namespace Test.Rimakiri.Container.Models.View {
	/// <summary>
	/// ダウンロードコンテキスト
	/// </summary>
	[ExcludeFromCodeCoverage]
	public abstract class AbstractFileViewModel : IFileViewModel {

		/// <summary>
		/// ファイルタイプ
		/// </summary>
		public FileType FileType { get; set; }

		/// <summary>
		/// 出力時のファイル名
		/// </summary>
		public string ViewFileName { get; set; }

		/// <summary>
		/// インスタンスを初期化する。
		/// </summary>
		/// <param name="fileType">ファイルタイプ</param>
		protected AbstractFileViewModel(FileType fileType = FileType.Unknown) {
			FileType = fileType;
		}

		/// <summary>
		/// 出力の初期化を行う。このクラスでは何もしない。
		/// </summary>
		public virtual void Init() {
			// Empty
		}

		/// <summary>
		/// ファイルを出力する。
		/// </summary>
		/// <param name="target">対象ファイル</param>
		public abstract void OutputFile(FileInfo target);

		/// <summary>
		/// 文字列を出力する。
		/// </summary>
		/// <param name="target">出力対象文字列</param>
		public abstract void Write(string target);

		/// <summary>
		/// バイナリコンテンツを出力する。
		/// </summary>
		/// <param name="target">出力対象コンテンツ</param>
		public abstract void BinaryWrite(byte[] target);

		#region IDisposable
		public void Dispose() {
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing) {
			// Empty
		}
		#endregion
	}
}
