﻿// ==============================================================================
//     Test.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.IO;
using Test.Rimakiri.Container.Models.File;

namespace Test.Rimakiri.Container.Models.View {
	/// <summary>
	/// ファイルを出力するビューモデル
	/// </summary>
	public interface IFileViewModel : IDisposable {
		/// <summary>
		/// ファイルタイプ
		/// </summary>
		FileType FileType { get; set; }

		/// <summary>
		/// ダウンロード時のファイル名
		/// </summary>
		string ViewFileName { get; set; }

		/// <summary>
		/// 出力の初期化を行う
		/// </summary>
		void Init();

		/// <summary>
		/// ファイルを出力する。
		/// </summary>
		/// <param name="target">対象ファイル</param>
		void OutputFile(FileInfo target);

		/// <summary>
		/// 文字列を出力する。
		/// </summary>
		/// <param name="target">出力対象文字列</param>
		void Write(string target);

		/// <summary>
		/// バイナリコンテンツを出力する。
		/// </summary>
		/// <param name="target">出力対象コンテンツ</param>
		void BinaryWrite(byte[] target);
	}
}
