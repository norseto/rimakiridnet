﻿// ==============================================================================
//     Test.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Text;
using System.Web;
using Test.Rimakiri.Container.Models.File;

namespace Test.Rimakiri.Container.Models.View {
	/// <summary>
	/// HTTPによるダウンロードモデル
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class HttpFileViewModel : AbstractFileViewModel {
		private bool ended;

		private readonly HttpContext context;
		private Encoding encoding = Encoding.GetEncoding("shift_jis");

		/// <summary>
		/// 出力対象
		/// </summary>
		public HttpContext Context {
			get { return context; }
		}

		/// <summary>
		/// Trueの場合、保存用のダウンロードとはしない。
		/// </summary>
		public bool ForView { get; set; }

		/// <summary>
		/// ヘッダのエンコーディング(古いIEの場合に使用)
		/// </summary>
		public Encoding Encoding {
			get { return encoding; }
			set {
				if (value != null) {
					encoding = value;
				}
			}
		}

		/// <summary>
		/// 現在のHTTPコンテキストでインスタンスを初期化する。
		/// </summary>
		public HttpFileViewModel() : this(HttpContext.Current) {
			// Empty
		}

		/// <summary>
		/// インスタンスを初期化する。
		/// </summary>
		/// <param name="context">Httpコンテキスト</param>
		/// <param name="fileName">ダウンロードファイル名</param>
		/// <param name="fileType">ファイルタイプ</param>
		public HttpFileViewModel(HttpContext context, string fileName = null, FileType fileType = FileType.Unknown) {
			this.context = context;
			if (!string.IsNullOrEmpty(fileName) && fileType == FileType.Unknown) {
				fileType = FileTypeExt.DetectFileType(fileName);
			}
			ViewFileName = fileName;
			FileType = fileType;
		}

		/// <summary>
		/// ダウンロード用HTTPヘッダを出力する
		/// </summary>
		public override void Init() {
			var response = Context.Response;
			response.Clear();
			response.ContentType = FileType.ContentType();
			if (ForView) {
				return;
			}
			if (IsOldIE()) {
				response.HeaderEncoding = Encoding;
				response.ContentEncoding = Encoding;
				response.AppendHeader("Content-Disposition",
					"Attachment; filename=" + ViewFileName);
			}
			else {
				response.AppendHeader("Content-Disposition", "Attachment; filename*=utf-8''"
					+ Context.Server.UrlEncode(ViewFileName));
			}
		}

		/// <summary>
		/// IE8以下かを判定する。
		/// </summary>
		/// <returns>IE8以下の場合True</returns>
		private bool IsOldIE() {
			// IE11: Trident表記のみ
			// IE8-10: MSIE & Trident併記
			// それより前: MSIEのみ
			// 互換モードの場合、MSIEの数字は実際のものと異なるので
			// MSIEの数字は見ない
			var agent = Context.Request.UserAgent;
			if (agent == null || !agent.Contains("MSIE")) {
				return false;
			}
			return !agent.Contains("Trident/") || agent.Contains("Trident/4.");
		}

		/// <summary>
		/// ファイルを出力する。
		/// </summary>
		/// <param name="target">対象ファイル</param>
		public override void OutputFile(FileInfo target) {
			Context.Response.TransmitFile(target.FullName);
		}

		/// <summary>
		/// 文字列を出力する。
		/// </summary>
		/// <param name="target">出力対象文字列</param>
		public override void Write(string target) {
			Context.Response.Write(target);
		}

		/// <summary>
		/// バイナリコンテンツを出力する。
		/// </summary>
		/// <param name="target">出力対象コンテンツ</param>
		public override void BinaryWrite(byte[] target) {
			Context.Response.BinaryWrite(target);
		}

		#region IDisposable
		protected override void Dispose(bool disposing) {
			if (!disposing) {
				return;
			}
			try {
				if (!ended) {
					ended = true;
					Context.Response.End();
				}
			}
			finally {
				base.Dispose();
			}
		}
		#endregion
	}
}
