﻿// ==============================================================================
//     Test.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Data;
using System.Diagnostics.CodeAnalysis;
using Test.Rimakiri.Container.Models.Command;

namespace Test.Rimakiri.Container.Models.Dao {
	[ExcludeFromCodeCoverage]
	public class SimpleQueryCommand : AbstractCommand<DataTable> {
		private const string Key1 = "Sample";
		private const string Key2 = "CSV";
		private const string Key3 = "File";
		private const string Key4 = "Num";

		public override void PostExecute(SampleContext ctx) {
			base.PostExecute(ctx);
		}

		public override void PreExecute(SampleContext ctx) {
			var ret = new DataTable();
			Result = ret;
		}

		public override void Execute(SampleContext ctx) {
			var ret = Result;
			ret.Columns.Add(Key1);
			ret.Columns.Add(Key2);
			ret.Columns.Add(Key3);
			ret.Columns.Add(Key4, typeof(int));

			AddRow("Please,", "Download", "Me", 1);
			AddRow("Don't", "Say", "\"Giveup\"", 2);
			AddRow("日本語", "だって", "大丈夫", 3);
			AddRow("改\n行", "だって", "大丈夫", 3);
			AddRow("He said,", "go and \"", "Get it\"", 400);
			AddRow(1, 2, 300, null);
		}

		protected void AddRow(object str1, object str2, object str3, object str4) {
			DataRow dr = Result.NewRow();
			AddIfNotNull(dr, Key1, str1);
			AddIfNotNull(dr, Key2, str2);
			AddIfNotNull(dr, Key3, str3);
			AddIfNotNull(dr, Key4, str4);
			Result.Rows.Add(dr);
		}

		private static void AddIfNotNull(DataRow row, string key, object item) {
			if (ReferenceEquals(item, null)) {
				return;
			}
			row[key] = item;
		}
	}
}
