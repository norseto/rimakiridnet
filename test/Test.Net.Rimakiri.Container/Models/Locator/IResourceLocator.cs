﻿// ==============================================================================
//     Test.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.
namespace Test.Rimakiri.Container.Models.Locator {
	/// <summary>
	/// リソースロケータ
	/// </summary>
	public interface IResourceLocator {
		/// <summary>
		/// 接続先文字列
		/// </summary>
		string ConnectionString { get; }

		/// <summary>
		/// リソースベース
		/// </summary>
		string ResourceBase { get; }

		/// <summary>
		/// 一時ファイル用リソースベース
		/// </summary>
		string TmpResourceBase { get; }
	}
}
