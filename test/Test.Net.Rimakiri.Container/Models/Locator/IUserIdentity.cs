﻿// ==============================================================================
//     Test.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.
namespace Test.Rimakiri.Container.Models.Locator {
	/// <summary>
	/// 利用者識別
	/// </summary>
	public interface IUserIdentity {
		/// <summary>
		/// 担当者ID
		/// </summary>
		int UserSeq { get; }

		/// <summary>
		/// 会社ID
		/// </summary>
		int CompanyId { get; }

		/// <summary>
		/// 担当者名
		/// </summary>
		string UserName { get; }
	}
}
