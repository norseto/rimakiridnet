// ==============================================================================
//     Test.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Diagnostics.CodeAnalysis;
using System.Linq;
using NUnit.Framework;
using Net.Rimakiri.Container;
using Test.Rimakiri.Container.Models.View;

namespace Test.Rimakiri.Container {
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class BasicCriteriaTest {
		[Test]
		public void SimpleTest() {
			var target = new BasicCriteria {
					Separator = "::",
					LevelNames = new [] { "CName", "AName", "MName"},
			};
			Assert.AreEqual("CName::AName::MName", target.First());
			Assert.AreEqual("CName::AName", target.Skip(1).First());
			Assert.AreEqual("CName", target.Skip(2).First());
		}
		[Test]
		public void SimpleConstTest() {
			var target = new BasicCriteria("CName", "AName", "MName") {
				Separator = "::",
			};
			Assert.AreEqual("CName::AName::MName", target.First());
			Assert.AreEqual("CName::AName", target.Skip(1).First());
			Assert.AreEqual("CName", target.Skip(2).First());
		}

		[Test]
		public void TestFindFull() {
			var target = new BasicCriteria {
					Separator = "::",
					LevelNames = new [] { "Parent", "Child", "GrandChild"},
			};
			var registry = DIContainerRegistry.Load("Test07.config");
			var obj = new DummyCriteriaModel();
			registry.Buildup(obj, target);

			var model = obj.Model as MockFileView;
			Assert.IsNotNull(model);
			Assert.AreEqual("C:\\GrandChild", model.OutDir);
		}

		[Test]
		public void TestFindPart() {
			var target = new BasicCriteria {
					Separator = "::",
					LevelNames = new [] { "Parent", "Child" },
			};
			var registry = DIContainerRegistry.Load("Test07.config");
			var obj = new DummyCriteriaModel();
			registry.Buildup(obj, target);

			var model = obj.Model as MockFileView;
			Assert.IsNotNull(model);
			Assert.AreEqual("C:\\Child", model.OutDir);
		}

		[Test]
		public void TestFindDiffPart() {
			var target = new BasicCriteria {
					Separator = "::",
					LevelNames = new [] { "Parent", "Child2", "GrandChild" },
			};
			var registry = DIContainerRegistry.Load("Test07.config");
			var obj = new DummyCriteriaModel();
			registry.Buildup(obj, target);

			var model = obj.Model as MockFileView;
			Assert.IsNotNull(model);
			Assert.AreEqual("C:\\Parent", model.OutDir);
		}

		[Test]
		public void TestNotFound() {
			var target = new BasicCriteria {
					Separator = "::",
					LevelNames = new [] { "Parenta", "Child2", "GrandChild" },
			};
			var registry = DIContainerRegistry.Load("Test07.config");
			var obj = new DummyCriteriaModel();
			registry.Buildup(obj, target);

			var model = obj.Model as MockFileView;
			Assert.IsNotNull(model);
			Assert.AreEqual("C:\\Root", model.OutDir);
		}
	}

	[ExcludeFromCodeCoverage]
	public class DummyCriteriaModel {
		[DIDepend]
		public IFileViewModel Model { get; set; }
	}
}
