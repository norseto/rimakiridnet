// ==============================================================================
//     Test.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Configuration;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using Microsoft.Practices.Unity;
using NUnit.Framework;
using Net.Rimakiri.Command;
using Net.Rimakiri.Container;
using Test.Rimakiri.Container.Models.Command;
using Test.Rimakiri.Container.Models.Dao;
using Test.Rimakiri.Container.Models.File;
using Test.Rimakiri.Container.Models.View;

namespace Test.Rimakiri.Container {
	/// <summary>
	/// DIContainerRegistryのテスト
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class DIContainerRegistryTest {
		[Test]
		public void TestLoadRoot() {
			var reg = DIContainerRegistry.Load();

			Assert.IsNotNull(reg["Test01"]);
		}

		[Test]
		public void TestLoadAll() {
			var reg = DIContainerRegistry.LoadAll();

			Assert.AreEqual(2, reg.Count);
			Assert.IsNotNull(reg[0]["Test01"]);
		}

		[Test]
		public void TestLoadAllWithConfig() {
			var reg = DIContainerRegistry.LoadAll("Test00-app.config");

			Assert.AreEqual(2, reg.Count);
			Assert.IsNotNull(reg[0]["Test01"]);
		}

		[Test]
		public void TestOverrideContainerDup() {
			try {
				var reg = DIContainerRegistry.LoadAll("Test00-app7.config");
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (ConfigurationErrorsException cee) {
				Assert.IsTrue(cee.Message.Contains("Test00-app7.config"));
			}
		}

		[Test]
		public void TestOverrideContainerDupOverride() {
			try {
				var reg = DIContainerRegistry.LoadAll("Test00-app8.config");
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (ConfigurationErrorsException cee) {
				Assert.IsTrue(cee.Message.Contains("Test00-app8.config"));
			}
		}

		[Test]
		public void TestLoadWithNoUnity() {
			try {
				DIContainerRegistry.Load("Test00-app4.config");
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (ConfigurationErrorsException cee) {
				Assert.IsTrue(cee.Message.Contains("Test00-app.config"));
			}
		}

		[Test]
		public void TestLoadWithUnityError() {
			try {
				DIContainerRegistry.Load("Test00-app5.config");
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (ConfigurationErrorsException cee) {
				Assert.IsTrue(cee.Message.Contains("Test00-app5.config"));
			}
		}

		[Test]
		public void TestLoadWithNotExistsConfig() {
			try {
				DIContainerRegistry.Load("Test99-app5.config");
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (ConfigurationErrorsException cee) {
				Assert.IsTrue(cee.Message.Contains("Test99-app5.config"));
			}
		}

		[Test]
		public void TestLoadWithEmptyError() {
			try {
				DIContainerRegistry.Load("Test00-app6.config");
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (NoContainerConfigurationException cee) {
				Assert.IsTrue(cee.Message.Contains("Test00-app6.config"));
			}
		}


		[Test]
		public void TestLoadHierarchy() {
			var reg = DIContainerRegistry.Load();

			Assert.IsNotNull(reg.RootContainer);
			Assert.IsNotNull(reg["Test01"]);
			Assert.AreNotEqual(reg.RootContainer, reg["Test01"]);
			Assert.AreEqual(reg["hoge"], reg["Test01"]);
		}

		[Test]
		public void TestOperators() {
			var reg = DIContainerRegistry.Load();

			var c01 = reg["Test01"];
			var c02 = reg["Test01"];

			Assert.IsNotNull(c01);
			Assert.IsNotNull(c02);
			Assert.AreSame(c01, c02);
			Assert.AreEqual(c01, c02);
			Assert.IsTrue(c01.Equals(c02));
			Assert.IsTrue(c02.Equals(c01));
			Assert.AreEqual(c01.GetHashCode(), c02.GetHashCode());
		}

		[Test]
		public void TestLoadOtherConfig() {
			var reg = DIContainerRegistry.Load("Test00-app.config");

			Assert.IsNotNull(reg.RootContainer);
			Assert.IsNotNull(reg["Test01"]);
			Assert.AreNotEqual(reg.RootContainer, reg["Test01"]);
		}

		[Test]
		[SuppressMessage("Microsoft.Usage",
			"CA2202:Do not dispose objects multiple times")]
		public void TestDispose() {
			var reg = DIContainerRegistry.Load("Test00-app.config");

			using (reg) {
				Assert.IsNotNull(reg.RootContainer);
				Assert.IsNotNull(reg["Test01"]);
				Assert.AreNotEqual(reg.RootContainer, reg["Test01"]);
			}
			Assert.IsNotNull(reg.RootContainer);
			Assert.AreEqual(reg.RootContainer, reg["Test01"]);

			// Safe multiple dispose.
			reg.Dispose();
		}


		[Test]
		public void TestDisposeChild() {
			var reg = DIContainerRegistry.Load();

			Assert.IsNotNull(reg.RootContainer);
			Assert.IsNotNull(reg["Test01"]);

			var child = reg["Test01"];
			var obj = child.TryResolve<IFileViewModel>();
			Assert.IsNotNull(obj);

			// Still valid.
			child.Dispose();
			Assert.IsNotNull(child.TryResolve<IFileViewModel>());
		}

		[Test]
		public void TestResolveChild() {
			var reg = DIContainerRegistry.Load();
			var child = reg["Test01"];
			Assert.IsNotNull(child);

			Assert.IsNotNull(child.Resolve<FileCreationCommand>("Basic01"));
			Assert.IsNull(reg.RootContainer.TryResolve<FileCreationCommand>("Basic01"));
			try {
				reg.RootContainer.Resolve<FileCreationCommand>("Base01");
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (DIContainerException) {
				Assert.IsTrue(true);
			}
		}

		[Test]
		public void TestNotSingleton() {
			var reg = DIContainerRegistry.Load();
			var child = reg["Test01"];
			Assert.IsNotNull(child);

			var childResolve = child.TryResolve<IFileViewModel>("MockDownload") as MockFileView;
			Assert.IsNotNull(childResolve);
			Assert.IsTrue(childResolve != child.TryResolve<IFileViewModel>("MockDownload"));
		}

		[Test]
		public void TestResolveChildSameName() {
			var reg = DIContainerRegistry.Load();
			var child = reg["Test01"];
			Assert.IsNotNull(child);

			var childResolve = child.TryResolve<IFileViewModel>("MockDownload") as MockFileView;
			var rootResolve = reg.RootContainer.TryResolve<IFileViewModel>("MockDownload") as MockFileView;
			Assert.IsNotNull(childResolve);
			Assert.IsNotNull(rootResolve);

			Assert.AreEqual("C:\\Work", rootResolve.OutDir);
			Assert.AreEqual("C:\\Temp", childResolve.OutDir);
		}

		[Test]
		public void TestResolveTorrerant() {
			var reg = DIContainerRegistry.Load("Test00-app2.config");
			var child = reg["Test01"];
			Assert.IsNotNull(child);

			var childResolve = child.TryResolve<IFileViewModel>() as MockFileView;
			Assert.IsNotNull(childResolve);

			Assert.AreEqual("C:\\Temp", childResolve.OutDir);
		}

		[Test]
		public void TestResolveTorrerantInherits() {
			var reg = DIContainerRegistry.Load("Test00-app2.config");
			var child = reg["Test01"];
			Assert.IsNotNull(child);

			var cmd1 = child.TryResolve<IQueryCommand>();
			Assert.IsNotNull(cmd1);
			Assert.IsTrue(cmd1 is SimpleQueryCommand2);

			var cmd2 = child.TryResolve<ISampleCommand<DataTable>>();
			Assert.IsNotNull(cmd2);
			Assert.IsTrue(cmd2 is SimpleQueryCommand);
		}

		[Test]
		public void TestTryResolveWithNoName() {
			var reg = DIContainerRegistry.Load("Test00-app3.config");
			var child = reg["Test01"];
			Assert.IsNotNull(child);

			var model = child.TryResolve<IFileViewModel>() as MockFileView;
			Assert.IsNotNull(model);
			Assert.AreEqual("C:\\Work2", model.OutDir);

			Assert.IsNull(child.TryResolve<DIContainerException>());
		}


		[Test]
		public void TestResolveWithNoName() {
			var reg = DIContainerRegistry.Load("Test00-app3.config");
			var child = reg["Test01"];
			Assert.IsNotNull(child);

			var model = child.Resolve<IFileViewModel>() as MockFileView;
			Assert.IsNotNull(model);
			Assert.AreEqual("C:\\Work2", model.OutDir);

			try {
				child.Resolve<DIContainerException>();
				Assert.IsFalse(true, "Should throw an exception.");
			}
			catch (DIContainerException) {
				Assert.IsTrue(true);
			}
		}


		[Test]
		public void TestBuildUp() {
			var reg = DIContainerRegistry.Load("Test00-app3.config");
			var child = reg["Test01"];
			Assert.IsNotNull(child);

			var mock = new InjectionMock();
			Assert.IsNull(mock.FileViewModel);

			reg.Buildup(mock, "Test01");
			Assert.IsNotNull(mock.FileViewModel);

			// Buildup to null is safe.
			reg.Buildup(null, "Test01");
		}

		[Test]
		public void TestBuildUpWithCustomAttr() {
			var reg = DIContainerRegistry.Load("Test00-app3.config");
			var child = reg["Test01"];
			Assert.IsNotNull(child);

			var mock = new CustomInjectionMock();
			Assert.IsNull(mock.FileViewModel);

			reg.Buildup(mock, "Test01");
			Assert.IsNotNull(mock.FileViewModel);

			// Buildup to null is safe.
			reg.Buildup(null, "Test01");
		}

		[Test]
		public void TestTolerantBuildUp() {
			var reg = DIContainerRegistry.Load("Test00-app2.config");
			var child = reg["Test01"];
			Assert.IsNotNull(child);

			var mock = new TolerantInjectionMock();
			Assert.IsNull(mock.QueryCommand);

			reg.Buildup(mock, "Test01");
			Assert.IsNotNull(mock.QueryCommand);

			// Buildup to null is safe.
			reg.Buildup(null, "Test01");
		}


		[Test]
		public void TestTolerantBuildUpWithName() {
			var reg = DIContainerRegistry.Load("Test00-app2.config");
			var child = reg["Test01"];
			Assert.IsNotNull(child);

			var mock = new NamedInjectionMock();
			reg.Buildup(mock, "Test01");
			Assert.IsNotNull(mock.Command);
			Assert.AreEqual(TxScope.Suppress, mock.Command.TxScope);
		}

		[Test]
		public void TestContainerBuildUp() {
			var reg = DIContainerRegistry.Load("Test00-app2.config");
			var child = reg["Test01"];
			Assert.IsNotNull(child);

			var mock = new ContainerInjectionMock();
			Assert.IsNull(mock.Container);

			reg.Buildup(mock, "Test01");
			Assert.IsNotNull(mock.Container);
		}

		[ExcludeFromCodeCoverage]
		private class InjectionMock {
			[Dependency]
			public IFileViewModel FileViewModel { get; set; }
		}

		[ExcludeFromCodeCoverage]
		private class CustomInjectionMock {
			[DIDepend]
			public IFileViewModel FileViewModel { get; set; }
		}

		[ExcludeFromCodeCoverage]
		private class TolerantInjectionMock {
			[DIDepend]
			public IQueryCommand QueryCommand { get; set; }
		}

		[ExcludeFromCodeCoverage]
		private class ContainerInjectionMock {
			[DIDepend]
			public IDIContainer Container { get; set; }
		}

		[ExcludeFromCodeCoverage]
		private class NamedInjectionMock {
			[DIDepend("SimpleAdapter2")]
			public ISampleCommand<DataTable> Command { get; set; }
		}
	}

	public interface IQueryCommand : ISampleCommand<DataTable> {
		// Empty just marker
	}

	[ExcludeFromCodeCoverage]
	public class SimpleQueryCommand2 : IQueryCommand {
		// Empty. Child type.
		public void PostExecute(SampleContext ctx) {
			throw new NotImplementedException();
		}

		public void PreExecute(SampleContext ctx) {
			throw new NotImplementedException();
		}

		public void Execute(SampleContext ctx) {
			throw new NotImplementedException();
		}

		public DataTable Result { get; set; }
		public TxScope TxScope { get; set; }
		public Idempotent Idempotent { get; set; }
	}
}
