// ==============================================================================
//     Test.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;
using Net.Rimakiri.Container;

namespace Test.Rimakiri.Container {
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class DIContainerExceptionTest {
		[Test]
		public void TestCause() {
			var cause = new Exception();
			var target = new DIContainerException(cause);

			Assert.AreSame(cause, target.InnerException);
		}

		[Test]
		public void TestCauseAndMessage() {
			var cause = new Exception();
			var target = new DIContainerException("Test message.", cause);

			Assert.AreSame(cause, target.InnerException);
			Assert.AreEqual("Test message.", target.Message);
		}
	}
}
