// ==============================================================================
//     Test.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using Microsoft.Practices.Unity;
using NUnit.Framework;
using Net.Rimakiri.Command;
using Net.Rimakiri.Container;
using Test.Rimakiri.Container.Models.Command;
using Test.Rimakiri.Container.Models.Dao;
using Test.Rimakiri.Container.Models.File;
using Test.Rimakiri.Container.Models.View;

namespace Test.Rimakiri.Container {
	/// <summary>
	/// SingleConfigDIContainerRegistryのテスト
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	[SuppressMessage("Microsoft.Usage", "CS0618:")]
	#pragma warning disable 0618
	public class SingleConfigDIContainerRegistryTest {
	[Test]
		public void TestLoadRoot() {
			var reg = new SingleConfigDIContainerRegistry();

			Assert.IsNull(reg.Root);
			Assert.IsNull(reg["Test01"]);

			reg.LoadConfiguration(null);
			Assert.IsNotNull(reg.Root);
			Assert.AreEqual(reg.Root, reg["Test01"]);
		}

		[Test]
		public void TestLoadHierarchy() {
			var reg = new SingleConfigDIContainerRegistry();

			Assert.IsNull(reg.Root);
			Assert.IsNull(reg["Test01"]);

			reg.LoadConfiguration("Test01");
			Assert.IsNotNull(reg.Root);
			Assert.IsNotNull(reg["Test01"]);
			Assert.AreNotEqual(reg.Root, reg["Test01"]);
		}

		[Test]
		public void TestLoadTwice() {
			var reg = new SingleConfigDIContainerRegistry();

			Assert.IsNull(reg.Root);
			Assert.IsNull(reg["Test01"]);

			reg.LoadConfiguration("Test01");
			Assert.IsNotNull(reg.Root);
			Assert.IsNotNull(reg["Test01"]);

			// Safely Load again.
			var container = reg["Test01"];
			reg.LoadConfiguration("Test01");
			Assert.IsNotNull(reg.Root);
			Assert.AreEqual(container, reg["Test01"]);
		}

		[Test]
		public void TestLoadRootTwice() {
			var reg = new SingleConfigDIContainerRegistry();

			Assert.IsNull(reg.Root);

			reg.LoadConfiguration(null);
			Assert.IsNotNull(reg.Root);
			var root = reg.Root;

			// Safely Load again.
			reg.LoadConfiguration(null);
			Assert.IsNotNull(reg.Root);
			Assert.AreEqual(root, reg.Root);
		}

		[Test]
		public void TestLoadChildren() {
			var reg = new SingleConfigDIContainerRegistry();

			Assert.IsNull(reg.Root);
			Assert.IsNull(reg["Test01"]);
			Assert.IsNull(reg["Test02"]);

			reg.LoadConfiguration("Test01");
			Assert.IsNotNull(reg.Root);
			Assert.IsNotNull(reg["Test01"]);
			Assert.AreNotEqual(reg.Root, reg["Test01"]);
			var root = reg.Root;
			var test01 = reg["Test01"];

			reg.LoadConfiguration("Test02");
			Assert.IsNotNull(reg.Root);
			Assert.IsNotNull(reg["Test02"]);
			Assert.AreNotEqual(reg.Root, reg["Test02"]);
			Assert.AreNotEqual(test01, reg["Test02"]);

			// Other containers are not changed.
			Assert.AreEqual(root, reg.Root);
			Assert.AreEqual(test01, reg["Test01"]);
		}


		[Test]
		public void TestOperators() {
			var reg = new SingleConfigDIContainerRegistry();
			reg.LoadConfiguration("Test01");

			var c01 = reg["Test01"];
			var c02 = reg["Test01"];

			Assert.IsNotNull(c01);
			Assert.IsNotNull(c02);
			Assert.AreSame(c01, c02);
			Assert.AreEqual(c01, c02);
			Assert.IsTrue(c01.Equals(c02));
			Assert.IsTrue(c02.Equals(c01));
			Assert.AreEqual(c01.GetHashCode(), c02.GetHashCode());
		}

		[Test]
		public void TestLoadOtherConfig() {
			var reg = new SingleConfigDIContainerRegistry() {
				Config = "Test01.config"
			};

			Assert.IsNull(reg.Root);
			Assert.IsNull(reg["Test01"]);

			reg.LoadConfiguration("Test01");
			Assert.IsNotNull(reg.Root);
			Assert.IsNotNull(reg["Test01"]);
		}

		[Test]
		[SuppressMessage("Microsoft.Usage",
			"CA2202:Do not dispose objects multiple times")]
		public void TestDispose() {
			var reg = new SingleConfigDIContainerRegistry();

			using (reg) {
				reg.LoadConfiguration("Test01");
				Assert.IsNotNull(reg.Root);
				Assert.IsNotNull(reg["Test01"]);
			}
			Assert.IsNull(reg.Root);
			Assert.IsNull(reg["Test01"]);

			// Safe multiple dispose.
			reg.Dispose();
		}


		[Test]
		public void TestDisposeChild() {
			var reg = new SingleConfigDIContainerRegistry();

			reg.LoadConfiguration("Test01");
			Assert.IsNotNull(reg.Root);
			Assert.IsNotNull(reg["Test01"]);

			var child = reg["Test01"];
			var obj = child.TryResolve<IFileViewModel>();
			Assert.IsNotNull(obj);

			// Still valid.
			child.Dispose();
			Assert.IsNotNull(child.TryResolve<IFileViewModel>());
		}

		[Test]
		public void TestDisposeWithNoRoot() {
			var reg = new SingleConfigDIContainerRegistry {
						Config = "Test02.config",
						Section = "di"
					};

			using (reg) {
				reg.LoadConfiguration("Test01");
				Assert.IsNull(reg.Root);
				Assert.IsNotNull(reg["Test01"]);
			}
			Assert.IsNull(reg.Root);
			Assert.IsNull(reg["Test01"]);
		}

		[Test]
		public void TestResolveChild() {
			var reg = new SingleConfigDIContainerRegistry().LoadConfiguration("Test01");
			var child = reg["Test01"];
			Assert.IsNotNull(child);

			Assert.IsNotNull(child.Resolve<FileCreationCommand>("Basic01"));
			Assert.IsNull(reg.Root.TryResolve<FileCreationCommand>("Basic01"));
			try {
				reg.Root.Resolve<FileCreationCommand>("Base01");
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (DIContainerException) {
				Assert.IsTrue(true);
			}
		}

		[Test]
		public void TestNotSingleton() {
			var reg = new SingleConfigDIContainerRegistry().LoadConfiguration("Test01");
			var child = reg["Test01"];
			Assert.IsNotNull(child);

			var childResolve = child.TryResolve<IFileViewModel>("MockDownload") as MockFileView;
			Assert.IsNotNull(childResolve);
			Assert.IsTrue(childResolve != child.TryResolve<IFileViewModel>("MockDownload"));
		}

		[Test]
		public void TestResolveChildSameName() {
			var reg = new SingleConfigDIContainerRegistry().LoadConfiguration("Test01");
			var child = reg["Test01"];
			Assert.IsNotNull(child);

			var childResolve = child.TryResolve<IFileViewModel>("MockDownload") as MockFileView;
			var rootResolve = reg.Root.TryResolve<IFileViewModel>("MockDownload") as MockFileView;
			Assert.IsNotNull(childResolve);
			Assert.IsNotNull(rootResolve);

			Assert.AreEqual("C:\\Work", rootResolve.OutDir);
			Assert.AreEqual("C:\\Temp", childResolve.OutDir);
		}

		[Test]
		public void TestResolveChildSameNameWithNoRoot() {
			var reg = new SingleConfigDIContainerRegistry {
							Config = "Test02.config",
							Section = "di" }.LoadConfiguration("Test01");
			var child = reg["Test01"];
			Assert.IsNotNull(child);
			Assert.IsNull(reg.Root);

			var childResolve = child.TryResolve<IFileViewModel>("MockDownload") as MockFileView;
			Assert.IsNotNull(childResolve);
			Assert.AreEqual("C:\\Temp", childResolve.OutDir);
		}

		[Test]
		public void TestResolveTorrerant() {
			var reg = new SingleConfigDIContainerRegistry {
				Config = "Test02.config",
				Section = "di"
			}.LoadConfiguration("Test01");
			var child = reg["Test01"];
			Assert.IsNotNull(child);
			Assert.IsNull(reg.Root);

			var childResolve = child.TryResolve<IFileViewModel>() as MockFileView;
			Assert.IsNotNull(childResolve);

			Assert.AreEqual("C:\\Temp", childResolve.OutDir);
		}

		[Test]
		public void TestResolveTorrerantInherits() {
			var reg = new SingleConfigDIContainerRegistry {
				Config = "Test02.config",
				Section = "di"
			}.LoadConfiguration("Test01");
			var child = reg["Test01"];
			Assert.IsNotNull(child);
			Assert.IsNull(reg.Root);

			var cmd1 = child.TryResolve<IQueryCommand>();
			Assert.IsNotNull(cmd1);
			Assert.IsTrue(cmd1 is SimpleQueryCommand2);

			var cmd2 = child.TryResolve<ISampleCommand<DataTable>>();
			Assert.IsNotNull(cmd2);
			Assert.IsTrue(cmd2 is SimpleQueryCommand);
		}

		[Test]
		public void TestTryResolveWithNoName() {
			var reg = new SingleConfigDIContainerRegistry().LoadConfiguration("Test01");
			var child = reg["Test01"];
			Assert.IsNotNull(child);

			var model = child.TryResolve<IFileViewModel>() as MockFileView;
			Assert.IsNotNull(model);
			Assert.AreEqual("C:\\Work2", model.OutDir);

			Assert.IsNull(child.TryResolve<DIContainerException>());
		}


		[Test]
		public void TestResolveWithNoName() {
			var reg = new SingleConfigDIContainerRegistry().LoadConfiguration("Test01");
			var child = reg["Test01"];
			Assert.IsNotNull(child);

			var model = child.Resolve<IFileViewModel>() as MockFileView;
			Assert.IsNotNull(model);
			Assert.AreEqual("C:\\Work2", model.OutDir);

			try {
				child.Resolve<DIContainerException>();
				Assert.IsFalse(true, "Should throw an exception.");
			}
			catch (DIContainerException) {
				Assert.IsTrue(true);
			}
		}


		[Test]
		public void TestBuildUp() {
			var reg = new SingleConfigDIContainerRegistry().LoadConfiguration("Test01");

			var mock = new InjectionMock();
			Assert.IsNull(mock.FileViewModel);

			reg.Buildup(mock, "Test01");
			Assert.IsNotNull(mock.FileViewModel);

			// Buildup to null is safe.
			reg.Buildup(null, "Test01");
		}

		[Test]
		public void TestBuildUpWithCustomAttr() {
			var reg = new SingleConfigDIContainerRegistry().LoadConfiguration("Test01");

			var mock = new CustomInjectionMock();
			Assert.IsNull(mock.FileViewModel);

			reg.Buildup(mock, "Test01");
			Assert.IsNotNull(mock.FileViewModel);

			// Buildup to null is safe.
			reg.Buildup(null, "Test01");
		}

		[Test]
		public void TestTolerantBuildUp() {
			var reg = new SingleConfigDIContainerRegistry {
				Config = "Test02.config",
				Section = "di"
			}.LoadConfiguration("Test01");

			var mock = new TolerantInjectionMock();
			Assert.IsNull(mock.QueryCommand);

			reg.Buildup(mock, "Test01");
			Assert.IsNotNull(mock.QueryCommand);

			// Buildup to null is safe.
			reg.Buildup(null, "Test01");
		}


		[Test]
		public void TestTolerantBuildUpWithName() {
			var reg = new SingleConfigDIContainerRegistry {
				Config = "Test02.config",
				Section = "di"
			}.LoadConfiguration("Test01");

			var mock = new NamedInjectionMock();
			reg.Buildup(mock, "Test01");
			Assert.IsNotNull(mock.Command);
			Assert.AreEqual(TxScope.Suppress, mock.Command.TxScope);
		}

		[Test]
		public void TestContainerBuildUp() {
			var reg = new SingleConfigDIContainerRegistry().LoadConfiguration("Test01");

			var mock = new ContainerInjectionMock();
			Assert.IsNull(mock.Container);

			reg.Buildup(mock, "Test01");
			Assert.IsNotNull(mock.Container);
		}

		[Test]
		[SuppressMessage("Microsoft.Usage",
			"CA2202:Do not dispose objects multiple times")]
		public void TestChangeConfigAfterLoad() {
			var target = new SingleConfigDIContainerRegistry();
			target.LoadConfiguration("Test01");
			try {
				target.Config = "Test02.config";
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (InvalidOperationException) {
				Assert.IsTrue(true);
			}

			try {
				target.Section = "di";
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (InvalidOperationException) {
				Assert.IsTrue(true);
			}

			// Can dispose.
			target.Dispose();

			// Can load again.
			target.Config = "Test02.config";
			target.Section = "di";
			target.LoadConfiguration("Test01");

			// Can dispose again.
			target.Dispose();
		}
		[Test]
		public void TestSectionName() {
			var target = new SingleConfigDIContainerRegistry {Config = "Test01.config"};
			target.LoadAll();

			Assert.IsNotNull(target["Test01"]);
		}

		private class InjectionMock {
			[Dependency]
			public IFileViewModel FileViewModel { get; set; }
		}

		private class CustomInjectionMock {
			[DIDepend]
			public IFileViewModel FileViewModel { get; set; }
		}

		private class TolerantInjectionMock {
			[DIDepend]
			public IQueryCommand QueryCommand { get; set; }
		}

		private class ContainerInjectionMock {
			[DIDepend]
			public IDIContainer Container { get; set; }
		}

		private class NamedInjectionMock {
			[DIDepend("SimpleAdapter2")]
			public ISampleCommand<DataTable> Command { get; set; }
		}
	}
}
