// ==============================================================================
//     Test.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using NUnit.Framework;
using Net.Rimakiri.Container.Converters;

namespace Test.Rimakiri.Container {
	internal enum DummyEnum {
		Front,
		Back,
		Left,
		Right,
	}

	[ExcludeFromCodeCoverage]
	internal class DummyCreation {
		// Empty
	}

	/// <summary>
	/// コンバータのテスト
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ConvertersTest {
		[Test]
		public void TestConvertEncoding() {
			var conv = new TextEncodingConverter();
			Assert.AreSame(Encoding.UTF32,
				conv.ConvertFrom("utf-32"));

			try {
				Assert.AreSame(Encoding.UTF32,
					conv.ConvertFrom("hogehoge"));
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (ArgumentException) {
				Assert.IsTrue(true);
			}

			try {
				Assert.AreSame(Encoding.UTF32,
					conv.ConvertFrom(null));
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (ArgumentException) {
				Assert.IsTrue(true);
			}
		}

		[Test]
		public void TestObjectCreation() {
			var conv = new ObjectCreationConverter<DummyCreation>();
			Assert.IsNotNull(conv.ConvertFrom("hogehoge"));

			// Null parameter is safe.
			Assert.IsNotNull(conv.ConvertFrom(null));
		}

		[Test]
		public void TestObjectCreationFail() {
			var conv = new ObjectCreationConverter<Hidden>();
			try {
				conv.ConvertFrom("");
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (NotSupportedException) {
				Assert.IsTrue(true);
			}
		}

		[Test]
		public void TestEnumConvert() {
			var conv = new EnumConverter<DummyEnum>();

			Assert.AreEqual(DummyEnum.Right, conv.ConvertFrom("RIGHT"));
			Assert.AreEqual(DummyEnum.Left, conv.ConvertFrom(DummyEnum.Left.ToString()));

			// Not found - Default value.
			Assert.AreEqual(DummyEnum.Front, conv.ConvertFrom("EXE"));

			try {
				conv.ConvertFrom("");
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (ArgumentException) {
				Assert.IsTrue(true);
			}

			try {
				conv.ConvertFrom(null);
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (ArgumentException) {
				Assert.IsTrue(true);
			}
		}


		[Test]
		public void TestTimeSpanConvert() {
			var conv = new TimeSpanConverter();

			Assert.AreEqual(TimeSpan.FromSeconds(90), conv.ConvertFrom("90"));

			try {
				conv.ConvertFrom("");
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (ArgumentException) {
				Assert.IsTrue(true);
			}

			try {
				conv.ConvertFrom("QWERTY");
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (ArgumentException) {
				Assert.IsTrue(true);
			}

			try {
				conv.ConvertFrom(null);
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (ArgumentException) {
				Assert.IsTrue(true);
			}
		}

		public class Hidden {
			private Hidden() {
			}
		}
	}
}
