// ==============================================================================
//     Test.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using NUnit.Framework;
using Net.Rimakiri.Container;

namespace Test.Rimakiri.Container {
	[ExcludeFromCodeCoverage]
	public static class ConfigUtils {
		public static Configuration LoadConfig(string config) {
			var configMap = new ExeConfigurationFileMap {
				ExeConfigFilename = config,
			};
			return ConfigurationManager.OpenMappedExeConfiguration(configMap,
				ConfigurationUserLevel.None);
		}
	}

	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class ReadConfigTest {
		private const string TargetConfig = "Test03.config";
		[Test]
		public void SimpleReadTest() {
			var conf = ConfigUtils.LoadConfig(TargetConfig);
			Assert.IsNotNull(conf.Sections["dicontainer"]);
			Assert.AreEqual(1, ConfigSection.GetAll(conf).Count());
			Assert.IsNotNull(ConfigSection.GetAll(conf).FirstOrDefault());
		}

		[Test]
		public void RootContainersTest() {
			var conf = ConfigUtils.LoadConfig(TargetConfig);
			var section = conf.Sections["dicontainer"];
			var diconfig = section as ConfigSection;
			Assert.IsNotNull(diconfig);

			Assert.IsNotNull(diconfig.Root);
			Assert.AreEqual("RootConfig.config", diconfig.Root.Config);

			Assert.AreEqual(2, diconfig.Containers.Count);
		}

		[Test]
		public void ContainersTest() {
			var conf = ConfigUtils.LoadConfig(TargetConfig);
			var section = conf.Sections["dicontainer"];
			var diconfig = section as ConfigSection;
			Assert.IsNotNull(diconfig);

			var containers = diconfig.Containers;
			Assert.AreEqual(2, containers.Count);

			var container = containers.All().FirstOrDefault();
			Assert.IsNotNull(container);
			Assert.AreEqual("Child02.config", container.Config);
			Assert.AreEqual("hoge", container.Name);
			Assert.AreEqual(0, container.Aliases.Count);
		}

		[Test]
		public void AliasTest() {
			var conf = ConfigUtils.LoadConfig(TargetConfig);
			var section = conf.Sections["dicontainer"];
			var diconfig = section as ConfigSection;
			Assert.IsNotNull(diconfig);

			var containers = diconfig.Containers;
			Assert.AreEqual(2, containers.Count);

			var container = containers.All().Skip(1).First();
			Assert.IsNotNull(container);
			Assert.AreEqual("Child03.config", container.Config);
			Assert.AreEqual("fuga", container.Name);
			Assert.AreEqual(2, container.Aliases.Count);

			var firstAlias = container.Aliases.All().First();
			Assert.IsNotNull(firstAlias);
			Assert.AreEqual("fuga_01", firstAlias.Name);

			var secondAlias = container.Aliases.All().Skip(1).First();
			Assert.IsNotNull(secondAlias);
			Assert.AreEqual("fuga_02", secondAlias.Name);
		}
	}
}
