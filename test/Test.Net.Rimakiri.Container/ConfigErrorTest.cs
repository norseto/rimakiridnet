// ==============================================================================
//     Test.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using NUnit.Framework;
using Net.Rimakiri.Container;

namespace Test.Rimakiri.Container {
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class ConfigErrorTest {
		private const string TargetConfig = "Test03.config";
		[Test]
		public void MultipleRootTest() {
			try {
				var conf = ConfigUtils.LoadConfig("Test05-1.config");
				var section = conf.Sections["dicontainer"];
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (ConfigurationErrorsException) {
				Assert.IsTrue(true);
			}
		}

		[Test]
		public void InvalidContainerTest() {
			try {
				var conf = ConfigUtils.LoadConfig("Test05-2.config");
				var section = conf.Sections["dicontainer"];
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (ConfigurationErrorsException) {
				Assert.IsTrue(true);
			}
		}

		[Test]
		public void InvalidAliasTest() {
			try {
				var conf = ConfigUtils.LoadConfig("Test05-3.config");
				var section = conf.Sections["dicontainer"];
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (ConfigurationErrorsException) {
				Assert.IsTrue(true);
			}
		}

		[Test]
		public void DuplicateContainerNameTest() {
			try {
				var conf = ConfigUtils.LoadConfig("Test06-1.config");
				var section = ConfigSection.GetAll(conf).First();
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (ConfigurationErrorsException cee) {
				Assert.IsTrue(cee.Message.Contains("'hoge'"));
			}
		}

		[Test]
		public void DuplicateCrossAliasContainerNameTest() {
			try {
				var conf = ConfigUtils.LoadConfig("Test06-2.config");
				var section = ConfigSection.GetAll(conf).First();
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (ConfigurationErrorsException cee) {
				Assert.IsTrue(cee.Message.Contains("hoge"));
			}
		}

		[Test]
		public void MultipleDuplicateCrossAliasContainerNameTest() {
			try {
				var conf = ConfigUtils.LoadConfig("Test06-3.config");
				var section = ConfigSection.GetAll(conf).First();
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (ConfigurationErrorsException cee) {
				Assert.IsTrue(cee.Message.Contains("fuga, hoge")
					|| cee.Message.Contains("hoge, fuga"));
			}
		}

		[Test]
		public void MultipleDuplicateCrossAliasNameTest() {
			try {
				var conf = ConfigUtils.LoadConfig("Test06-4.config");
				var section = ConfigSection.GetAll(conf).First();
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (ConfigurationErrorsException cee) {
				Assert.IsTrue(cee.Message.Contains("hoge, fuga"));
			}
		}
	}
}
