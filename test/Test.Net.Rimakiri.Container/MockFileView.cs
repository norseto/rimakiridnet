﻿// ==============================================================================
//     Test.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Text;
using Test.Rimakiri.Container.Models.View;

namespace Test.Rimakiri.Container {
	[ExcludeFromCodeCoverage]
	public class MockFileView : AbstractFileViewModel {
		/// <summary>
		/// 出力先ディレクトリ
		/// </summary>
		public string OutDir { get; set; }
		private Stream stream;

		public override void BinaryWrite(byte[] target) {
			stream.Write(target, 0, target.Length);
		}

		public override void Init() {
			base.Init();
			stream = new BufferedStream(
				new FileInfo(Path.Combine(OutDir, "Mock_" + ViewFileName)).OpenWrite());
		}

		public override void OutputFile(FileInfo target) {
			var buff = new byte[8192];

			using (BufferedStream instr = new BufferedStream(target.OpenRead())) {
				int read;
				while ((read = instr.Read(buff, 0, buff.Length)) > 0) {
					stream.Write(buff, 0, read);
				}
			}
		}

		public override void Write(string target) {
			if (string.IsNullOrEmpty(target)) {
				return;
			}
			var data = Encoding.UTF8.GetBytes(target);
			stream.Write(data, 0, data.Length);
		}

		protected override void Dispose(bool disposing) {
			if (!disposing) {
				return;
			}
			if (ReferenceEquals(null, stream)) {
				return;
			}
			stream.Dispose();
			stream = null;
		}
	}
}
