// ==============================================================================
//     Test.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;
using Test.Rimakiri.Container.Models.File;

namespace Test.Rimakiri.Container {
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class FileTypeTest {
		[Test]
		public void TestDetectType() {
			Assert.AreEqual(FileType.Unknown, FileTypeExt.DetectFileType(null));
			Assert.AreEqual(FileType.Unknown, FileTypeExt.DetectFileType("Hoge"));
			Assert.AreEqual(FileType.Pdf, FileTypeExt.DetectFileType("aaa.pdf"));
			Assert.AreEqual(FileType.Xls, FileTypeExt.DetectFileType("aaa.XLS"));
			Assert.AreEqual(FileType.Csv, FileTypeExt.DetectFileType("aaa.cSv"));
		}

		[Test]
		public void TestExtension() {
			Assert.AreEqual(null, FileType.Unknown.Extension());
			Assert.AreEqual(".pdf", FileType.Pdf.Extension());
			Assert.AreEqual(".xls", FileType.Xls.Extension());
			Assert.AreEqual(".csv", FileType.Csv.Extension());
			Assert.AreEqual(".tsv", FileType.Tsv.Extension());
		}

		[Test]
		public void TestContentType() {
			Assert.AreEqual("application/octet-stream", FileType.Unknown.ContentType());
			Assert.AreEqual("application/pdf", FileType.Pdf.ContentType());
			Assert.AreEqual("application/vnd.ms-excel", FileType.Xls.ContentType());
			Assert.AreEqual("application/octet-stream", FileType.Csv.ContentType());
			Assert.AreEqual("application/octet-stream", FileType.Tsv.ContentType());
		}
	}
}
