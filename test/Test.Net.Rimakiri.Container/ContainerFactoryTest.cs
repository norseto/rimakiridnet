// ==============================================================================
//     Test.Rimakiri.Container
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;
using Net.Rimakiri.Container;

namespace Test.Rimakiri.Container {
	public interface IFactoryTestModel {
		string Property01 { get; set; }
		string Property02 { get; set; }
		string Property03 { get; set; }
		string Property04 { get; set; }
		string Property05 { get; set; }
	}

	[ExcludeFromCodeCoverage]
	public class FactoryTestModel : IFactoryTestModel {
		public string Property01 { get; set; }
		public string Property02 { get; set; }
		public string Property03 { get; set; }
		public string Property04 { get; set; }
		public string Property05 { get; set; }
	}

	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ContainerFactoryTest {
		[Test]
		public void TestInherited() {
			var registry = DIContainerRegistry.Load("Test08.config");

			var grandChild = registry["Parent::Child::GrandChild"].TryResolve<IFactoryTestModel>();
			Assert.AreEqual("GrandChildContainer", grandChild.Property04);
			Assert.AreEqual("ChildContainer", grandChild.Property03);
			Assert.AreEqual("ParentContainer", grandChild.Property02);
			Assert.AreEqual("RootContainer", grandChild.Property01);

			var child = registry["Parent::Child"].TryResolve<IFactoryTestModel>();
			Assert.IsNull(child.Property04);
			Assert.AreEqual("ChildContainer", child.Property03);
			Assert.AreEqual("ParentContainer", child.Property02);
			Assert.AreEqual("RootContainer", child.Property01);

			var parent = registry["Parent"].TryResolve<IFactoryTestModel>();
			Assert.IsNull(parent.Property04);
			Assert.IsNull(parent.Property03);
			Assert.AreEqual("ParentContainer", parent.Property02);
			Assert.AreEqual("RootContainer", parent.Property01);

			var root = registry[""].TryResolve<IFactoryTestModel>();
			Assert.IsNull(root.Property04);
			Assert.IsNull(root.Property03);
			Assert.IsNull(root.Property02);
			Assert.AreEqual("RootContainer", root.Property01);
		}

		[Test]
		public void TestIndependent() {
			var registry = DIContainerRegistry.Load("Test08-2.config");

			var grandChild = registry["Parent::Child::GrandChild"].TryResolve<IFactoryTestModel>();
			Assert.AreEqual("GrandChildContainer", grandChild.Property04);
			Assert.AreEqual("ChildContainer", grandChild.Property03);
			Assert.AreEqual("GrandChildContainer", grandChild.Property02);
			Assert.AreEqual("RootContainer", grandChild.Property01);

			var grandChild2 = registry["GrandChild2"].TryResolve<IFactoryTestModel>();
			Assert.AreEqual("GrandChild2Container", grandChild2.Property04);
			Assert.AreEqual("ChildContainer", grandChild2.Property03);
			Assert.AreEqual("GrandChild2Container", grandChild2.Property02);
			Assert.AreEqual("RootContainer", grandChild2.Property01);

			var child = registry["Parent::Child"].TryResolve<IFactoryTestModel>();
			Assert.IsNull(child.Property04);
			Assert.AreEqual("ChildContainer", child.Property03);
			Assert.AreEqual("ParentContainer", child.Property02);
			Assert.AreEqual("RootContainer", child.Property01);

			var parent = registry["Parent"].TryResolve<IFactoryTestModel>();
			Assert.IsNull(parent.Property04);
			Assert.IsNull(parent.Property03);
			Assert.AreEqual("ParentContainer", parent.Property02);
			Assert.AreEqual("RootContainer", parent.Property01);

			var root = registry[""].TryResolve<IFactoryTestModel>();
			Assert.IsNull(root.Property04);
			Assert.IsNull(root.Property03);
			Assert.IsNull(root.Property02);
			Assert.AreEqual("RootContainer", root.Property01);
		}

		[Test]
		public void TestCyclic() {
			try {
				DIContainerRegistry.Load("Test09.config");
				Assert.IsTrue(false, "Should be thrown an exception.");
			}
			catch (ConfigurationErrorsException e) {
				var msg = e.Message;
				Assert.IsTrue(msg.Contains("Parent"));
				Assert.IsTrue(msg.Contains("Parent::Child"));
				Assert.IsTrue(msg.Contains("Parent::Child::GrandChild"));
			}
		}


	}
}
