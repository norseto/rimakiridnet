// ==============================================================================
//     Test.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;
using Net.Rimakiri.Data;

namespace Test.Rimakiri.Data {
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class TestWhereFragment {
		[Test]
		public void TestSimple() {
			var fragment = new SimpleFragment("SELECT * FROM T_HOGE");
			var condA = new BasicCondition("ID = {0}", BindValue.Create("id", "hoge"));
			var statement = fragment.Concat(new WhereFragment(condA));

			var text = statement.ToText();
			Assert.AreEqual("SELECT * FROM T_HOGE WHERE ID = @id", text);
		}

		[Test]
		public void TestSimpleNull() {
			var fragment = new SimpleFragment("SELECT * FROM T_HOGE");
			var condA = new BasicFragment("ID = {0}", BindValue.Create("id", "hoge")) {
				ExcludeFilter = Filters.NotNullOrEmpty,
			};
			var statement = fragment.Concat(new WhereFragment(condA));

			var text = statement.ToText();
			Assert.AreEqual("SELECT * FROM T_HOGE WHERE ID = @id", text);
			Assert.AreSame(condA.ExcludeFilter, Filters.NotNullOrEmpty);

			condA.ReplaceValue(BindValue.Create<int?>("id", null));
			text = statement.ToText();
			Assert.AreEqual("SELECT * FROM T_HOGE", text);
		}

		[Test]
		public void TestSimple2Cond() {
			var fragment = new SimpleFragment("SELECT * FROM T_HOGE");
			var condA = new NotNullCondition("ID = {0}", BindValue.Create("id", "hoge"));
			var condB = new NotNullCondition("NAME = {0}", BindValue.Create("name", "hoge"));
			var statement = fragment.Concat(new WhereFragment(condA.And(condB)));

			Assert.AreEqual("SELECT * FROM T_HOGE WHERE ID = @id AND NAME = @name",
							statement.ToText());

			condA.ReplaceValue(BindValue.Create<int?>("id", null));
			Assert.AreEqual("SELECT * FROM T_HOGE WHERE NAME = @name",
					statement.ToText());

			condB.ReplaceValue(BindValue.Create<int?>("name", null));
			Assert.AreEqual("SELECT * FROM T_HOGE",
							statement.ToText());
		}
	}
}
