// ==============================================================================
//     Test.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;
using Net.Rimakiri.Data;
using Net.Rimakiri.Data.Interfaces;

namespace Test.Rimakiri.Data {
	/// <summary>
	/// 条件式のテストクラスです。
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestCondition {
		[Test]
		public void TestSimple() {
			var ctx = new DummyFragmentContext();
			var condA = new SimpleCondition("A = B");
			Assert.AreEqual("A = B", condA.ToLiteralString(ctx));
		}

		[Test]
		public void TestSimpleAnd() {
			var ctx = new DummyFragmentContext();
			var condA = new SimpleCondition("A = B");
			var condB = new SimpleCondition("X = Y");
			Assert.AreEqual("A = B AND X = Y",
				CondUtils.AndJoin(condA, condB).ToLiteralString(ctx));
		}

		[Test]
		public void TestSimpleOr() {
			var ctx = new DummyFragmentContext();
			var condA = new SimpleCondition("A = B");
			var condB = new SimpleCondition("X = Y");
			Assert.AreEqual("A = B OR X = Y",
				CondUtils.OrJoin(condA, condB).ToLiteralString(ctx));
			Assert.AreEqual("A = B OR X = Y",
				condA.Or(condB).ToLiteralString(ctx));
		}

		[Test]
		public void TestComplexJoinAndOr() {
			var ctx = new DummyFragmentContext();
			var condA = new SimpleCondition("A = B");
			var condB = new SimpleCondition("X = Y");
			var condC = new SimpleCondition("C = D");
			var condD = new SimpleCondition("W = Z");
			Assert.AreEqual("(A = B AND X = Y) OR (C = D AND W = Z)",
				CondUtils.OrJoin(
					CondUtils.AndJoin(condA, condB),
					CondUtils.AndJoin(condC, condD)).ToLiteralString(ctx));
			Assert.AreEqual("(A = B AND X = Y) OR (C = D AND W = Z)",
				condA.And(condB).Or(condC.And(condD)).ToLiteralString(ctx));
			Assert.AreEqual("((A = B AND X = Y) OR C = D) AND W = Z",
				condA.And(condB).Or(condC).And(condD).ToLiteralString(ctx));
		}

		[Test]
		public void TestComplexJoinOrAnd() {
			var ctx = new DummyFragmentContext();
			var condA = new SimpleCondition("A = B");
			var condB = new SimpleCondition("X = Y");
			var condC = new SimpleCondition("C = D");
			var condD = new SimpleCondition("W = Z");
			Assert.AreEqual("(A = B OR X = Y) AND (C = D OR W = Z)",
				CondUtils.AndJoin(
					CondUtils.OrJoin(condA, condB),
					CondUtils.OrJoin(condC, condD)).ToLiteralString(ctx));
			Assert.AreEqual("(A = B OR X = Y) AND (C = D OR W = Z)",
				condA.Or(condB).And(condC.Or(condD)).ToLiteralString(ctx));
		}

		[Test]
		public void TestSimpleFlatten() {
			var ctx = new DummyFragmentContext();
			var condA = new SimpleCondition("A = B");
			var condB = new SimpleCondition("X = Y");
			var condC = new SimpleCondition("C = D");
			var condD = new SimpleCondition("W = Z");
			Assert.AreEqual("A = B AND X = Y AND C = D AND W = Z",
				CondUtils.AndJoin(
					CondUtils.AndJoin(condA, condB),
					CondUtils.AndJoin(condC, condD)).ToLiteralString(ctx));
			Assert.AreEqual("A = B AND X = Y AND C = D AND W = Z",
				condA.And(condB).And(condC.And(condD)).ToLiteralString(ctx));
		}

		[Test]
		public void TestComplexFlatten() {
			var ctx = new DummyFragmentContext();
			var condA = new SimpleCondition("A = B");
			var condB = new SimpleCondition("X = Y");
			var condC = new SimpleCondition("C = D");
			var condD = new SimpleCondition("W = Z");
			var condE = new SimpleCondition("S = T");
			Assert.AreEqual("A = B AND X = Y AND (C = D OR W = Z OR S = T)",
				CondUtils.AndJoin(
					CondUtils.AndJoin(condA, condB),
					CondUtils.OrJoin(
						CondUtils.OrJoin(condC, condD), condE)).ToLiteralString(ctx));
			Assert.AreEqual("A = B AND X = Y AND (C = D OR W = Z OR S = T)",
				condA.And(condB).And(condC.Or(condD, condE)).ToLiteralString(ctx));
			Assert.AreEqual("A = B AND X = Y AND (C = D OR W = Z OR S = T)",
				condA.And(condB).And(condC.Or(condD).Or(condE)).ToLiteralString(ctx));
		}

		[Test]
		public void TestAppend() {
			var ctx = new DummyFragmentContext();
			var condA = new SimpleFragment { Text = "A = B" };
			var condB = new SimpleFragment("X = Y");
			var condC = new SimpleFragment("C = D");
			var condD = new SimpleFragment("W = Z");
			var condE = new SimpleFragment("S = T");
			Assert.AreEqual("A = B X = Y C = D W = Z S = T",
				condA.Concat(condB).Concat(condC.Concat(condD, condE)).ToLiteralString(ctx));
			Assert.AreEqual("A = B X = Y C = D W = Z S = T",
				condA.Concat(condB).Concat(condC.Concat(condD).Concat(condE)).ToLiteralString(ctx));
		}

		[Test]
		public void TestNullJoin() {
			Assert.IsNull(CondUtils.AndJoin(null));
			Assert.IsNull(CondUtils.AndJoin(null, null));

			var condA = new SimpleCondition { Text = "A = B" };
			Assert.AreSame(condA, CondUtils.AndJoin(null, condA));
			Assert.AreSame(condA, CondUtils.AndJoin(condA, null));
			Assert.AreSame(condA, condA.And(null));
			Assert.AreSame(condA, condA.Or(null));

			condA = null;
			Assert.IsNull(condA.And(null));

			IFragment la = null;
			IFragment lb = null;
			Assert.IsNull(la.Concat(lb));

			Assert.IsNull(CondUtils.Join(AndOp.Singleton, null));
			Assert.IsNull(CondUtils.Join(AndOp.Singleton, (IEnumerable<ICondition>)null));
		}

		[Test]
		public void TestPractical() {
			int id = 100;
			string name = "...";
			var fragment = new SimpleFragment("SELECT * FROM T_SAMPLE WHERE");
			var condA = new BasicCondition("ID = {0}", BindValue.Create("id", id));
			var condB = new BasicCondition("NAME = {0}", BindValue.Create("name", name, 20)) {
				ExcludeFilter = v => v.Value == null
			};
			var statement = fragment.Concat(condA.And(condB));
			var ctx = new DummyFragmentContext {
				DatabaseDialect = new GenericDatabaseDialect(),
			};
			var ret = statement.ToLiteralString(ctx);

			Assert.AreEqual("SELECT * FROM T_SAMPLE WHERE ID = @id AND NAME = @name", ret);
		}

		[Test]
		public void TestPracticalNull() {
			int id = 100;
			string name = null;
			var fragment = new SimpleFragment("SELECT * FROM T_SAMPLE WHERE");
			var condA = new BasicCondition("ID = {0}", BindValue.Create("id", id));
			var condB = new BasicCondition("NAME = {0}", BindValue.Create("name", name, 20)) {
				ExcludeFilter = v => v.Value == null
			};
			var statement = fragment.Concat(condA.And(condB));
			var ctx = new DummyFragmentContext {
				DatabaseDialect = new GenericDatabaseDialect(),
			};
			var ret = statement.ToLiteralString(ctx);

			Assert.AreEqual("SELECT * FROM T_SAMPLE WHERE ID = @id", ret);
		}
	}
}
