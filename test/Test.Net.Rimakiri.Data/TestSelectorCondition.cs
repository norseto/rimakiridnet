// ==============================================================================
//     Test.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;
using Net.Rimakiri.Data;

namespace Test.Rimakiri.Data {
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestSelectorCondition {
		[Test]
		public void SingleChildTest() {
			Assert.AreEqual("A = @id", new BasicFragment("A = {0}", BindValue.Create("id", "B")).ToText());
		}

		[Test]
		public void SingleDisabledChildTest() {
			Assert.IsNull(new NotNullCondition("A = {0}", BindValue.Create<string>("id", null)).ToText());
		}

		[Test]
		public void SelectionTest() {
			var condA = new NotNullCondition("ID = {0}", BindValue.Create("id", "hoge"));
			var condB = new NotNullCondition("NAME = {0}", BindValue.Create("name", "hoge"));
			var condC = new NotNullCondition("ADDRESS = {0}", BindValue.Create("addr", "hoge"));
			var statement = new SelectorCondition(condA, condB.And(condC));

			Assert.AreEqual("ID = @id", statement.ToText());

			condA.ReplaceValue(BindValue.Create<int?>("id", null));
			Assert.AreEqual("NAME = @name AND ADDRESS = @addr", statement.ToText());

			condB.ReplaceValue(BindValue.Create<int?>("name", null));
			condC.ReplaceValue(BindValue.Create<int?>("addr", null));
			Assert.IsNull(statement.ToText());
		}
	}
}
