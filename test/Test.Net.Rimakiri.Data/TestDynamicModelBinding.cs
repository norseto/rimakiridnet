// ==============================================================================
//     Test.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;
using Net.Rimakiri.Data;
using Net.Rimakiri.Data.TypeWrappers;
using Net.Rimakiri.Data.Util;

namespace Test.Rimakiri.Data {
	[ExcludeFromCodeCoverage]
	public class ModelBindingModel {
		[EntityBinding(Size = 10, Identity = "given")]
		public string Name { get; set; }

		[EntityBinding(Size = 20, Identity = "wrapped")]
		public NCharType NCharName { get; set; }

		[EntityBinding(Precision = 12, Scale = 3, Identity = "total")]
		public decimal PayAmount { get; set; }

		public string NoLengthName { get; set; }

		[return: EntityBinding(Identity = "hello", Size = 30)]
		public string Hello() {
			return "Hello world";
		}

		public ModelBindingChildModel Child { get; set; }
		
		public ModelBindingModel Brother { get; set; }
	}

	[ExcludeFromCodeCoverage]
	public class ModelBindingChildModel {
		[EntityBinding(Size = 40, Identity = "fullname")]
		public string FullName {get; set; }
	}

	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestDynamicModelBinding {
		[Test]
		public void SimpleTest() {
			var binding = new ModelBindingFactory<ModelBindingModel> { Dynamic = true }.Create(
				new ModelBindingModel {
					Name = "Hoge",
				});
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
				binding.Create("name", m => m.Name));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual("name", bound.Identity);
			Assert.AreEqual(10, bound.BindMetaData.Size);
			Assert.AreEqual(0, bound.BindMetaData.Precision);
			Assert.AreEqual(0, bound.BindMetaData.Scale);
			Assert.AreEqual("Hoge", bound.Value);
		}

		[Test]
		public void SimplePerformanceTest() {
			var binding = new ModelBindingFactory<ModelBindingModel> { Dynamic = true }.Create(
				new ModelBindingModel {
					Name = "Hoge",
				});
			var start = DateTime.Now;
			for (var i = 0; i < 100000; i++) {
				binding.Create("name", m => m.Name);
			}
			var end = DateTime.Now;
			Console.Write("Elapsed : " + (end - start).TotalMilliseconds);
		}

		[Test]
		public void ChildTest() {
			var binding = new ModelBindingFactory<ModelBindingModel> { Dynamic = true }.Create(
				new ModelBindingModel {
					Name = "Hoge",
					Child = new ModelBindingChildModel {
						FullName = "Full Name",
					},
				});
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
				binding.Create(m => m.Child.FullName));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual("fullname", bound.Identity);
			Assert.AreEqual(40, bound.BindMetaData.Size);
			Assert.AreEqual(0, bound.BindMetaData.Precision);
			Assert.AreEqual(0, bound.BindMetaData.Scale);
			Assert.AreEqual("Full Name", bound.Value);
		}
		
		[Test]
		public void BrotherTest() {
			var brother = new ModelBindingModel {
				Name = "Brother",
			};
			var binding = new ModelBindingFactory<ModelBindingModel> { Dynamic = true }.Create(
					new ModelBindingModel {
							Name = "Hoge",
							Brother = brother,
					});
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
					binding.Create("name", m => m.Brother.Name));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual("name", bound.Identity);
			Assert.AreEqual(10, bound.BindMetaData.Size);
			Assert.AreEqual(0, bound.BindMetaData.Precision);
			Assert.AreEqual(0, bound.BindMetaData.Scale);
			Assert.AreEqual("Brother", bound.Value);
		}

		[Test]
		public void NoIdSimpleTest() {
			var binding = new ModelBindingFactory<ModelBindingModel> { Dynamic = true }.Create(
				new ModelBindingModel {
					Name = "Hoge",
				});
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
				binding.Create(m => m.Name));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual("given", bound.Identity);
			Assert.AreEqual(10, bound.BindMetaData.Size);
			Assert.AreEqual(0, bound.BindMetaData.Precision);
			Assert.AreEqual(0, bound.BindMetaData.Scale);
			Assert.AreEqual("Hoge", bound.Value);
		}

		[Test]
		public void ReturnValueTest() {
			var binding = new ModelBindingFactory<ModelBindingModel> { Dynamic = true }.Create(
				new ModelBindingModel {
					Name = "Hoge",
				});
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
				binding.Create("name", m => m.Hello()));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual("name", bound.Identity);
			Assert.AreEqual(30, bound.BindMetaData.Size);
			Assert.AreEqual(0, bound.BindMetaData.Precision);
			Assert.AreEqual(0, bound.BindMetaData.Scale);
			Assert.AreEqual("Hello world", bound.Value);
		}

		[Test]
		public void NoIdReturnValueTest() {
			var binding = new ModelBindingFactory<ModelBindingModel> { Dynamic = true }.Create(
				new ModelBindingModel {
					Name = "Hoge",
				});
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
				binding.Create(m => m.Hello()));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual("hello", bound.Identity);
			Assert.AreEqual(30, bound.BindMetaData.Size);
			Assert.AreEqual(0, bound.BindMetaData.Precision);
			Assert.AreEqual(0, bound.BindMetaData.Scale);
			Assert.AreEqual("Hello world", bound.Value);
		}

		[Test]
		public void NoAttributeTest() {
			var binding = new ModelBindingFactory<ModelBindingModel> { Dynamic = true }.Create(
								new ModelBindingModel {
									NoLengthName = "Hoge",
								});
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
								binding.Create("name", m => m.NoLengthName));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual("name", bound.Identity);
			Assert.AreEqual(-1, bound.BindMetaData.Size);
			Assert.AreEqual(0, bound.BindMetaData.Precision);
			Assert.AreEqual(0, bound.BindMetaData.Scale);
			Assert.AreEqual("Hoge", bound.Value);
		}

		[Test]
		public void SimpleNumericTest() {
			var binding = new ModelBindingFactory<ModelBindingModel> { Dynamic = true }.Create(
								new ModelBindingModel {
									PayAmount = 1234.34m,
								});
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
								binding.Create("name", m => m.PayAmount));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual("name", bound.Identity);
			Assert.AreEqual(0, bound.BindMetaData.Size);
			Assert.AreEqual(12, bound.BindMetaData.Precision);
			Assert.AreEqual(3, bound.BindMetaData.Scale);
			Assert.AreEqual(1234.34m, bound.Value);
		}

		[Test]
		public void NoIdSimpleNumericTest() {
			var binding = new ModelBindingFactory<ModelBindingModel> { Dynamic = true }.Create(
				new ModelBindingModel {
					PayAmount = 1234.34m,
				});
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
				binding.Create(m => m.PayAmount));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual("total", bound.Identity);
			Assert.AreEqual(0, bound.BindMetaData.Size);
			Assert.AreEqual(12, bound.BindMetaData.Precision);
			Assert.AreEqual(3, bound.BindMetaData.Scale);
			Assert.AreEqual(1234.34m, bound.Value);
		}

		[Test]
		public void FormatTest() {
			var binding = new ModelBindingFactory<ModelBindingModel> { Dynamic = true }.Create(
								new ModelBindingModel {
									Name = "Hoge",
								});
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
								binding.WithFormat("name", BindValue.ForwardMatch, m => m.Name));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual("name", bound.Identity);
			Assert.AreEqual(10, bound.BindMetaData.Size);
			Assert.AreEqual("Hoge%", bound.Value);
			Assert.AreEqual("Hoge", bound.RawValue);
		}

		[Test]
		public void NoIdFormatTest() {
			var binding = new ModelBindingFactory<ModelBindingModel> { Dynamic = true }.Create(
				new ModelBindingModel {
					Name = "Hoge",
				});
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
				binding.WithFormat(BindValue.ForwardMatch, m => m.Name));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual("given", bound.Identity);
			Assert.AreEqual(10, bound.BindMetaData.Size);
			Assert.AreEqual("Hoge%", bound.Value);
			Assert.AreEqual("Hoge", bound.RawValue);
		}

		[Test]
		public void FormatWrapperTest() {
			var binding = new ModelBindingFactory<ModelBindingModel> { Dynamic = true }.Create(
								new ModelBindingModel {
									NCharName = new NCharType("Hoge"),
								});
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
								binding.WithFormat("name", BindValue.ForwardMatch, m => m.NCharName));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual("name", bound.Identity);
			Assert.AreEqual(20, bound.BindMetaData.Size);
			Assert.AreEqual("Hoge%", bound.Value);
			Assert.AreEqual("Hoge", ((NCharType)(bound.RawValue)).Value);
		}

		[Test]
		public void NoIdFormatWrapperTest() {
			var binding = new ModelBindingFactory<ModelBindingModel> { Dynamic = true }.Create(
				new ModelBindingModel {
					NCharName = new NCharType("Hoge"),
				});
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
				binding.WithFormat(BindValue.ForwardMatch, m => m.NCharName));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual("wrapped", bound.Identity);
			Assert.AreEqual(20, bound.BindMetaData.Size);
			Assert.AreEqual("Hoge%", bound.Value);
			Assert.AreEqual("Hoge", ((NCharType)(bound.RawValue)).Value);
		}

		[Test]
		public void FormatOverrideWrapperTest() {
			var binding = new ModelBindingFactory<ModelBindingModel> { Dynamic = true }.Create(
								new ModelBindingModel {
									NCharName = new NCharType("Hoge", 100),
								});
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
								binding.WithFormat("name", BindValue.ForwardMatch, m => m.NCharName));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual(20, bound.BindMetaData.Size);
			Assert.AreEqual("Hoge%", bound.Value);
			Assert.AreEqual("Hoge", ((NCharType)(bound.RawValue)).Value);
		}

		[EntityBinding(Size = 100, Identity = "longlong")] private string LongLongName = "Hoge";

		[Test]
		public void FieldAttributeWithInstanceTest() {
			var binding =
					new ModelBindingFactory<TestDynamicModelBinding> { Dynamic = true }.Create(this);
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
				binding.WithFormat("name", BindValue.ForwardMatch, (m) => m.LongLongName));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual("name", bound.Identity);
			Assert.AreEqual(100, bound.BindMetaData.Size);
			Assert.AreEqual("Hoge%", bound.Value);
			Assert.AreEqual("Hoge", bound.RawValue);
		}

		[Test]
		public void NoIdFieldAttributeWithInstanceTest() {
			var binding =
					new ModelBindingFactory<TestDynamicModelBinding> { Dynamic = true }.Create(this);
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
				binding.WithFormat(BindValue.ForwardMatch, (m) => m.LongLongName));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual("longlong", bound.Identity);
			Assert.AreEqual(100, bound.BindMetaData.Size);
			Assert.AreEqual("Hoge%", bound.Value);
			Assert.AreEqual("Hoge", bound.RawValue);
		}
	}
}
