// ==============================================================================
//     Test.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using NUnit.Framework;
using Net.Rimakiri.Data;
using Net.Rimakiri.Data.Interfaces;
using Net.Rimakiri.Data.Util;

namespace Test.Rimakiri.Data {
	[ExcludeFromCodeCoverage]
	internal class DumpMap : IDatabaseTypeMap<object> {
		private readonly object dummy = new object();
		#region Implementation of IDatabaseTypeMap<out object>
		public object GetDatabaseType(string parameterName, IBindValue value) {
			return dummy;
		}
		#endregion
	}

	[ExcludeFromCodeCoverage]
	internal class DummyDialectImpl : TypeMapDatabaseDialect<object> {
		public static readonly IDatabaseTypeMap<object> DummyMap = new DumpMap();
		public DummyDialectImpl(IDatabaseTypeMap<object> typemap) : base(typemap) {
		}

		public DummyDialectImpl() : this(DummyMap) {
		}

		#region Implementation of IDatabaseDialect
		/// <inheritdoc />
		public override string CurrentTimestampLiteral {
			get { return "NOW()"; }
		}

		/// <inheritdoc />
		public override string CurrentUtcTimestampLiteral {
			get { return "UTCNOW()"; }
		}

		/// <inheritdoc />
		public override string DummyTable {
			get { return string.Empty; }
		}

		/// <inheritdoc />
		public override string DummyTableWithFrom {
			get { return string.Empty; }
		}

		/// <inheritdoc />
		public override string ParameterPrefix {
			get { return "@"; }
		}

		public override void SetupParameter(IDbDataParameter target, IBindValue value) {
			target.Value = value.Value;
			target.ParameterName = NameFor(value.Identity);
		}
		#endregion
	}
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class TestBasicCondition {
		[Test]
		public void TestNoParameter() {
			var dialect = new DummyDialectImpl();
			var ctx = new DummyFragmentContext {
				DatabaseDialect = dialect
			};
			var cond = new BasicCondition("SELECT * FROM HOGE");

			var text = cond.ToLiteralString(ctx);
			Assert.AreEqual("SELECT * FROM HOGE", text);
			Assert.IsNotNull(ctx.BindValues);
			Assert.AreEqual(0, ctx.BindValues.Count());
		}

		[Test]
		public void TestSingleParameter() {
			var dialect = new DummyDialectImpl();
			var ctx = new DummyFragmentContext {
				DatabaseDialect = dialect
			};
			var cond = new BasicCondition("SELECT * FROM HOGE WHERE NAME = {0}")
							.AddValues(BindValue.Create("name", "HogeHoge"));

			var text = cond.ToLiteralString(ctx);
			Assert.AreEqual("SELECT * FROM HOGE WHERE NAME = @name", text);
			Assert.IsNotNull(ctx.BindValues);
			Assert.AreEqual(1, ctx.BindValues.Count());
			var value = ctx.BindValues.First();
			Assert.AreEqual("name", value.Identity);
			Assert.AreEqual("HogeHoge", value.Value);
		}

		[Test]
		public void TestMultiParameter() {
			var dialect = new DummyDialectImpl();
			var ctx = new DummyFragmentContext {
				DatabaseDialect = dialect
			};
			var cond = new BasicCondition("SELECT * FROM HOGE WHERE NAME = {0} AND ADDRESS = {1}",
				BindValue.Create("name", "HogeHoge"),
					BindValue.Create("address", "Foo"));

			var text = cond.ToLiteralString(ctx);
			Assert.AreEqual("SELECT * FROM HOGE WHERE NAME = @name AND ADDRESS = @address", text);
			Assert.IsNotNull(ctx.BindValues);
			Assert.AreEqual(2, ctx.BindValues.Count());
			var value = ctx.BindValues.First();
			Assert.AreEqual("name", value.Identity);
			Assert.AreEqual("HogeHoge", value.Value);

			value = ctx.BindValues.Skip(1).First();
			Assert.AreEqual("address", value.Identity);
			Assert.AreEqual("Foo", value.Value);
		}

		[Test]
		public void TestMultiParameterWithOne() {
			var dialect = new DummyDialectImpl();
			var ctx = new DummyFragmentContext {
				DatabaseDialect = dialect
			};
			var cond = new BasicCondition("SELECT * FROM HOGE WHERE ADDRESS = {1}",
				BindValue.Create("name", "HogeHoge"),
				BindValue.Create("address", "Foo"));

			var text = cond.ToLiteralString(ctx);
			Assert.AreEqual("SELECT * FROM HOGE WHERE ADDRESS = @address", text);
			Assert.IsNotNull(ctx.BindValues);
			Assert.AreEqual(1, ctx.BindValues.Count());
			var value = ctx.BindValues.First();
			Assert.AreEqual("address", value.Identity);
			Assert.AreEqual("Foo", value.Value);
		}

		[Test]
		public void TestMultiParameterWithJoin() {
			var dialect = new DummyDialectImpl();
			var ctx = new DummyFragmentContext {
				DatabaseDialect = dialect
			};
			var cond1 = new BasicCondition("NAME = {0}",
				BindValue.Create("name", "HogeHoge"));
			var cond2 = new BasicCondition("ADDRESS = {0}",
				BindValue.Create("address", "Foo"));
			var cond = CondUtils.AndJoin(cond1, cond2);

			var text = cond.ToLiteralString(ctx);
			Assert.AreEqual("NAME = @name AND ADDRESS = @address", text);
			Assert.IsNotNull(ctx.BindValues);
			Assert.AreEqual(2, ctx.BindValues.Count());
			var value = ctx.BindValues.First();
			Assert.AreEqual("name", value.Identity);
			Assert.AreEqual("HogeHoge", value.Value);

			value = ctx.BindValues.Skip(1).First();
			Assert.AreEqual("address", value.Identity);
			Assert.AreEqual("Foo", value.Value);
		}

		[Test]
		public void TestReplaceValue() {
			var dialect = new DummyDialectImpl();
			var ctx = new DummyFragmentContext {
				DatabaseDialect = dialect
			};
			var head = new SimpleFragment("SELECT * FROM T_HOGE WHERE");
			var cond1 = new NotNullCondition("NAME = {0}", null)
						.AddValues(BindValue.Create("name", "HogeHoge"));
			var cond2 = new BasicCondition("ADDRESS = {0}",
							BindValue.Create("address", "Foo"));
			var cond = head.Concat(CondUtils.AndJoin(cond1, cond2));

			var text = cond.ToText();
			Assert.AreEqual("SELECT * FROM T_HOGE WHERE NAME = @name AND ADDRESS = @address", text);
			Assert.AreEqual(text, cond.ToLiteralString(ctx));

			cond1.ReplaceValue(BindValue.Create<string>("name", null));
			text = cond.ToText();
			Assert.AreEqual("SELECT * FROM T_HOGE WHERE ADDRESS = @address", text);
		}
	}
}
