// ==============================================================================
//     Test.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;
using Net.Rimakiri.Data;
using Net.Rimakiri.Data.TypeWrappers;

namespace Test.Rimakiri.Data {
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class TestBindValue {
		[Test]
		public void TestInt() {
			var value = BindValue.Create("id", 5);
			Assert.IsNotNull(value);
			Assert.IsNotNull(value.BindMetaData);

			Assert.AreEqual("id", value.Identity);
			Assert.AreEqual(5, value.Value);
			Assert.AreEqual(typeof(int), value.BindMetaData.DataType);
		}

		[Test]
		public void TestString() {
			var value = BindValue.Create("id", "name", 25);
			Assert.IsNotNull(value);
			Assert.IsNotNull(value.BindMetaData);

			Assert.AreEqual("id", value.Identity);
			Assert.AreEqual("name", value.Value);
			Assert.AreEqual(25, value.BindMetaData.Size);
			Assert.AreEqual(typeof(string), value.BindMetaData.DataType);
		}

		[Test]
		public void TestFloat() {
			var value = BindValue.Create("id", 20f, 0, 10, 3);
			Assert.IsNotNull(value);
			Assert.IsNotNull(value.BindMetaData);

			Assert.AreEqual("id", value.Identity);
			Assert.AreEqual(20f, value.Value);
			Assert.AreEqual(0, value.BindMetaData.Size);
			Assert.AreEqual(typeof(float), value.BindMetaData.DataType);
			Assert.AreEqual(3, value.BindMetaData.Scale);
			Assert.AreEqual(10, value.BindMetaData.Precision);
			Assert.IsFalse(value.BindMetaData.Nullable);
		}

		[Test]
		public void TestNullableNull() {
			int? intval = null;
			// ReSharper disable once ExpressionIsAlwaysNull
			var value = BindValue.Create("id", intval);
			Assert.IsNotNull(value);
			Assert.IsNotNull(value.BindMetaData);

			Assert.AreEqual("id", value.Identity);
			Assert.IsNull(value.Value);
			Assert.AreEqual(-1, value.BindMetaData.Size);
			Assert.AreEqual(typeof(int), value.BindMetaData.DataType);
		}

		[Test]
		public void TestNullableValue() {
			long? longval = 8;
			var value = BindValue.Create("id", longval);
			Assert.IsNotNull(value);
			Assert.IsNotNull(value.BindMetaData);

			Assert.AreEqual("id", value.Identity);
			Assert.AreEqual(8L, value.Value);
			Assert.AreEqual(-1, value.BindMetaData.Size);
			Assert.AreEqual(typeof(long), value.BindMetaData.DataType);
		}

		[Test]
		public void TestFormattedValue() {
			var value = BindValue.WithFormat("id", BindValue.ForwardMatch, "Hoge", 20);
			Assert.IsNotNull(value);
			Assert.IsNotNull(value.BindMetaData);

			Assert.AreEqual("id", value.Identity);
			Assert.AreEqual("Hoge%", value.Value);
			Assert.AreSame(value.Value, value.Value);
			Assert.AreEqual(typeof(string), value.BindMetaData.DataType);

			value = BindValue.WithFormat("id", BindValue.ForwardMatch, null, 20);
			Assert.IsNull(value.Value);
		}

		[Test]
		public void TestFormattedValueWithEscape() {
			var value = BindValue.WithFormat("id", BindValue.ForwardMatch, "H#o%ge", '#', 20);
			Assert.IsNotNull(value);
			Assert.AreEqual("H##o#%ge%", value.Value);
			Assert.AreSame(value.Value, value.Value);
			Assert.AreEqual(typeof(string), value.BindMetaData.DataType);

			Assert.AreEqual("H##o#%ge%", BindValueUtils.PeekBound(new BasicCondition("ID LIKE {0}", value), 0).Value);

			value = BindValue.WithFormat("id", BindValue.ForwardMatch, null, '#', 20);
			Assert.IsNull(value.Value);

			value = BindValue.WithFormat("id", BindValue.ForwardMatch, "Hoge", '#', 20);
			Assert.IsNotNull(value);
			Assert.AreEqual("Hoge%", value.Value);

			value = BindValue.WithFormat("id", BindValue.ForwardMatch, "H#o%ge", '$', 20);
			Assert.IsNotNull(value);
			Assert.AreEqual("H#o$%ge%", value.Value);

			value = BindValue.WithFormat("id", BindValue.MiddleMatch, "%", '#', 20);
			Assert.IsNotNull(value);
			Assert.AreEqual("%#%%", value.Value);
		}

		[Test]
		public void TestFormattedValueWithWrapper() {
			var value = BindValue.WithFormat("id", BindValue.ForwardMatch,
				new VarCharType("Hoge"), 20);
			Assert.IsNotNull(value);
			Assert.IsNotNull(value.BindMetaData);

			Assert.AreEqual("id", value.Identity);
			Assert.AreEqual("Hoge%", value.Value);
			Assert.AreSame(value.Value, value.Value);
			Assert.AreEqual(typeof(VarCharType), value.BindMetaData.DataType);

			value = BindValue.WithFormat("id", BindValue.ForwardMatch, null, 20);
			Assert.IsNull(value.Value);

			value = BindValue.WithFormat("id", BindValue.ForwardMatch,
				new NVarCharType("Hoge"), 20);
			Assert.AreEqual("Hoge%", value.Value);
			Assert.AreEqual(typeof(NVarCharType), value.BindMetaData.DataType);

			value = BindValue.WithFormat("id", BindValue.ForwardMatch,
				new CharType("Hoge"), 20);
			Assert.AreEqual("Hoge%", value.Value);
			Assert.AreEqual(typeof(CharType), value.BindMetaData.DataType);

			value = BindValue.WithFormat("id", BindValue.ForwardMatch,
				new NCharType("Hoge"), 20);
			Assert.AreEqual("Hoge%", value.Value);
			Assert.AreEqual(typeof(NCharType), value.BindMetaData.DataType);
		}

		[Test]
		#pragma warning disable CS1718 
		public void TestEquality() {
			var value = BindValue.Create("id", 100);
			var another = BindValue.Create("id", 100);
			var nullVal = BindValue.Create<string>("id", null);
			var anotherNullVal = BindValue.Create<string>("id", null);
			var stringVal = BindValue.WithFormat("id", "{0}", "Hoge");
			var formattedNullVal = BindValue.WithFormat("id", "{0}", null);

			// ReSharper disable once EqualExpressionComparison
			Assert.IsTrue(value == value);
			// ReSharper disable once EqualExpressionComparison
			Assert.IsFalse(value != value);
			Assert.IsTrue(value == another);
			Assert.IsFalse(value != another);
			Assert.IsFalse(value == formattedNullVal);
			Assert.IsFalse(nullVal == formattedNullVal);
			Assert.IsFalse(stringVal == formattedNullVal);
			Assert.AreEqual(value.GetHashCode(), another.GetHashCode());

			Assert.IsTrue(value != null);
			Assert.IsFalse(value.Equals(new object()));
			Assert.IsFalse(value == null);
			Assert.IsTrue(value != nullVal);
			Assert.IsTrue(nullVal != null);
			Assert.IsTrue(nullVal == anotherNullVal);
		}
	}
}
