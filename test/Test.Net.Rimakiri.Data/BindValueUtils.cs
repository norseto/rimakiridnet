// ==============================================================================
//     Test.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using Net.Rimakiri.Data;
using Net.Rimakiri.Data.Interfaces;

namespace Test.Rimakiri.Data {
	/// <summary>
	/// Utilitiy for <see cref="IBindValue"/>
	/// </summary>
	[ExcludeFromCodeCoverage]
	public static class BindValueUtils {
		/// <summary>
		/// Peek bound <see cref="IBindValue"/> of <see cref="IFragment"/>
		/// </summary>
		/// <param name="fragment">Target <see cref="IFragment"/></param>
		/// <param name="index">Index of <see cref="IBindValue"/> for peek</param>
		/// <returns>Bound value</returns>
		public static IBindValue PeekBound(IFragment fragment, int index) {
			var builder = new ValueBinderBuilder(fragment);
			var binder = builder.Build(new DummyDialectImpl());

			var types = new[] { typeof(Int32) };
			var pi = binder.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)
					.First(pp => pp.GetIndexParameters().Select(pr => pr.ParameterType).SequenceEqual(types));
			var bound = pi.GetValue(binder, new object[] {index});

			return bound as IBindValue;
		}
	}
}
