// ==============================================================================
//     Test.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace Test.Rimakiri.Data {
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestTypeMapDatabaseDialect {
		[Test]
		public void TestGetTypeMap() {
			var target = new DummyDialectImpl();
			Assert.AreSame(DummyDialectImpl.DummyMap, target.TypeMap);

			var map = new DumpMap();
			target.TypeMap = map;
			Assert.AreSame(map, target.TypeMap);

			target.TypeMap = null;
			Assert.AreSame(DummyDialectImpl.DummyMap, target.TypeMap);
			
			target = new DummyDialectImpl(null);
			Assert.IsNull(target.TypeMap);
		}
	}
}
