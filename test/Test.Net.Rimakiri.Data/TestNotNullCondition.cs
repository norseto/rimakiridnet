// ==============================================================================
//     Test.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Diagnostics.CodeAnalysis;
using System.Linq;
using NUnit.Framework;
using Net.Rimakiri.Data;

namespace Test.Rimakiri.Data {
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class TestNotNullCondition {
		[Test]
		public void TestNoParameter() {
			var dialect = new DummyDialectImpl();
			var ctx = new DummyFragmentContext {
				DatabaseDialect = dialect
			};
			var cond = new NotNullCondition("SELECT * FROM HOGE");

			var text = cond.ToLiteralString(ctx);
			Assert.AreSame(Filters.NotNull, cond.ExcludeFilter);
			Assert.AreEqual("SELECT * FROM HOGE", text);
			Assert.IsNotNull(ctx.BindValues);
			Assert.AreEqual(0, ctx.BindValues.Count());
		}

		[Test]
		public void TestMultiParameterWithJoin() {
			var dialect = new DummyDialectImpl();
			var ctx = new DummyFragmentContext {
				DatabaseDialect = dialect
			};
			var cond1 = new NotNullCondition("NAME = {0}",
				BindValue.Create("name", "HogeHoge"));
			var cond2 = new NotNullCondition("ADDRESS = {0}",
				BindValue.Create("address", "Foo"));
			var cond = CondUtils.AndJoin(cond1, cond2);

			var text = cond.ToLiteralString(ctx);
			Assert.AreEqual("NAME = @name AND ADDRESS = @address", text);
			Assert.IsNotNull(ctx.BindValues);
			Assert.AreEqual(2, ctx.BindValues.Count());
			var value = ctx.BindValues.First();
			Assert.AreEqual("name", value.Identity);
			Assert.AreEqual("HogeHoge", value.Value);

			value = ctx.BindValues.Skip(1).First();
			Assert.AreEqual("address", value.Identity);
			Assert.AreEqual("Foo", value.Value);
		}

		[Test]
		public void TestMultiParameterWithNullParamJoin() {
			var dialect = new DummyDialectImpl();
			var ctx = new DummyFragmentContext {
				DatabaseDialect = dialect
			};
			var cond1 = new BasicCondition("NAME = {0}",
				BindValue.Create("name", "HogeHoge"));
			var cond2 = new NotNullCondition("ADDRESS = {0}",
				BindValue.Create<string>("address", null));
			var cond = CondUtils.AndJoin(cond1, cond2);

			var text = cond.ToLiteralString(ctx);
			Assert.AreEqual("NAME = @name", text);
			Assert.IsNotNull(ctx.BindValues);
			Assert.AreEqual(1, ctx.BindValues.Count());
			var value = ctx.BindValues.First();
			Assert.AreEqual("name", value.Identity);
			Assert.AreEqual("HogeHoge", value.Value);
		}

		[Test]
		public void TestMultiParameterWithUnusedNullParamJoin() {
			var dialect = new DummyDialectImpl();
			var ctx = new DummyFragmentContext {
				DatabaseDialect = dialect
			};
			var cond1 = new BasicCondition("NAME = {0}",
				BindValue.Create("name", "HogeHoge"));
			var cond2 = new NotNullCondition("ADDRESS = {1}",
				BindValue.Create<string>("zip", null),
				BindValue.Create("address", "Japan"));
			var cond = CondUtils.AndJoin(cond1, cond2);

			var text = cond.ToLiteralString(ctx);
			Assert.AreEqual("NAME = @name AND ADDRESS = @address", text);
			Assert.IsNotNull(ctx.BindValues);
			Assert.AreEqual(2, ctx.BindValues.Count());
			var value = ctx.BindValues.First();
			Assert.AreEqual("name", value.Identity);
			Assert.AreEqual("HogeHoge", value.Value);

			value = ctx.BindValues.Skip(1).First();
			Assert.AreEqual("address", value.Identity);
			Assert.AreEqual("Japan", value.Value);
		}

		[Test]
		public void TestMultiParameterWithEmptyParamJoin() {
			var dialect = new DummyDialectImpl();
			var ctx = new DummyFragmentContext {
				DatabaseDialect = dialect
			};
			var cond1 = new BasicCondition("NAME = {0}",
				BindValue.Create("name", "HogeHoge"));
			var cond2 = new BasicCondition("ADDRESS = {0}",
				BindValue.Create<string>("address", "")) {
				ExcludeFilter = Filters.NotNullOrEmpty
			};
			var cond = CondUtils.AndJoin(cond1, cond2);

			var text = cond.ToLiteralString(ctx);
			Assert.AreEqual("NAME = @name", text);
			Assert.IsNotNull(ctx.BindValues);
			Assert.AreEqual(1, ctx.BindValues.Count());
			var value = ctx.BindValues.First();
			Assert.AreEqual("name", value.Identity);
			Assert.AreEqual("HogeHoge", value.Value);
		}
	}
}
