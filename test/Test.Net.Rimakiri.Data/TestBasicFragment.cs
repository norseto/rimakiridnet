// ==============================================================================
//     Test.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Diagnostics.CodeAnalysis;
using System.Linq;
using NUnit.Framework;
using Net.Rimakiri.Data;

namespace Test.Rimakiri.Data {
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class TestBasicFragment {
		[Test]
		public void TestNoParameter() {
			var dialect = new DummyDialectImpl();
			var ctx = new DummyFragmentContext {
				DatabaseDialect = dialect
			};
			var cond = new BasicFragment("SELECT * FROM HOGE");

			var text = cond.ToLiteralString(ctx);
			Assert.AreEqual("SELECT * FROM HOGE", text);
			Assert.IsNotNull(ctx.BindValues);
			Assert.AreEqual(0, ctx.BindValues.Count());
		}

		[Test]
		public void TestSingleParameter() {
			var dialect = new DummyDialectImpl();
			var ctx = new DummyFragmentContext {
				DatabaseDialect = dialect
			};
			var cond = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}")
							.AddValues(BindValue.Create("name", "HogeHoge"));

			var text = cond.ToLiteralString(ctx);
			Assert.AreEqual("SELECT * FROM HOGE WHERE NAME = @name", text);
			Assert.IsNotNull(ctx.BindValues);
			Assert.AreEqual(1, ctx.BindValues.Count());
			var value = ctx.BindValues.First();
			Assert.AreEqual("name", value.Identity);
			Assert.AreEqual("HogeHoge", value.Value);
		}

		[Test]
		public void TestMultiParameter() {
			var dialect = new DummyDialectImpl();
			var ctx = new DummyFragmentContext {
				DatabaseDialect = dialect
			};
			var cond = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0} AND ADDRESS = {1}",
				BindValue.Create("name", "HogeHoge"),
					BindValue.Create("address", "Foo"));

			var text = cond.ToLiteralString(ctx);
			Assert.AreEqual("SELECT * FROM HOGE WHERE NAME = @name AND ADDRESS = @address", text);
			Assert.IsNotNull(ctx.BindValues);
			Assert.AreEqual(2, ctx.BindValues.Count());
			var value = ctx.BindValues.First();
			Assert.AreEqual("name", value.Identity);
			Assert.AreEqual("HogeHoge", value.Value);

			value = ctx.BindValues.Skip(1).First();
			Assert.AreEqual("address", value.Identity);
			Assert.AreEqual("Foo", value.Value);
		}

		[Test]
		public void TestMultiParameterWithOne() {
			var dialect = new DummyDialectImpl();
			var ctx = new DummyFragmentContext {
				DatabaseDialect = dialect
			};
			var cond = new BasicFragment("SELECT * FROM HOGE WHERE ADDRESS = {1}",
				BindValue.Create("name", "HogeHoge"),
				BindValue.Create("address", "Foo"));

			var text = cond.ToLiteralString(ctx);
			Assert.AreEqual("SELECT * FROM HOGE WHERE ADDRESS = @address", text);
			Assert.IsNotNull(ctx.BindValues);
			Assert.AreEqual(1, ctx.BindValues.Count());
			var value = ctx.BindValues.First();
			Assert.AreEqual("address", value.Identity);
			Assert.AreEqual("Foo", value.Value);
		}

		[Test]
		public void TestReplaceValue() {
			var dialect = new DummyDialectImpl();
			var ctx = new DummyFragmentContext {
				DatabaseDialect = dialect
			};
			var cond = new BasicFragment("SELECT * FROM T_HOGE WHERE NAME = {0} AND ADDRESS = {1}")
						.AddValues(BindValue.Create("name", "HogeHoge"))
						.AddValues(BindValue.Create("address", "Foo"));

			var text = cond.ToLiteralString(ctx);
			Assert.AreEqual("SELECT * FROM T_HOGE WHERE NAME = @name AND ADDRESS = @address", text);
			Assert.AreEqual(text, cond.ToLiteralString(ctx));

			// Replace with null does not effect
			cond.ReplaceValue(null);
			Assert.AreEqual(text, cond.ToLiteralString(ctx));

			// Replace with invalid value does not effect
			cond.ReplaceValue(BindValue.Create<string>("foo", null));
			Assert.AreEqual(text, cond.ToLiteralString(ctx));

			cond.ReplaceValue(BindValue.Create<string>("name", null));
			text = cond.ToLiteralString(ctx);
			Assert.AreEqual("SELECT * FROM T_HOGE WHERE NAME = @name AND ADDRESS = @address", text);
		}
	}
}
