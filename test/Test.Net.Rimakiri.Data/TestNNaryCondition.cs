// ==============================================================================
//     Test.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;
using Net.Rimakiri.Data;
using Net.Rimakiri.Data.Interfaces;

namespace Test.Rimakiri.Data {
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestNNaryCondition {
		private static IBindValue NullValue(string id) {
			return BindValue.Create<string>(id, null);
		}

		private static IBindValue NotNullValue(string id) {
			return BindValue.Create(id, id);
		}

		[Test]
		public void SimpleTest() {
			var cond = new BasicCondition("A = {0}", NotNullValue("a"))
					.And(new BasicCondition("B = {0}", NotNullValue("b")));
			Assert.AreEqual("A = @a AND B = @b", cond.ToText());
		}

		[Test]
		public void NestedBracketTest() {
			var condA = new BasicCondition("A = {0}", NotNullValue("a"))
					.And(new BasicCondition("B = {0}", NotNullValue("b")));
			var condB = new BasicCondition("C = {0}", NotNullValue("c"))
					.And(new BasicCondition("D = {0}", NotNullValue("d")));
			Assert.AreEqual("(A = @a AND B = @b) OR (C = @c AND D = @d)", condA.Or(condB).ToText());
		}

		[Test]
		public void NestedSimpleFirstTest() {
			var condA = new BasicCondition("A = {0}", NotNullValue("a"));
			var condB = new BasicCondition("C = {0}", NotNullValue("c"))
					.And(new BasicCondition("D = {0}", NotNullValue("d")));
			Assert.AreEqual("A = @a OR (C = @c AND D = @d)", condA.Or(condB).ToText());
		}

		[Test]
		public void NestedSimpleSecondTest() {
			var condA = new BasicCondition("A = {0}", NotNullValue("a"))
					.And(new BasicCondition("B = {0}", NotNullValue("b")));
			var condB = new BasicCondition("C = {0}", NotNullValue("c"));
			Assert.AreEqual("(A = @a AND B = @b) OR C = @c", condA.Or(condB).ToText());
		}

		[Test]
		public void TriCondNestedBracketTest() {
			var condA = new BasicCondition("A = {0}", NotNullValue("a"))
					.And(new BasicCondition("B = {0}", NotNullValue("b")));
			var condB = new BasicCondition("C = {0}", NotNullValue("c"))
					.And(new BasicCondition("D = {0}", NotNullValue("d")));
			var condC = new BasicCondition("E = {0}", NotNullValue("e"))
					.And(new BasicCondition("F = {0}", NotNullValue("f")));
			Assert.AreEqual("(A = @a AND B = @b) OR (C = @c AND D = @d) OR (E = @e AND F = @f)", condA.Or(condB, condC).ToText());
		}

		[Test]
		public void NestedSimpleFirstNullTest() {
			var condA = new NotNullCondition("A = {0}", NullValue("a"));
			var condB = new NotNullCondition("C = {0}", NotNullValue("c"))
					.And(new NotNullCondition("D = {0}", NotNullValue("d")));
			Assert.AreEqual("C = @c AND D = @d", condA.Or(condB).ToText());
		}

		[Test]
		public void NestedSimpleSecondNullTest() {
			var condA = new NotNullCondition("A = {0}", NotNullValue("a"));
			var condB = new NotNullCondition("C = {0}", NullValue("c"))
					.And(new NotNullCondition("D = {0}", NotNullValue("d")));
			Assert.AreEqual("A = @a OR D = @d", condA.Or(condB).ToText());
		}

		[Test]
		public void NestedSimpleSecondAllNullTest() {
			var condA = new NotNullCondition("A = {0}", NotNullValue("a"));
			var condB = new NotNullCondition("C = {0}", NullValue("c"))
					.And(new NotNullCondition("D = {0}", NullValue("d")));
			Assert.AreEqual("A = @a", condA.Or(condB).ToText());
		}

		[Test]
		public void NestedSimpleMiddleNullTest() {
			var condA = new NotNullCondition("A = {0}", NullValue("a"));
			var condB = new NotNullCondition("C = {0}", NotNullValue("c"))
					.And(new NotNullCondition("D = {0}", NullValue("d")));
			Assert.AreEqual("C = @c", condA.Or(condB).ToText());
		}

		[Test]
		public void TriCondNestedBracketNullTest() {
			var condAa = new NotNullCondition("A = {0}", NotNullValue("a"));
			var condAb = new NotNullCondition("B = {0}", NotNullValue("b"));
			var condBa = new NotNullCondition("C = {0}", NotNullValue("c"));
			var condBb = new NotNullCondition("D = {0}", NotNullValue("d"));
			var condCa = new NotNullCondition("E = {0}", NotNullValue("e"));
			var condCb = new NotNullCondition("F = {0}", NotNullValue("f"));

			var condA = condAa.And(condAb);
			var condB = condBa.And(condBb);
			var condC = condCa.And(condCb);

			var cond = condA.Or(condB, condC);

			Assert.AreEqual("(A = @a AND B = @b) OR (C = @c AND D = @d) OR (E = @e AND F = @f)", cond.ToText());

			condAa.ReplaceValue(NullValue("a"));
			Assert.AreEqual("B = @b OR (C = @c AND D = @d) OR (E = @e AND F = @f)", cond.ToText());
			condAa.ReplaceValue(NotNullValue("a"));

			condAb.ReplaceValue(NullValue("b"));
			Assert.AreEqual("A = @a OR (C = @c AND D = @d) OR (E = @e AND F = @f)", cond.ToText());
			condAa.ReplaceValue(NullValue("a"));
			Assert.AreEqual("(C = @c AND D = @d) OR (E = @e AND F = @f)", cond.ToText());
			condAa.ReplaceValue(NotNullValue("a"));
			condAb.ReplaceValue(NotNullValue("b"));


			condBa.ReplaceValue(NullValue("c"));
			Assert.AreEqual("(A = @a AND B = @b) OR D = @d OR (E = @e AND F = @f)", cond.ToText());
			condBa.ReplaceValue(NotNullValue("c"));

			condBb.ReplaceValue(NullValue("d"));
			Assert.AreEqual("(A = @a AND B = @b) OR C = @c OR (E = @e AND F = @f)", cond.ToText());
			condBa.ReplaceValue(NullValue("c"));
			Assert.AreEqual("(A = @a AND B = @b) OR (E = @e AND F = @f)", cond.ToText());
			condBa.ReplaceValue(NotNullValue("c"));
			condBb.ReplaceValue(NotNullValue("d"));


			condCa.ReplaceValue(NullValue("e"));
			Assert.AreEqual("(A = @a AND B = @b) OR (C = @c AND D = @d) OR F = @f", cond.ToText());
			condCa.ReplaceValue(NotNullValue("e"));

			condCb.ReplaceValue(NullValue("f"));
			Assert.AreEqual("(A = @a AND B = @b) OR (C = @c AND D = @d) OR E = @e", cond.ToText());
			condCa.ReplaceValue(NullValue("e"));
			Assert.AreEqual("(A = @a AND B = @b) OR (C = @c AND D = @d)", cond.ToText());
			condCa.ReplaceValue(NotNullValue("e"));
			condCb.ReplaceValue(NotNullValue("f"));

			condAa.ReplaceValue(NullValue("a"));
			condBa.ReplaceValue(NullValue("c"));
			condCa.ReplaceValue(NullValue("e"));
			Assert.AreEqual("B = @b OR D = @d OR F = @f", cond.ToText());
		}

		[Test]
		public void TriCondDeepNestedBracketTest() {
			var condAa = new NotNullCondition("A = {0}", NotNullValue("a"));
			var condAb = new NotNullCondition("B = {0}", NotNullValue("b"));
			var condBa = new NotNullCondition("C = {0}", NotNullValue("c"));
			var condBb = new NotNullCondition("D = {0}", NotNullValue("d"));
			var condCa = new NotNullCondition("E = {0}", NotNullValue("e"));
			var condCb = new NotNullCondition("F = {0}", NotNullValue("f"));

			var condA = condAa.And(condAb);
			var condC = condCa.Or(condCb);
			var condB = condBa.Or(condBb.And(condC));

			var cond = condA.And(condB);

			Assert.AreEqual("A = @a AND B = @b AND (C = @c OR (D = @d AND (E = @e OR F = @f)))", cond.ToText());

			condCb.ReplaceValue(NullValue("f"));
			Assert.AreEqual("A = @a AND B = @b AND (C = @c OR (D = @d AND E = @e))", cond.ToText());
			condCb.ReplaceValue(NotNullValue("f"));

			condBa.ReplaceValue(NullValue("c"));
			Assert.AreEqual("A = @a AND B = @b AND D = @d AND (E = @e OR F = @f)", cond.ToText());

			condCa.ReplaceValue(NullValue("e"));
			Assert.AreEqual("A = @a AND B = @b AND D = @d AND F = @f", cond.ToText());
		}

		[Test]
		public void SelectorSingleChildTest() {
			var fragment = new SimpleFragment("SELECT * FROM T_HOGE");
			var condZ = new NotNullCondition("ZIP = {0}", BindValue.Create("zip", "hoge"));
			var condA = new NotNullCondition("ID = {0}", BindValue.Create("id", "hoge"));
			var statement = fragment.Concat(new WhereFragment(condZ.And(new SelectorCondition(condA))));
			Assert.AreEqual("SELECT * FROM T_HOGE WHERE ZIP = @zip AND ID = @id",
					statement.ToText());
		}

		[Test]
		public void SelectorSingleDisabledChildTest() {
			var fragment = new SimpleFragment("SELECT * FROM T_HOGE");
			var condZ = new NotNullCondition("ZIP = {0}", BindValue.Create("zip", "hoge"));
			var condA = new NotNullCondition("ID = {0}", BindValue.Create<string>("id", null));
			var statement = fragment.Concat(new WhereFragment(condZ.And(new SelectorCondition(condA))));
			Assert.AreEqual("SELECT * FROM T_HOGE WHERE ZIP = @zip",
					statement.ToText());
		}

		[Test]
		public void SelectorMultipleChoiceTest() {
			var fragment = new SimpleFragment("SELECT * FROM T_HOGE");
			var condZ = new NotNullCondition("ZIP = {0}", BindValue.Create("zip", "hoge"));
			var condA = new NotNullCondition("ID = {0}", BindValue.Create("id", "hoge"));
			var condB = new NotNullCondition("NAME = {0}", BindValue.Create("name", "hoge"));
			var condC = new NotNullCondition("ADDRESS = {0}", BindValue.Create("addr", "hoge"));
			var statement = fragment.Concat(new WhereFragment(condZ.And(new SelectorCondition(condA, condB.And(condC)))));

			Assert.AreEqual("SELECT * FROM T_HOGE WHERE ZIP = @zip AND ID = @id",
					statement.ToText());

			condA.ReplaceValue(BindValue.Create<int?>("id", null));
			Assert.AreEqual("SELECT * FROM T_HOGE WHERE ZIP = @zip AND NAME = @name AND ADDRESS = @addr",
					statement.ToText());

			condB.ReplaceValue(BindValue.Create<int?>("name", null));
			condC.ReplaceValue(BindValue.Create<int?>("addr", null));
			Assert.AreEqual("SELECT * FROM T_HOGE WHERE ZIP = @zip",
					statement.ToText());
		}
	}
}
