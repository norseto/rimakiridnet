// ==============================================================================
//     Test.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using NUnit.Framework;
using Net.Rimakiri.Data;
using Net.Rimakiri.Data.TypeWrappers;

namespace Test.Rimakiri.Data {
	[ExcludeFromCodeCoverage]
	internal class DummyNumeric : DecimalType {
		#region Overrides of DecimalType
		public override byte Precision {
			get { return 10; }
		}

		public override byte Scale {
			get { return 3; }
		}
		#endregion

		public DummyNumeric(decimal? value) : base(value) {
		}
	}
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestWrapperClass {
		[Test]
		public void TestGetValue() {
			var now = DateTime.Now;
			var cv = new CharType("Hoge");
			var ncv = new NCharType("Foo");
			var vcv = new VarCharType("NHoge");
			var nvcv = new NVarCharType("NFoo");
			var dv = new DateType(now);
			var tv = new TimestampType(now);
			var dmv = new DummyNumeric(100.05m);

			Assert.AreEqual("Hoge", cv.Value);
			Assert.AreEqual("Foo", ncv.Value);
			Assert.AreEqual("NHoge", vcv.Value);
			Assert.AreEqual("NFoo", nvcv.Value);
			Assert.AreNotEqual(now, dv.Value);
			Assert.AreEqual(now.Date, dv.Value);
			Assert.AreEqual(now, tv.Value);
			Assert.AreEqual(100.05m, dmv.Value);
			Assert.AreEqual(10, dmv.Precision);
			Assert.AreEqual(3, dmv.Scale);
		}

		[Test]
		public void TestNullValue() {
			var cv = new CharType(null);
			var ncv = new NCharType(null);
			var vcv = new VarCharType(null);
			var nvcv = new NVarCharType(null);
			var dv = new DateType(null);
			var tv = new TimestampType(null);
			var dmv = new DummyNumeric(null);

			Assert.IsNull(cv.Value);
			Assert.IsNull(ncv.Value);
			Assert.IsNull(vcv.Value);
			Assert.IsNull(nvcv.Value);
			Assert.IsNull(dv.Value);
			Assert.IsNull(tv.Value);
			Assert.IsNull(dmv.Value);
			Assert.AreEqual(10, dmv.Precision);
			Assert.AreEqual(3, dmv.Scale);
		}

		[Test]
		public void TestMultiParameterWithWrapperParamJoin() {
			var dialect = new DummyDialectImpl();
			var ctx = new DummyFragmentContext {
				DatabaseDialect = dialect
			};
			var cond1 = new NotNullCondition("NAME = {0}",
				BindValue.Create("name", "HogeHoge"));
			var cond2 = new NotNullCondition("ADDRESS = {0}",
				BindValue.Create("address", new CharType("Japan")));
			var cond = CondUtils.AndJoin(cond1, cond2);

			var text = cond.ToLiteralString(ctx);
			Assert.AreEqual("NAME = @name AND ADDRESS = @address", text);
			Assert.IsNotNull(ctx.BindValues);
			Assert.AreEqual(2, ctx.BindValues.Count());
			var value = ctx.BindValues.First();
			Assert.AreEqual("name", value.Identity);
			Assert.AreEqual("HogeHoge", value.Value);

			value = ctx.BindValues.Skip(1).First();
			Assert.AreEqual("address", value.Identity);
			Assert.AreEqual("Japan", value.Value);

			// Wrapped NULL value
			ctx = new DummyFragmentContext {
				DatabaseDialect = dialect
			};
			cond2.ReplaceValue(BindValue.Create("address", new CharType(null)));
			text = cond.ToLiteralString(ctx);
			Assert.AreEqual("NAME = @name", text);
			Assert.IsNotNull(ctx.BindValues);
			Assert.AreEqual(1, ctx.BindValues.Count());

			// Wrapped NULL itself
			ctx = new DummyFragmentContext {
				DatabaseDialect = dialect
			};
			cond2.ReplaceValue(BindValue.Create<NVarCharType>("address", null));
			text = cond.ToLiteralString(ctx);
			Assert.AreEqual("NAME = @name", text);
			Assert.IsNotNull(ctx.BindValues);
			Assert.AreEqual(1, ctx.BindValues.Count());
		}
	}
}
