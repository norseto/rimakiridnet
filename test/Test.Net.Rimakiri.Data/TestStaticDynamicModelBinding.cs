// ==============================================================================
//     Test.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System;
using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;
using Net.Rimakiri.Data;
using Net.Rimakiri.Data.TypeWrappers;
using Net.Rimakiri.Data.Util;

namespace Test.Rimakiri.Data {
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestStaticDynamicModelBinding {
		[Test]
		public void SimpleTest() {
			var m = new ModelBindingModel {
				Name = "Hoge",
			};
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
								DynamicModelBinding.Create("name", () => m.Name));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual("name", bound.Identity);
			Assert.AreEqual(10, bound.BindMetaData.Size);
			Assert.AreEqual(0, bound.BindMetaData.Precision);
			Assert.AreEqual(0, bound.BindMetaData.Scale);
			Assert.AreEqual("Hoge", bound.Value);
		}

		[Test]
		public void NoIdSimpleTest() {
			var m = new ModelBindingModel {
				Name = "Hoge",
			};
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
				DynamicModelBinding.Create(() => m.Name));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual("given", bound.Identity);
			Assert.AreEqual(10, bound.BindMetaData.Size);
			Assert.AreEqual(0, bound.BindMetaData.Precision);
			Assert.AreEqual(0, bound.BindMetaData.Scale);
			Assert.AreEqual("Hoge", bound.Value);
		}

		[Test]
		public void ReturnValueTest() {
			var m = new ModelBindingModel {
				Name = "Hoge",
			};
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
								DynamicModelBinding.Create("name", () => m.Hello()));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual("name", bound.Identity);
			Assert.AreEqual(30, bound.BindMetaData.Size);
			Assert.AreEqual(0, bound.BindMetaData.Precision);
			Assert.AreEqual(0, bound.BindMetaData.Scale);
			Assert.AreEqual("Hello world", bound.Value);
		}

		[Test]
		public void NoIdReturnValueTest() {
			var m = new ModelBindingModel {
				Name = "Hoge",
			};
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
				DynamicModelBinding.Create(() => m.Hello()));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual("hello", bound.Identity);
			Assert.AreEqual(30, bound.BindMetaData.Size);
			Assert.AreEqual(0, bound.BindMetaData.Precision);
			Assert.AreEqual(0, bound.BindMetaData.Scale);
			Assert.AreEqual("Hello world", bound.Value);
		}

		[Test]
		public void NoAttributeTest() {
			var m = new ModelBindingModel {
				NoLengthName = "Hoge",
			};
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
								DynamicModelBinding.Create("name", () => m.NoLengthName));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual(-1, bound.BindMetaData.Size);
			Assert.AreEqual(0, bound.BindMetaData.Precision);
			Assert.AreEqual(0, bound.BindMetaData.Scale);
			Assert.AreEqual("Hoge", bound.Value);
		}

		[Test]
		public void NoIdNoAttributeTest() {
			var m = new ModelBindingModel {
				NoLengthName = "Hoge",
			};
			try {
				var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
					DynamicModelBinding.Create(() => m.NoLengthName));
				var bound = BindValueUtils.PeekBound(fragment, 0);
				Assert.IsTrue(false, "Should throw an exception.");
			}
			catch (ArgumentNullException e) {
				Assert.AreEqual("identity", e.ParamName);
			}
		}

		[Test]
		public void SimpleNumericTest() {
			var m = new ModelBindingModel {
				PayAmount = 1234.34m,
			};
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
								DynamicModelBinding.Create("name", () => m.PayAmount));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual("name", bound.Identity);
			Assert.AreEqual(0, bound.BindMetaData.Size);
			Assert.AreEqual(12, bound.BindMetaData.Precision);
			Assert.AreEqual(3, bound.BindMetaData.Scale);
			Assert.AreEqual(1234.34m, bound.Value);
		}

		[Test]
		public void NoIdSimpleNumericTest() {
			var m = new ModelBindingModel {
				PayAmount = 1234.34m,
			};
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
				DynamicModelBinding.Create(() => m.PayAmount));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual("total", bound.Identity);
			Assert.AreEqual(0, bound.BindMetaData.Size);
			Assert.AreEqual(12, bound.BindMetaData.Precision);
			Assert.AreEqual(3, bound.BindMetaData.Scale);
			Assert.AreEqual(1234.34m, bound.Value);
		}

		[Test]
		public void FormatTest() {
			var m = new ModelBindingModel {
				Name = "Hoge",
			};
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
								DynamicModelBinding.WithFormat("name", BindValue.ForwardMatch, () => m.Name));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual("name", bound.Identity);
			Assert.AreEqual(10, bound.BindMetaData.Size);
			Assert.AreEqual("Hoge%", bound.Value);
			Assert.AreEqual("Hoge", bound.RawValue);
		}

		[Test]
		public void NoIdFormatTest() {
			var m = new ModelBindingModel {
				Name = "Hoge",
			};
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
				DynamicModelBinding.WithFormat(BindValue.ForwardMatch, () => m.Name));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual("given", bound.Identity);
			Assert.AreEqual(10, bound.BindMetaData.Size);
			Assert.AreEqual("Hoge%", bound.Value);
			Assert.AreEqual("Hoge", bound.RawValue);
		}

		[Test]
		public void FormatWrapperTest() {
			var m = new ModelBindingModel {
				NCharName = new NCharType("Hoge"),
			};
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
								DynamicModelBinding.WithFormat("name", BindValue.ForwardMatch, () => m.NCharName));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual("name", bound.Identity);
			Assert.AreEqual(20, bound.BindMetaData.Size);
			Assert.AreEqual("Hoge%", bound.Value);
			Assert.AreEqual("Hoge", ((NCharType)(bound.RawValue)).Value);
		}

		[Test]
		public void NoIdFormatWrapperTest() {
			var m = new ModelBindingModel {
				NCharName = new NCharType("Hoge"),
			};
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
				DynamicModelBinding.WithFormat(BindValue.ForwardMatch, () => m.NCharName));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual("wrapped", bound.Identity);
			Assert.AreEqual(20, bound.BindMetaData.Size);
			Assert.AreEqual("Hoge%", bound.Value);
			Assert.AreEqual("Hoge", ((NCharType)(bound.RawValue)).Value);
		}

		[Test]
		public void FormatOverrideWrapperTest() {
			var m = new ModelBindingModel {
				NCharName = new NCharType("Hoge", 100),
			};
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
								DynamicModelBinding.WithFormat("name", BindValue.ForwardMatch, () => m.NCharName));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual(20, bound.BindMetaData.Size);
			Assert.AreEqual("Hoge%", bound.Value);
			Assert.AreEqual("Hoge", ((NCharType)(bound.RawValue)).Value);
		}

		[EntityBinding(Size = 100, Identity = "longlong")] private string LongLongName = "Hoge";

		[Test]
		public void FieldAttributeTest() {
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
				DynamicModelBinding.WithFormat("name", BindValue.ForwardMatch, () => LongLongName));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual("name", bound.Identity);
			Assert.AreEqual(100, bound.BindMetaData.Size);
			Assert.AreEqual("Hoge%", bound.Value);
			Assert.AreEqual("Hoge", bound.RawValue);
		}

		[Test]
		public void NoIdFieldAttributeTest() {
			var fragment = new BasicFragment("SELECT * FROM HOGE WHERE NAME = {0}",
				DynamicModelBinding.WithFormat(BindValue.ForwardMatch, () => LongLongName));
			var bound = BindValueUtils.PeekBound(fragment, 0);
			Assert.AreEqual("longlong", bound.Identity);
			Assert.AreEqual(100, bound.BindMetaData.Size);
			Assert.AreEqual("Hoge%", bound.Value);
			Assert.AreEqual("Hoge", bound.RawValue);
		}
	}
}
