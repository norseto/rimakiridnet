// ==============================================================================
//     Test.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Collections.Generic;
using System.Data.Common;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Net.Rimakiri.Data;
using Net.Rimakiri.Data.Interfaces;

namespace Test.Rimakiri.Data {
	/// <summary>
	/// 条件構文生成時のコンテキストです。
	/// </summary>
	/// <inheritdoc />
	[ExcludeFromCodeCoverage]
	public class DummyFragmentContext : IFragmentContext {
		private readonly IList<IBindValue> values = new List<IBindValue>();

		/// <summary>
		/// バインドされる値を取得します。
		/// </summary>
		public IEnumerable<IBindValue> BindValues {
			get { return values.AsEnumerable(); }
		}

		#region Implementation of IFragmentContext
		/// <summary>
		/// <see cref="IDatabaseDialect"/>を取得または設定します。
		/// </summary>
		/// <inheritdoc />
		public IDatabaseDialect DatabaseDialect { get; set; }

		/// <summary>
		/// 条件階層の深さを取得します。
		/// </summary>
		/// <inheritdoc />
		public int Depth { get; private set; }

		/// <summary>
		/// 条件階層の深さをインクリメントします。
		/// </summary>
		/// <inheritdoc />
		public void IncrementDepth() {
			Depth++;
		}

		/// <summary>
		/// 条件階層の深さをデクリメントします。
		/// </summary>
		/// <inheritdoc />
		public void DecrementDepth() {
			if (Depth > 0) {
				Depth--;
			}
		}

		/// <summary>
		/// バインドされる値情報を通知します。
		/// </summary>
		/// <param name="value">バインドされる値</param>
		/// <inheritdoc />
		public void NotifyWillBound(IBindValue value) {
			values.Add(value);
		}
		#endregion

		#region Implementation of IFragmentContext
		/// <inheritdoc />
		public IConditionOperator CondOperator { get; set; }
		#endregion
	}

	[ExcludeFromCodeCoverage]
	public static class DummyFragmentContextEx {
		public static string ToText(this IFragment fragment) {
			var ctx = new DummyFragmentContext { DatabaseDialect = new DummyDialectImpl() };
			return fragment.ToLiteralString(ctx);
		}
	}

	[ExcludeFromCodeCoverage]
	public class SampleSource {
		#region Sample
		public void DoBind(IDatabaseDialect dialect, DbCommand command,
					string name, string address) {
			var select = new SimpleFragment("SELECT * FROM SAMPLE");
			var cond1 = new BasicCondition("NAME = {0}",
							BindValue.Create("name", name, 20));
			var cond2 = new NotNullCondition("ADDRESS LIKE {0}",
							BindValue.WithFormat("address", BindValue.ForwardMatch,
											address, 20));
			var statement = select.Concat(new WhereFragment(cond1.And(cond2)));
			var builder = new ValueBinderBuilder(statement);
			var binder = builder.Build(dialect);

			binder.Bind(command);
		}

		public void DoIn(IDatabaseDialect dialect, DbCommand command,
			string name, string address) {
			var expander = new InExpander<string>("id", "NAME IN ({0})", 5, 20);
			expander.AddValue("Hoge");
			expander.AddValue("Fuga");

			var cond = new BasicCondition(expander.Text, expander.BindValues.ToArray());
			var statement = new SimpleFragment("SELECT * FROM T_SAMPLE")
									.Concat(new WhereFragment(cond));
			var builder = new ValueBinderBuilder(statement);
			var binder = builder.Build(dialect);
			binder.Bind(command);
		}
		#endregion
	}
}
