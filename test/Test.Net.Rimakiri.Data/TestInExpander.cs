// ==============================================================================
//     Test.Rimakiri.Data
// ==============================================================================
// Copyright (c) Norihiro Seto.  All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License. You may
// obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied. See the License for the specific language governing permissions
// and limitations under the License.

using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using NUnit.Framework;
using Net.Rimakiri.Data;
using Net.Rimakiri.Data.Interfaces;

namespace Test.Rimakiri.Data {
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestInExpander {
		[Test]
		public void TestSimpleExpand() {
			var expander = new InExpander<string>("id").AddValues(new string[] { "Hoge", "Foo" });
			Assert.AreEqual("{0}, {1}", expander.Text);
			var values = new List<IBindValue>(expander.BindValues);
			Assert.AreEqual(2, values.Count);
			Assert.AreEqual("id1", values[0].Identity);
			Assert.AreEqual("Hoge", values[0].Value);
			Assert.AreEqual("id2", values[1].Identity);
			Assert.AreEqual("Foo", values[1].Value);
		}
		[Test]
		public void TestSimpleExpandWithFormat() {
			var expander = new InExpander<string>("id", "NAME IN ({0})", 5, 20);
			expander.AddValue("Hoge");
			expander.AddValue("Fuga");

			var cond = new BasicCondition(expander.Text, expander.BindValues.ToArray());
		}

		[Test]
		public void TestFixedExpand() {
			var expander = new InExpander<string>("id", 5).AddValues(new [] { "Hoge", "Foo" });
			Assert.AreEqual("{0}, {1}, {2}, {3}, {4}", expander.Text);
			var values = new List<IBindValue>(expander.BindValues);
			Assert.AreEqual(5, values.Count);
			Assert.AreEqual("id1", values[0].Identity);
			Assert.AreEqual("Hoge", values[0].Value);
			Assert.AreEqual("id2", values[1].Identity);
			Assert.AreEqual("Foo", values[1].Value);
			Assert.AreEqual("id3", values[2].Identity);
			Assert.AreEqual("Hoge", values[2].Value);
			Assert.AreEqual("id4", values[3].Identity);
			Assert.AreEqual("Hoge", values[3].Value);
			Assert.AreEqual("id5", values[4].Identity);
			Assert.AreEqual("Hoge", values[4].Value);

			expander.AddValue("Bar");
			values = new List<IBindValue>(expander.BindValues);
			Assert.AreEqual("{0}, {1}, {2}, {3}, {4}", expander.Text);
			Assert.AreEqual(5, values.Count);
			Assert.AreEqual("id1", values[0].Identity);
			Assert.AreEqual("Hoge", values[0].Value);
			Assert.AreEqual("id2", values[1].Identity);
			Assert.AreEqual("Foo", values[1].Value);
			Assert.AreEqual("id3", values[2].Identity);
			Assert.AreEqual("Bar", values[2].Value);
			Assert.AreEqual("id4", values[3].Identity);
			Assert.AreEqual("Hoge", values[3].Value);
			Assert.AreEqual("id5", values[4].Identity);
			Assert.AreEqual("Hoge", values[4].Value);
		}

		[Test]
		public void TestFixedOverflow() {
			var expander = new InExpander<string>("id", 5)
					.AddValues(new[] {"Hoge", "Foo"})
					.AddValues(null).AddValue(null)
					.AddValue("Hoge2").AddValue("Foo2")
					.AddValues(new[] {"Hoge3", "Foo3"});
			Assert.AreEqual("{0}, {1}, {2}, {3}, {4}", expander.Text);
			var values = new List<IBindValue>(expander.BindValues);
			Assert.AreEqual(5, values.Count);
			Assert.AreEqual("id1", values[0].Identity);
			Assert.AreEqual("Hoge", values[0].Value);
			Assert.AreEqual("id2", values[1].Identity);
			Assert.AreEqual("Foo", values[1].Value);
			Assert.AreEqual("id3", values[2].Identity);
			Assert.AreEqual("Hoge2", values[2].Value);
			Assert.AreEqual("id4", values[3].Identity);
			Assert.AreEqual("Foo2", values[3].Value);
			Assert.AreEqual("id5", values[4].Identity);
			Assert.AreEqual("Hoge3", values[4].Value);
		}

		[Test]
		public void TestNumeric() {
			var expander = new InExpander<decimal?>("id", 5, -1, 5, 1)
					.AddValue(4m)
					.AddValue(100m);
			Assert.AreEqual("{0}, {1}, {2}, {3}, {4}", expander.Text);
			var values = new List<IBindValue>(expander.BindValues);
			Assert.AreEqual(5, values.Count);
			Assert.AreEqual("id1", values[0].Identity);
			Assert.AreEqual(4m, values[0].Value);
			Assert.AreEqual(5, values[0].BindMetaData.Precision);
			Assert.AreEqual(1, values[0].BindMetaData.Scale);
			Assert.AreEqual("id2", values[1].Identity);
			Assert.AreEqual(100m, values[1].Value);
			Assert.AreEqual(5, values[1].BindMetaData.Precision);
			Assert.AreEqual(1, values[1].BindMetaData.Scale);
			Assert.AreEqual("id3", values[2].Identity);
			Assert.AreEqual(4m, values[2].Value);
			Assert.AreEqual(5, values[2].BindMetaData.Precision);
			Assert.AreEqual(1, values[2].BindMetaData.Scale);
			Assert.AreEqual("id4", values[3].Identity);
			Assert.AreEqual(4m, values[3].Value);
			Assert.AreEqual(5, values[3].BindMetaData.Precision);
			Assert.AreEqual(1, values[3].BindMetaData.Scale);
			Assert.AreEqual("id5", values[4].Identity);
			Assert.AreEqual(4m, values[4].Value);
			Assert.AreEqual(5, values[4].BindMetaData.Precision);
			Assert.AreEqual(1, values[4].BindMetaData.Scale);
		}

		[Test]
		public void TestNumericWrapper() {
			var expander = new InExpander<DummyNumeric>("id", 5)
					.AddValue(new DummyNumeric(4))
					.AddValue(new DummyNumeric(100))
					.AddValue(new DummyNumeric(null))
					;
			Assert.AreEqual("{0}, {1}, {2}, {3}, {4}", expander.Text);
			var values = new List<IBindValue>(expander.BindValues);
			Assert.AreEqual(5, values.Count);
			Assert.AreEqual("id1", values[0].Identity);
			Assert.AreEqual(4m, values[0].Value);
			Assert.AreEqual(10, values[0].BindMetaData.Precision);
			Assert.AreEqual(3, values[0].BindMetaData.Scale);
			Assert.AreEqual("id2", values[1].Identity);
			Assert.AreEqual(100m, values[1].Value);
			Assert.AreEqual(10, values[1].BindMetaData.Precision);
			Assert.AreEqual(3, values[1].BindMetaData.Scale);
			Assert.AreEqual("id3", values[2].Identity);
			Assert.AreEqual(4m, values[2].Value);
			Assert.AreEqual(10, values[2].BindMetaData.Precision);
			Assert.AreEqual(3, values[2].BindMetaData.Scale);
			Assert.AreEqual("id4", values[3].Identity);
			Assert.AreEqual(4m, values[3].Value);
			Assert.AreEqual(10, values[3].BindMetaData.Precision);
			Assert.AreEqual(3, values[3].BindMetaData.Scale);
			Assert.AreEqual("id5", values[4].Identity);
			Assert.AreEqual(4m, values[4].Value);
			Assert.AreEqual(10, values[4].BindMetaData.Precision);
			Assert.AreEqual(3, values[4].BindMetaData.Scale);
		}
	}
}
